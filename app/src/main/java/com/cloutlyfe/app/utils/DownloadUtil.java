package com.cloutlyfe.app.utils;

import android.content.Context;

import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

public class DownloadUtil {
    private  static Cache cache;
    private  static DownloadManager  downloadManager;

    public  static synchronized Cache getCache(Context context) {
        if (cache == null) {
           // LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            File cacheDirectory = new File(context.getExternalFilesDir(null), "downloadsr");
            cache = new SimpleCache(cacheDirectory, new NoOpCacheEvictor());

            }
            return cache;
        }
    }

