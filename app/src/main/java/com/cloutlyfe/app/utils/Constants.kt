package com.cloutlyfe.app.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Environment
import android.view.Window
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.cloutlyfe.app.App
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.LoginActivity
import com.cloutlyfe.app.R
import java.util.*


class Constants {
    companion object {
        var max_recording_duration = 60000
        var recording_duration = 60000
        var root = Environment.getExternalStorageDirectory().toString() + "/CloutVideoVideo/"
//        var root0 = Environment.getExternalStorageDirectory().toString() + "/CloutVideo/" + "CloutVideooCreator/"
        var root0 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CloutVideo/" + "CloutVideooCreator/"
        var rootD = Environment.getExternalStorageDirectory()
            .toString() + "/CloutVideo/" + "CloutVideoCreator/"
        var root2 = Environment.getExternalStorageDirectory().toString() + "/CloutVideo"
        var outputfile: String = root0 + "output.mp4"
        var outputfile2: String = root + "output2.mp4"
        var output_filter_file: String =
            root0 + "output-filtered.mp4"

        var gallery_trimed_video: String =
            root0 + "gallery_trimed_video.mp4"
        var gallery_resize_video: String =
            root0 + "gallery_resize_video.mp4"

        val PREF_NAME = "io.agora.openlive"
        val DEFAULT_PROFILE_IDX = 2
        val PREF_RESOLUTION_IDX = "pref_profile_index"
        val PREF_ENABLE_STATS = "pref_enable_stats"
        val PREF_MIRROR_LOCAL = "pref_mirror_local"
        val PREF_MIRROR_REMOTE = "pref_mirror_remote"
        val PREF_MIRROR_ENCODE = "pref_mirror_encode"

        val KEY_CLIENT_ROLE = "key_client_role"

// - - Constant Values
const val SPLASH_TIME_OUT = 1500
//        const val SOCKET_URL = "http://161.97.132.85:3020/"
        const val SOCKET_URL = "https://www.comeonnow.io:3010/"

        /*
    * Data Transfer
    * */
        const val USER_NAME = "user_name"
        const val ROOM_ID = "room_id"
        const val HOME_PATH = "home_path"
/*
* Shared Preferences Keys
* */
val GET_TRIMMED_VIDEO = 123
val TRIMMED_VIDEO_URI = "trimmedVideoUri"
const val EXTRA_VIDEO_PATH="extra_video_path"
const val ISLOGIN = "is_login"
const val ISSIMPLELOGIN = "is_simple_login"
const val ID = "user_id"
const val BIO="bio"
const val LAST_NAME="last_name"
const val PROFILE_IMAGE="profile_image"
const val NAME="name"
const val PHONE="phone"
const val COVER_IMAGE="cover_image"
const val FB_ID="fb_id"
const val VERIFIED="verified"
const val COUNTRY="country"
const val CHANGE_STATUS="change_status"
const val BLOCK_USER="block_user"
const val USERNAME = "username"
const val EMAIL = "email"
const val OTHERUSERID = "other_user_id"
const val DESCRIPTION = "description"
const val PASSWORD ="password"
const val DEVICETYPE ="deviceType"
const val DEVICETOKEN = "deviceToken"
const val LATITUDE ="latitude"
const val LONGITUDE ="logitude"
const val LOCATION ="location"
const val AUTHTOKEN = "authToken"
const val GOOGLE_ID = "google_id"
const val TWITTER_ID = "twitter_id"
const val PROFILEPIC = "photo"
const val FOLLOWING ="totalFollowing"
const val FOLLOWERS ="totalFollowers"
const val FOLLOWER_ID ="follower_id"
const val REMEMBER_ME ="remember_me"
const val SEARCH_LAT ="search_lat"
const val SEARCH_LNG ="search_lng"
const val SEARCH_ADD ="search_add"


// - - Fragment Tags
const val HOME_TAG = "home_tag"
const val SAVE_TAG="save_tag"
const val ADD_TAG = "add_tag"
const val CHAT_TAG = "chat_tag"
const val PROFILE_TAG = "profile_tag"
const val CLOUT_TAG = "clout_tag"
const val CLOUTERS_TAG = "clouters_tag"
const val CREATE_POST_TAG = "create_post_tag"
         var SOUND = true
         var VALUE_HOME = true
         var SAVED_SOUND = true
         var POSTS_SOUND = true
         var CHAT_POSTS_SOUND = true
         var POSITION = 0
        const val SOUND_ON = "true"
const val SOUND_OFF = "sound_off"
        const  val MODEL = "model"
        var LAST_POSITION = -1
        var LAST_POSITION_CHAT_POST = -1

/*
* View Holder
* */
const val LEFT_VIEW_HOLDER = 0
const val RIGHT_VIEW_HOLDER = 1
const val LEFT_POST_VIEW_HOLDER = 2
const val RIGHT_POST_VIEW_HOLDER = 3
const val LEFT_VIDEO_POST_VIEW_HOLDER = 4
const val RIGHT_VIDEO_POST_VIEW_HOLDER = 5
const val POST_UNAVAILABLE_LEFT_VIEW_HOLDER = 6
const val POST_UNAVAILABLE_RIGHT_VIEW_HOLDER = 7
        const val CLOUT_VISIBLE_VIEW_HOLDER = 8
        const val CLOUT_INVISIBLE_VIEW_HOLDER = 9
        const val LEFT_IMAGE_VIEW_HOLDER = 10
        const val RIGHT_IMAGE_VIEW_HOLDER = 11
        const val LEFT_SHARE_STORY_VIEW_HOLDER = 15
        const val RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER = 18
        const val LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER = 17
        const val RIGHT_SHARE_STORY_VIEW_HOLDER = 16
        const val LEFT_VIDEO_VIEW_HOLDER = 12
        const val RIGHT_VIDEO_VIEW_HOLDER = 13
const val LAST_MSG = "last_msg"
var SOUND_VALUE = "sound_value"


        //Live Stream
        const val STREAM_ROOMID = "stream_roomid"
        const val STREAM_USERNAME = "stream_username"
        const val STREAM_NAME = "stream_name"
        const val STREAM_USERID = "stream_userid"
        const val STREAM_ID = "stream_id"
        const val STREAM_USERPHOTO = "stream_userphoto"
        const val COMMENT_DISABLE = "comment_disable"
        const val LIKE_DISABLE = "like_disable"

//        var endStreamDone = false

const val TERMS_CONDITIONS ="https://cloutlyfe.online/CloutLyfe/TermsAndConditions.html"
const val PRIVACY_POLICY ="https://cloutlyfe.online/CloutLyfe/Privacy-Policy.html"

//const val TERMS_CONDITIONS ="https://cloutlyfe.online/CloutLyfe/TermsAndConditions.html"
//const val PRIVACY_POLICY ="https://cloutlyfe.online/CloutLyfe/Privacy-Policy.html"

        const val PLACES_API_KEY = "AIzaSyAfI25bSC3rD6gteJIHFCZ7vBCglGl6OkE"
        const val GOOGLE_PLACES_SEARCH =
            "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
        const val GOOGLE_PLACES_LAT_LONG =
            "https://maps.googleapis.com/maps/api/place/details/json?place_id="
        private var progressDialog: Dialog? = null

//Internet connection check
fun hasInternetConnection(application: App): Boolean {
    val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        connectivityManager.activeNetworkInfo?.run {
            return when (type) {
                ConnectivityManager.TYPE_WIFI -> true
                ConnectivityManager.TYPE_MOBILE -> true
                ConnectivityManager.TYPE_ETHERNET -> true
                else -> false
            }
        }
    }
    return false
}

        //  Show Alert Dialog
        fun showAuthDismissDialog(mActivity: Activity?, strMessage: String?) {
            val alertDialog = Dialog(mActivity!!)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.dialog_alert)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set the custom dialog components - text, image and button
            val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
            val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
            txtMessageTV.text = strMessage
            btnDismiss.setOnClickListener { alertDialog.dismiss()
                performSwitchDel(mActivity)
            }
            alertDialog.show()
        }

        //AUTH TOKEN EXPIRE DIALOG
        private fun performSwitchDel(mActivity: Activity) {
            val preferences: SharedPreferences = AppPrefrences().getPreferences(
                Objects.requireNonNull(mActivity)
            )
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            val mIntent = Intent(mActivity, LoginActivity::class.java)
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            mActivity.startActivity(mIntent)
        }


        /*
           * Show Progress Dialog
           * */
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @JvmStatic
        fun showProgressDialog(mActivity: Activity?) {
            progressDialog = Dialog(mActivity!!)
            progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog!!.setContentView(R.layout.dialog_progress)
            Objects.requireNonNull<Window>(progressDialog!!.getWindow())
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog!!.setCanceledOnTouchOutside(false)
            progressDialog!!.setCancelable(false)
            if (progressDialog != null&& !progressDialog!!.isShowing)
            {progressDialog!!.show()}
        }


        /*
         * Hide Progress Dialog
         * */
        @JvmStatic
        fun dismissProgressDialog() {
            if (progressDialog != null && progressDialog!!.isShowing()) {
                progressDialog!!.dismiss()
            }
        }
    }


}

