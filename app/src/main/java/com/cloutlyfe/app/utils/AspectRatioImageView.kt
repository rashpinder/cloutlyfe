package com.cloutlyfe.app.utils

import android.content.Context
import android.util.AttributeSet


class AspectRatioImageView : androidx.appcompat.widget.AppCompatImageView {
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!,
        attrs,
        defStyle
    ) {
    }

    protected override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height: Int =
            width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth()
        setMeasuredDimension(width, height)
    }
}