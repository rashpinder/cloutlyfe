package com.cloutlyfe.app.Agora.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.cloutlyfe.app.Agora.ConstantsA;

public class PrefManager {
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(ConstantsA.PREF_NAME, Context.MODE_PRIVATE);
    }
}
