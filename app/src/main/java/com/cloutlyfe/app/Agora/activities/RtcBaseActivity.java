package com.cloutlyfe.app.Agora.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;

import com.cloutlyfe.app.Agora.ConstantsA;
import com.cloutlyfe.app.Agora.externl.RtcTokenBuilder;
import com.cloutlyfe.app.Agora.rtc.EventHandler;
import com.cloutlyfe.app.R;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.models.UserInfo;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

public abstract class RtcBaseActivity extends BaseActivity implements EventHandler {
    static String appId = "30043ad9897246e3b1e160d59a43a09f";

    static String appCertificate = "922390509dac4841a1467838069919e5";
    static String channelName = "Cloutlyfe";
    static String userAccount = "2082341233";
    static int uid = (int) (System.currentTimeMillis() / 1000L);
    static int expirationTimeInSeconds = 360000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerRtcEventHandler(this);
        RtcTokenBuilder token = new RtcTokenBuilder();
        int timestamp = (int) (System.currentTimeMillis() / 1000 + expirationTimeInSeconds);

        channelName = ConstantsA.channelNameA;
        try {
            uid = Integer.parseInt(ConstantsA.useridA);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            uid = (int) System.currentTimeMillis();
        }
        String result = token.buildTokenWithUserAccount(appId, appCertificate, channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        result = token.buildTokenWithUid(appId, appCertificate, channelName, uid, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);

        joinChannel(result, channelName);
        //joinChannel("00695aa63d5a1344a6b96a1224e6062ae95IACIt2o2lHQ6ajfGgBhYeuAEnNE9e19IDOWP2Ezep+3cmk1XQdkAAAAAIgAGMGkoEOGdYQQAAQAQ4Z1hAgAQ4Z1hAwAQ4Z1hBAAQ4Z1h","dirty");
    }


    public void RtcInit()
    {
        registerRtcEventHandler(this);
        RtcTokenBuilder token = new RtcTokenBuilder();
        int timestamp = (int) (System.currentTimeMillis() / 1000 + expirationTimeInSeconds);
        channelName = ConstantsA.channelNameA;
        try {
            uid = Integer.parseInt(ConstantsA.useridA);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            uid = (int) System.currentTimeMillis();
        }

        String result = token.buildTokenWithUserAccount(appId, appCertificate, channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        Log.e("TAG","TOKEN::"+result);
        result = token.buildTokenWithUid(appId, appCertificate, channelName, uid, RtcTokenBuilder.Role.Role_Publisher, timestamp);
        System.out.println(result);
        joinChannel(result, channelName);

    }

    private void configVideo() {
        VideoEncoderConfiguration configuration = new VideoEncoderConfiguration(
                ConstantsA.VIDEO_DIMENSIONS[config().getVideoDimenIndex()],
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
        );
        configuration.mirrorMode = ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorEncodeIndex()];
        rtcEngine().setVideoEncoderConfiguration(configuration);
    }

    private void joinChannel(String result,String channelName) {
        String token = result;

        String accessToken = getApplicationContext().getString(R.string.agora_access_token);
        if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "007eJxTYMjaO2n616UZ3VHN6U4SN3Znzj1iMOXts+bXz+9e3fQgkWefAoOxgYGJcWKKpYWluZGJWapxkmGqoZlBiqllIlDYwDJtp35xckMgI0Or/x4GRiBkAWIQnwlMMoNJFjDJyeCck19a4lOZlsrAAAB6Cya5")) {
        }
        rtcEngine().setChannelProfile(io.agora.rtc.Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
        rtcEngine().enableVideo();
        configVideo();
        rtcEngine().joinChannel(result, channelName, "Clout" + " " + "Lyfe", uid);
        config().setChannelName(channelName);
        Log.e("Agora","C_NAME: "+channelName+"  UID:"+uid+"  token:"+result);
        ConstantsA.channelNameA=channelName;
        int uid = (int) (System.currentTimeMillis() / 1000L);
        ConstantsA.useridA= String.valueOf(uid);
        ConstantsA.tokenA= result;
        Log.e("TOKENNNNNNNNNN", ConstantsA.tokenA);

    }


    protected SurfaceView prepareRtcVideo(int uid, boolean local) {
        SurfaceView surface = RtcEngine.CreateRendererView(getApplicationContext());
        if (local) {
            rtcEngine().setupLocalVideo(
                    new VideoCanvas(
                            surface,
                            VideoCanvas.RENDER_MODE_HIDDEN,
                            0,
                            ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorLocalIndex()]
                    )
            );
        } else {
            rtcEngine().setupRemoteVideo(
                    new VideoCanvas(
                            surface,
                            VideoCanvas.RENDER_MODE_HIDDEN,
                            uid,
                            ConstantsA.VIDEO_MIRROR_MODES[config().getMirrorRemoteIndex()]
                    )
            );
        }

//        renderNetworkQualities();
        return surface;
    }

  /*private void renderNetworkQualities() {
        rtcEngine().addHandler(new IRtcEngineEventHandler() {
            @Override
            public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
                VideoChatViewActivity.this.mTxQuality = txQuality;
                VideoChatViewActivity.this.mRxQuality = txQuality;
                updateStatsDisplay();
            }

            @Override
            public void onRemoteAudioStats(RemoteAudioStats stats) {
                mAudioStats = stats;
                updateStatsDisplay();
            }

            @Override
            public void onRemoteVideoStats(RemoteVideoStats stats) {
                mVideoStats = stats;
                updateStatsDisplay();
            }
        });
    }*/

    protected void removeRtcVideo(int uid, boolean local) {
        if (local) {
            rtcEngine().setupLocalVideo(null);
        } else {
            rtcEngine().setupRemoteVideo(new VideoCanvas(null, VideoCanvas.RENDER_MODE_HIDDEN, uid));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeRtcEventHandler(this);
        rtcEngine().leaveChannel();
    }


}
