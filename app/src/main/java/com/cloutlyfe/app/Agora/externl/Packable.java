package com.cloutlyfe.app.Agora.externl;

public interface Packable {
    ByteBuf marshal(ByteBuf out);
}
