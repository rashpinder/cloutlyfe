package com.cloutlyfe.app

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Constants.Companion.AUTHTOKEN
import com.cloutlyfe.app.utils.Constants.Companion.DESCRIPTION
import com.cloutlyfe.app.utils.Constants.Companion.EMAIL
import com.cloutlyfe.app.utils.Constants.Companion.ID
import com.cloutlyfe.app.utils.Constants.Companion.ISLOGIN
import com.cloutlyfe.app.utils.Constants.Companion.OTHERUSERID
import com.cloutlyfe.app.utils.Constants.Companion.PROFILEPIC
import com.cloutlyfe.app.utils.Constants.Companion.USERNAME
import java.io.*
import java.util.*
import java.util.regex.Pattern

open class BaseActivity : AppCompatActivity() {
    /*
 * Get Class Name
 * */
    open var TAG = this@BaseActivity.javaClass.simpleName


    /*
  * Initialize Activity
  * */
    open var mActivity: Activity = this@BaseActivity
    /*
      * Initialize Other Classes Objects...
      * */
    var progressDialog: Dialog? = null
    private var mLastClickTab1: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    fun isUserLogin() : Boolean{
        return AppPrefrences().readBoolean(mActivity, ISLOGIN, false)
    }


    //  Show Alert Dialog
    fun showDismissDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            mActivity.finish()}
        alertDialog.show()
    }


    //  Show Alert Dialog
    fun showDismissforgotPasswordDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
           }
        alertDialog.show()
    }

    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }
    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }
    /*
    * Getting User Auth Token
    * */
    fun getAuthToken(): String {
        return AppPrefrences().readString(mActivity, AUTHTOKEN, "")!!
    }
    /*
    * Getting User ID
    * */
    fun getLoggedInUserID(): String {
        return AppPrefrences().readString(mActivity, ID, "")!!
    }

    /*
* Getting other User ID
* */
    fun getOtherUserID(): String {
        return AppPrefrences().readString(mActivity, OTHERUSERID, "")!!
    }


    /*
    *
    * Get User Name
    * */
    fun getUserName(): String {
        return AppPrefrences().readString(mActivity, USERNAME, "")!!
    }
    /*
    * Get User Email
    * */
    fun getUserEmail(): String {
        return AppPrefrences().readString(mActivity, EMAIL, "")!!
    }
    /*
    * Get User Bio
    * */
    fun getUserDescription(): String {
        return AppPrefrences().readString(mActivity, DESCRIPTION, "")!!
    }
    /*
    * Get User Profile Pic
    * */
    fun getUserProfilePic(): String {
        return AppPrefrences().readString(mActivity, PROFILEPIC, "")!!
    }

    /*
* Getting follower  ID
* */
    fun getFollowerID(): String {
        return AppPrefrences().readString(mActivity, Constants.FOLLOWER_ID, "")!!
    }

    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    /*
    * Toast Message
    * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }
    /*
    * Validate Email Address
    * */
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }



    /*
    *
    * Error Alert Dialog
    * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    /*Switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
//            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }


    /*
    * Show Progress Dialog
    * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
//        if (progressDialog != null) progressDialog!!.show()
        progressDialog!!.show()
    }
    /*
      * Hide Progress Dialog
      * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }

//    /*
//    *
//    * Resend For Open SignInActivity
//    * */
//    fun showDoubleButtonAlertDialog(mActivity: Activity?, strMessage: String?, strEmail: String?) {
//        val alertDialog = Dialog(mActivity!!)
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        alertDialog.setContentView(R.layout.dialog_resend_email)
//        alertDialog.setCanceledOnTouchOutside(false)
//        alertDialog.setCancelable(false)
//        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        // set the custom dialog components - text, image and button
//        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
//        val txtNoTV = alertDialog.findViewById<TextView>(R.id.txtNoTV)
//        val txtYesTV = alertDialog.findViewById<TextView>(R.id.txtYesTV)
//
//        txtMessageTV.text = strMessage
//
//        txtNoTV.setOnClickListener {
//            alertDialog.dismiss()
//        }
//        txtYesTV.setOnClickListener {
//            alertDialog.dismiss()
////          executeResendEmailApi(strEmail)
//        }
//
//        alertDialog.show()
//    }

    //    private fun executeResendEmailApi(strEmail: String?) {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["email"] = strEmail
//        showProgressDialog(mActivity)
//        val call = RetrofitClient.apiInterface.resendNotificationRequest(mMap)
//        call.enqueue(object : Callback<ResendEmailModel> {
//            override fun onFailure(call: Call<ResendEmailModel>, t: Throwable) {
//                Log.e(TAG, t.message.toString())
//                dismissProgressDialog()
//            }
//            override fun onResponse(call: Call<ResendEmailModel>, response: Response<ResendEmailModel>) {
//                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
//                val mResendNotiModel = response.body()
//                if (mResendNotiModel!!.status == 1) {
//                    showAlertDialog(mActivity, mResendNotiModel.message)
//                }else if (mResendNotiModel!!.status == 0){
//                    showAlertDialog(mActivity, mResendNotiModel.message)
//                } else{
//                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
//                }
//            }
//        })
//    }
///*
//*
//* Error Alert Dialog
//* */
//    fun showAccountDisableAlertDialog(mActivity: Activity?, mMessage : String) {
//        val alertDialog = Dialog(mActivity!!)
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        alertDialog.setContentView(R.layout.dialog_access_token_decline)
//        alertDialog.setCanceledOnTouchOutside(false)
//        alertDialog.setCancelable(false)
//        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        // set the custom dialog components - text, image and button
//        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
//        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
//        txtMessageTV.text = mMessage
//        btnDismiss.setOnClickListener { alertDialog.dismiss()
//            val preferences: SharedPreferences =
//                mActivity?.let { AppPrefrences().getPreferences(it) }!!
//            val editor = preferences.edit()
//            editor.clear()
//            editor.apply()
//            editor.commit()
//            val mIntent = Intent(mActivity, SignInActivity::class.java)
//            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent)
//                .startActivities()
//            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//            startActivity(mIntent)
//            finish()
//            finishAffinity()
//        }
//        alertDialog.show()
//    }

    /*
    *
    * Open SignInActivity
    * */
    fun showOpenActivityAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        alertDialog.show()
    }

    /*
    *
    * Open SignInActivity
    * */
    fun showEditProfileActivityAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            //startActivity(Intent(this, ProfileFragment::class.java))
            // onBackPressed()
            finish()
        }
        alertDialog.show()
    }
    /*
    *
    * Finish Activity Alert Dialog
    * */
//    fun showEmailAlertDialog(mActivity: Activity?, strMessage: String?) {
//        val alertDialog = Dialog(mActivity!!)
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        alertDialog.setContentView(R.layout.email_alert_dialog)
//        alertDialog.setCanceledOnTouchOutside(false)
//        alertDialog.setCancelable(false)
//        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        // set the custom dialog components - text, image and button
//        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
//        val editEmailET = alertDialog.findViewById<TextView>(R.id.editEmailET)
//        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
//        txtMessageTV.text = strMessage
//        btnDismiss.setOnClickListener {
//            alertDialog.dismiss()
//            finish()
//        }
//        alertDialog.show()
//    }

//    fun convertFileToByteArray(f: File): ByteArray? {
//        var byteArray: ByteArray? = null
//        try { /*from w  ww.  j  a  v a 2  s .c  om*/
//            val inputStream: InputStream = FileInputStream(f)
//            val bos = ByteArrayOutputStream()
//            val b = ByteArray(1024 * 8)
//            var bytesRead = 0
//            while (inputStream.read(b).also { bytesRead = it } != -1) {
//                bos.write(b, 0, bytesRead)
//            }
//            byteArray = bos.toByteArray()
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//        return byteArray
//    }
}