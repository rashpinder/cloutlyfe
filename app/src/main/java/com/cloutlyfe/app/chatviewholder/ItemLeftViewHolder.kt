package com.merger.app.chatviewholder

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.AllMessage
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ItemLeftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtTimeTV = itemView.findViewById(R.id.txtTimeTV) as TextView
    var txtMessageTV = itemView.findViewById(R.id.txtMessageTV) as TextView
    var imgOtherUserProfileIV = itemView.findViewById<ImageView>(R.id.imgOtherUserProfileIV) as de.hdodenhof.circleimageview.CircleImageView
    var delInterface: DeleteChatInterface? = null
    fun bindData(
        mActivity: Activity?,
        mModel: AllMessage?,
        mDeleteChatInterface: DeleteChatInterface
    ) {
        delInterface = mDeleteChatInterface

if(!mModel!!.creation_date.isNullOrEmpty()){
    val Timestamp: Long = mModel!!.creation_date.toLong()
        val timeD = Date(Timestamp * 1000)
        val sdf = SimpleDateFormat("hh:mm a")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
        txtTimeTV.setText(Time)}
//        txtTimeTV.setText(gettingLongToFormatedTime(mModel!!.created!!.toLong()))
        txtMessageTV.setText(mModel.message)
        mActivity?.let {
            Glide.with(it)
                .load(mModel.photo)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(imgOtherUserProfileIV)
        }
//        itemView.setOnLongClickListener {
//            showDeleteDialog(
//                mModel
//                    .id
//            )
//            true
//        }


    }

    private fun showDeleteDialog(id: String) {
        val view: View =
            LayoutInflater.from(itemView.context).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? =
            itemView.context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility = View.GONE

        deleteRL?.setOnClickListener {
            delInterface!!.onItemClickListner(adapterPosition, id, "")
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }

}