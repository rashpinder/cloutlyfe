package com.merger.app.chatviewholder

import android.app.Activity
import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.*
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.AllMessage
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ItemLeftPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtNameTV = itemView.findViewById(R.id.txtNameTV) as TextView
    var txtTimeTV = itemView.findViewById(R.id.txtTimeTV) as TextView
    var txtMessageTV = itemView.findViewById(R.id.txtMessageTV) as TextView
    var postIV = itemView.findViewById(R.id.postIV) as ImageView
    var mainLL = itemView.findViewById(R.id.mainLL) as LinearLayout
    var imgUserProfileIV = itemView.findViewById<ImageView>(R.id.imgUserProfileIV) as de.hdodenhof.circleimageview.CircleImageView
    var delInterface: DeleteChatInterface? = null
    var namee:String?=""
    fun bindData(
        mActivity: Activity?,
        mModel: AllMessage?,
        mDeleteChatInterface: DeleteChatInterface
    ) {
        delInterface = mDeleteChatInterface

        val Timestamp: Long = mModel!!.creation_date!!.toLong()
        val timeD = Date(Timestamp * 1000)
        val sdf = SimpleDateFormat("hh:mm a")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
        txtTimeTV.setText(Time)
//        txtTimeTV.setText(gettingLongToFormatedTime(mModel!!.created!!.toLong()))
//        txtMessageTV.setText(mModel!!.message)
        if(mModel.post_details!!.username=="N/A"||mModel.post_details!!.username==""||mModel.post_details!!.username==null){
            namee=mModel.post_details!!.name
        }
        else{
            namee=mModel.post_details!!.username
        }
        var sourceString = "<b>"+namee +"</b>"+ " "+ (mModel!!.post_details?.description)
        txtMessageTV.setText(Html.fromHtml(sourceString))

        txtNameTV.setText(namee)
        mActivity?.let {
            Glide.with(it)
                .load(mModel.post_details!!.photo)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(imgUserProfileIV)
        }
        if(mModel.post_details!!.image.isNotEmpty()){
        mActivity?.let {
            Glide.with(it)
                .load(mModel.post_details!!.image[0])
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into(postIV)
        }}
        mainLL.setOnClickListener {
//            var intent = Intent( mActivity, PostsFromChatActivity::class.java)
//            intent.putExtra("post_id", mModel.post_details!!.post_id)
//            intent.putExtra("user_id", mModel.post_details!!.user_id)
////            intent.putExtra("value","post_click")
//            mActivity!!.startActivity(intent)
            var intent = Intent( mActivity, CommentsActivity::class.java)
            intent.putExtra("post_id", mModel.post_details!!.post_id)
            intent.putExtra("user_id", mModel.post_details!!.user_id)
            intent.putExtra("value","post_click")
            mActivity!!.startActivity(intent)
        }

//        itemView.setOnLongClickListener {
//            showDeleteDialog(
//                mModel
//                    .id
//            )
//            true
//        }


    }


    private fun showDeleteDialog(id: String) {
        val view: View =
            LayoutInflater.from(itemView.context).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? =
            itemView.context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility = View.GONE

        deleteRL?.setOnClickListener {
            delInterface!!.onItemClickListner(adapterPosition, id, "")
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }

}