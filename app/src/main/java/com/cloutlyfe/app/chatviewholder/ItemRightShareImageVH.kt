package com.merger.app.chatviewholder

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OpenStatusActivity
import com.cloutlyfe.app.model.StatusDetails
import com.makeramen.roundedimageview.RoundedImageView

class ItemRightShareImageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var postIV = itemView.findViewById(R.id.postIV) as RoundedImageView
    var imgVideoIV = itemView.findViewById(R.id.imgVideoIV) as RoundedImageView
    var msgRL = itemView.findViewById(R.id.msgRL) as RelativeLayout
    var txtMessageTV = itemView.findViewById(R.id.txtMessageTV) as TextView
    var value: String? = null

    fun bindData(
        mActivity: Activity?,
        image: StatusDetails?,
        message: String,
        username: String,
        photo: String,
        name: String
    ) {
        if(image!!.status_image!=null){
            mActivity?.let {
                Glide.with(it)
                    .load(image.status_image)
                    .placeholder(R.drawable.ic_post_ph)
                    .error(R.drawable.ic_post_ph)
                    .into(postIV)
            }}

        if(!message.isNullOrEmpty()){
            msgRL.visibility=View.VISIBLE
            txtMessageTV.setText(message)
        }
        else{
            msgRL.visibility=View.GONE
        }

        if(image!!.status_image.contains(".mp4") || image!!.status_image.contains("video")|| image!!.status_image.contains(".3gp")){
            imgVideoIV.visibility=View.VISIBLE
            value = "video"
            mActivity?.let {
                Glide.with(it)
                    .load(image.video_thumbnail)
                    .placeholder(R.drawable.ic_post_ph)
                    .error(R.drawable.ic_post_ph)
                    .into(postIV)
            }
        }
        else{
            imgVideoIV.visibility=View.GONE
            value = "image"
        }


        itemView.setOnClickListener {
            val intent = Intent(mActivity, OpenStatusActivity::class.java)
            intent.putExtra("data", image.status_image)
            intent.putExtra("value", value)
            intent.putExtra("duration", image.duration)
            intent.putExtra("username", username)
            intent.putExtra("photo", photo)
            intent.putExtra("name", name)
            intent.putExtra("statusdate", image.statusdate)
            mActivity!!.startActivity(intent)
        }
//        mainLL.setOnClickListener {
////            var intent = Intent( mActivity, PostsFromChatActivity::class.java)
////            intent.putExtra("post_id", mModel.post_details!!.post_id)
////            intent.putExtra("user_id", mModel.post_details!!.user_id)
//////            intent.putExtra("value","post_click")
////            mActivity!!.startActivity(intent)
//            var intent = Intent( mActivity, CommentsActivity::class.java)
//            intent.putExtra("post_id", mModel.post_details!!.post_id)
//            intent.putExtra("user_id", mModel.post_details!!.user_id)
//            intent.putExtra("value","post_click")
//            mActivity!!.startActivity(intent)
//        }

//        itemView.setOnLongClickListener {
//            showDeleteDialog(
//                mModel
//                    .id
//            )
//            true
//        }


    }

}