package com.merger.app.chatviewholder

import android.app.Activity
import android.view.View
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.CloutHomeAdapter
import com.cloutlyfe.app.model.LiveDataModel

class ItemCloutInvisibleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var mainRL: RelativeLayout = itemView.findViewById(R.id.mainRL)

    fun bindData(
        mActivity: Activity?,
        mData: ArrayList<LiveDataModel>,
        listener: CloutHomeAdapter.onItemClickClickListener?,
        position: Int
    ) {

    }
}