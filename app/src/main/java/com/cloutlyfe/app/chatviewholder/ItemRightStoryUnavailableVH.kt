package com.cloutlyfe.app.chatviewholder

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.StatusDetails
import java.text.SimpleDateFormat
import java.util.*

class ItemRightStoryUnavailableVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtTimeTV = itemView.findViewById<TextView>(R.id.txtTimeTV) as TextView
    var txtMessageTV = itemView.findViewById<TextView>(R.id.txtMessageTV) as TextView
//    var mainLL = itemView.findViewById(R.id.mainLL) as LinearLayout
    var delInterface: DeleteChatInterface? = null

    fun bindData(
        mActivity: Activity?,
        mModel: StatusDetails?,
        message: String,
        time: String
    ) {
        if(mModel==null){
            val Timestamp: Long = time.toLong()
            val timeD = Date(Timestamp * 1000)
            val sdf = SimpleDateFormat("hh:mm a")
            sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
            val Time: String = sdf.format(timeD)
            txtTimeTV.setText(Time)
            txtMessageTV.setText(message)

        }
        else{
            val Timestamp: Long = mModel!!.createdate.toLong()
            val timeD = Date(Timestamp * 1000)
            val sdf = SimpleDateFormat("hh:mm a")
            sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
            val Time: String = sdf.format(timeD)
            txtTimeTV.setText(Time)
            txtMessageTV.setText(message)
        }


//        itemView.setOnLongClickListener {
//            showDeleteDialog(
//                mModel
//                    .id
//            )
//            true
        }


    }
//
//    private fun showDeleteDialog(id: String) {
//        val view: View =
//            LayoutInflater.from(itemView.context).inflate(R.layout.edit_delete_dialog, null)
//
//        var dialogDelete: BottomSheetDialog? =
//            itemView.context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
//        dialogDelete!!.setContentView(view)
//        dialogDelete.setCanceledOnTouchOutside(true)
////disabling the drag down of sheet
//        dialogDelete.setCancelable(true)
////cancel button click
//        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
//        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
//        editPostRL!!.visibility = View.GONE
//
//        deleteRL?.setOnClickListener {
//            delInterface!!.onItemClickListner(adapterPosition, id, "")
//            dialogDelete.dismiss()
//        }
//        dialogDelete.show()
//    }

//}