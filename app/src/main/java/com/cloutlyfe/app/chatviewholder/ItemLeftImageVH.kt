package com.merger.app.chatviewholder

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.SharedImageVideoPlayActivity
import com.makeramen.roundedimageview.RoundedImageView


class ItemLeftImageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var postIV = itemView.findViewById(R.id.postIV) as RoundedImageView
    var imgVideoIV = itemView.findViewById(R.id.imgVideoIV) as RoundedImageView
    var msgRL = itemView.findViewById(R.id.msgRL) as RelativeLayout
    var txtMessageTV = itemView.findViewById(R.id.txtMessageTV) as TextView
    var value: String? = null

    fun bindData(
        mActivity: Activity?,
        image: String,
        message: String
    ) {
        if (image.isNotEmpty()) {
            mActivity?.let {
                Glide.with(it)
                    .load(image)
                    .placeholder(R.drawable.ic_post_ph)
                    .error(R.drawable.ic_post_ph)
                    .into(postIV)
            }
        }
        if(!message.isNullOrEmpty()){
            msgRL.visibility=View.VISIBLE
            txtMessageTV.setText(message)
        }
        else{
            msgRL.visibility=View.GONE
        }

        if (image.contains(".mp4") || image.contains("video") || image.contains(".3gp")) {
            imgVideoIV.visibility = View.VISIBLE
            value = "video"
        } else {
            imgVideoIV.visibility = View.GONE
            value = "image"
        }

        itemView.setOnClickListener {
            var intent = Intent(mActivity, SharedImageVideoPlayActivity::class.java)
            intent.putExtra("data", image)
            intent.putExtra("value", value)
            mActivity!!.startActivity(intent)
        }

//        mainLL.setOnClickListener {
//            var intent = Intent( mActivity, PostsFromChatActivity::class.java)
//            intent.putExtra("post_id", mModel.post_details!!.post_id)
//            intent.putExtra("user_id", mModel.post_details!!.user_id)
////            intent.putExtra("value","post_click")
//            mActivity!!.startActivity(intent)
//            var intent = Intent( mActivity, CommentsActivity::class.java)
//            intent.putExtra("post_id", mModel.post_details!!.post_id)
//            intent.putExtra("user_id", mModel.post_details!!.user_id)
//            intent.putExtra("value","post_click")
//            mActivity!!.startActivity(intent)
//        }

//        itemView.setOnLongClickListener {
//            showDeleteDialog(
//                mModel
//                    .id
//            )
//            true
//        }


    }

}