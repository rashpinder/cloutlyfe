package com.cloutlyfe.app.repository

import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.retrofit.RetrofitInstance

class AppRepository {

    suspend fun loginUser(body: RequestBodies.LoginBody) = RetrofitInstance.appApi.loginUser(body)

    suspend fun googleLoginUser(body: RequestBodies.GoogleLoginBody) = RetrofitInstance.appApi.googleLoginRequest(body)

    suspend fun fbLoginUser(body: RequestBodies.FacebookLoginBody) = RetrofitInstance.appApi.fbLoginRequest(body)

    suspend fun twitterLoginUser(body: RequestBodies.TwitterLoginBody) = RetrofitInstance.appApi.twitterLoginRequest(body)

    suspend fun signUpUser(body: RequestBodies.SignUpBody) = RetrofitInstance.appApi.signUpUser(body)

    suspend fun forgotPassword(body: RequestBodies.ForgotPswdBody) = RetrofitInstance.appApi.forgotPswd(body)

    suspend fun changePasssword(body: RequestBodies.ChangePswdBody)= RetrofitInstance.appApi.changePswd(body)

//    suspend fun homeData(authToken: String,body: RequestBodies.HomeBody)= RetrofitInstance.appApi.homeRequest(authToken,body)

//    suspend fun savedData(authToken: String,body: RequestBodies.HomeBody)= RetrofitInstance.appApi.savedRequest(authToken,body)

    suspend fun profileData(authToken: String,body: RequestBodies.ProfileBody)= RetrofitInstance.appApi.profileRequest(authToken,body)

    suspend fun followRequestData(authToken: String,body: RequestBodies.FollowBody)= RetrofitInstance.appApi.followRequest(authToken,body)

    suspend fun removeUserRequestData(authToken: String,body: RequestBodies.FollowBody)= RetrofitInstance.appApi.removeUserRequest(authToken,body)

    suspend fun logOut(authToken: String,body: RequestBodies.LogoutBody) = RetrofitInstance.appApi.logOut(authToken,body)

    suspend fun searchData(authToken: String,body: RequestBodies.SearchHomeBody) = RetrofitInstance.appApi.searchHomeRequest(authToken,body)

    suspend fun getReportData(body: RequestBodies.ReportReasonsBody) = RetrofitInstance.appApi.getReportReasonsRequest(body)

    suspend fun addReportData(body: RequestBodies.ReportBody) = RetrofitInstance.appApi.reportRequest(body)

    suspend fun likeUnlikeData(body: RequestBodies.LikeUnlikeBody) = RetrofitInstance.appApi.likeUnlikeRequest(body)

    suspend fun saveUnsaveData(body: RequestBodies.SaveUnsaveBody) = RetrofitInstance.appApi.saveUnsaveRequest(body)

    suspend fun likeUnlikeCommentData(body: RequestBodies.LikeUnlikeCommentBody) = RetrofitInstance.appApi.likeUnlikeCommentRequest(body)
//    fun createPostRequest(
//        Token: String,
//        uploads: ArrayList<MultipartBody.Part>,
//        user_id: RequestBody,
//        bio: RequestBody,
//        location: RequestBody,
//        latitude: RequestBody,
//        longitude: RequestBody
//    ) = RetrofitInstance.appApi.createPostRequest(Token, uploads,user_id,bio,location,latitude,longitude)

//    suspend fun home2Data(authToken: String, body: RequestBodies.Home2Body) = RetrofitInstance.appApi.homeVideoAudioData(authToken,body)
//
//    suspend fun home1Data(body: RequestBodies.Home5Body, authToken: String) = RetrofitInstance.appApi.home1Data(authToken,body)
//
//    suspend fun home4Data(body: RequestBodies.Home5Body, authToken: String)=RetrofitInstance.appApi.home4Data(authToken,body)
//
//    suspend fun home5Data(authToken: String, body: RequestBodies.Home5Body) = RetrofitInstance.appApi.homePrayers(authToken,body)
//
//    suspend fun addPrayer(authToken: String, body: RequestBodies.PrayerBody) = RetrofitInstance.appApi.addPrayer(authToken,body)
//
//    suspend fun contactUs(authToken: String, body: RequestBodies.ContactUsBody) = RetrofitInstance.appApi.contactUs(authToken,body)
//

//
//    suspend fun getNotifications(authToken: String, body: RequestBodies.Home5Body)= RetrofitInstance.appApi.getNotifications(authToken,body)

//    suspend fun searchData(authToken: String, body: RequestBodies.SearchBody)= RetrofitInstance.appApi.searchData(authToken,body)
//
//    suspend fun setInAppPurchaseData( body: RequestBodies.InappPuchaseBody)=RetrofitInstance.appApi.inappPurchase(body)
//
//    suspend fun getPaymentSatus(authToken: String)=RetrofitInstance.appApi.getPaymentStatus(authToken)
//
//    suspend fun freemembershipCheck(authToken: String)=RetrofitInstance.appApi.freeMembershipCheck(authToken)
//
//
//    fun profileSaveDataRequest(
//        token: String,
//        profileimage: MultipartBody.Part?,
//        firstname: RequestBody,
//        lastname: RequestBody,
//        phone: RequestBody,
//        password: RequestBody
//    ) = RetrofitInstance.appApi.profileSaveDataRequest(token, profileimage,firstname,lastname,phone,password)



}