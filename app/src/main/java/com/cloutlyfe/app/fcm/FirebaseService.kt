package com.cloutlyfe.app.fcm

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.cloutlyfe.app.Agora.ConstantsA
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.ChatActivity
import com.cloutlyfe.app.activities.CommentsActivity
import com.cloutlyfe.app.activities.LiveStreaming2Activity
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.utils.Constants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import org.json.JSONObject


@SuppressLint("MissingFirebaseInstanceTokenRefresh")
 public class FirebaseService : FirebaseMessagingService() {

    val TAG = "FirebaseMessagingService"
    var mNotificationModel : HomeData? = null
    var mTitle = ""
    var mBody = ""
    var bodyy =""
    var mIntent : Intent? = null
    var json: JSONObject? = null
    var namee: String? = ""
    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var mData = remoteMessage.data
        bodyy= remoteMessage.notification?.body.toString()
        Log.e(TAG,"*****DATA*****$mData")

    if (remoteMessage.data != null) {
            try{
//                json = JSONObject(remoteMessage.data["notificationData"])
                var mJSONData : JSONObject = JSONObject(mData as Map<*, *>?)
                mNotificationModel = Gson().fromJson(mJSONData.toString(), HomeData::class.java)
            }catch (e: Exception){
                e.printStackTrace()
            }
            sendNotifications(json)
        }
    }


    private fun sendNotifications(json: JSONObject?) {
       mBody = bodyy
        if (mNotificationModel!!.username == "N/A" || mNotificationModel!!.username == null || mNotificationModel!!.username == "") {
            namee = mNotificationModel!!.name
        } else {
            namee = mNotificationModel!!.username
        }
//       mBody = mNotificationModel?.message!!
        if (mNotificationModel?.notification_type.equals("0")) {
            mNotificationModel =
                mNotificationModel?.is_delete?.let {
                    HomeData("","","","",null,0,0,"","",null,"","",
                        mNotificationModel?.name.toString(),"",
                        mNotificationModel?.post_id.toString(),null,
                        mNotificationModel?.otherID.toString(),"",
                        mNotificationModel?.username.toString(),false,0,0,0,
                        mNotificationModel?.notification_type.toString(),
                        mNotificationModel?.room_id.toString(),
                        mNotificationModel?.otherID.toString(),
                        mNotificationModel?.postuser.toString(),
                        mNotificationModel?.notification_id.toString(), mNotificationModel?.creation_date.toString(), mNotificationModel?.message.toString(), mNotificationModel?.followID.toString(), mNotificationModel?.title_message.toString(),
                        mNotificationModel?.status_id.toString(),
                        mNotificationModel?.notification_read_status.toString(),
                        mNotificationModel?.viewed_status.toString(),
                        it,"","","","",""
                    )
                }
            mIntent = Intent(this, OtherUserProfileActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val gson = Gson()
            val mModell = gson.toJson(mNotificationModel)
            mIntent!!.putExtra("homeData", mModell)
            mIntent!!.putExtra("value", "other_user")
            mIntent!!.putExtra("other_user_id", mNotificationModel!!.otherID)
            mIntent!!.putExtra("name",namee)
//            startActivity(i)

        }else if (mNotificationModel?.notification_type.equals("1")) {
            mNotificationModel =
                mNotificationModel?.is_delete?.let {
                    HomeData("","","","",null,0,0,"","",null,"","",
                        mNotificationModel?.name.toString(),"",
                        mNotificationModel?.post_id.toString(),null,
                        mNotificationModel?.otherID.toString(),"",
                        mNotificationModel?.username.toString(),false,0,0,0,
                        mNotificationModel?.notification_type.toString(),
                        mNotificationModel?.room_id.toString(),
                        mNotificationModel?.otherID.toString(),
                        mNotificationModel?.postuser.toString(),
                        mNotificationModel?.notification_id.toString(), mNotificationModel?.creation_date.toString(), mNotificationModel?.message.toString(), mNotificationModel?.followID.toString(), mNotificationModel?.title_message.toString(),
                        mNotificationModel?.status_id.toString(),
                        mNotificationModel?.notification_read_status.toString(),
                        mNotificationModel?.viewed_status.toString(),
                        it,"","","","",""
                    )
                }
            mIntent = Intent(this, OtherUserProfileActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val gson = Gson()
            val mModell = gson.toJson(mNotificationModel)
            mIntent!!.putExtra("homeData", mModell)
            mIntent!!.putExtra("value", "other_user")
            mIntent!!.putExtra("other_user_id", mNotificationModel!!.otherID)
            mIntent!!.putExtra("name", namee)
//            startActivity(i)
        }else if (mNotificationModel?.notification_type.equals("2")) {
//            AppPrefrences().writeString(this, OTHERUSERID,mNotificationModel?.otherID)
//            mIntent = Intent(this, OtherUserProfileActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }else if (mNotificationModel?.notification_type.equals("3")) {
//            AppPrefrences().writeString(this, OTHERUSERID,mNotificationModel?.otherID)
//            mIntent = Intent(this, OtherUserProfileActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }else if (mNotificationModel?.notification_type.equals("5")) {
            mIntent = Intent(this, ChatActivity::class.java)
                            mIntent!!.putExtra(Constants.ROOM_ID, mNotificationModel!!.room_id)
                            mIntent!!.putExtra(Constants.OTHERUSERID, mNotificationModel!!.otherID)
                            mIntent!!.putExtra(Constants.USER_NAME, namee)
                            mIntent!!.putExtra("redirect_value", "push")
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            startActivity(mIntent)
//
        }
        else if (mNotificationModel?.notification_type.equals("8")) {
            mIntent = Intent(this, ChatActivity::class.java)
                            mIntent!!.putExtra(Constants.ROOM_ID, mNotificationModel!!.room_id)
                            mIntent!!.putExtra(Constants.OTHERUSERID, mNotificationModel!!.otherID)
                            mIntent!!.putExtra(Constants.USER_NAME, namee)
                            mIntent!!.putExtra("redirect_value", "push")
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            startActivity(mIntent)
//
        }else if (mNotificationModel?.notification_type.equals("")) {
//            var mAllUsersItem: AllUsersItem? = AllUsersItem(mNotificationModel?.roomId, "","", mNotificationModel?.otherID,
//                mNotificationModel?.photo,"","","", mNotificationModel?.userId,mNotificationModel?.username)
//            mIntent = Intent(this, ChatActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            mIntent!!.putExtra(MODEL, mAllUsersItem)
//            mIntent!!.putExtra("User_profile_pic", mNotificationModel?.photo)
        }else if (mNotificationModel?.notification_type.equals("6")) {
            mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                mNotificationModel?.name.toString(),"",
                mNotificationModel?.post_id.toString(),null,
                mNotificationModel?.otherID.toString(),"",
                mNotificationModel?.username.toString(),false,0,0,0,
                mNotificationModel?.notification_type.toString(),
                mNotificationModel?.room_id.toString(),
                mNotificationModel?.otherID.toString(),
                mNotificationModel?.postuser.toString(),
                mNotificationModel?.notification_id.toString(), mNotificationModel?.creation_date.toString(), mNotificationModel?.message.toString(), mNotificationModel?.followID.toString(), mNotificationModel?.title_message.toString(),
                mNotificationModel?.status_id.toString(),
                mNotificationModel?.notification_read_status.toString(),
                mNotificationModel?.viewed_status.toString(),
                mNotificationModel!!.is_delete,"","","","",""
            )
            mIntent = Intent(this, CommentsActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val gson = Gson()
            val mModell = gson.toJson(mNotificationModel)
            mIntent!!.putExtra("homeData", mModell)
            mIntent!!.putExtra("value", "push")
            mIntent!!.putExtra("other_user_id", mNotificationModel!!.otherID)
            mIntent!!.putExtra("name", namee)
            mIntent!!.putExtra("post_id", mNotificationModel!!.post_id)
        }else if (mNotificationModel?.notification_type.equals("7")) {
            mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                mNotificationModel?.name.toString(),"",
                mNotificationModel?.post_id.toString(),null,
                mNotificationModel?.otherID.toString(),"",
                mNotificationModel?.username.toString(),false,0,0,0,
                mNotificationModel?.notification_type.toString(),
                mNotificationModel?.room_id.toString(),
                mNotificationModel?.otherID.toString(),
                mNotificationModel?.postuser.toString(),
                mNotificationModel?.notification_id.toString(), mNotificationModel?.creation_date.toString(), mNotificationModel?.message.toString(), mNotificationModel?.followID.toString(), mNotificationModel?.title_message.toString(),
                mNotificationModel?.status_id.toString(),
                mNotificationModel?.notification_read_status.toString(),
                mNotificationModel?.viewed_status.toString(),
                mNotificationModel!!.is_delete,"","","","",""
            )
            mIntent = Intent(this, CommentsActivity::class.java)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val gson = Gson()
            val mModell = gson.toJson(mNotificationModel)
            mIntent!!.putExtra("homeData", mModell)
            mIntent!!.putExtra("value", "push")
            mIntent!!.putExtra("other_user_id", mNotificationModel!!.otherID)
            mIntent!!.putExtra("name", namee)
            mIntent!!.putExtra("post_id", mNotificationModel!!.post_id)
        } else if (mNotificationModel?.notification_type.equals("10")) {
            mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                mNotificationModel?.name.toString(),"",
                mNotificationModel?.post_id.toString(),null,
                mNotificationModel?.otherID.toString(),"",
                mNotificationModel?.username.toString(),false,0,0,0,
                mNotificationModel?.notification_type.toString(),
                mNotificationModel?.room_id.toString(),
                mNotificationModel?.otherID.toString(),
                mNotificationModel?.postuser.toString(),
                mNotificationModel?.notification_id.toString(), mNotificationModel?.creation_date.toString(), mNotificationModel?.message.toString(), mNotificationModel?.followID.toString(), mNotificationModel?.title_message.toString(),
                mNotificationModel?.status_id.toString(),
                mNotificationModel?.notification_read_status.toString(),
                mNotificationModel?.viewed_status.toString(),
                mNotificationModel!!.is_delete,"",mNotificationModel?.disable_like.toString(),mNotificationModel?.stream_id.toString(),mNotificationModel?.disable_comment.toString(),mNotificationModel?.stream_room.toString()
            )


            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.STREAM_USERID,
               mNotificationModel?.user_id.toString()
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.STREAM_ROOMID,
                mNotificationModel?.stream_room.toString()
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.STREAM_USERNAME,
                namee
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.STREAM_ID,
                mNotificationModel?.stream_id.toString()
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.STREAM_USERPHOTO,
                mNotificationModel?.photo.toString()
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.COMMENT_DISABLE,
                mNotificationModel?.disable_comment.toString()
            )
            CloutNewPrefernces.writeString(
                this,
                CloutNewPrefernces.LIKE_DISABLE,
                mNotificationModel?.disable_like.toString()
            )
            gotoRoleActivity(mNotificationModel?.stream_room.toString(),
                AppPrefrences().readString(this,Constants.ID,null).toString()
            )
            mIntent = Intent(this, LiveStreaming2Activity::class.java)
        }
//        var pendingIntent = PendingIntent.getActivity(this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT)

//        var pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           var pendingIntent= PendingIntent.getActivity(
                this,
                0,
                mIntent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE or FLAG_ONE_SHOT
            )
//        }
//        else {
//            PendingIntent.getActivity(
//                this,
//                0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT
//            )
//        }
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(mBody)
                .setContentText(mTitle)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            if (notificationManager != null) {
                val channel = NotificationChannel(
                    channelId, getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT)
                channel.enableVibration(true)
                notificationManager.createNotificationChannel(channel)
//            }
//        }
        notificationManager?.notify(getAlphaNumeric()!!, notificationBuilder.build())
    }

    fun gotoRoleActivity(stream_room: String, userID: String) {
        val mil = (System.currentTimeMillis() / 1000L).toInt()
        ConstantsA.channelNameA = stream_room
        ConstantsA.useridA = userID
        onJoinAsAudience()
    }
    fun onJoinAsAudience() {
        gotoLiveActivity(io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE)
    }

    private fun gotoLiveActivity(role: Int) {
        mIntent = Intent(this, LiveStreaming2Activity::class.java)
        mIntent!!.putExtra(ConstantsA.KEY_CLIENT_ROLE, role)
    }


    open fun getAlphaNumeric(): Int? {
        val n = 3
        // chose a Character random from this String
        val AlphaNumericString = ("0123456789")
        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length * Math.random()).toInt()
            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString().toInt()
    }


}