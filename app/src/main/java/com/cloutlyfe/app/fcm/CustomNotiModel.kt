package com.nutshell.app.fcm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomNotiModel(

	@field:SerializedName("notification_type")
	var notificationType: String? = "",

	@field:SerializedName("room_id")
	var roomId: String? = "",

	@field:SerializedName("post_id")
	var post_id: String? = "",

	@field:SerializedName("otherID")
	var otherID: String? = "",

	@field:SerializedName("postuser")
	var postuser: String? = "",

	@field:SerializedName("photo")
	var photo: String? = "",

	@field:SerializedName("notification_id")
	var notificationId: String? = "",

	@field:SerializedName("creation_date")
	var creationDate: String? = "",

	@field:SerializedName("message")
	var message: String? = "",

	@field:SerializedName("followID")
	var followID: String? = "",

	@field:SerializedName("title_message")
	var titleMessage: String? = "",

	@field:SerializedName("status_id")
	var statusId: String? = "",

	@field:SerializedName("notification_read_status")
	var notificationReadStatus: String? = "",

	@field:SerializedName("user_id")
	var userId: String? = "",

	@field:SerializedName("name")
	var name: String? = "",

	@field:SerializedName("viewed_status")
	var viewedStatus: String? = "",

	@field:SerializedName("username")
	var username: String? = ""
) : Parcelable
