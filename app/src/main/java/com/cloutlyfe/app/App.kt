package com.cloutlyfe.app

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.StrictMode
import com.cloutlyfe.app.Agora.rtc.AgoraEventHandler
import com.cloutlyfe.app.Agora.rtc.EngineConfig
import com.cloutlyfe.app.Agora.rtc.EventHandler
import com.cloutlyfe.app.Agora.stats.StatsManager
import com.cloutlyfe.app.Agora.utils.FileUtil
import com.cloutlyfe.app.Agora.utils.PrefManager
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Constants.Companion.SOCKET_URL
import com.danikula.videocache.HttpProxyCacheServer
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import io.agora.rtc.RtcEngine
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.Polling
import io.socket.engineio.client.transports.PollingXHR
import io.socket.engineio.client.transports.WebSocket
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import java.net.URISyntaxException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


class App : Application() {
    /*
   * Socket
   * */
    var mSocket: Socket? = null
    /**
     * Initialize the Applications Instance
     */
    var mInstance: App? = null

    //Agora
    private var mRtcEngine: RtcEngine? = null
    private val mGlobalConfig: EngineConfig = EngineConfig()
    private val mHandler: AgoraEventHandler = AgoraEventHandler()
    private val mStatsManager: StatsManager = StatsManager()


    val CONNECTION_TIMEOUT = 50000 * 1000 //120 Seconds    private RequestQueue mRequestQueue;


    private var proxy: HttpProxyCacheServer? = null
    /*private fun newProxy(): HttpProxyCacheServer {
        return HttpProxyCacheServer.Builder(this)
            .maxCacheSize((1024 * 1024).toLong())
            .build()
    }*/

    private fun newProxy(): HttpProxyCacheServer {
        return HttpProxyCacheServer(this)
    }


//
    companion object {
        fun getProxy(context: Context): HttpProxyCacheServer {
//            CoroutineScope(Dispatchers.IO).launch {
//            }
            try {
                val app = context.applicationContext as App
                return if (app.proxy == null) app.newProxy().also { app.proxy = it } else app.proxy!!
            }
            catch (e:Exception){
                val app = context.applicationContext as App
                return if (app.proxy == null) app.newProxy().also { app.proxy = it } else app.proxy!!
            }
        }
    }
    /**
     * Synchronized the Application Class Instance
     */
    @Synchronized
    fun getInstance(): App? {
        return mInstance
    }


    override fun onCreate() {
        super.onCreate()
        // Required initialization logic here!
        mInstance = this
        try {
            val myHostnameVerifier = HostnameVerifier {_,_->
                return@HostnameVerifier true
            }

            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager
            {
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}

                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            })

            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, trustAllCerts, null)

            val okHttpClient = OkHttpClient.Builder()
                .hostnameVerifier(myHostnameVerifier).connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .sslSocketFactory(sslContext.socketFactory, trustAllCerts[0] as X509TrustManager)
                .build()
            val options = IO.Options()
//            options.callFactory = clientBuilder.build()
//            options.transports = arrayOf(Polling.NAME)
//            options.secure=true
            options.callFactory = okHttpClient
            options.webSocketFactory = okHttpClient
            mSocket = IO.socket(SOCKET_URL,options)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        try {
            mRtcEngine =
                RtcEngine.create(applicationContext, getString(R.string.private_app_id), mHandler)
            mRtcEngine!!.setLogFile(FileUtil.initializeLogFile(this))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private fun initConfig() {
        val pref: SharedPreferences = PrefManager.getPreferences(applicationContext)
        mGlobalConfig.videoDimenIndex = pref.getInt(
            Constants.PREF_RESOLUTION_IDX, Constants.DEFAULT_PROFILE_IDX
        )
        val showStats = pref.getBoolean(Constants.PREF_ENABLE_STATS, false)
        mGlobalConfig.setIfShowVideoStats(showStats)
        mStatsManager.enableStats(showStats)
        mGlobalConfig.mirrorLocalIndex = pref.getInt(Constants.PREF_MIRROR_LOCAL, 0)
        mGlobalConfig.mirrorRemoteIndex = pref.getInt(Constants.PREF_MIRROR_REMOTE, 0)
        mGlobalConfig.mirrorEncodeIndex = pref.getInt(Constants.PREF_MIRROR_ENCODE, 0)
    }

    fun engineConfig(): EngineConfig? {
        return mGlobalConfig
    }

    fun rtcEngine(): RtcEngine? {
        return mRtcEngine
    }

    fun statsManager(): StatsManager? {
        return mStatsManager
    }

    fun registerEventHandler(handler: EventHandler?) {
        mHandler.addHandler(handler)
    }

    fun removeEventHandler(handler: EventHandler?) {
        mHandler.removeHandler(handler)
    }

    override fun onTerminate() {
        super.onTerminate()
        RtcEngine.destroy()
    }

    fun getSocket(): Socket? {
        return mSocket
    }
}