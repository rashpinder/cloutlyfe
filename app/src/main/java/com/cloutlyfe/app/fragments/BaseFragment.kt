package com.cloutlyfe.app.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.SystemClock
import androidx.fragment.app.Fragment
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.utils.Constants.Companion.AUTHTOKEN
import com.cloutlyfe.app.utils.Constants.Companion.DESCRIPTION
import com.cloutlyfe.app.utils.Constants.Companion.FOLLOWERS
import com.cloutlyfe.app.utils.Constants.Companion.FOLLOWER_ID
import com.cloutlyfe.app.utils.Constants.Companion.FOLLOWING
import com.cloutlyfe.app.utils.Constants.Companion.ID
import com.cloutlyfe.app.utils.Constants.Companion.NAME
import com.cloutlyfe.app.utils.Constants.Companion.OTHERUSERID
import com.cloutlyfe.app.utils.Constants.Companion.PROFILEPIC
import com.cloutlyfe.app.R
import java.io.*
import java.util.*

open class BaseFragment : Fragment() {
    // - - Get Class Name
    open var TAG = this@BaseFragment.javaClass.simpleName
    var progressDialog: Dialog? = null
    private var mLastClickTab1: Long = 0
    /*Switch between fragments*/


//    fun switchFragment(
//        fragment: Fragment?,
//        Tag: String?,
//        addToStack: Boolean,
//        bundle: Bundle?
//    ) {
//    //    val fragmentManager: FragmentManager = fragment!!.childFragmentManager
//       val fragmentManager = (activity as FragmentActivity).supportFragmentManager
//      //  val fragmentManager = supportFragmentManager
//        if (fragment != null) {
//            val fragmentTransaction =
//                fragmentManager.beginTransaction()
//            fragmentTransaction.replace(R.id.frameLayout, fragment, Tag)
//            if (addToStack) fragmentTransaction.addToBackStack(Tag)
//            if (bundle != null) fragment.arguments = bundle
//            fragmentTransaction.commit()
//            fragmentManager.executePendingTransactions()
//        }
//    }
    /*
   * Check Internet Connections
   * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    /*
* Toast Message
* */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }
    /*
  * Getting User Auth Token
 * */
    fun getAuthToken(): String {
        return AppPrefrences().readString(requireActivity(), AUTHTOKEN, "")!!
    }
    /*
*
* Get User Name
* */
    fun getUserName(): String {
        return AppPrefrences().readString(requireActivity(), NAME, "")!!
    }
    /*
* Getting other User ID
* */
    fun getOtherUserID(): String {
        return AppPrefrences().readString(requireActivity(), OTHERUSERID, "")!!
    }


    /*
* Get User Bio
* */
    fun getUserDescription(): String {
        return AppPrefrences().readString(requireActivity(), DESCRIPTION, "")!!
    }


    /*
    * Get User Profile Pic
    * */
    fun getUserProfilePic(): String {
        return AppPrefrences().readString(requireActivity(), PROFILEPIC, "")!!
    }
    /*
   * Get User Followers
   * */
    fun getUserFollowers(): String {
        return AppPrefrences().readString(requireActivity(), FOLLOWERS, "")!!
    }
    /*
* Get User Followers
* */
    fun getUserFollowings(): String {
        return AppPrefrences().readString(requireActivity(), FOLLOWING, "")!!
    }
    /*
* Getting User ID
* */
    fun getLoggedInUserID(): String {
        if(activity!=null && isAdded){
        return AppPrefrences().readString(requireActivity(), ID, "")!!}
        else{
            return null.toString()
        }

    }
    /*
* Getting follower  ID
* */
    fun getFollowerID(): String {
        return AppPrefrences().readString(requireActivity(), FOLLOWER_ID, "")!!
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }
    /*
        * Hide Progress Dialog
        * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        if(mActivity!=null){
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()}
    }

//
//    // show block alert dialog
//    fun showDeleteAlertDialog(mActivity: Activity?, strMessage: String?) {
//        val alertDialog = mActivity.let { Dialog(it!!) }
//        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        alertDialog.setContentView(R.layout.dialog_delete)
//        alertDialog.setCanceledOnTouchOutside(false)
//        alertDialog.setCancelable(false)
//        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
//        txtMessageTV.text = strMessage
//        // set the custom dialog components - text, image and button
//        val btnOk = alertDialog.findViewById<TextView>(R.id.btnOk)
//        val btnCancel = alertDialog.findViewById<TextView>(R.id.btnCancel)
//
//        btnCancel.setOnClickListener {
//            alertDialog.dismiss()
//        }
//        btnOk.setOnClickListener {
//            alertDialog.dismiss()
////            val intent = Intent(mActivity, SignInActivity::class.java)
////            startActivity(intent)
////            mActivity?.let { ActivityCompat.finishAffinity(it) }
//        }
//        alertDialog.show()
//    }
    // show block alert dialog
    fun showBlockAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = mActivity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        txtMessageTV.text = strMessage
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
//            val intent = Intent(mActivity, SignInActivity::class.java)
//            startActivity(intent)
//            mActivity?.let { ActivityCompat.finishAffinity(it) }
        }
        alertDialog.show()
    }
    // show block alert dialog
    fun showUnfollowAlertDialog(mActivity: Activity?, strMessage: String?, otherUserid: String?) {
        val alertDialog = mActivity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        txtMessageTV.text = strMessage
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
        alertDialog.dismiss()

        }
        alertDialog.show()
    }


    fun convertFileToByteArray(f: File): ByteArray? {
        var byteArray: ByteArray? = null
        try { /*from w  ww.  j  a  v a 2  s .c  om*/
            val inputStream: InputStream = FileInputStream(f)
            val bos = ByteArrayOutputStream()
            val b = ByteArray(1024 * 8)
            var bytesRead = 0
            while (inputStream.read(b).also { bytesRead = it } != -1) {
                bos.write(b, 0, bytesRead)
            }
            byteArray = bos.toByteArray()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return byteArray
    }
    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }
    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }


    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }
}

