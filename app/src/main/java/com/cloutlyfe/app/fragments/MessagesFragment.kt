package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.ChatActivity
import com.cloutlyfe.app.adapters.ChatListAdapter
import com.cloutlyfe.app.databinding.FragmentMessagesBinding
import com.cloutlyfe.app.fragments.BaseFragment
import com.cloutlyfe.app.interfaces.ChatItemClickListner
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.*

import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants.Companion.ROOM_ID
import com.cloutlyfe.app.utils.Constants.Companion.USER_NAME
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MessagesFragment : BaseFragment() {
    // - - Initialize Widgets
    var chatListAdapter: ChatListAdapter? = null
    var ChatDetailList: ArrayList<AllUser> = ArrayList()
    lateinit var binding: FragmentMessagesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMessagesBinding.inflate(inflater, container, false)
        return binding.root;
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()
        binding.header.txtHeadingTV.setText(getString(R.string.chat))
        ChatDetailList.clear()
//        setAdapter()
        if(isNetworkAvailable(activity!!)){
            getChatUsersRequest()
        }
        else {
            showToast(activity!!, getString(R.string.no_internet_connection))
        }
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
//        mMap["user_id"] = "10"
        mMap["page_no"] = "1"
        mMap["per_page"] = "400"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun getChatUsersRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.getChatUsersRequest(mHeaders, mParam())
        call.enqueue(object : Callback<ChatUsersModel> {
            override fun onFailure(call: Call<ChatUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<ChatUsersModel>,
                response: Response<ChatUsersModel>
            ) {
//                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
//                if (response.body()!!.status == 1) {
                    Log.e(TAG, response.body().toString())
                    dismissProgressDialog()
                    val mGetChatUsersModel: ChatUsersModel = response.body()!!

                    if (mGetChatUsersModel!!.status == 1) {
                        //    (activity as HomeActivity?)?.badgeChatCount(mGetChatUsersModel.unreadMessageCount!!.toInt())
                        ChatDetailList.addAll(mGetChatUsersModel.all_users)

                        if (ChatDetailList.size == 0) {
//                        binding.upArraowRL.visibility = View.GONE
                            binding.txtNoDataTV.text =
                                "No Data found."
                            binding.txtNoDataTV.visibility = View.VISIBLE

                        } else {
//                        binding.upArraowRL.visibility = View.VISIBLE
                            setAdapter()
                            binding.txtNoDataTV.visibility = View.GONE
                        }
                    } else if (mGetChatUsersModel!!.status == 2) {
                        showAlertDialog(activity, mGetChatUsersModel.message)
                        // showDoubleButtonAlertDialog(mActivity, mSignInModel.message, editEmailET.text.toString())
                    } else if (mGetChatUsersModel!!.status == 0) {
                        binding.txtNoDataTV.text =
                            "No Data found."
                        binding.txtNoDataTV.visibility = View.VISIBLE
//                        showAlertDialog(activity, mGetChatUsersModel.message)
                        //    showAlertDialog(activity, mGetChatUsersModel.message)

                    } else if (response.body()!!.status == 3) {
                        dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                    } else {
                        showAlertDialog(activity, getString(R.string.server_error))
                    }}
                })
    }


    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.chatRV.setLayoutManager(layoutManager)
        chatListAdapter = ChatListAdapter(activity,ChatDetailList,mChatItemClickListner,delInterface)
        binding.chatRV.setAdapter(chatListAdapter)
    }

    var mChatItemClickListner : ChatItemClickListner = object : ChatItemClickListner {
        override fun onItemClickListner(mModel: AllUser) {
            if (isNetworkAvailable(activity!!)){
//                var mIntent = Intent(activity, ChatActivity::class.java)
//                mIntent.putExtra(USER_NAME, name)
//                mIntent.putExtra(ROOM_ID, mCreateRoomModel.room_id)
//                startActivity(mIntent)
                if(mModel.username=="N/A"||mModel.username==""||mModel.username==null){
                    executeCreateRoomApi(mModel,mModel!!.name)
                }
                else{
                    executeCreateRoomApi(mModel,mModel!!.username)
                }

            }else{
                showToast(activity,getString(R.string.no_internet_connection))
            }
        }

    }

    var roomId: String? = ""
    var msg_id: String? = ""
    var mPos: Int? = 0

    var delInterface : DeleteChatInterface = object : DeleteChatInterface {
        override fun onItemClickListner(mPosition: Int, id: String, room_id: String) {
            mPos = mPosition
            msg_id = id
            roomId = room_id
            if (isNetworkAvailable(activity!!)){
                executeDeleteApi()
            }else{
                showToast(activity,getString(R.string.no_internet_connection))
            }
        }

    }

    private fun executeDeleteApi() {
        if (activity?.let { isNetworkAvailable(it) }!!)
            RequestDeleteData()
        else
            showToast(activity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["id"] = ""
        mMap["room_id"] = roomId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteData() {
//        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.deleteChatRequest(mDeleteParam())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
//                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
                if (response.body()!!.status == 1) {
//                    showToast(activity, response.body()!!.message)
                    ChatDetailList.removeAt(mPos!!)
                    chatListAdapter!!.notifyDataSetChanged()
                    if(ChatDetailList.size==0){
                        binding.txtNoDataTV.text =
                            "No Data found."
                        binding.txtNoDataTV.visibility = View.VISIBLE
                    }
                    else{
                        binding.txtNoDataTV.visibility = View.GONE
                    }
//getChatUsersRequest()
                } else if (response.body()!!.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

                }
            }
        })
    }



    private fun executeCreateRoomApi(mModel: AllUser?, name: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        mHeaders["Token"] = "931cbac9c9cf192912b21ae908a90e55"

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
//        mMap["user_id"] = "10"
        mMap["owner_id"] = mModel?.receiver_id
        showProgressDialog(activity)

            val call = RetrofitInstance.appApi.createRoomRequest(mHeaders, mMap)
        call.enqueue(object : Callback<CreateRoomModel> {
            override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<CreateRoomModel>, response: Response<CreateRoomModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mCreateRoomModel = response.body()!!
                if (mCreateRoomModel.status == 1) {
                    var mIntent = Intent(activity, ChatActivity::class.java)
                    if(mModel!!.username=="N/A"||mModel.username==""||mModel.username==null){
                        mIntent.putExtra(USER_NAME, mModel.name)
                    }
                    else{
                        mIntent.putExtra(USER_NAME, mModel.username)
                    }

                    mIntent.putExtra(ROOM_ID, mCreateRoomModel.room_id)
                    startActivity(mIntent)
                } else {
                    showToast(activity, mCreateRoomModel.message)
                }
            }
        })
    }
}