package com.cloutlyfe.app.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.databinding.FragmentSavedBinding
import com.cloutlyfe.app.mode.Feed
import com.cloutlyfe.app.mode.Photo
import com.cloutlyfe.app.mode.Video
import com.cloutlyfe.app.ui.adapter.SavedAdapter
import com.cloutlyfe.app.ui.view.CenterLayoutManager
/**
 * Create By Rashpinder
 */
class SavedFragment : BaseFragment() {

    lateinit var binding: FragmentSavedBinding
    val layoutManager = LinearLayoutManager(context)
    val list: ArrayList<String> = ArrayList()
    private var adapter: SavedAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSavedBinding.inflate(inflater, container, false)
        return binding.root;
    }



    private fun initView() {
        adapter = SavedAdapter(requireContext())
//        binding.homeListRV.layoutManager = layoutManager
        binding.savedListRV.setLayoutManager(CenterLayoutManager(context))
//        adapter = activity?.let { SavedAdapter(it) }
        binding.savedListRV.adapter = adapter
//        initData()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
//        initRecyclerView()
//        initScrollListener()
    }

    override fun onPause() {
        super.onPause()
        if (binding.savedListRV.handingVideoHolder != null) binding.savedListRV.handingVideoHolder.stopVideo()
    }

    override fun onDetach() {
        super.onDetach()
        if (binding.savedListRV.handingVideoHolder != null) binding.savedListRV.handingVideoHolder.stopVideo()
    }


    override fun onStop() {
        super.onStop()
        if (binding.savedListRV.handingVideoHolder != null) binding.savedListRV.handingVideoHolder.stopVideo()
    }

    override fun onResume() {
        super.onResume()
        if (binding.savedListRV.handingVideoHolder != null) binding.savedListRV.handingVideoHolder.playVideo()
    }





}
