package com.cloutlyfe.app.fragments
import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import jp.shts.android.storiesprogressview.StoriesProgressView
import kotlinx.android.synthetic.main.fragment_screen_slide_page.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.FragmentScreenSlidePageBinding
import com.cloutlyfe.app.model.CodeStatusModel
import com.cloutlyfe.app.model.Snap
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import jp.shts.android.storiesprogressview.StoriesProgressView.StoriesListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class ScreenSlidePageFragment : BaseFragment(), StoriesListener,Player.EventListener {
    lateinit var binding: FragmentScreenSlidePageBinding
    private var pressTime = 0L
    private var limit = 500L
//    private var idExoPlayerVIew: PlayerView? = null
//    private var stories: StoriesProgressView? = null
//    private var image: ImageView? = null
//    private var profileIV: ImageView? = null
    private var counter = 0
    private var time_counter = 0
    private var status_id_counter = 0
//    private var reverse: View ?=null
//    private var skip: View?=null
    var photo:String=""
    var username:String=""
    var positionN=0
    var mActivity = activity
    private var mStatusId:String=""
    var noOfUserStoryCount=0
    var mPager: ViewPager?=null
    private var selectedImageList: ArrayList<String>? = ArrayList()
    private var selectedTimeList: ArrayList<String>? = ArrayList()
    private var selectedStatusIdList: ArrayList<String>? = ArrayList()

    lateinit var statusList1: MutableList<Snap>
    private var selectedPosition by Delegates.notNull<Int>()

    private var status_room_id: String?=null

    var otherUserid:String=""
    @SuppressLint("ClickableViewAccessibility")

    private val onTouchListener =
        View.OnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    pressTime = System.currentTimeMillis()
                    stories!!.pause()
                    return@OnTouchListener false
                }
                MotionEvent.ACTION_UP -> {
                    val now = System.currentTimeMillis()
                    stories!!.resume()
                    return@OnTouchListener limit < now - pressTime
                }
            }
            false
        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        selectedPosition = arguments?.getInt(POSITION, 0)!!
        try {
            for(i in statusList1){
                val statusImages: String? =i.status_image
                val statusTime: String? =i.statusdate
                val statusId: String? =i.status_id
                var durationn: String? =i.duration
                selectedImageList!!.add(statusImages!!)
                selectedTimeList!!.add(statusTime!!)
                selectedStatusIdList!!.add(statusId!!)
                durationList!!.add(durationn!!)
                al.add(Integer.parseInt(i.toString()),durationn.toLong())
            }
        }
        catch (e: Exception)
        {
            e.printStackTrace()
        }
        getDataMehod()
    }

    private var durationList: ArrayList<String>? = ArrayList()
    val al: MutableList<Long> = ArrayList()

    @SuppressLint("UseRequireInsteadOfGet", "SetTextI18n", "SimpleDateFormat")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentScreenSlidePageBinding.inflate(inflater, container, false)
//        mStatusId= statusList1[positionN].status_id!!
//        stories = view!!.findViewById(R.id.stories) as StoriesProgressView
//        image = view!!.findViewById(R.id.image) as ImageView
//        reverse= view!!.findViewById(R.id.reverse)
//        skip = view!!.findViewById(R.id.skip)
//        profileIV= view!!.findViewById(R.id.profileIV) as ImageView
        Glide.with(activity!!).load(photo)
            .placeholder(R.drawable.ic_profile_ph).thumbnail(0.5f).into(binding.profileIV!!)
        binding.txtUsersNameTV.text = username
//        executeCreateRoomApi()

        return binding.root
    }

    override fun onNext() {
        if(!selectedImageList!![++status_images_counter].contains("mp4")){
//            storiesProgressView!!.setStoryDuration(5000L)
            binding.idExoPlayerVIew!!.visibility = View.GONE
            binding.image!!.visibility = View.VISIBLE
            Glide.with(context as Activity)
                .load(selectedImageList!![++counter])
                .placeholder(R.color.grey)
                .thumbnail(0.01f)
                .into(binding.image!!)}
        else{
//            storiesProgressView!!.setStoryDuration(durr)
             binding.idExoPlayerVIew!!.visibility = View.VISIBLE
            binding.image!!.visibility = View.GONE
            initializePlayer(selectedImageList!![++counter])
        }

        val today =Date()
        val timestamp: Long = selectedTimeList!![++time_counter].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date:Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date:Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
                else{
                    binding.txtTimeTV.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    binding.txtTimeTV.text = "$minutes minutes ago"
                }
                else{
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
            }
        }
        else if(hours.toInt() > 0){
            binding.txtTimeTV.text = "$hours hours ago"
        }
        Log.e(TAG, "onNext: "+"on next called" )
        executeUpdateViewApi(selectedStatusIdList!![++status_id_counter])
    }

    override fun onPrev() {
        if (counter - 1 < 0) return
        if(!selectedImageList!![--status_images_counter].contains("mp4")){
//           storiesProgressView!!.setStoryDuration(5000L)
             binding.idExoPlayerVIew!!.visibility = View.GONE
            binding.image!!.visibility = View.VISIBLE
            Glide.with(context as Activity)
                .load(selectedImageList!![--counter])
                .placeholder(R.color.grey)
                .thumbnail(0.01f)
//            .centerCrop()
                .into(binding.image!!)}
        else{
             binding.idExoPlayerVIew!!.visibility = View.VISIBLE
            binding.image!!.visibility = View.GONE
//            storiesProgressView!!.setStoryDuration(durr)
            initializePlayer(selectedImageList!![--counter])
        }
//        Glide.with(context as Activity)
//            .load(selectedImageList!![--counter]).thumbnail(0.5f)
//            .placeholder(R.color.grey)
//            .into(binding.image!!)

        val today =Date()
        val timestamp: Long = selectedTimeList!![--time_counter].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date:Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date:Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
                else{
                    binding.txtTimeTV.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    binding.txtTimeTV.text = "$minutes minutes ago"
                }
                else{
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
            }
        }
        else if(hours.toInt() > 0){
            binding.txtTimeTV.text = "$hours hours ago"
        }
        executeUpdateViewApi(selectedStatusIdList!![--status_id_counter])
        Log.e(TAG, "onNext: "+"on prev called" )
    }

    override fun onComplete() {
            if (mPager!!.currentItem + 1 < mPager!!.adapter?.count ?: 0) {
                try {
                    if (mPager!!.currentItem + 1 == mPager!!.adapter?.count ?: 0){
                        mActivity?.finish()
                        Log.e(TAG,"try")
                    }else{
                        Log.e(TAG," try else")
                        mActivity?.finish()
//                        mPager!!.setCurrentItem(positionN + 1, true)
                    }
                } catch (e: Exception) {
                    mActivity?.finish()
                    Log.e(TAG,"catch"+e.toString())
                }
            } else {
                Log.e(TAG,"else")
                mActivity?.finish()
            }
    }
    override fun onDestroy() {
        super.onDestroy()
        binding.stories!!.destroy()

    }

    private var status_images_counter = 0

    @SuppressLint("UseRequireInsteadOfGet")
    fun getDataMehod(){
        val today =Date()
        val timestamp: Long = selectedTimeList!![time_counter].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date:Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date:Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
                else{
                    binding.txtTimeTV.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    binding.txtTimeTV.text = "$minutes minutes ago"
                }
                else{
                    binding.txtTimeTV.text = "$seconds seconds ago"
                }
            }
        }
        else if(hours.toInt() > 0){
            binding.txtTimeTV.text = "$hours hours ago"
        }
        binding.stories!!.setStoriesCount(selectedImageList!!.size)

        val objects: LongArray = al.toTypedArray().toLongArray()
        for (obj in objects){
            binding.stories!!.setStoriesCountWithDurations(objects)
        }
        binding.stories!!.setStoriesListener(this)
        binding.stories!!.startStories(counter)
//        showProgressDialog(activity)
//        binding.stories!!.pause()

        if (selectedImageList!![status_images_counter].contains("mp4")){
             binding.idExoPlayerVIew!!.visibility = View.VISIBLE
            binding.image!!.visibility = View.GONE
            initializePlayer(selectedImageList!![counter])
        }
        else{
            binding.idExoPlayerVIew!!.visibility = View.GONE
            binding.image!!.visibility = View.VISIBLE
            Glide.with(context as Activity)
                .load(selectedImageList!![counter])
                .placeholder(R.color.grey)
                .thumbnail(0.01f)
                .into(binding.image!!)
        }

//        dismissProgressDialog()
        executeUpdateViewApi(selectedStatusIdList!![status_id_counter])
        Log.e(TAG, "onNext: "+"on current called" )
        binding.reverse!!.setOnClickListener {
            binding.stories!!.reverse()
        }
        binding.reverse!!.setOnTouchListener(onTouchListener)
        binding.skip!!.setOnClickListener {
            binding.stories!!.skip()
        }
        binding.skip!!.setOnTouchListener(onTouchListener)
    }
    var exoPlayer: SimpleExoPlayer? = null
    private fun executeUpdateViewApi(statusId: String) {
        mStatusId=statusId
        if (isNetworkAvailable(mActivity!!)){
            requestUpdateStatus()
        }
        else{
            showToast(mActivity, getString(R.string.no_internet_connection))
    }}
    fun initializePlayer(link: String?)
    {
        if (!link.isNullOrEmpty()) {
            Log.e("Videos", "***Link***" + link)
// progressBarVid.visibility = View.VISIBLE
             binding.idExoPlayerVIew!!.visibility = View.VISIBLE
            binding.image!!.visibility = View.GONE

            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode = DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

            val loadControl = DefaultLoadControl.Builder().setBufferDurationsMs(25000, 50000, 100, 300).createDefaultLoadControl()

            val renderersFactory = DefaultRenderersFactory(requireActivity()).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(requireActivity(), renderersFactory).build()


            val dataSourceFactory = DefaultDataSourceFactory(requireActivity(), Util.getUserAgent(requireActivity(), "CloutLyfe"))

            val mediaSources = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(
                Uri.parse(link))
            exoPlayer!!.prepare(mediaSources)
//            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
             binding.idExoPlayerVIew!!.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
             binding.idExoPlayerVIew!!.setShutterBackgroundColor(Color.TRANSPARENT);
             binding.idExoPlayerVIew!!.player = exoPlayer
            exoPlayer!!.playWhenReady = true
             binding.idExoPlayerVIew!!.requestFocus()


            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {
                    if(isLoading){
                        showProgressDialog(mActivity)
                        binding.stories?.pause()
                    }
                    else{
                        dismissProgressDialog()
                        binding.stories?.resume()
                    }
                }
                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            dismissProgressDialog()
                            exoPlayer!!.playWhenReady = true
                            binding.stories?.resume()
//                            storiesProgressView!!.startStories(); // <- start progress

                        }
                        Player.STATE_BUFFERING -> {
                            binding.stories?.pause()
                            exoPlayer!!.seekTo(0)
                        }
                        Player.STATE_IDLE -> {

                        }

                        else ->
                        {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }
                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {}
                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {

                }
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }

    private fun mUpdateViewParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["status_id"] = mStatusId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun requestUpdateStatus() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.updateStatusViewUser(mHeaders, mUpdateViewParam())
        call.enqueue(object : Callback<CodeStatusModel> {
            override fun onFailure(call: Call<CodeStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CodeStatusModel>,
                response: Response<CodeStatusModel>
            ) {
                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
                if (response.body()!!.status == 1) {
//                    showToast(mActivity, response.body()!!.message)
//                    dismissProgressDialog()
//                    onBackPressed()
//                    finish()

                } else if (response.body()!!.status == 3) {
//                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
//                    dismissProgressDialog()
                }
            }
        })
    }
    companion object {
        const val POSITION = "position"
        fun newInstance(
            mActivity: Activity?,
            position: Int,
            size: Int,
            mPager: ViewPager?,
            statusList1: MutableList<Snap>,
            photo: String,
            username: String,
            otherUserid: String) = ScreenSlidePageFragment().apply {
            this.mActivity= mActivity as FragmentActivity?
            this.positionN=position
            this.noOfUserStoryCount=size
            this.mPager=mPager
            this.statusList1=statusList1
            this.photo=photo
            this.username=username
            this.otherUserid=otherUserid
            arguments = Bundle().apply {
                putInt(POSITION, position)
            }
        }
    }
}

