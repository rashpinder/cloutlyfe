package com.cloutlyfe.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.MediaStore.Images
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.HomeActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.TrimVideoActivity
import com.cloutlyfe.app.TrimVideoActivity.Companion.trimmedVideoUri
import com.cloutlyfe.app.activities.*
import com.cloutlyfe.app.adapters.CreatePostAdapter
import com.cloutlyfe.app.databinding.FragmentCreatePostBinding
import com.cloutlyfe.app.interfaces.OptionsInterface
import com.cloutlyfe.app.model.StatusModel
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.DecodeImage
import com.cloutlyfe.app.utils.FileHelper
import com.facebook.FacebookSdk.getApplicationContext

import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.khostul.app.utils.HandleAndroidPermissions
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.URISyntaxException
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created By Rashpinder
 */
class CreatePostFragment : BaseFragment() {
    var mArrayList: ArrayList<Uri> = ArrayList<Uri>()
    var mBitmap: Bitmap? = null
    var binding: FragmentCreatePostBinding? =null
    private val SELECT_IMAGE = 1
    private val CAPTURE_VIDEO = 2
    val REQUEST_CODE = 100
    var videoThumbnail_bitmap: Bitmap? = null
    var videoFullPath: String? = null
    var createPostAdapter: CreatePostAdapter? = null

    var AUTOCOMPLETE_REQUEST_CODE = 5

    var count:Int = 10
    var mLatitude: String = ""
    var mLongitude: String = ""
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
        )
    }

    companion object{
        var lati=""
        var longitu=""
        var addressLocation=""
    }
    var latitude = 0.0
    var longitude = 0.0
    private var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext=context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCreatePostBinding.inflate(inflater, container, false)
        binding!!.header.txtHeadingTV.text = getString(R.string.create_post)
        binding!!.txtPostTV.isEnabled=true
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                requireActivity(),
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(requireActivity(), PERMISSIONS, PERMISSION_ALL)
        }
        binding!!.addPostRL.setOnClickListener {
            performAddPostlick()
        }


        binding!!.editLocationET.setText("")
        binding!!.txtPostTV.setOnClickListener {
            executeAddNewPostRequest()
        }
        binding!!.locationLL.setOnClickListener {
//            showProgressDialog(activity)
            performLocationClick()
        }
//        initializePlaceSdKLocation()
        return binding!!.root
    }


    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE



    var mOptionsClickListener: OptionsInterface = object : OptionsInterface {
        override fun onItemClickListner(mPosition: Int, postId: String) {
            performRemoveClick(mPosition)
        }
    }


    private var mLastClickCity: Long = 0
    private fun performLocationClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickCity < 1500) {
            return
        }
        mLastClickCity = SystemClock.elapsedRealtime()
        val mIntent = Intent(activity, SearchLocationActivity::class.java)
        startActivityForResult(mIntent, 567)
    }


    private fun performRemoveClick(mPosition: Int) {
        if (mArrayList.size > 0) {
            mArrayList.removeAt(mPosition)}
        if(imageList.size >0){
            imageList.removeAt(mPosition)}
        if(uriList.size >0){
            uriList.removeAt(mPosition)}
        if(count!=10){
            count++}
        createPostAdapter?.notifyDataSetChanged()
    }


    override fun onResume() {
        super.onResume()
        if (addressLocation!=""){
            binding!!.editLocationET.setText(addressLocation)
            mLatitude= lati
            mLongitude= longitu
        }

//      showToast(activity,trimmedVideoUri.toString())

        if(trimmedVideoUri.toString()!="null" && trimmedVideoUri.toString()!="" && trimmedVideoUri.toString()!=null){
        if (uriList != null && uriList.size > 0) {
            if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
            ) {
                showToast(activity, "You can add a single video only")
            } else {
                showToast(activity, "You can add images only")
            }
        } else {
            try {
//                val uri = data!!.getStringExtra("TRIMMED_VIDEO_URI")
//                    saveVideo(uri!!.toUri())
                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(trimmedVideoUri.toString())
                val time =
                    retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                val VideoDuration = time!!.toInt()
                Log.e(TAG, "onActivityResult: " + VideoDuration)

//                    if (VideoDuration >= 60000) {
////                        startTrimActivity(uri)
////                        showToast(activity, "You can upload video upto 1 minute")
//                    } else {
                videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                    trimmedVideoUri!!.toString(),
                    MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                )
                mArrayList.add(Uri.fromFile(File(trimmedVideoUri!!.path.toString())))
//                    mArrayList.add(addVideo(File(uri!!.toUri().path))!!)
                imageList.add(videoThumbnail_bitmap!!)
                setAdapter(mArrayList, "image")
                uriList.add(Uri.fromFile(File(trimmedVideoUri!!.path.toString())))
//                    uriList.add(addVideo(File(uri!!.toUri().path))!!)
//                    }
//            manageVideo(path) //Do whatever you want with your vid9eo
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }}

    }

    override fun onDestroy() {
        super.onDestroy()
        lati=""
        longitu=""
        addressLocation=""
        binding?.editLocationET?.setText("")
        trimmedVideoUri=null
    }

    override fun onStop() {
        super.onStop()
        lati=""
        longitu=""
        addressLocation=""
        binding?.editLocationET?.setText("")
        trimmedVideoUri=null
    }

    /*
    * Initailze places location
    *
    * */
    fun initializePlaceSdKLocation() {
        // Initialize the SDK
        activity?.let { Places.initialize(it, getString(R.string.places_api_key)) }

        // Create a new Places client instance
//        val placesClient: PlacesClient = activity?.let { Places.createClient(it) }
    }

    private fun searchItem() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields: List<Place.Field> = Arrays.asList<Place.Field>(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )
        // Start the autocomplete intent.
        val intent: Intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        )
            .build(requireActivity())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }


    private fun performAddPostlick() {

        if (mArrayList.size == 10) {
            showToast(activity, getString(R.string.you_can_add_max_of_10image))
        }
        if (uriList!=null && uriList.size>0){
            if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")) {
                showToast(activity, "You can add a single video only")
            }
            else{
                if (mArrayList.size == 10) {
                    showToast(activity, getString(R.string.you_can_add_max_of_10image))
                }
                else{
                    performAddClick()}
            }
        }
        else {
            performAddClick()
        }
    }

    fun performAddClick() {
                Pick()
    }

    fun Pick() {
        val alertDialog = Dialog(requireActivity())
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtCameraTV = alertDialog.findViewById<TextView>(R.id.txtCameraTV)
        val txtCaptureVideoTV = alertDialog.findViewById<TextView>(R.id.txtCaptureVideoTV)
        val txtGalleryTV = alertDialog.findViewById<TextView>(R.id.txtGalleryTV)
        val txtPickVideoTV = alertDialog.findViewById<TextView>(R.id.txtPickVideoTV)
        val btnCancelTV = alertDialog.findViewById<TextView>(R.id.btnCancelTV)
        val videoView = alertDialog.findViewById<View>(R.id.videoView)
        val pickvideoView = alertDialog.findViewById<View>(R.id.pickvideoView)

        btnCancelTV.setOnClickListener { alertDialog.dismiss() }
        if (uriList!=null && uriList.size>0){
            if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
            ) {
                txtCaptureVideoTV.visibility=View.GONE
                videoView.visibility=View.GONE
                pickvideoView.visibility=View.GONE
                txtPickVideoTV.visibility=View.GONE
            }

            if (uriList.toString().contains(".jpg") || uriList.toString().contains(".jpeg") || uriList.toString()
                    .contains(".png")|| uriList.toString()
                    .contains("image") || uriList.toString().contains(".gif") ||uriList.toString().contains(".JPG") || uriList.toString().contains(".JPEG") || uriList.toString()
                    .contains(".PNG")|| uriList.toString()
                    .contains("IMAGE") || uriList.toString().contains(".GIF")
            ) {
                txtCaptureVideoTV.visibility=View.GONE
                videoView.visibility=View.GONE
                pickvideoView.visibility=View.GONE
                txtPickVideoTV.visibility=View.GONE
            }
//                showToast(activity, "You can add a single video only")
        }


        txtCaptureVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performCaptureVideoClick()
        }
        txtCameraTV.setOnClickListener {
            alertDialog.dismiss()
            performCameraClick()
        }
        txtGalleryTV.setOnClickListener {
            alertDialog.dismiss()
            performGalleryClick()
        }
        txtPickVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performPickVideoClick()
        }
        alertDialog.show()
    }


    private fun performPickVideoClick() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        intent.type = "video/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select Video"),
            357
        )
    }

    private fun performCaptureVideoClick() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
//    takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,60)
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0) // Quality Low
//    takeVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 5491520L) // 5MB
        if (takeVideoIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, CAPTURE_VIDEO)
        }


    }

    private fun executeAddNewPostRequest() {
        if (isValidate()) {
            if (isNetworkAvailable(requireActivity())) {
                binding!!.txtPostTV.isEnabled=false
//                if(videoThumbnail_bitmap==null){
//                    showToast(activity,"There is something wrong with video uploading, Please try again.")
//               }
//                else{
                executeCreateNewPostRequest()
//                }
            } else {
                showToast(activity, getString(R.string.no_internet_connection))
            }
        }
    }

    @SuppressLint("NewApi")
    @Throws(URISyntaxException::class)
    fun getFilePath(uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                context?.applicationContext,
                uri
            )
        ) {
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                if ("image" == type) {
                    uri = Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(
                    split[1]
                )
            }
        }
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }
            val projection = arrayOf(
                Images.Media.DATA
            )
            var cursor: Cursor? = null
            try {
                cursor = context?.contentResolver
                    ?.query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    private fun executeCreateNewPostRequest() {
        var videoMultipartBody: MultipartBody.Part?=null
        val descriptionList: MutableList<MultipartBody.Part> = ArrayList<MultipartBody.Part>()
        val uriListString = ArrayList<String>()

        uriList.forEach {
            getFilePath(it)?.let { it1 -> uriListString.add(it1) }
        }

        uriListString.forEach {
            val selectedFile = File(it)
            val filePathBody: RequestBody =
                selectedFile.asRequestBody("image/*".toMediaTypeOrNull())

            descriptionList.add(
                MultipartBody.Part.createFormData(
                    "uploads[]",
                    System.currentTimeMillis().toString()+selectedFile.name,
                    filePathBody
                )
            )}

        val user_id: RequestBody =
            getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val description: RequestBody = binding!!.editCaptionET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val location: RequestBody = binding!!.editLocationET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val latitude: RequestBody = mLatitude
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val longitude: RequestBody = mLongitude
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        if(videoThumbnail_bitmap!=null){
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(videoThumbnail_bitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            videoMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "video_thumbnail",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }

        showProgressDialog(activity)

        val headers: HashMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitInstance.appApi.createPostRequest(
            headers,
            descriptionList,
            user_id,
            description,
            location,
            latitude,
            longitude,
            videoMultipartBody
        )

        call.enqueue(object : Callback<StatusModel> {
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                dismissProgressDialog()
                val mAddPostModel = response.body()
                if (mAddPostModel!!.status == 1) {
                    showFinishAlertDialog(activity, mAddPostModel.message)
                } else if (mAddPostModel.status == 0) {
                    binding!!.txtPostTV.isEnabled=true
                    showAlertDialog(activity, mAddPostModel.message)
                } else if (mAddPostModel.status == 3) {
                    binding!!.txtPostTV.isEnabled=true
                    showAlertDialog(activity, mAddPostModel.message)
                } else {
                    binding!!.txtPostTV.isEnabled=true
                    showAlertDialog(activity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

        })
    }


    private fun setAdapter(mgg: ArrayList<Uri>,value:String) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding!!.cloutersRV.layoutManager = layoutManager
        createPostAdapter = CreatePostAdapter(activity, mgg, mOptionsClickListener,videoThumbnail_bitmap, value)
        binding!!.cloutersRV.adapter = createPostAdapter
    }


    private fun performCameraClick() {
        ImagePicker.with(this) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)
            //Final image resolution will be less than 1080 x 1080(Optional)
            .cameraOnly().crop()
            .start(SELECT_IMAGE)
    }


    private fun performGalleryClick() {
        openGalleryForImage()
    }

    private fun openGalleryForImage() {
        val intent = Lassi(activity!!)
            .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA or CAMERA_AND_GALLERY
            .setMaxCount(count)
            .setGridSize(3)
            .setMediaType(MediaType.IMAGE)
            .build()
        receiveData.launch(intent)
        Log.e(TAG, "counttt: "+count )
        Log.e(TAG, "counttt: "+uriList.size )
    }

    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
                if (!selectedMedia .isNullOrEmpty()) {
                    Log.e("imagesLoadSize", selectedMedia.size.toString())
//                    count=count-selectedMedia.size
                    count=10-(uriList.size+selectedMedia.size)
                    Log.e(TAG, "counttt: "+count)

                    for (i in 0..selectedMedia.size-1) {
                        val uri: Uri  = Uri.fromFile(File(selectedMedia.get(i).path))
                        mArrayList.add(uri)
                        uriList.add(uri)
                    }

                    setAdapter(mArrayList,"")
//                    uriList.addAll(mArrayList)
                }
            }
        }


    private val receiveeData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
                if (!selectedMedia .isNullOrEmpty()) {
                    Log.e("imagesLoadSize", selectedMedia.size.toString())
//                    count=10-selectedMedia.size


                    for (i in 0..selectedMedia.size-1) {
                        val uri: Uri  = Uri.fromFile(File(selectedMedia.get(i).path))
                        val retriever = MediaMetadataRetriever()
                        retriever.setDataSource(selectedMedia.get(i).path)
                        val time =
                            retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                        val VideoDuration = time!!.toInt()
                        Log.e(TAG, "onActivityResult: "+VideoDuration)
                        if (VideoDuration>60090){
                            val selectedImageUri: Uri =uri

                            // MEDIA GALLERY
                            var selectedImagePath = getPathFromUri(requireActivity(), selectedImageUri)
                            Log.e("selectedImagePath", selectedImagePath.toString())
                            if (selectedImagePath != null) {
                                val intent = Intent(
                                    requireActivity(),
                                    TrimVideoActivity::class.java
                                )
                                intent.putExtra("EXTRA_VIDEO_PATH", selectedImagePath)
                                startActivity(intent)
                            }
//                            showToast(activity, "You can upload video upto 1 minute")
//                            startTrimActivity(uri)
                        }
                        else{
                            mArrayList.add(uri)
                            videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                                selectedMedia.get(i).path!!,
                                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                            )
                            imageList.add(videoThumbnail_bitmap!!)
                            setAdapter(mArrayList,"")
                            uriList.add(uri)
//                            uriList.addAll(mArrayList)
                        }
                    }
                }
            }
        }


    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (requireActivity().getContentResolver() != null)
        {
            val cursor: Cursor = requireActivity().getContentResolver().query(uri!!, null, null, null, null)!!
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }


    var bitmap:Bitmap?=null
    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var selectedMediaUri: Uri?=null
        if (requestCode == 357) {
            val selectedImageUri: Uri? = data?.data
            if (selectedImageUri != null) {
                val intent = Intent(activity, TrimVideoActivity::class.java)
                intent.putExtra("EXTRA_VIDEO_PATH", selectedImageUri)
                startActivity(intent)
            }
        }
        else if (requestCode == 800 && data!=null) {
            if (uriList != null && uriList.size > 0) {
                if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
                ) {
                    showToast(activity, "You can add a single video only")
                } else {
                    showToast(activity, "You can add images only")
                }
            } else {
                try {
                    val uri = data.getStringExtra("TRIMMED_VIDEO_URI")
//                    saveVideo(uri!!.toUri())
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(uri)
                    val time =
                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    val VideoDuration = time!!.toInt()
                    Log.e(TAG, "onActivityResult: " + VideoDuration)

//                    if (VideoDuration >= 60000) {
////                        startTrimActivity(uri)
////                        showToast(activity, "You can upload video upto 1 minute")
//                    } else {
                    videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                        uri!!,
                        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
                    mArrayList.add(Uri.fromFile(File(uri.toUri().path.toString())))
//                    mArrayList.add(addVideo(File(uri!!.toUri().path))!!)
                    imageList.add(videoThumbnail_bitmap!!)
                    setAdapter(mArrayList, "image")
                    uriList.add(Uri.fromFile(File(uri.toUri().path.toString())))
//                    uriList.add(addVideo(File(uri!!.toUri().path))!!)
//                    }
//            manageVideo(path) //Do whatever you want with your vid9eo
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        else if (resultCode == RESULT_OK && requestCode == CAPTURE_VIDEO) {

            try {
                selectedMediaUri = data!!.getData()!!
                videoFullPath = getRealPathFromURI(selectedMediaUri)

                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(videoFullPath)
                val time =
                    retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                val VideoDuration = time!!.toInt()
                Log.e(TAG, "onActivityResult: "+VideoDuration)

                if (VideoDuration>60090){
                    showToast(activity, "You can upload video upto 1 minute")
                }
                else{
                    videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                        videoFullPath!!,
                        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
                    mArrayList.add(selectedMediaUri!!)
                    imageList.add(videoThumbnail_bitmap!!)
                    setAdapter(mArrayList,"")
                    uriList.add(selectedMediaUri!!)
                }
//            manageVideo(path) //Do whatever you want with your video
            }
            catch (e: IOException) {
                e.printStackTrace()
            }
        }

        else if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            if (data != null) {
                if (uriList!=null && uriList.size>0){
                    if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
                    ) {
                        showToast(activity, "You can add a single video only")
                    }
                    else{
                        try {
                            selectedMediaUri = data?.data!!
                            val fileHelper = FileHelper(getApplicationContext())
                            val picturePath = fileHelper.getRealPathFromUri(selectedMediaUri)
//                        var picturePath = getRealPathFromURI(selectedMediaUri!!)
                            val bmp = BitmapFactory.decodeFile(DecodeImage.decodeFile(picturePath!!, 600, 600))
                            val bounds = BitmapFactory.Options()
                            bounds.inJustDecodeBounds = true
                            BitmapFactory.decodeFile(picturePath, bounds)
                            val opts = BitmapFactory.Options()
                            val bm = BitmapFactory.decodeFile(picturePath, opts)
                            val exif = ExifInterface(picturePath!!)
                            val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
                            val orientation =
                                if (orientString != null) Integer.parseInt(orientString) else ExifInterface.ORIENTATION_NORMAL
                            var rotationAngle = 0
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270
                            val matrix = Matrix()
                            matrix.setRotate(
                                rotationAngle.toFloat(),
                                bm.width.toFloat() / 2,
                                bm.height.toFloat() / 2
                            )

                            val rotatedBitmap = Bitmap.createBitmap(
                                bm,
                                0,
                                0,
                                bounds.outWidth,
                                bounds.outHeight,
                                matrix,
                                true
                            )

                            val bytes = ByteArrayOutputStream()
                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                            val path = Images.Media.insertImage(activity!!.contentResolver, rotatedBitmap, "Clout", null)
//                        selectedMediaUri = activity?.let { getImageUri(it, rotatedBitmap) }
                            selectedMediaUri =  Uri.parse(path)
                            uriList.add(selectedMediaUri!!)
                            mArrayList.add(selectedMediaUri!!)
                            setAdapter(mArrayList, "")
                            count=10-uriList.size
                            Log.e(TAG, "countt: "+count )
                            Log.e(TAG, "countt: "+uriList.size )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    } }
                else if (resultCode == RESULT_CANCELED) {
                    // Toast.makeText(activity, "Cancel", Toast.LENGTH_SHORT).show()
                }
                else{
                    try {
                        selectedMediaUri = data?.data!!
                        mBitmap= Images.Media.getBitmap(context?.getContentResolver(), selectedMediaUri)
                        val fileHelper = FileHelper(getApplicationContext())
                        val picturePath = fileHelper.getRealPathFromUri(selectedMediaUri)
//                        var picturePath = getRealPathFromURI(selectedMediaUri!!)
                        val bmp = BitmapFactory.decodeFile(DecodeImage.decodeFile(picturePath!!, 600, 600))
                        val bounds = BitmapFactory.Options()
                        bounds.inJustDecodeBounds = true
                        BitmapFactory.decodeFile(picturePath, bounds)
                        val opts = BitmapFactory.Options()
                        val bm = BitmapFactory.decodeFile(picturePath, opts)
                        val exif = ExifInterface(picturePath!!)
                        val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
                        val orientation =
                            if (orientString != null) Integer.parseInt(orientString) else ExifInterface.ORIENTATION_NORMAL
                        var rotationAngle = 0
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270
                        val matrix = Matrix()
                        matrix.setRotate(
                            rotationAngle.toFloat(),
                            bm.width.toFloat() / 2,
                            bm.height.toFloat() / 2
                        )

                        val rotatedBitmap = Bitmap.createBitmap(
                            bm,
                            0,
                            0,
                            bounds.outWidth,
                            bounds.outHeight,
                            matrix,
                            true
                        )



                        val bytes = ByteArrayOutputStream()
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                        val path = Images.Media.insertImage(activity?.contentResolver, rotatedBitmap, "Clout", "Clout")
//                        selectedMediaUri = activity?.let { getImageUri(it, rotatedBitmap) }
                        selectedMediaUri =  Uri.parse(path)

                        uriList.add(selectedMediaUri!!)
                        mArrayList.add(selectedMediaUri!!)
                        setAdapter(mArrayList, "")
                        count=10-uriList.size

                        Log.e(TAG, "countt: "+count )
                        Log.e(TAG, "countt: "+uriList.size )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

            }
        }
//        if (requestCode == 800) {
//            if (uriList!=null && uriList.size>0){
//                if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
//                ) {
//                    showToast(activity, "You can add a single video only")
//                }
//            else{
//                    showToast(activity, "You can add images only")
//                }
//            }
//            else{
//                try {
////                    selectedMediaUri = data!!.getData()!!
//                    val uri = data!!.getStringExtra(Constants.TRIMMED_VIDEO_URI)
////                    selectedMediaUri = Uri.parse(data!!.getStringExtra(Constants.TRIMMED_VIDEO_URI))
////                    var path = ""
////                    if (requireActivity().getContentResolver() != null)
////                    {
////                        val cursor: Cursor = requireActivity().getContentResolver().query(Uri.parse(uri), null, null, null, null)!!
////                        if (cursor != null) {
////                            cursor.moveToFirst()
////                            val idx: Int = cursor.getColumnIndex(Images.ImageColumns.DATA)
////                            videoFullPath = cursor.getString(idx)
////                            cursor.close()
////                        }
////                    }
////                     = getRealPathFromURI(selectedMediaUri)
//
//                    val retriever = MediaMetadataRetriever()
//                    retriever.setDataSource(uri)
//                    val time =
//                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
//                    val VideoDuration = time!!.toInt()
//                    Log.e(TAG, "onActivityResult: "+VideoDuration)
//
//                    if (VideoDuration>60090){
////                        startTrimActivity(uri)
//                        showToast(activity, "You can upload video upto 1 minute")
//                    }
//                    else{
//                        videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
//                            uri!!,
//                            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
//                        )
//                        mArrayList.add(Uri.parse(uri!!))
//                        imageList.add(videoThumbnail_bitmap!!)
//                        setAdapter(mArrayList, "image")
//                        uriList.add(Uri.parse(uri!!))
//                    }
////            manageVideo(path) //Do whatever you want with your video
//                }
//                catch (e: IOException) {
//                    e.printStackTrace()
//                }
//            }
//          }

    }

    fun getPathFromUri(context: Context, uri: Uri): String? {
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    fun getDataColumn(
        context: Context, uri: Uri?, selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.getContentResolver().query(
                uri!!, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    var uriList = ArrayList<Uri>()
    var imageList = ArrayList<Bitmap>()

    // - - To Show Alert Dialog
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            binding!!.txtPostTV.isEnabled=true
            alertDialog.dismiss()
            val intent = Intent(activity, HomeActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        alertDialog.show()
    }

    private fun isValidate(): Boolean {
        var flag = true
        when {
            (mArrayList.size <= 0) -> {
                showAlertDialog(activity, getString(R.string.add_image))
                flag = false
            }
//            binding.editLocationET.text.toString().trim { it <= ' ' } == "" -> {
//                showAlertDialog(activity, getString(R.string.add_locationn))
//                flag = false
//            }

//            binding.editCaptionET.text.toString().trim { it <= ' ' } == "" -> {
//                showAlertDialog(activity, getString(R.string.write_a_captionn))
//                flag = false
//            }
        }
        return flag
    }

//    private fun startTrimActivity(uri: Uri) {
//        val intent = Intent(activity, VideoTrimActivity::class.java)
//        intent.putExtra(Constants.EXTRA_VIDEO_PATH, FileUtils().getRealPathFromURI_API19(activity, uri))
//        startActivityForResult(intent, 800)
//    }

}
