package com.cloutlyfe.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.Window.FEATURE_NO_TITLE
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.App
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.LoginActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.HomeSearchActivity
import com.cloutlyfe.app.activities.NotificationsActivity
import com.cloutlyfe.app.adapters.ReportAdapter
import com.cloutlyfe.app.adapters.ShareBottomSheetAdapter
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.*
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.ui.adapter.FeedAdapter
import com.cloutlyfe.app.ui.view.CenterLayoutManager
import com.cloutlyfe.app.ui.view.VideoViews
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Constants.Companion.ID
import com.cloutlyfe.app.utils.Constants.Companion.SOUND
import com.cloutlyfe.app.utils.Constants.Companion.SOUND_VALUE
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.wonderkiln.camerakit.*
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_add_story.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * Created By Rashpinder
 */
class HomeFragment : BaseFragment() {
    var binding: com.cloutlyfe.app.databinding.FragmentHomeBinding? = null
    val layoutManager = LinearLayoutManager(context)
    val list: ArrayList<String> = ArrayList()
    val reportResonslist: ArrayList<DataX> = ArrayList()
    private var adapter: FeedAdapter? = null
    private var mReportAdapter: ReportAdapter? = null
    private var mAdapter: ShareBottomSheetAdapter? = null
    lateinit var homeViewModel: HomeViewModel
    lateinit var reportViewModel: ReportViewModel
    lateinit var likeUnlikeViewModel: LikeUnlikeViewModel
    lateinit var getReportReasonViewModel: GetReportReasonsViewModel
    lateinit var saveUnsaveViewModel: SaveUnsaveViewModel
    var mHomeDataModel: HomeModel?=null
    var viewtype: String? = null
    var reasonId: String? = null
    var mPostId: String? = null
    var mApiStatus: String? = "1"
    var mCurrentDate: String? = ""

    //    var mCurrentdate: String? = ""
    var mDelPos: Int? = 0
    var mPos: Int? = 0
    var mPositionn: Int? = 0
    var vvInfoo: VideoViews? = null
    var imgVol: ImageView? = null
    var mOtheruserId: String? = null
    var mSharePostId: String? = null
    var mShareOtheruserId: String? = null
    private var mSocket: Socket? = null
    var mRoomId = ""
    val userslist: ArrayList<RoomId> = ArrayList()

    var emptyObject: HomeData? = null

    companion object {
        var imageList: ArrayList<HomeData> = ArrayList()
        var newImageList: ArrayList<HomeData> = ArrayList()
        var emptyObjectList: ArrayList<HomeData> = ArrayList()

    }
    val REQUEST_CODE = 100
    var mShareList: ArrayList<String> = ArrayList()
    var mPageNo: Int = 1
    var mPerPage: Int = 6
    var isLoading: Boolean = true
    var firstElementPosition: Int = 0
    var firstElementPositionn: Int = 0
    lateinit var mCloutsDataModel: FollowingUsersModel
    var cloutsList: java.util.ArrayList<DataXXX> = java.util.ArrayList()
    var myList: java.util.ArrayList<DataXXX> = java.util.ArrayList()


    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Permission is granted. Continue the action or workflow in your
            // app.
        } else {
            // Explain to the user that the feature is unavailable because the
            // features requires a permission that the user has denied. At the
            // same time, respect the user's decision. Don't link to system
            // settings in an effort to convince the user to change their
            // decision.
        }
    }


    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            com.cloutlyfe.app.databinding.FragmentHomeBinding.inflate(inflater, container, false)
//        check_permissions()
        val tsLong = System.currentTimeMillis() / 1000
        mCurrentDate = tsLong.toString()
        if (imageList != null) {
            imageList.clear()
        }
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        init()
        setUpSocketData()


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requestPermissionLauncher.launch(
                Manifest.permission.POST_NOTIFICATIONS
            )
        }


        if (mApiStatus != "0") {
            executeHomeApi()
        } else {
            binding!!.mProgressRL.visibility = View.GONE
        }

        setTopSwipeRefresh()


        binding!!.homeListRV.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        //Dragging
                    } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    }
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val lManager = binding!!.homeListRV.layoutManager as LinearLayoutManager?
                    firstElementPositionn = lManager!!.findFirstCompletelyVisibleItemPosition()
                    Log.e(TAG, "onScrollStateChanged: " + firstElementPositionn)

                    Log.e("Position", "sizee: " + imageList.size)
                    Log.e("Position", "sizee: " + firstElementPosition)

//                    if(mApiStatus=="2" && imageList.size-1==firstElementPosition){
//                       onScroll("2","mTime",true)
//                        Log.e("Position", "BindViewHoldwer: " + mApiStatus)
//                    }
//
//                    else if(mApiStatus=="3" && imageList.size-1==firstElementPosition){
//                        onScroll("3","mTime",true)
//                        Log.e("Position", "BindViewHoldwer: " + mApiStatus)
//                    }
//                     if(imageList.size - 1 == firstElementPosition && mApiStatus=="2"){
//                        executeMoreHomeDataRequest()
//                    }
//
//                    else if(imageList.size - 1 == firstElementPosition && mApiStatus=="3"){
//                         executeMoreHomeDataRequest()
//                    }
//                    if (imageList.size>0 && imageList[0].thumbnail_image!!.size>0){

                    if (imageList.size > 0) {
                        //here we find the current item number
                        val lManager = binding!!.homeListRV.layoutManager as LinearLayoutManager?
                        firstElementPosition = lManager!!.findFirstCompletelyVisibleItemPosition()
                        Log.e(TAG, "onScrollStateChanged: " + firstElementPosition)
                        if (firstElementPosition == -1) {
                            firstElementPosition = 0
                        }
                        if (!imageList[0].thumbnail_image.isNullOrEmpty()) {
                            if (imageList[0].thumbnail_image!!.size > 0) {
                                if (SOUND) {
                                    Log.e(TAG, "onScrolled: " + "unmute")
//                        showToast(activity,"true")
                                    if (firstElementPosition >= 0) {
                                        imageList[firstElementPosition].vol = true
                                        (binding!!.homeListRV.layoutManager?.findViewByPosition(
                                            firstElementPosition
                                        ))?.findViewById<com.cloutlyfe.app.ui.view.VideoViews>(
                                            R.id.vvInfo
                                        )?.unmute()
                                        (binding!!.homeListRV.layoutManager?.findViewByPosition(
                                            firstElementPosition
                                        ))?.findViewById<ImageView>(
                                            R.id.imgVolumeIV
                                        )
                                            ?.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_on))
//                            val value=(binding!!.homeListRV.layoutManager?.findViewByPosition(
//                                firstElementPosition
//                            ))?.findViewById<com.cloutlyfe.app.ui.view.VideoView>(
//                                R.id.vvInfo)?.setOnInfoListerner()
//                            Log.e(TAG, "onScrolled: ", value.toString())
                                    }
                                } else {
                                    Log.e(TAG, "onScrolled: " + "mute")
                                    if (firstElementPosition >= 0) {
                                        imageList[firstElementPosition].vol = false
                                        (binding!!.homeListRV.layoutManager?.findViewByPosition(
                                            firstElementPosition
                                        ))?.findViewById<com.cloutlyfe.app.ui.view.VideoViews>(
                                            R.id.vvInfo
                                        )?.mute()
                                        (binding!!.homeListRV.layoutManager?.findViewByPosition(
                                            firstElementPosition
                                        ))?.findViewById<ImageView>(
                                            R.id.imgVolumeIV
                                        )
                                            ?.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_mute))
//                            showToast(activity,"false")
//                            val valuee=(binding!!.homeListRV.layoutManager?.findViewByPosition(
//                                firstElementPosition
//                            ))?.findViewById<com.cloutlyfe.app.ui.view.VideoView>(
//                                R.id.vvInfo)?.setOnInfoListerner().toString()
//                            Log.e(TAG, "onScrolled: ", valuee)
                                    }
                                }

                            }
                        }
                    }
                }
            })

        return binding!!.root
    }



    fun check_permissions(): Boolean {
        var PERMISSIONS = arrayOfNulls<String>(0)
        PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arrayOf(
                Manifest.permission.READ_MEDIA_IMAGES,
                Manifest.permission.CAMERA
            )
        } else {
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
            )
        }
        if (!hasPermissions(requireActivity(), PERMISSIONS.toString())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, REQUEST_CODE)
            }
        } else if (hasPermissions(requireActivity(), PERMISSIONS.toString())) {


        } else {
            return true
        }
        return false
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE -> {
                for (result in grantResults) {
                    if (result == PackageManager.PERMISSION_DENIED) {
                        Toast.makeText(
                            requireActivity(),
                            R.string.permission_warning,
                            Toast.LENGTH_LONG
                        ).show()
                        return
                    }
                    else if (check_permissions()) {

                    } else {
                        showToast(
                            requireActivity(),
                            "Please allow required permissions from settings"
                        )
                        Log.e(TAG, "**Permission Denied**")
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    fun hasPermissions(context: Context?, vararg permissions: String?): Boolean {
        if (context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission!!
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }
    fun getCurrentDate(): String {
        val tsLong = System.currentTimeMillis() / 1000
        return tsLong.toString()
    }

    var mIsPagination = true

    private fun setTopSwipeRefresh() {
//        imageList.clear()
        binding!!.mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.black))
        binding!!.mSwipeRefreshSR.setOnRefreshListener {
            if(binding!!.mProgressRL.visibility == View.GONE){
            mPageNo = 1
            mApiStatus = "1"
            val tsLong = System.currentTimeMillis() / 1000
            mCurrentDate = tsLong.toString()
            Log.e(TAG, "setTopSwipeRefresh: " + mCurrentDate)
            executeHomeApi()}
            else{
                binding!!.mSwipeRefreshSR.isRefreshing = false
            }
        }
    }
    // put below code (method) in Adapter class
//    fun filter(charText: String) {
//        var charText = charText
//        charText = charText.toLowerCase(Locale.getDefault())
//        myList.clear()
//        if (charText.length == 0) {
//            myList.addAll(cloutsList)
//        } else {
//            for (wp in cloutsList) {
//                if (wp.name.toLowerCase(Locale.getDefault()).contains(charText)) {
//                    myList.add(wp)
//                }
//            }
//        }
//        mAdapter!!.notifyDataSetChanged()
//    }

    fun filter(text: String?) {
        val temp: MutableList<DataXXX> = ArrayList()
        for (d in cloutsList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.name.lowercase(Locale.getDefault()).contains(text.toString()) || d.username.lowercase(Locale.getDefault()).contains(text.toString())) {
                temp.add(d)
            }
        }
        //update recyclerview
        mAdapter!!.updateList(temp)
    }


    private fun init() {
        val repository = AppRepository()
        val factory = requireActivity()?.let { ViewModelProviderFactory(it.application, repository) }
        homeViewModel =
            factory?.let { ViewModelProvider(this, it).get(HomeViewModel::class.java) }!!
        getReportReasonViewModel =
            factory.let {
                ViewModelProvider(
                    this,
                    it
                ).get(GetReportReasonsViewModel::class.java)
            }
        reportViewModel =
            factory.let { ViewModelProvider(this, it).get(ReportViewModel::class.java) }
        likeUnlikeViewModel =
            factory.let { ViewModelProvider(this, it).get(LikeUnlikeViewModel::class.java) }
        saveUnsaveViewModel =
            factory.let { ViewModelProvider(this, it).get(SaveUnsaveViewModel::class.java) }
    }


    private fun setUpSocketData() {
        val app: App = requireActivity().application as App
        mSocket = app.getSocket()
//        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
//        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
//        mSocket!!.on("newMessage", onNewMessage)
//        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()

    }

    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {

    }
    private val onDisconnect = Emitter.Listener {

    }

    private val onConnectError = Emitter.Listener {

    }


    override fun onResume() {
        super.onResume()
        SOUND_VALUE = "home"
        SOUND = true
//        mPageNo = 1
//        if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder.playVideo()
        executeHomeeApi()
        if (Constants.LAST_POSITION != -1) {
            Log.e(TAG, "onResume: "+ "playing")
            if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder.playVideo()
            binding?.homeListRV?.adapter?.notifyDataSetChanged()
            initView()
            Constants.LAST_POSITION = -1
        }
        else{
            if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder.playVideo()
        }
    }

    private fun executeHomeApi() {
        if(activity!=null && isAdded){
        if (isNetworkAvailable(requireActivity()))
            RequestHomeData()
        else
            showToast(requireActivity(), getString(R.string.no_internet_connection))
    }}


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        mMap["last_post_id"] = ""
        mMap["api_status"] = mApiStatus
        mMap["current_date"] = mCurrentDate
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestHomeData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        Log.e(TAG, "Token: " + getAuthToken())
        mHeaders["Token"] = getAuthToken()
        if (!binding!!.mSwipeRefreshSR.isRefreshing) {
            showProgressDialog(requireActivity())
        }
        val call = RetrofitInstance.appApi.homeRequestCaughtUp(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                imageList.clear()
                newImageList.clear()
                list.clear()
                mHomeDataModel = response.body()
                if (mHomeDataModel!!.status == 1) {
                    dismissProgressDialog()
                    binding!!.mSwipeRefreshSR.isRefreshing = false

                    if (mApiStatus == "1" && mHomeDataModel!!.lastPage == "FALSE") {
                        imageList.addAll(response.body()!!.data)
                    }


                    else if (mApiStatus == "1" && mHomeDataModel!!.lastPage == "TRUE") {
                        if (mHomeDataModel!!.data.size > 0) {
                            imageList.addAll(response.body()!!.data)
                            newImageList.addAll(response.body()!!.data)
                            val list: MutableList<HomeData> = newImageList
                            for(i in newImageList.indices){
                                val itr = list.iterator()
                                while (itr.hasNext())
                                {
                                    val curr = itr.next()

                                    if (curr.user_id == getLoggedInUserID()) {        // remove odd values
                                        itr.remove()
                                    }
                                }

                                println(list)
                            }
                            if(!list.isNullOrEmpty()){
//                                Log.e(TAG, "new_list: "+ list.size)
                                var emptyObject = HomeData(
                                    "",
                                    "",
                                    "",
                                    "",
                                    null,
                                    0,
                                    0,
                                    "",
                                    "",
                                    null,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    null,
                                    "",
                                    "",
                                    "",
                                    false,
                                    0,
                                    0,
                                    0,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    0,
                                    "","","","",""
                                )
                                imageList.add(emptyObject)
//                                Log.e(TAG, "size: " + imageList.size)
                            }}
                        mApiStatus = "2"
                        mPageNo = 1
                    }

                    else if (mApiStatus == "2" && mHomeDataModel!!.lastPage == "FALSE") {
                        mHomeDataModel!!.data.let {
                            imageList.addAll<HomeData>(
                                it
                            )
                        }
                        mApiStatus = "2"
                    }

                    else if (mApiStatus == "2" && mHomeDataModel!!.lastPage == "TRUE") {
                        if (mHomeDataModel!!.data.size > 0) {
                            imageList.addAll(mHomeDataModel!!.data)
//                            Log.e(TAG, "size: " + imageList.size)
                        }
                        mApiStatus = "3"
                        mPageNo = 1
                    }



                    else if (mApiStatus == "3" && mHomeDataModel!!.lastPage == "FALSE") {
                        mHomeDataModel!!.data.let {
                            imageList.addAll<HomeData>(
                                it
                            )
                        }
                        mApiStatus = "3"
                    }


                    else if (mApiStatus == "3" && mHomeDataModel!!.lastPage == "TRUE") {
                        if (mHomeDataModel!!.data.size > 0) {
                            imageList.addAll(mHomeDataModel!!.data)
//                            Log.e(TAG, "size: " + imageList.size)
                        }
                        mApiStatus = "0"
                        mPageNo = 1
                    }

                    if (response.body()!!.notificationCount > 0) {
                        if(isAdded && requireActivity()!=null){
                        binding!!.imgNotificationIV.setImageDrawable(
                            requireActivity()?.resources?.getDrawable(
                                R.drawable.ic_notifiaction_badge
                            )
                        )
                    } }else {
                        if(isAdded && requireActivity()!=null){
                        binding!!.imgNotificationIV.setImageDrawable(
                            requireActivity()?.resources?.getDrawable(
                                R.drawable.ic_notifiaction
                            )
                        )}
                    }

                    initView()

                    if (imageList.size == 0) {
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                    } else {
                        binding!!.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mHomeDataModel!!.status == 3) {
                    showAccountDisableAlertDialog(requireActivity(), mHomeDataModel!!.message)
                } else if (mHomeDataModel!!.status == 2) {
                    showAccountDisableAlertDialog(requireActivity(), mHomeDataModel!!.message)
                }
                else if (mHomeDataModel!!.status == 0) {
                    if (mApiStatus == "1" && mHomeDataModel!!.data.isNullOrEmpty()) {
                        mApiStatus = "2"
                        mPageNo = 1
                        val tsLong = System.currentTimeMillis() / 1000
                        mCurrentDate = tsLong.toString()
                        if (mApiStatus != "0") {
                            executeHomeApi()
                        } else {
                            binding!!.mProgressRL.visibility = View.GONE
                        }
                    }
                    else if (mApiStatus == "2" && mHomeDataModel!!.data.isNullOrEmpty()) {
                        mApiStatus = "3"
                        mPageNo = 1
                        val tsLong = System.currentTimeMillis() / 1000
                        mCurrentDate = tsLong.toString()

//                        val calendar = Calendar.getInstance()
//                        calendar.time = myDate
//                        calendar.add(Calendar.ZONE_OFFSET, -3)
//                        val newDate = calendar.time
//                        val offset: Int = TimeZone.getDefault().rawOffset + TimeZone.getDefault().dstSavings
//                        mCurrentDate= (newDate.time - offset).toString()
//                        mCurrentDate = newDate.time.toString()
                        Log.e(TAG, "setTopSwipeRefresh: " + mCurrentDate)
                        if (mApiStatus != "0") {
                            executeHomeApi()
                        } else {
                            binding!!.mProgressRL.visibility = View.GONE
                        }
                    } else if (mApiStatus == "3" && mHomeDataModel!!.data.isNullOrEmpty()) {
                        dismissProgressDialog()
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)
                    } else {
                        dismissProgressDialog()
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)
                    }
                } else {
                    dismissProgressDialog()
                    binding!!.txtNoDataTV.visibility = View.VISIBLE
                    binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)
                }
            }
        })
    }


    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: ArrayList<String>) {
            if (isLoading) {
                Log.e(TAG, "isLoadingBoolean: " + isLoading)
                ++mPageNo
                binding!!.mProgressRL.visibility = View.VISIBLE
                Handler().postDelayed({
                    if (mApiStatus != "0") {
                        executeMoreHomeDataRequest()
                    } else {
                        binding!!.mProgressRL.visibility = View.GONE
                    }
                }, 1500)

            }
        }
    }

    var mHomeApiCAllListner: CallHomeApiInterface = object : CallHomeApiInterface {
        override fun onScroll(mStatus: String, mTime: String, isLoad: Boolean) {
            mApiStatus = mStatus
            mCurrentDate = mTime
            isLoading = isLoad
            if (isLoading) {
                Log.e(TAG, "isLoadingggg: " + isLoading)
//                ++mPageNo
                binding!!.mProgressRL.visibility = View.VISIBLE
                Handler().postDelayed({
                    if (mApiStatus != "0") {
                        executeMoreHomeDataRequest()
                    } else {
                        binding!!.mProgressRL.visibility = View.GONE
                    }
                }, 1500)
            }
        }
    }

//    fun onScroll(mStatus: String, mTime: String, isLoad: Boolean) {
//        mApiStatus=mStatus
////            mCurrentDate=mTime
//        isLoading=isLoad
//        if (isLoading) {
//            Log.e(TAG, "isLoadingggg: " + isLoading)
//            ++mPageNo
//            binding!!.mProgressRL.visibility = View.VISIBLE
//            Handler().postDelayed({
//                executeMoreHomeDataRequest()
//            }, 1500)
//        }
//    }

    var mOnItemCheckListener: ShareBottomSheetAdapter.OnItemCheckListener = object:
        ShareBottomSheetAdapter.OnItemCheckListener {
        override fun onItemCheck(item: DataXXX?) {
            mShareList.add(item!!.user_id)
        }

        override fun onItemUncheck(item: DataXXX?) {
            mShareList.remove(item!!.user_id)
        }
    }


//    private fun mLoadMoreParam(): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["user_id"] = getLoggedInUserID()
//        mMap["page_no"] = mPageNo.toString()
//        mMap["par_page"] = mPerPage.toString()
//        mMap["last_post_id"] = ""
//        mMap["api_status"] = mApiStatus
//        mMap["current_date"] = mCurrentDate
//        Log.e(TAG, "**PARAM**$mMap")
//        return mMap
//    }


    private fun executeMoreHomeDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        if(activity!=null && isAdded){
        mHeaders["Token"] = getAuthToken()}

        val call = RetrofitInstance.appApi.homeRequestCaughtUp(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                dismissProgressDialog()
                binding!!.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                binding!!.mProgressRL.visibility = View.GONE
                val mGetSavedModel = response.body()
                if (mGetSavedModel!!.status == 1) {
//                    ++mPageNo
                    if (mApiStatus == "1" && mGetSavedModel.lastPage == "FALSE") {
                        mGetSavedModel.data.let {
                            imageList.addAll<HomeData>(
                                it
                            )
                        }
                        mApiStatus = "1"
                        isLoading = true
                    }
                    else if (mApiStatus == "1" && mGetSavedModel.lastPage == "TRUE") {
                        if (mGetSavedModel.data.size > 0) {
                            imageList.addAll(mGetSavedModel.data)
                            var emptyObject = HomeData(
                                "",
                                "",
                                "",
                                "",
                                null,
                                0,
                                0,
                                "",
                                "",
                                null,
                                "",
                                "",
                                "",
                                "",
                                "",
                                null,
                                "",
                                "",
                                "",
                                false,
                                0,
                                0,
                                0,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                0,
                                "","","","",""
                            )
                            imageList.add(emptyObject)
//                            Log.e(TAG, "size: " + imageList.size)

                        }
                        mApiStatus = "2"
                        mPageNo = 1
                        isLoading = true
                    } else if (mApiStatus == "2" && mGetSavedModel.lastPage == "FALSE") {
                        mGetSavedModel.data.let {
                            imageList.addAll<HomeData>(
                                it
                            )
                        }
                        mApiStatus = "2"
                        isLoading = true
                    } else if (mApiStatus == "2" && mGetSavedModel.lastPage == "TRUE") {
                        if (mGetSavedModel.data.size > 0) {
                            imageList.addAll(mGetSavedModel.data)
                            Log.e(TAG, "size: " + imageList.size)
                        }
                        mApiStatus = "3"
                        mPageNo = 1
                        isLoading =true
                    } else if (mApiStatus == "3" && mGetSavedModel.lastPage == "FALSE") {
                        mGetSavedModel.data.let {
                            imageList.addAll<HomeData>(
                                it
                            )
                        }
                        mApiStatus = "3"
                        isLoading =true
                    } else if (mApiStatus == "3" && mGetSavedModel.lastPage == "TRUE") {
                        if (mGetSavedModel.data.size > 0) {
                            imageList.addAll(mGetSavedModel.data)
                        }

                        mApiStatus = "0"
                        mPageNo = 1
                        isLoading = false
                    }

                    if (imageList.size == 0) {
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                    }
                    else {
                        binding!!.txtNoDataTV.visibility = View.GONE
                        adapter!!.setData(mApiStatus!!)
                        isLoading = true
                        ++mPageNo
//                        isLoading = mGetSavedModel.lastPage == "FALSE"
                    }

                    Log.e(TAG, "isLoading: " + isLoading)
                }

                else if (mGetSavedModel.status == 0) {
                    if (mApiStatus == "1" && mGetSavedModel.data.isNullOrEmpty()) {
                        mApiStatus = "2"
                        mPageNo = 1
                        val tsLong = System.currentTimeMillis() / 1000
                        mCurrentDate = tsLong.toString()
                        if (mApiStatus != "0") {
                            executeHomeApi()
                        } else {
                            binding!!.mProgressRL.visibility = View.GONE
                        }
                    }

                    else if (mApiStatus == "2" && mGetSavedModel.data.isNullOrEmpty()) {
                        mApiStatus = "3"
                        mPageNo = 1
                        val tsLong = System.currentTimeMillis() / 1000
                        mCurrentDate = tsLong.toString()

//                        val calendar = Calendar.getInstance()
//                        calendar.time = myDate
//                        calendar.add(Calendar.ZONE_OFFSET, -3)
//                        val newDate = calendar.time
//                        val offset: Int = TimeZone.getDefault().rawOffset + TimeZone.getDefault().dstSavings
//                        mCurrentDate= (newDate.time - offset).toString()
//                        mCurrentDate = newDate.time.toString()
                        Log.e(TAG, "setTopSwipeRefresh: " + mCurrentDate)
                        if (mApiStatus != "0") {
                            initView()
//                            executeHomeApi()
                        } else {
                            binding!!.mProgressRL.visibility = View.GONE
                        }
                    }
                    else if (mApiStatus == "3" && mGetSavedModel.data.isNullOrEmpty()) {
                        if(isAdded && requireActivity()!=null){
                        dismissProgressDialog()
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)}
                    } else {
                        if(isAdded && requireActivity()!=null){
                        dismissProgressDialog()
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)}
                    }
                }
            }
        })
    }

    fun showAccountDisableAlertDialog(mActivity: Activity?, mMessage: String) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.item_decline_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        txtMessageTV.text = mMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences =
                mActivity.let { AppPrefrences().getPreferences(it) }
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, LoginActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent)
                .startActivities()
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(mIntent)
            requireActivity().finish()
            requireActivity().finishAffinity()
        }
        alertDialog.show()
    }

//    private fun executeHomeApi() {
//        if (!activity?.let { isNetworkAvailable(it) }!!) {
//            showAlertDialog(activity, getString(R.string.no_internet_connection))
//        } else {
//            RequestHomeData()
//        }
//    }

//    private fun RequestHomeData() {
//        var user_id = getLoggedInUserID()
//        val body = RequestBodies.HomeBody(user_id)
//        activity?.let { homeViewModel.homeGetData(getAuthToken(), body, it) }
//        activity?.let {
//            homeViewModel.homeResponse.observe(it, Observer { event ->
//                event.getContentIfNotHandled()?.let { response ->
//                    when (response) {
//                        is Resource.Success -> {
//                            response.data?.let { homeResponse ->
//                                if (homeResponse.status == 1 || homeResponse.status == 200) {
//                                    dismissProgressDialog()
//                                    imageList.addAll(homeResponse.data)
//                                    Log.e(TAG, "RequestHomeDataAPI: " + imageList)
//                                    initView()
//                                    if (imageList.size == 0) {
//                                        binding!!.txtNoDataTV.visibility = View.VISIBLE
//                                        binding!!.txtNoDataTV.text =
//                                            getString(R.string.no_posts_available)
//                                    } else {
//                                        binding!!.txtNoDataTV.visibility = View.GONE
//                                    }
////                                    binding!!.txtNoDataTV.text=homeResponse.message
////                                    showToast(activity, homeResponse.message)
//                                } else {
//                                    dismissProgressDialog()
//                                    binding!!.txtNoDataTV.visibility = View.VISIBLE
//                                    binding!!.txtNoDataTV.text = homeResponse.message
////                                    showAlertDialog(activity, homeResponse.message)
//                                }
//                            }
//                        }
//
//                        is Resource.Error -> {
//                            dismissProgressDialog()
//                            response.message?.let { message ->
//                                showAlertDialog(activity, response.message)
//                            }
//                        }
//
//                        is Resource.Loading -> {
//                            showProgressDialog(activity)
//                        }
//                    }
//                }
//            })
//        }
//    }


    private fun executeLikeUnlikeApi(
        mPosition: Int,
        post_id: String,
        likeIV: ImageView,
        likeTV: TextView
    ) {
        if (!requireActivity()?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(requireActivity(), getString(R.string.no_internet_connection))
        } else {
            RequestLikeUnlikeData(mPosition, post_id, likeIV, likeTV)
        }
    }

    private fun RequestLikeUnlikeData(
        mPosition: Int,
        post_id: String,
        likeIV: ImageView,
        likeTV: TextView
    ) {
        var user_id = getLoggedInUserID()
        var post_id = post_id
        val body = RequestBodies.LikeUnlikeBody(user_id, post_id)
        likeUnlikeViewModel.likeUnlikeRequest(body)
        requireActivity()?.let {
            likeUnlikeViewModel._likeUnlikeResponse.observe(it, Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data?.let { likeUnlikeResponse ->
                                if (likeUnlikeResponse.status == 1 || likeUnlikeResponse.status == 200) {
//                                    increaseLikeCount(mPosition,likeIV,post_id,likeTV)
//                                    if (likeUnlikeResponse.isLike.equals("0")){
//                                        likeUnlikeResponse.isLike="0"
//                                        imageList[mPos!!].isLike=likeUnlikeResponse.likeCount
//                                        adapter?.notifyItemChanged(mPos!!)
//                                    }
//                                    else{
//                                        likeUnlikeResponse.isLike="1"
//                                        var mLike= likeUnlikeResponse.likeCount
//                                        imageList[mPos!!].isLike=mLike
//                                        adapter?.notifyItemChanged(mPos!!)
//                                    }

                                } else {
//                                    dismissProgressDialog()
                                }
                            }
                        }

                        is Resource.Error -> {
//                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(requireActivity(), response.message)
                            }
                        }

                        is Resource.Loading -> {
//                            showProgressDialog(activity)
                        }
                    }
                }
            })
        }
    }

    // this is the scroll listener of recycler view which will tell the current item number


    // this is the scroll listener of recycler view which will tell the current item number


    private fun increaseLikeCount(
        position: Int,
        imageView: ImageView,
        post_id: String,
        likeTextView: TextView
    ) {
        if (imageList[position].isLike == 0) {
            imageView.setImageResource(R.drawable.ic_fav)
            imageList[position].isLike = 1
            val count = ((imageList[position].likeCount.toInt()) + 1).toString()
            imageList[position].likeCount = count
            likeTextView.text = imageList[position].likeCount
//Like Api Execute
            executeLikeUnlikeApi(position, post_id, imageView, likeTextView)
        } else {
            imageView.setImageResource(R.drawable.ic_un_fav)
            imageList[position].isLike = 0
            val count = ((imageList[position].likeCount.toInt()) - 1).toString()
            imageList[position].likeCount = count
            likeTextView.text = imageList[position].likeCount
////Like Api Execute
            executeLikeUnlikeApi(position, post_id, imageView, likeTextView)
        }
    }


    private fun executeSaveUnsaveApi() {
        if (!requireActivity()?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(requireActivity(), getString(R.string.no_internet_connection))
        } else {
            RequestSaveUnsaveData()
        }
    }

    private fun RequestSaveUnsaveData() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        val body = RequestBodies.SaveUnsaveBody(user_id, post_id!!)
        saveUnsaveViewModel.saveUnsaveRequest(body)
        requireActivity()?.let {
            saveUnsaveViewModel._save_unsaveResponse.observe(it, Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data?.let { saveUnsaveResponse ->
                                if (saveUnsaveResponse.status == 1 || saveUnsaveResponse.status == 200) {
//                                    dismissProgressDialog()
//                                    showToast(activity, likeUnlikeResponse.message)
//                                    if (saveUnsaveResponse.isSave.equals("0")){
//                                        saveUnsaveResponse.isSave="0"
//                                        imageList[mPos!!].isSave=0
//                                        adapter?.notifyItemChanged(mPos!!)
//                                    }
//                                    else{
//                                        saveUnsaveResponse.isSave="1"
//                                        imageList[mPos!!].isSave=1
//                                        adapter?.notifyItemChanged(mPos!!)
//                                    }

                                } else {
//                                    dismissProgressDialog()
                                }
                            }
                        }

                        is Resource.Error -> {
//                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(requireActivity(), response.message)
                            }
                        }

                        is Resource.Loading -> {
//                            showProgressDialog(activity)
                        }
                    }
                }
            })
        }
    }


    private fun showReportDialog() {
        val view: View = LayoutInflater.from(context).inflate(R.layout.report_dilaog, null)

        var dialogPrivacy: BottomSheetDialog? =
            context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
//cancel button click
        val reportLL: LinearLayout? = dialogPrivacy.findViewById(R.id.reportLL)

        reportLL?.setOnClickListener {
            if (reportResonslist != null) {
                reportResonslist.clear()
            }
            getReasons()
            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }


    private fun showDeleteDialog() {
        val view: View = LayoutInflater.from(context).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? =
            context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility = View.GONE

        deleteRL?.setOnClickListener {
            executeDeleteApi()
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }


    private fun executeDeleteApi() {
        if (requireActivity()?.let { isNetworkAvailable(it) }!!)
            RequestDeleteData()
        else
            showToast(requireActivity(), getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(requireActivity())
        val call = RetrofitInstance.appApi.deletePostRequest(mHeaders, mDeleteParam())
        call.enqueue(object : Callback<CodeStatusModel> {
            override fun onFailure(call: Call<CodeStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CodeStatusModel>,
                response: Response<CodeStatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                if (response.body()!!.status == 1) {
                    showToast(requireActivity(), response.body()!!.message)
                    if (vvInfoo?.isEmpty() == false) {
                        vvInfoo!!.stop()
                    }
//                    vvInfoo?.stopPlayBack()
                    imageList.removeAt(mDelPos!!)
                    adapter!!.setData(mApiStatus!!)
//                    setTopSwipeRefresh()
//                    executeHomeApi()
                    adapter!!.notifyDataSetChanged()
//                    adapter!!.notifyItemChanged(mDelPos!!)
                    dismissProgressDialog()

                } else if (response.body()!!.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
                    dismissProgressDialog()
                }
            }
        })
    }


    var mItemClickListener: ReportItemClickInterface = object : ReportItemClickInterface {
        override fun onItemClickListner(mId: String) {
            reasonId = mId
            performSubmitClick()
        }
    }

    var mLikeClickListener: LikeunlikeInterface = object : LikeunlikeInterface {
        override fun onItemClickListner(
            mPosition: Int,
            post_id: String,
            likeIV: ImageView,
            likeTV: TextView
        ) {
            mPostId = post_id
            mPos = mPosition
            var mimg: ImageView = likeIV
            var mlikeTV: TextView = likeTV
            increaseLikeCount(mPos!!, mimg, mPostId!!, mlikeTV)
//            executeLikeUnlikeApi(mPos!!, mPostId!!,mimg,mlikeTV)
        }
    }

    var mSaveClickListener: SaveUnsaveInterface = object : SaveUnsaveInterface {
        override fun onItemClickListner(mPosition: Int, post_id: String, saveIV: ImageView) {
            mPostId = post_id
            mPos = mPosition
            var msaveImg: ImageView = saveIV
//            saveUnsaveCount(mPos!!,msaveImg, mPostId!!)
            if (imageList[mPos!!].isSave.equals(0)) {
                imageList[mPos!!].isSave = 1
                msaveImg.setImageResource(R.drawable.ic_save)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            } else {
                imageList[mPos!!].isSave = 0
                msaveImg.setImageResource(R.drawable.ic_unsave)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            }

        }

//        private fun saveUnsaveCount(mPos: Int, msaveImg: ImageView, mPostId: String) {
//
//        }
    }
    var dialogReport: BottomSheetDialog? = null
    private fun showBottomReportDialog() {
        val view: View = LayoutInflater.from(context).inflate(R.layout.report_bottomsheet, null)
        dialogReport =
            context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogReport!!.setContentView(view)
        dialogReport!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogReport!!.setCancelable(true)
//cancel button click
        val reportRV: RecyclerView? = dialogReport!!.findViewById(R.id.reportRV)

        reportRV!!.layoutManager = LinearLayoutManager(requireActivity())
        mReportAdapter = ReportAdapter(
            requireActivity(),
            reportResonslist, mItemClickListener
        )
        reportRV.adapter = mReportAdapter

//        imageList[mDelPos!!].is_report="1"
//        adapter!!.notifyItemChanged(mDelPos!!)


//        reportRV?.setOnClickListener {
//            dialogReport.dismiss()
//        }
        dialogReport!!.show()
    }

    var dialogsghare: BottomSheetDialog?=null
    private fun showShareDialog() {
        dismissProgressDialog()
        val view: View = LayoutInflater.from(context).inflate(R.layout.share_bottomsheet, null)
       dialogsghare =
            context.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }

        dialogsghare!!.setContentView(view)
        dialogsghare!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogsghare!!.setCancelable(true)
//cancel button click
        val bottomsheetRV: RecyclerView? = dialogsghare!!.findViewById(R.id.bottomsheetRV)
        val txtSendTV: TextView? = dialogsghare!!.findViewById(R.id.txtSendTV)
        val editSearchET: EditText? = dialogsghare!!.findViewById(R.id.editSearchET)

        editSearchET!!.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                filter(s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
//        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//        list.add("Grave Yard")
//        list.add("Danel Bros")
        mAdapter = ShareBottomSheetAdapter(context, cloutsList, mOnItemCheckListener)

        bottomsheetRV?.layoutManager = LinearLayoutManager(context)
        bottomsheetRV?.adapter = mAdapter

        txtSendTV?.setOnClickListener {
            performSendClick()
        }
        dialogsghare!!.show()
    }

    private fun isValidate(): Boolean {
        var flag = true
        when {
            (mShareList.size <= 0) -> {
                showAlertDialog(requireActivity(), getString(R.string.share_list))
                flag = false
            }
        }
        return flag
    }

    private fun performSendClick() {
//        userslist.clear()
        preventMultipleClick()
        if (isNetworkAvailable(requireActivity())) {
            if (isValidate()) {
                executeSendPostRequest()
            }
        } else {
            showToast(requireActivity(), getString(R.string.no_internet_connection))
        }
    }

    /*
    * Execute api param
    * */
    private fun mShareParam(): MutableMap<String?, String?> {
        val s = TextUtils.join(", ", mShareList)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["message"] = ""
        mMap["room_id"] = ""
        mMap["other_id"] = s
        mMap["post_id"] = mSharePostId
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendPostRequest() {
        val call = RetrofitInstance.appApi.sharePostRequest(mShareParam())
        call.enqueue(object : Callback<SendMessageModel> {
            override fun onFailure(call: Call<SendMessageModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<SendMessageModel>,
                response: Response<SendMessageModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel?.status == 1) {
                    dialogsghare!!.dismiss()
                    showAlertDialog(requireActivity(), mModel.message)
//                    userslist.addAll(mModel.room_ids)
                    Log.e(TAG, "onResponse: " + mModel.room_ids)
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    for (i in mModel.room_ids.indices) {
                        Log.e(TAG, "room_id: " + mModel.room_ids[i].room_id)
                        Log.e(TAG, "room_id: " + mOjectString)
                        mSocket!!.emit("newMessage", mModel.room_ids[i].room_id, mOjectString)
                    }
//                    var mIntent = Intent(activity, ChatActivity::class.java)
//                    mIntent.putExtra(Constants.USER_NAME,  mModel.data.username)
//                    mIntent.putExtra(Constants.ROOM_ID, mModel.data.room_id)
//                    startActivity(mIntent)
//                    mMsgArrayList!!.add(mModel.data)

                } else if (mModel?.status == 0) {
                    showToast(requireActivity(), mModel.message)
                }
            }
        })
    }

    private fun getReasons() {
        if (!isNetworkAvailable(requireActivity())) {
            showAlertDialog(requireActivity(), getString(R.string.no_internet_connection))
        } else {
            performResonsRequest()
        }
    }

    private fun performResonsRequest() {
        var user_id = getLoggedInUserID()
        val body = RequestBodies.ReportReasonsBody(
            user_id
        )
        getReportReasonViewModel.getReportResons(body)
        getReportReasonViewModel.reportReasonsResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { getReportResonse ->
                            if (getReportResonse.status == 1 || getReportResonse.status == 200) {
                                reportResonslist.addAll(getReportResonse.data)
                                showBottomReportDialog()
                            } else {
                                showAlertDialog(requireActivity(), getReportResonse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(requireActivity(), message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(requireActivity())
                    }
                }
            }
        })

    }

    private fun performSubmitClick() {
        if (!isNetworkAvailable(requireActivity())) {
            showAlertDialog(requireActivity(), getString(R.string.no_internet_connection))
        } else {
            performSubmit()
        }
    }

    private fun performSubmit() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        var reasonId = reasonId
        var reportedId = mOtheruserId
        var reportType = "1"
        var reportedBy = getLoggedInUserID()
        val body = RequestBodies.ReportBody(
            user_id, post_id.toString(), reasonId!!, reportedId!!, reportType, reportedBy
        )
        reportViewModel.addReportRequest(body)
        reportViewModel._reportResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { forgotResponse ->
                            if (forgotResponse.status == 1 || forgotResponse.status == 200) {
                                showAlerttDialog(requireActivity(), forgotResponse.message)
                                dialogReport!!.dismiss()
                                imageList[mDelPos!!].is_report="1"
//                    setTopSwipeRefresh()
//                    executeHomeApi()
                                adapter!!.notifyItemChanged(mDelPos!!)

                            } else {
                                dialogReport!!.dismiss()
                                showAlertDialog(requireActivity(), forgotResponse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(requireActivity(), message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(activity)
                    }
                }
            }
        })

    }


    // - - To Show Alert Dialog
    fun showAlerttDialog(mActivity: Activity?, strMessage: String?) {
        if (mActivity != null) {
            val alertDialog = Dialog(mActivity)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.dialog_alert)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set the custom dialog components - text, image and button
            val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
            val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
            txtMessageTV.text = strMessage
            btnDismiss.setOnClickListener {
//                executeHomeApi()
//                imageList.removeAt(mDelPos!!)
//                adapter!!.notifyItemChanged(mDelPos!!)
                alertDialog.dismiss()
            }
            alertDialog.show()
        }
    }


    private fun initView() {
        if(isAdded && requireActivity()!=null){
        binding!!.homeListRV.layoutManager = CenterLayoutManager(requireActivity())
        binding!!.homeListRV.smoothScrollBy(0, 1)
        binding!!.homeListRV.smoothScrollBy(0, -1)
        adapter =
            activity?.let {
                FeedAdapter(
                    it,
                    mShareClickListner,
                    mLikeClickListener,
                    mSaveClickListener,
                    mOptionsClickListener,
                    mVolumeClickListener,
                    imageList,
                    mLoadMoreScrollListner, mHomeApiCAllListner, mApiStatus, mCurrentDate
                )
            }
        binding!!.homeListRV.adapter = adapter
//        Handler().postDelayed({
        if (Constants.LAST_POSITION != -1) {
            binding!!.homeListRV.scrollToPosition(Constants.LAST_POSITION)
        }}
//        }, 500)
    }


    var mShareClickListner: ShareInterface = object : ShareInterface {
        override fun onItemClickListner(mPosition: Int, post_id: String, userId: String) {
            mSharePostId = post_id
            mShareOtheruserId = userId
            cloutsList.clear()
            mShareList.clear()
            executeGetCloutersApi()
        }
    }

    private fun executeGetCloutersApi() {
        if (isNetworkAvailable(requireActivity()))
            RequestCloutersData()
        else
            showToast(activity, getString(R.string.no_internet_connection))
    }


    private fun mFollowingParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["search"] = ""
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCloutersData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.getFollowRequest(mHeaders, mFollowingParam())
        call.enqueue(object : Callback<FollowingUsersModel> {
            override fun onFailure(call: Call<FollowingUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<FollowingUsersModel>,
                response: Response<FollowingUsersModel>
            ) {
                Log.e(TAG, response.body().toString())
                mCloutsDataModel = response.body()!!
                if (mCloutsDataModel.status == 1) {
//                    isLoading = !mCloutsDataModel.lastPage.equals(true)
//                    cloutsList.addAll(response.body()!!.data)

                    for (i in response.body()!!.data.indices) {
                        if (response.body()!!.data[i].is_delete != 1) {
                            cloutsList.add(response.body()!!.data[i])
                        }
                    }
                    Log.e(TAG, "RequestHomeDataAPI: " + cloutsList)
                    if (cloutsList.size == 0) {
                        dismissProgressDialog()
                        showToast(activity, "There are no users available to share post")
//                        binding!!.upArraowRL.visibility = View.GONE
//                        binding!!.txtNoDataTV.text =
//                            "No Clouters found."
//                        binding!!.txtNoDataTV.visibility = View.VISIBLE

                    } else {
                        showShareDialog()
//                        binding!!.upArraowRL.visibility = View.VISIBLE
//                        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//                        binding!!.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCloutsDataModel.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCloutsDataModel.message!!)
                } else {
                    dismissProgressDialog()
//                    binding!!.upArraowRL.visibility = View.GONE
//                    binding!!.txtNoDataTV.visibility = View.VISIBLE
//                    binding!!.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    var mVolumeClickListener: VolumeInterface =
        object : VolumeInterface {
            override fun onItemClickListner(
                mPosition: Int,
                imgVolumeIV: ImageView,
                vvInfo: com.cloutlyfe.app.ui.view.VideoViews,
                sound: Boolean
            ) {
                imgVol = imgVolumeIV
                vvInfoo = vvInfo
                mPositionn = mPosition
                if (SOUND) {
//                for (i in 0..imageList.size-1){
                    imgVolumeIV.setImageResource(R.drawable.ic_volume_mute)
                    vvInfo.mute()
                    imageList[mPositionn!!].vol = false
                    SOUND = false
//            }
                } else {
//                for (i in 0..imageList.size-1){
                    vvInfo.unmute()
                    imgVolumeIV.setImageResource(R.drawable.ic_volume_on)
                    imageList[mPositionn!!].vol = true
                    SOUND = true
//                }
                }
            }
        }


    var mOptionsClickListener: HomeMenuClickListenerInterface =
        object : HomeMenuClickListenerInterface {
            override fun onItemClickListner(
                mPosition: Int,
                post_id: String,
                otherUserId: String,
                loggedInUser: Boolean
            ) {
                mPostId = post_id
                mDelPos = mPosition
                mOtheruserId = otherUserId
                if (mOtheruserId == AppPrefrences().readString(activity!!, ID, null)) {
                    showDeleteDialog()
                } else {
                    showReportDialog()
                }


            }
        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding!!.imgNotificationIV.setOnClickListener {
            val i = Intent(context, NotificationsActivity::class.java)
            startActivity(i)
        }

        binding!!.imgSearchIV.setOnClickListener {
            val i = Intent(context, HomeSearchActivity::class.java)
            startActivity(i)
        }
    }


    override fun onPause() {
        super.onPause()
        if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder
            .stopVideo()
    }


//    override fun onDestroyView() {
//        super.onDestroyView()
//        if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder
//            .stopVideo()
//    }

//    override fun onDetach() {
//        super.onDetach()
//        if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder
//            .stopVideo()
//    }

    override fun onDetach() {
        Log.e(TAG, "onDetach: "+"on detach called" )
        super.onDetach()
        try{
            if (binding!!.homeListRV.handingVideoHolder != null)
                binding!!.homeListRV.handingVideoHolder.stopVideo()
        }
        catch (e:java.lang.NullPointerException){

        }

    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop: "+"on stop called" )
        if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder
            .stopVideo()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "onDestroy: "+"on destroy called" )
        if (activity != null && isAdded) {
            if (binding!!.homeListRV.handingVideoHolder != null) binding!!.homeListRV.handingVideoHolder
                .stopVideo()
        }
    }


    private fun executeHomeeApi() {
        if (isNetworkAvailable(requireActivity()))
            RequestHomeeData()
        else
            showToast(activity, getString(R.string.no_internet_connection))
    }


    private fun mHomeParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        mMap["last_post_id"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestHomeeData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.homeRequest(mHeaders, mHomeParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                if (mHomeDataModel!!.status == 1) {
//                    dismissProgressDialog()

//                    imageList.addAll(response.body()!!.data)

                    if (response.body()!!.notificationCount > 0) {
                        binding!!.imgNotificationIV.setImageDrawable(
                            activity?.resources?.getDrawable(
                                R.drawable.ic_notifiaction_badge
                            )
                        )
                    } else {
                        binding!!.imgNotificationIV.setImageDrawable(
                            activity?.resources?.getDrawable(
                                R.drawable.ic_notifiaction
                            )
                        )
                    }

                } else if (mHomeDataModel!!.status == 3) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message)
                } else if (mHomeDataModel!!.status == 2) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message)
                } else {
//                    dismissProgressDialog()
//                    binding!!.txtNoDataTV.visibility = View.VISIBLE
//                    binding!!.txtNoDataTV.text = getString(R.string.no_posts_available)
                }
            }
        })
    }

}