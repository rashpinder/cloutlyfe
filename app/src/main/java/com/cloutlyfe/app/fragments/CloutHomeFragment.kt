package com.cloutlyfe.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.audiofx.BassBoost
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.Agora.ConstantsA
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.BuildConfig
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.AddStoryActivity
import com.cloutlyfe.app.activities.LiveStreaming1Activity
import com.cloutlyfe.app.activities.LiveStreaming2Activity
import com.cloutlyfe.app.activities.StatusActivity
import com.cloutlyfe.app.adapters.*
import com.cloutlyfe.app.databinding.FragmentCloutHomeBinding
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.API
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.SpannedGridLayoutManager
import com.cloutlyfe.app.viewmodel.ProfileViewModel
import com.google.gson.Gson
import com.khostul.app.utils.HandleAndroidPermissions
import io.agora.rtc.Constants
import kotlinx.android.synthetic.main.activity_add_story.*
import kotlinx.android.synthetic.main.fragment_clout_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Create By Rashpinder
 */
class CloutHomeFragment : BaseFragment() {
    lateinit var binding: FragmentCloutHomeBinding
    var manager: SpannedGridLayoutManager? = null
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    var newList: ArrayList<LiveDataModel> = ArrayList()
    var recordList: ArrayList<DataItem> = ArrayList()
    var liveUsers: ArrayList<LiveDataModel> = ArrayList()
    var mArraylistLiveUsers: ArrayList<LiveDataModel> = ArrayList()
    var adapter1: CloutHomeAdapter? = null
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCloutHomeBinding.inflate(inflater, container, false)
        binding.header.txtHeadingTV.text = "Clout"

        binding.image.setOnClickListener {
            storyDialog.visibility = View.VISIBLE

            var addStoryLL = storyDialog.findViewById<LinearLayout>(R.id.addStoryLL)
            val goLiveLL = storyDialog.findViewById<LinearLayout>(R.id.goLiveLL)
            val viewStoryLL = storyDialog.findViewById<LinearLayout>(R.id.viewStoryLL)
            val imgCrossIV = storyDialog.findViewById<ImageView>(R.id.imgCrossIV)

            imgCrossIV.setOnClickListener {
                storyDialog.visibility = View.GONE
            }

            goLiveLL.setOnClickListener {
                storyDialog.visibility = View.GONE
                var mIntent = Intent(activity, LiveStreaming1Activity::class.java)
                startActivity(mIntent)
                }

            addStoryLL.setOnClickListener {
                    storyDialog.visibility = View.GONE
                    var mIntent = Intent(activity, AddStoryActivity::class.java)
                    startActivity(mIntent)
            }
            viewStoryLL?.setOnClickListener {
                if(!storiesModel!!.data.myStories.isNullOrEmpty() && !storiesModel!!.data.myStories.get((storiesModel!!.data.myStories.size) - 1).snaps.isNullOrEmpty()){
                if (storiesModel!!.data.myStories.get((storiesModel!!.data.myStories.size) - 1).snaps.size > 0) {
                    val intent = Intent(activity, StatusActivity::class.java)
                    startActivity(intent)
                    storyDialog.visibility = View.GONE
                } }else {
                    storyDialog.visibility = View.GONE
                    showAlertDialog(activity, "No story Added Yet.")
                }
            }
        }
        setRecordedStreams()

        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                requireActivity(),
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(requireActivity(), PERMISSIONS, PERMISSION_ALL)
        }
        return binding.root
    }

    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE

    fun setDataLiveUsers() {
        if (isNetworkAvailable((context as Activity?)!!)) {
            executeLiveUsersApi()
//            setRecordedStreams()
        } else {
//            binding.swiperefreshLiveusers.isRefreshing = false
            showToast(context as Activity?, resources.getString(R.string.no_internet_connection))
        }
    }

    /*
     * Execute api
     * */
    private fun mParamS(): Map<String, String> {
        val mMap: MutableMap<String, String> = java.util.HashMap()
        mMap["user_id"] =
            CloutNewPrefernces.readString(activity, com.cloutlyfe.app.utils.Constants.ID, null)
        mMap["page_no"] = "1"
        mMap["per_page"] = "1000"
        return mMap
    }

    private fun setRecordedStreams() {
        val mApiInterface: API = RetrofitInstance.appApi
        mApiInterface.getAllStreams(getAuthToken(), mParamS())!!
            .enqueue(object : Callback<StreamRecordedModel?> {
                override fun onResponse(
                    call: Call<StreamRecordedModel?>,
                    response: Response<StreamRecordedModel?>
                ) {
                    recordList.clear()
                    val model = response.body()
                    if (model!!.status == 1) {
                        setRecordedStreamAdapter(model.data as ArrayList<DataItem>)
                    } else {
//                        showToast(activity, model.message)
                    }
                }

                override fun onFailure(call: Call<StreamRecordedModel?>, t: Throwable) {}
            })
    }


    private fun setRecordedStreamAdapter(data: ArrayList<DataItem>) {
        for (i in 0..data.size - 1) {
            if ((i % 2) != 0) {
//                newList.add()
            } else {
                recordList.add(data[i])
            }
        }

        val layoutManager = GridLayoutManager(activity, 3)
        binding.recyclerStreamUsers.layoutManager = layoutManager
        val adapter = StreamRecHomeScreenAdapter(recordList, context as Activity)
        binding.recyclerStreamUsers.adapter = adapter
    }

    private fun mLiveUsersParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    fun executeLiveUsersApi() {
//        binding.swiperefreshLiveusers.isRefreshing = true
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
//        val authtoken: String = getAuthToken()
        val mApiInterface: API = RetrofitInstance.appApi
        Log.e(TAG, "executeLiveUsersApi: " + getAuthToken())
        mApiInterface.showLiveUsers(mHeaders, mLiveUsersParam())!!
            .enqueue(object : Callback<LiveUsersModel?> {
                override fun onResponse(
                    call: Call<LiveUsersModel?>,
                    response: Response<LiveUsersModel?>
                ) {
//                binding.swiperefreshLiveusers.isRefreshing = false
                    val mModel = response.body()
                    Log.e(TAG, "executeLiveUsersApiResponse: " + Gson().toJson(response.body()))
                    mArraylistLiveUsers.clear()
                    newList.clear()
                    liveUsers.clear()
                    if (mModel != null) {
                        if (mModel.status == 1) {
                            mArraylistLiveUsers.addAll(mModel.data)
                           for (i in mArraylistLiveUsers.indices){
                               liveUsers=mModel.data
//                               Log.e(TAG, "onResponse: "+AppPrefrences().readString(requireContext(),com.cloutlyfe.app.utils.Constants.ID,null) )
//if(activity!=null && isAdded){
//                               if(mArraylistLiveUsers[i].user_id!=AppPrefrences().readString(
//                                       activity!!,com.cloutlyfe.app.utils.Constants.ID,null)){
//                                   liveUsers=mModel.data
//                               }}
                           }

                        }
                    }
                    setLiveUsersAdapter(liveUsers)
                }

                override fun onFailure(call: Call<LiveUsersModel?>, t: Throwable) {
//                binding.swiperefreshLiveusers.isRefreshing = false
                }
            })
    }


    private fun setLiveUsersAdapter(mArraylistLiveUsers: ArrayList<LiveDataModel>) {
        if(mArraylistLiveUsers.isNullOrEmpty()){

        }
        else{
        for (i in 0..mArraylistLiveUsers.size - 1) {
//            if ((i % 2) != 0) {
//                var emptyObject = LiveDataModel(
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "",
//                    "", "", "", "", "", "",
//                    "", ""
//                )
//                newList.add(emptyObject)
//                newList.add(mArraylistLiveUsers[i])
//            } else {
                newList.add(mArraylistLiveUsers[i])
//            }
        }
        val layoutManager = GridLayoutManager(activity, 3)
        binding.recyclerLiveUsers.layoutManager = layoutManager
        adapter1 = CloutHomeAdapter(
            newList,
            context as FragmentActivity?,
            object : CloutHomeAdapter.onItemClickClickListener {
                override fun onItemClick(
                    position: Int,
                    mArrayList: ArrayList<LiveDataModel>,
                    view: View?
                ) {
                    when (view!!.id) {
                        R.id.imgImageIV -> performJoinLiveUser(mArrayList, position)
                    }
                }
            })
        binding.recyclerLiveUsers.adapter = adapter1
    }}


    //See live user stream
    private fun performJoinLiveUser(
        mArrayList: ArrayList<LiveDataModel>,
        position: Int
    ) {
        if (isNetworkAvailable(requireContext())) {
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.STREAM_USERID,
                mArrayList[position].user_id
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.STREAM_ROOMID,
                mArrayList[position].stream_room
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.STREAM_USERNAME,
                mArrayList[position].name
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.STREAM_ID,
                mArrayList[position].stream_id
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.STREAM_USERPHOTO,
                mArrayList[position].photo
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.COMMENT_DISABLE,
                mArrayList[position].disable_comment
            )
            CloutNewPrefernces.writeString(
                context,
                CloutNewPrefernces.LIKE_DISABLE,
                mArrayList[position].disable_like
            )
            gotoRoleActivity(mArrayList[position].stream_room, mArrayList[position].user_id)
        } else {
            showAlertDialog2(
                context as Activity?,
                resources.getString(R.string.no_internet_connection)
            )
        }
    }


    /* *
     * Error Alert Dialog
     * */
    fun showAlertDialog2(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    fun gotoRoleActivity(stream_room: String, userID: String?) {
        val mil = (System.currentTimeMillis() / 1000L).toInt()
        // Intent intent = new Intent(mActivity, LiveStreaming2Activity.class);
        val room = mil.toString()
        //  config().setChannelName(stream_room);
        ConstantsA.channelNameA = stream_room
        // Toast.makeText(getContext(), ""+userID, Toast.LENGTH_SHORT).show();
        //  config().setUid(getUserID());
        ConstantsA.useridA =
            CloutNewPrefernces.readString(activity, com.cloutlyfe.app.utils.Constants.ID, null)
        onJoinAsAudience()
        // onJoinAsBroadcaster();
    }

    fun onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER)
    }

    fun onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE)
    }

    private fun gotoLiveActivity(role: Int) {
        val intent = Intent()
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role)
        intent.setClass(requireActivity(), LiveStreaming2Activity::class.java)
        startActivity(intent)
    }


    var storiesModel: GetStoriesModel? = null
    var mStatusAdapter: StatusAdapter? = null

    private fun getStatusStories() {
        if (context?.let { isNetworkAvailable(it) } == true) {
            exceuteStoriesApi()
        } else {
            showToast(activity, getString(R.string.no_internet_connection))
        }
    }

    private fun exceuteStoriesApi() {
//        showProgressDialog(activity)
        val mHeaders: MutableMap<String, String> = HashMap()
        Log.e(TAG, "Token: " + getAuthToken())
        mHeaders["Token"] = getAuthToken()
        val call = RetrofitInstance.appApi.getStories(mHeaders, mParam())
        call.enqueue(object : Callback<GetStoriesModel> {
            override fun onFailure(call: Call<GetStoriesModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetStoriesModel>,
                response: Response<GetStoriesModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                storiesModel = response.body()
                if (storiesModel!!.status == 1) {
                    activity?.let {
                        Glide.with(it).load(storiesModel!!.data.myStories.get(0).user.photo)
                            .placeholder(R.drawable.ic_profile_ph)
                            .error(R.drawable.ic_profile_ph)
                            .into(homeListIV)
                    }
                    if (storiesModel!!.data.myStories[0].snaps.size == 0) {
                        binding.imgPlusIV.visibility=View.VISIBLE
                    } else {
                        binding.imgPlusIV.visibility=View.GONE
                    }
                    setstatusAdapter(storiesModel!!.data)
                }
            }
        })
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun setstatusAdapter(storiesModel: StoriesData) {
        if(activity!=null && isAdded){
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        storiesRV.layoutManager = layoutManager
        mStatusAdapter = StatusAdapter(activity, storiesModel)
        storiesRV.adapter = mStatusAdapter
    }}


    override fun onResume() {
        super.onResume()
        storyDialog.visibility = View.GONE
        getStatusStories()
        setDataLiveUsers()

    }


}
