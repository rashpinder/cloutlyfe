package com.cloutlyfe.app.fragments
import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.CloutAdapter
import com.cloutlyfe.app.databinding.FragmentCloutBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.CloutsLoadMoreListener
import com.cloutlyfe.app.interfaces.UnfollowClickInterface
import com.cloutlyfe.app.model.DataXXX
import com.cloutlyfe.app.model.FollowingUsersModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.RemoveUserViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created By Rashpinder
 */
class CloutFragment : BaseFragment() {

    lateinit var binding: FragmentCloutBinding
    var cloutAdapter: CloutAdapter? = null
    lateinit var mCloutsDataModel: FollowingUsersModel
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mPerPage: Int = 100
    var cloutsList: ArrayList<DataXXX> = ArrayList()
    var cloutsListt: ArrayList<DataXXX> = ArrayList()
    var strotherid:String?=""
    lateinit var removeViewModel: RemoveUserViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        strotherid = requireArguments().getString("other_id")
        binding = FragmentCloutBinding.inflate(inflater, container, false)
        init()
        executeGetCloutsApi()
//        setAdapter()

        return binding.root;
    }


    private fun init() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        removeViewModel =
            factory.let { ViewModelProvider(this, it!!).get(RemoveUserViewModel::class.java) }
    }

//    private fun setAdapter() {
//        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//        binding.cloutRV.setLayoutManager(layoutManager)
////        chatListAdapter = ChatListAdapter(activity,ChatDetailList,mChatItemClickListner)
//        cloutAdapter = CloutAdapter(activity)
//        binding.cloutRV.setAdapter(cloutAdapter)
//    }

    var mItemClickListener: UnfollowClickInterface = object : UnfollowClickInterface {
        override fun onItemClickListner(mPosition: Int, mId: String) {
            executeUnfollowApi(mPosition, mId)
        }
    }

    private fun executeUnfollowApi(mPosition: Int, mId: String) {
        if (!isNetworkAvailable(requireActivity())) {
            showAlertDialog(activity, getString(R.string.no_internet_connection))
        } else {
            followUserRequest(mPosition, mId)
        }
    }


    private fun followUserRequest(mPosition: Int, mId: String) {
        var user_id = getLoggedInUserID()
        var followUserID = mId
        Log.e(TAG, "followUserRequest: " + followUserID)
        val body = RequestBodies.FollowBody(user_id, followUserID)
        removeViewModel.removeUserData(getAuthToken(), body, requireActivity())
        removeViewModel.removeResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { followResponse ->
                            if (followResponse.status == 1 || followResponse.status == 200) {
                                cloutsList.removeAt(mPosition)
                                cloutAdapter!!.notifyDataSetChanged()

//                                    showToast(activity, loginResponse.message)
//                                binding.txtFollowTV.text=getString(R.string.foll)
//                                dismissProgressDialog()
                            } else {
//                                dismissProgressDialog()
                                showAlertDialog(activity, followResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
//                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(activity, response.data?.message)
                        }
                    }

                    is Resource.Loading -> {
//                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    private fun executeGetCloutsApi() {
        if (isNetworkAvailable(requireActivity()))
            RequestCloutsData()
        else
            showToast(activity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        if(!strotherid.isNullOrEmpty()){
            mMap["user_id"] =strotherid
        }
        else{
            mMap["user_id"] = getLoggedInUserID()
        }
        mMap["followType"] = "1"
        if(!strotherid.isNullOrEmpty()){
        mMap["profileUserId"] = strotherid}
        else{
            mMap["profileUserId"] = getLoggedInUserID()
        }
        mMap["search"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCloutsData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.getFollowingRequest(mHeaders, mParam())
        call.enqueue(object : Callback<FollowingUsersModel> {
            override fun onFailure(call: Call<FollowingUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<FollowingUsersModel>,
                response: Response<FollowingUsersModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mCloutsDataModel = response.body()!!
                if (mCloutsDataModel.status == 1) {

//                    isLoading = !mCloutsDataModel.lastPage.equals(true)
//                    cloutsList.addAll(response.body()!!.data)
                    for (i in response.body()!!.data.indices){
                        if(response.body()!!.data[i].is_delete!=1){
                            cloutsList.add(response.body()!!.data[i])
                        }

                    }
                    Log.e(TAG, "RequestHomeDataAPI: " + cloutsList)
                    if (cloutsList.size == 0) {
//                        binding.upArraowRL.visibility = View.GONE
                        binding.txtNoDataTV.text ="No Clouts found"

                    } else {
//                        binding.upArraowRL.visibility = View.VISIBLE
                        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner,mItemClickListener)
                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCloutsDataModel.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mCloutsDataModel.message!!)
                } else {
//                    binding.upArraowRL.visibility = View.GONE
                    binding.txtNoDataTV.visibility = View.VISIBLE
//                    binding.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    private fun setAdapter(
        mActivity: Activity,
        cloutsList: ArrayList<DataXXX>,
        mLoadMoreScrollListner: CloutsLoadMoreListener,
        mItemClickListener: UnfollowClickInterface
    ) {
        cloutAdapter = CloutAdapter(mActivity, cloutsList, mLoadMoreScrollListner,mItemClickListener,strotherid)
        binding.cloutRV.layoutManager = LinearLayoutManager(mActivity)
        binding.cloutRV.adapter = cloutAdapter
    }

    var mLoadMoreScrollListner: CloutsLoadMoreListener = object : CloutsLoadMoreListener {
        override fun onLoadMoreListner(mModel: ArrayList<DataXXX>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreCloutsRequest()
//            }
        }
        }


    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreCloutsRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        binding.mProgressRL.visibility = View.VISIBLE
        val call = RetrofitInstance.appApi.getFollowingRequest(mHeaders, mLoadMoreParam())
        call.enqueue(object : Callback<FollowingUsersModel> {
            override fun onFailure(call: Call<FollowingUsersModel>, t: Throwable) {
                dismissProgressDialog()
                binding.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<FollowingUsersModel>,
                response: Response<FollowingUsersModel>
            ) {
                binding.mProgressRL.visibility = View.GONE
                val mGetSavedModel = response.body()
                if (mGetSavedModel!!.status == 1) {
                    mGetSavedModel.data.let {
                        cloutsList.addAll<DataXXX>(
                            it
                        )
                    }
                    cloutAdapter?.notifyDataSetChanged()
                    isLoading = !mGetSavedModel.lastPage.equals(true)
                } else if (mGetSavedModel.status == 0) {
//                    showToast(mActivity, mGetSavedModel.message)
                }
            }
        })
    }


}
