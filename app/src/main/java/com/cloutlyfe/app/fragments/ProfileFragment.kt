package com.cloutlyfe.app.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.*
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.AllPostsActivity
import com.cloutlyfe.app.activities.CloutCloutersActivity
import com.cloutlyfe.app.activities.EditProfileActivity
import com.cloutlyfe.app.activities.SettingsActivity
import com.cloutlyfe.app.adapters.MySavedlistAdapter
import com.cloutlyfe.app.databinding.FragmentProfileBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.ProfilePostsLoadMoreListener
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.HomeModel
import com.cloutlyfe.app.model.ProfilePostsData
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.utils.SpannedGridLayoutManager
import com.cloutlyfe.app.viewmodel.ProfileViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import com.github.chrisbanes.photoview.PhotoView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Create By Rashpinder
 */
class ProfileFragment : BaseFragment() {
    var mySavedAdapter: MySavedlistAdapter? = null
    lateinit var binding: FragmentProfileBinding
    var manager: SpannedGridLayoutManager? = null
    lateinit var profileViewModel: ProfileViewModel
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    var mType: String = ""
    var mTabType: String = "1"
    lateinit var mProfileDataModel: HomeModel
    var dataList: ArrayList<HomeData> = ArrayList()
    var profile_url: String? = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        init()
        binding.header.txtHeadingTV.text = getString(R.string.profile)
        binding.header.imgNotificationIV.visibility = View.VISIBLE
        binding.header.imgNotificationIV.setImageDrawable(resources.getDrawable(R.drawable.ic_settings))

        binding.header.imgNotificationIV.setOnClickListener {
            val intent = Intent(activity, SettingsActivity::class.java)
            activity?.startActivity(intent)
        }
        binding.txtEditProfileTV.setOnClickListener {
            val intent = Intent(activity, EditProfileActivity::class.java)
            activity?.startActivity(intent)
        }
        binding.cloutLL.setOnClickListener {
            val intent = Intent(activity, CloutCloutersActivity::class.java)
            intent.putExtra("value", "clout")
            activity?.startActivity(intent)
        }
        binding.cloutersLL.setOnClickListener {
            val intent = Intent(activity, CloutCloutersActivity::class.java)
            intent.putExtra("value", "clouters")
            activity?.startActivity(intent)
        }
        binding.txtPostssTV.setOnClickListener {
//            val intent = Intent(activity, CloutCloutersActivity::class.java)
//            intent.putExtra("value", "clouters")
//            activity?.startActivity(intent)
        }
        binding.txtManagePostTV.setOnClickListener {
//            val intent = Intent(activity, ManagePostActivity::class.java)
            var intent = Intent(context, AllPostsActivity::class.java)
            intent.putExtra("value", "Manage Post")
            intent.putExtra("user_id", getLoggedInUserID())
            intent.putExtra("type", "3")
            activity?.startActivity(intent)
        }
        binding.allPostsLL.setOnClickListener {
            mTabType = "1"
            mType = "1"
            performAllPostsClick(mTabType)

        }
        binding.videoLL.setOnClickListener {
            mTabType = "2"
            mType = "2"
            performVideoClick(mTabType)

        }
        binding.profileeIV.setOnClickListener {
            if(!profile_url.isNullOrEmpty()){
            openPicDialog(activity!!, profile_url!!)
        }}

//        setupSpannedGridLayout(mActivity, commentsList, mLoadMoreScrollListner)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if (mTabType == "1") {
            binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_sel))
            binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_unsel))
            mType = "1"
            mTabType = "1"
        } else {
            binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_sel))
            binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_unsel))
            mType = "2"
            mTabType = "2"
        }
        setUpProfileData(mType)

    }

    private var mLastClickTab1: Long = 0
    private fun performVideoClick(type: String) {
//        preventMultipleClick()
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        dataList.clear()
        binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_sel))
        binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_unsel))
        executeGetProfilePostsApi("2")

    }

    private fun performAllPostsClick(type: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        dataList.clear()
        binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_sel))
        binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_unsel))
        executeGetProfilePostsApi("1")
    }

    private fun setUpProfileData(mType: String) {
//        if (AppPrefrences().readString(requireActivity(),Constants.USERNAME,null)=="N/A"||AppPrefrences().readString(requireActivity(),Constants.USERNAME,null)==""||AppPrefrences().readString(requireActivity(),Constants.USERNAME,null)==null){
            binding.txtUsernameTV.text = getUserName()
//        }
//        else{
//            binding.txtUsernameTV.text = AppPrefrences().readString(requireActivity(),Constants.USERNAME,null)
//        }

//        binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_sel))
//        binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_unsel))
        executeProfileApi(mType)
    }

    private fun init() {
        val repository = AppRepository()
        val factory = activity?.let { ViewModelProviderFactory(it.application, repository) }
        profileViewModel =
            factory?.let { ViewModelProvider(this, it).get(ProfileViewModel::class.java) }!!
    }


    private fun executeProfileApi(mType: String) {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.no_internet_connection))
        } else {
            RequestProfileData(mType)
        }
    }

    private fun RequestProfileData(mType: String) {
        var user_id = getLoggedInUserID()
        var followUserID = getLoggedInUserID()
        val body = RequestBodies.ProfileBody(user_id, followUserID)
        activity?.let { profileViewModel.getProfileData(getAuthToken(), body, it) }
        activity?.let {
            profileViewModel.profileResponse.observe(it, Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data?.let { loginResponse ->
                                if (loginResponse.status == 1 || loginResponse.status == 200) {
//                                    showToast(activity, loginResponse.message)
                                    binding.txtPostsTV.text = loginResponse.postCount
                                    binding.txtCloutTV.text = loginResponse.Followers
                                    binding.txtCloutersTV.text = loginResponse.Followings
                                    binding.txtArtistTV.text = loginResponse.data.bio
                                    if (activity != null) {
                                        Glide.with(requireActivity())
                                            .load(loginResponse.data.photo)
                                            .placeholder(R.drawable.ic_profile_ph)
                                            .error(R.drawable.ic_profile_ph)
                                            .into(binding.profileeIV)
                                        profile_url = loginResponse.data.photo
                                        Glide.with(requireActivity())
                                            .load(loginResponse.data.cover_image)
                                            .placeholder(R.drawable.placeholder_clout)
                                            .error(R.drawable.placeholder_clout)
                                            .into(binding.imgCoverIV)
                                        AppPrefrences().writeString(
                                            requireActivity(),
                                            Constants.PROFILE_IMAGE, loginResponse.data.photo
                                        )
                                        AppPrefrences().writeString(
                                            requireActivity(),
                                            Constants.COVER_IMAGE, loginResponse.data.cover_image
                                        )
                                        AppPrefrences().writeString(
                                            requireActivity(),
                                            Constants.NAME, loginResponse.data.name
                                        )
                                        AppPrefrences().writeString(
                                            requireActivity(),
                                            Constants.USERNAME, loginResponse.data.username
                                        )
                                        AppPrefrences().writeString(
                                            requireActivity(),
                                            Constants.BIO, loginResponse.data.bio
                                        )
                                    }
                                    executeGetProfilePostsApi(mType)
//                                    dismissProgressDialog()
                                } else {
                                    dismissProgressDialog()
                                    showAlertDialog(activity, loginResponse.message)
                                }
                            }
                        }

                        is Resource.Error -> {
                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(activity, response.message)
                            }
                        }

                        is Resource.Loading -> {
                            showProgressDialog(activity)
                        }
                    }
                }
            })
        }
    }

    // Sample usage from your Activity/Fragment
    private fun setupSpannedGridLayout(
        activity: FragmentActivity,
        dataList: ArrayList<HomeData>,
        mLoadMoreScrollListner: ProfilePostsLoadMoreListener,
        mType: String
    ) {

        manager = SpannedGridLayoutManager(
            object : SpannedGridLayoutManager.GridSpanLookup {
                override fun getSpanInfo(position: Int): SpannedGridLayoutManager.SpanInfo {
                    if (dataList.size == 1) {
                        dataList.addAll(mProfileDataModel.data)
                    }
                    if (((position - 4) % 6 == 0) || ((position - 4) % 6 == 2)) {
                        if (position == dataList.size - 2) {
                            return SpannedGridLayoutManager.SpanInfo(1, 1)
                        } else {
                            return SpannedGridLayoutManager.SpanInfo(2, 2)
                        }
                    } else {
                        return SpannedGridLayoutManager.SpanInfo(1, 1)
                    }
                }
            },
            3,  // number of columns
            1f // how big is default item
        )

        binding.savedRV.layoutManager = manager
        mySavedAdapter = MySavedlistAdapter(activity, dataList, mLoadMoreScrollListner, mType)
        binding.savedRV.adapter = mySavedAdapter
        mySavedAdapter!!.notifyDataSetChanged()
    }


    private fun executeGetProfilePostsApi(type: String) {
        dataList.clear()
//        mType = type
        if (isNetworkAvailable(requireActivity()))
            RequestProfilePostsData()
        else
            showToast(activity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_userid"] = getLoggedInUserID()
        mMap["type"] = mType
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestProfilePostsData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.getProfilePostsRequest(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mProfileDataModel = response.body()!!
                if (mProfileDataModel.status == 1) {

                    isLoading = !mProfileDataModel.lastPage.equals(true)
                    dataList.addAll(response.body()!!.data)

                    Log.e(TAG, "RequestHomeDataAPI: " + dataList)
                    if (dataList.size == 0) {
                        binding.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                        binding.txtNoDataTV.visibility = View.VISIBLE
                    } else {
                        setupSpannedGridLayout(
                            activity!!, dataList, mLoadMoreScrollListner,
                            mType
                        )
                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mProfileDataModel.status == 3) {

//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else if (mProfileDataModel.status == 0) {
                    binding.txtNoDataTV.text =
                        getString(R.string.no_posts_available)
                    binding.txtNoDataTV.visibility = View.VISIBLE
//binding.txtNoDataTV.text= "No posts found"
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

//                    binding.txtNoDataTV.visibility = View.VISIBLE
//                    binding.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    var mLoadMoreScrollListner: ProfilePostsLoadMoreListener =
        object : ProfilePostsLoadMoreListener {
            override fun onLoadMoreListner(mModel: ArrayList<ProfilePostsData>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
            }
        }

    fun openPicDialog(context: Context, image_url: String) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.item_profile_img_dialog)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(true)
//        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation

        val photoView = dialog.findViewById(R.id.photoView) as PhotoView
        val window = dialog.window
        window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.setGravity(Gravity.CENTER)
        val lp = dialog.window!!.attributes
        lp.dimAmount = 0.6f

        dialog.window!!.attributes = lp
        dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.show()

        Glide.with(context)
            .load(image_url)
            .placeholder(R.drawable.ic_profile_ph)
            .into(photoView)
    }

}
