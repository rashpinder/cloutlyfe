package com.cloutlyfe.app

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cloutlyfe.app.databinding.ActivityHomeBinding
import com.cloutlyfe.app.fragments.*
import com.cloutlyfe.app.utils.Constants.Companion.CHAT_TAG
import com.cloutlyfe.app.utils.Constants.Companion.CREATE_POST_TAG
import com.cloutlyfe.app.utils.Constants.Companion.HOME_TAG
import com.cloutlyfe.app.utils.Constants.Companion.PROFILE_TAG
import com.cloutlyfe.app.utils.Constants.Companion.SAVE_TAG
import com.google.gson.Gson
import com.industree.app.main.fragment.MessagesFragment
import com.khostul.app.utils.HandleAndroidPermissions

class HomeActivity : BaseActivity() {
    private lateinit var homeBinding: ActivityHomeBinding
    var latitude = ""
    var value = ""
    var longitude = ""
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA

        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(homeBinding.root)
        if (ContextCompat.checkSelfPermission(
                this@HomeActivity,
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                this@HomeActivity,
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(this@HomeActivity, PERMISSIONS, PERMISSION_ALL)
        }
        performClicks()
        performHomeClick()
    }


    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE


    override fun onResume() {
        super.onResume()
        if(!intent.getStringExtra("value").toString().isNullOrEmpty()){
             var intent_value= intent.getStringExtra("value")
            if(intent_value=="livescreen"){
                performSaveClick()
            }
            else if(intent_value=="fromManage")
            {
              performProfileClick()
            }
            else if(intent_value=="fromLike"){
                performHomeClick()
            }
//            else{
//                performHomeClick()
//            }

    }}


    private fun getIntentData() {
        val value = intent.getStringExtra("value")
        val gson = Gson()
        val data = intent.getStringExtra("postData")
//        postData = gson.fromJson(
//            data,
//            AllMessage::class.java
//        )
        if(value=="post_click"){
            performHomeClick()
        }
    }

    fun performClicks() {
        homeBinding.homeRL.setOnClickListener {
            performHomeClick()
        }
        homeBinding.savedRL.setOnClickListener {
            performSaveClick()
        }
        homeBinding.addRL.setOnClickListener {
            performAddClick()
        }
        homeBinding.chatRL.setOnClickListener {
            performChatClick()
        }
        homeBinding.profileRL.setOnClickListener {
            performProfileClick()
        }
    }

    private fun performHomeClick() {
        Log.e(TAG, "performHomeClick: "+"trueeeee" )
        tabSelection(0)
        switchFragment(HomeFragment(), HOME_TAG, false, null)
    }
    private fun performSaveClick() {
        tabSelection(1)
//        switchFragment(AllSavedFragment(), SAVE_TAG, false, null)
        switchFragment(CloutHomeFragment(), SAVE_TAG, false, null)
    }
    private fun performAddClick() {
        tabSelection(4)
        switchFragment(CreatePostFragment(), CREATE_POST_TAG ,false,null )
    }
    private fun performChatClick() {
        tabSelection(2)
        switchFragment(MessagesFragment(), CHAT_TAG,false , null)
    }
    private fun performProfileClick() {
        tabSelection(3)
        switchFragment(ProfileFragment() , PROFILE_TAG , false , null)
    }

    private fun tabSelection(mPos: Int) {
        homeBinding.imgHomeIV.setImageResource(R.drawable.ic_home_unsel)
        homeBinding.imgSavedIV.setImageResource(R.drawable.ic_unsel_cloud_home)
        homeBinding.imgChatIV.setImageResource(R.drawable.ic_chat_unsel)
        homeBinding.imgProfileIV.setImageResource(R.drawable.ic_profile_unsel)
        when(mPos){
            0 ->{
                homeBinding.imgHomeIV.setImageResource(R.drawable.ic_home)
            }
            1 ->{
                homeBinding.imgSavedIV.setImageResource(R.drawable.ic_home_cloud)
            }
            2 ->{
                homeBinding.imgChatIV.setImageResource(R.drawable.ic_chat)}
            3 ->{
                homeBinding.imgProfileIV.setImageResource(R.drawable.ic_profile)
            }
        }
    }
}