package com.cloutlyfe.app.interfaces

import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.ProfilePostsData

interface PostsMenuClickListenerInterface {
    public fun onItemClickListner(
        mPosition: Int,
        mModel: HomeData,
        post_id: String,
        otherUserId: String,
        loggedInUser: Boolean
    )
}