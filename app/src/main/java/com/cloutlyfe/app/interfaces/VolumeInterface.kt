package com.cloutlyfe.app.interfaces

import android.widget.ImageView
import com.cloutlyfe.app.ui.view.VideoViews

interface VolumeInterface {
    public fun onItemClickListner(
        mPosition: Int,
        imgVolumeIV: ImageView,
        vvInfo: VideoViews,
        sound: Boolean
    )
}