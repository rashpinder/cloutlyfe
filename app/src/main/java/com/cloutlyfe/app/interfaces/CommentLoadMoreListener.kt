package com.cloutlyfe.app.interfaces

import com.cloutlyfe.app.model.DataXX
import java.util.ArrayList

interface CommentLoadMoreListener {
    public fun onLoadMoreListner(mModel: ArrayList<DataXX>)
}