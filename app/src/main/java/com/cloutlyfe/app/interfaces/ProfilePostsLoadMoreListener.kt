package com.cloutlyfe.app.interfaces
import com.cloutlyfe.app.model.ProfilePostsData
import java.util.ArrayList

interface ProfilePostsLoadMoreListener {
    public fun onLoadMoreListner(mModel: ArrayList<ProfilePostsData>)
}