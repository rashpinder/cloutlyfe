package com.cloutlyfe.app.interfaces

interface DeleteChatInterface {
    public fun onItemClickListner(
        mPosition: Int,
        id: String,
        room_id: String
    )
}