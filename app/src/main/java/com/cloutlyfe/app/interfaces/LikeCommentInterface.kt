package com.cloutlyfe.app.interfaces

import android.widget.ImageView

interface LikeCommentInterface {
    public fun onItemClickListner(
        mPosition: Int,
        post_id: String,
        likeIV: ImageView,
        postId: String,
        commentBy: String
    )
}