package com.cloutlyfe.app.interfaces


interface HomeMenuClickListenerInterface {
    public fun onItemClickListner(
        mPosition: Int,
        post_id: String,
        otherUserId: String,
        loggedInUser: Boolean
    )
}