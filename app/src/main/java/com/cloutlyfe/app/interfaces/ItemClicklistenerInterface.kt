package com.cloutlyfe.app.interfaces

import com.cloutlyfe.app.model.PredictionsItem

interface ItemClicklistenerInterface {
    public fun onItemClickListner(mPosition: Int, userId: String)
}