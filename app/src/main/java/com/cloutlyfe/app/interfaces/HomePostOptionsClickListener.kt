package com.cloutlyfe.app.interfaces

import com.cloutlyfe.app.model.HomeData

interface HomePostOptionsClickListener {
    public fun onItemClickListner(
        mPosition: Int,
        mModel: HomeData,
        post_id: String,
        otherUserId: String,
        loggedInUser: Boolean
    )
}