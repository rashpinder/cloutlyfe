package com.cloutlyfe.app.dataobject

import android.graphics.Bitmap

object RequestBodies {
    data class LoginBody
        (
        val email: String,
        val password: String,
        val deviceToken: String,
        val deviceType: String
        )

    data class  SignUpBody
        (
        val userName:String,
        val name:String,
        val email:String,
        val password:String,
        val device_token:String,
        val device_type:String,
        val location:String
        )

    data class  GoogleLoginBody
        (
        val name:String,
        val email:String,
        val google_id:String,
        val device_token:String,
        val device_type:String,
        val user_image:String
        )

  data class  FacebookLoginBody
        (
        val name:String,
        val email:String,
        val facebook_id:String,
        val device_token:String,
        val device_type:String,
        val user_image:String
        )

  data class  TwitterLoginBody
        (
        val name:String,
        val email:String,
        val twitter_id:String,
        val device_token:String,
        val device_type:String,
        val user_image:String
        )

    data class ForgotPswdBody
        (
        val email: String
        )

    data class LogoutBody
        (
        val user_id: String
        )

    data class ChangePswdBody (
        val  user_id:String,
        val  old_password:String,
        val  new_password:String
    )

    data class LikeUnlikeBody (
        val  user_id:String,
        val  post_id:String
    )

    data class SaveUnsaveBody (
        val  user_id:String,
        val  post_id:String
    )

    data class CreatePostBody (
        val  user_id:String,
        val  bio:String,
        val  location:String,
        val  latitude:String,
        val  longitude:String,
        val  uploads:Any
    )

    data class ReportReasonsBody (
        val  user_id:String
    )

    data class ReportBody (
        val  user_id:String,
        val  post_id:String,
        val  reasonId:String,
        val  reportedId:String,
        val  reportType:String,
        val  reportedBy:Any
    )

    data class HomeBody
        (
        val user_id:String
        )

     data class ProfileBody
        (
        val user_id:String,
        val followUserID:String
        )

     data class FollowBody
        (
        val user_id:String,
        val followUserID:String
        )

    data class SearchHomeBody
        (
        val user_id:String,
        val search: String,
        val lastUserId: String,
        val perPage: String
    )

   data class PrayerBody(
       val name: String,
       val title: String,
       val detail: String
       )

    data class EditProfileBody(
        val firstName: String,
        val lastName: String,
        val email: String,
        val password: String,
        val phone: String,
        val mBitmap: Bitmap?
    )
    data class ContactUsBody(
        val name: String,
        val email: String,
        val message: String
    )

    data class SearchBody(
        val title:String,
        val perPage:String,
        val pageNo:String,
    )

    data class InappPuchaseBody(
        val userid: String,
        val title: String,
        val endDate: String,
        val referralby: String,
        val devicetype: String,
        val devicemodel: String,
        val transactionid:String
        )

    data class LikeUnlikeCommentBody (
        val  user_id:String,
        val  comment_id:String
    )
}