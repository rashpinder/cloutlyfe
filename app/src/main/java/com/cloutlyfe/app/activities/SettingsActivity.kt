package com.cloutlyfe.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.App
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.LoginActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivitySettingsBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.StatusModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.LogOutViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SettingsActivity : BaseActivity() {
    lateinit var binding: ActivitySettingsBinding
    lateinit var logOutViewModel: LogOutViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.text=getString(R.string.settings)
        init()
if (AppPrefrences().readBoolean(mActivity, Constants.ISSIMPLELOGIN, false).equals(true)){
    binding.changePasswordRL.visibility=View.VISIBLE
}
        else{
    binding.changePasswordRL.visibility=View.GONE
        }
        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.changePasswordRL.setOnClickListener {
            val i = Intent(mActivity, ChangePasswordActivity::class.java)
            startActivity(i)
        }
        binding.privacyRL.setOnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(getString(R.string.value),getString(R.string.privacy))
            startActivity(i)
        }
        binding.termsRL.setOnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(getString(R.string.value),getString(R.string.terms))
            startActivity(i)
        }
        binding.blockRL.setOnClickListener {
            val i = Intent(mActivity, BlockedUsersActivity::class.java)
            startActivity(i)
        }
        binding.LogoutRL.setOnClickListener {
            logOut()
        }
   binding.deleteAccountRL.setOnClickListener {
           deleteAccount()
        }
        binding.savedPostsRL.setOnClickListener {
           performSavedPostsClick()
        }

    }

    private fun performSavedPostsClick() {
        val intent = Intent(mActivity, AllSavedPostsActivity::class.java)
        startActivity(intent)
    }

    private fun deleteAccount() {
        value="delete"
        if (Constants.hasInternetConnection(application as App)) {
            showLogoutAlertDialog(mActivity,getString(R.string.are_you_sure_you_want_to_del_account))
        } else {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        }
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        logOutViewModel = ViewModelProvider(this, factory).get(LogOutViewModel::class.java)
    }

    var value:String?=""

    fun logOut() {
        value="logout"
        if (Constants.hasInternetConnection(application as App)) {
            showLogoutAlertDialog(mActivity,getString(R.string.are_you_sure_you_want_to_log_out))
        } else {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        }
    }


    // show block alert dialog
    fun showLogoutAlertDialog(mActivity: Activity?,strMessage: String) {
        val alertDialog = mActivity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        txtMessageTV.text = strMessage
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
            if (value=="logout"){
            executeLogOutApi()}
            else{
executeDeleteApi()
            }
        }
        alertDialog.show()
    }


    //API execution
    private fun executeLogOutApi() {
        var user_id = AppPrefrences().readString(mActivity,Constants.ID,null)
        val body = RequestBodies.LogoutBody(user_id!!)
        logOutViewModel.logOut(AppPrefrences().readString(mActivity,Constants.AUTHTOKEN,null)!!,mActivity,body)
        logOutViewModel.resultResponse.observe(this, androidx.lifecycle.Observer {
                event -> event.getContentIfNotHandled()?.let { response ->
            when (response) {
                is Resource.Success -> {
                    Constants.dismissProgressDialog()
                    response.data?.let { profileF ->
                        if (profileF.status == 1 || profileF.status == 200) {
                            switchTOLogOut()
                        }
                        else if (profileF.status == 301 || profileF.status == 401)
                        {
                            Constants.showAuthDismissDialog(mActivity, getString(R.string.authorization_failure))
                        }
                        else {
                            showAlertDialog(mActivity, profileF.message)
                        }
                    }
                }

                is Resource.Error -> {
                    Constants.dismissProgressDialog()
                    response.message?.let { message ->
                        showAlertDialog(mActivity, message)

                    }
                }

                is Resource.Loading -> {
                    Constants.showProgressDialog(mActivity)
                }
            }
        }
        })

    }
    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeDeleteApi() {
        val call = RetrofitInstance.appApi.deleteUserAccountRequest(mParams())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {

                Log.e(TAG, response.body().toString())
                val mDeleteModel = response.body()
                if (mDeleteModel!!.status == 1) {
                    switchTOLogOut()
                } else if (mDeleteModel.status == 0) {
                    showAlertDialog(mActivity, mDeleteModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }


    private fun logoutFromFacbook() {
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions",
                null,
                HttpMethod.DELETE,
                GraphRequest.Callback {
                    AccessToken.setCurrentAccessToken(null)
                    LoginManager.getInstance().logOut()
                }).executeAsync()
        }}


    //Logout clear data and switching
    private fun switchTOLogOut() {
        logoutFromFacbook()
        val preferences: SharedPreferences = AppPrefrences().getPreferences(
            Objects.requireNonNull(mActivity)
        )
        val preferencess: SharedPreferences = CloutNewPrefernces.getPreferences(
            Objects.requireNonNull(mActivity)
        )
        val editor = preferences.edit()
        val editorr = preferencess.edit()
        editor.clear()
        editorr.clear()
        editor.apply()
        editorr.apply()
        val mIntent = Intent(mActivity, LoginActivity::class.java)
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mIntent)
    }


}