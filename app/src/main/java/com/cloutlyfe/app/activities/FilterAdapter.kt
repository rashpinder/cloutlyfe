package com.cloutlyfe.app.activities

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.databinding.ItemFilterBinding

class FilterAdapter(
    private val dummyClick: (strTag:FilterType) -> Unit,
    private val activity: Activity,
    private val filters: Array<FilterType>,

) : RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): ViewHolder {
        val binding = ItemFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            binding.filterNameTV.text=""+filters[position].name
            holder.itemView.setOnClickListener {
                dummyClick(filters[position])
            }
        }
    }


    override fun getItemCount(): Int {
        return if (filters.toList().isEmpty()) {
            0
        } else {
            filters.size
        }
    }

    inner class ViewHolder(val binding: ItemFilterBinding) : RecyclerView.ViewHolder(binding.root)
}