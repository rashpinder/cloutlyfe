package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider

import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.HomeActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.BlockAdapter
import com.cloutlyfe.app.adapters.CommentsAdapter
import com.cloutlyfe.app.adapters.LikesListAdapter
import com.cloutlyfe.app.databinding.ActivityLikesBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.UnfollowClickInterface
import com.cloutlyfe.app.model.GetLikeUsersModel
import com.cloutlyfe.app.model.LikeUsersData
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.FollowUserViewmodel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class LikesActivity : BaseActivity() {
    lateinit var binding: ActivityLikesBinding
    var post_id=""
    var likeCount=""
    var from_comment=""
    lateinit var mGetLikeUsersModel: GetLikeUsersModel
    lateinit var followViewModel: FollowUserViewmodel
    val likesList: ArrayList<LikeUsersData> = ArrayList()
    var likesAdapter: LikesListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLikesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.text = "Likes"
        post_id = intent.getStringExtra("post_id")!!
        if(!intent.getStringExtra("from_comment").isNullOrEmpty()){
        from_comment = intent.getStringExtra("from_comment")!!
        }
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        followViewModel = ViewModelProvider(this, factory).get(FollowUserViewmodel::class.java)

        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }
        executeLikesApi()
    }

    private fun executeLikesApi() {
        if (isNetworkAvailable(mActivity))
            RequestSavedData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = post_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestSavedData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getUsersLikeRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetLikeUsersModel> {
            override fun onFailure(call: Call<GetLikeUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetLikeUsersModel>,
                response: Response<GetLikeUsersModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mGetLikeUsersModel = response.body()!!
                if (mGetLikeUsersModel.status == 1) {
                    dismissProgressDialog()
                    likesList.clear()
                    likeCount=mGetLikeUsersModel.likeCount
                    binding.txtCountTV.text=likeCount+" "+"Likes"
                    likesList.addAll(response.body()!!.data)
                    setLikeAdapter(likesList)
                    Log.e(TAG, "RequestHomeDataAPI: " + SavedActivity.imageList)
                    if (likesList.size == 0) {
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text =mGetLikeUsersModel.message
                    } else {
                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mGetLikeUsersModel.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mHomeDataModel.message!!)
                } else {
                    dismissProgressDialog()
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.txtNoDataTV.text = mGetLikeUsersModel.message
                }
            }
        })
    }

    private fun setLikeAdapter(
        mList: ArrayList<LikeUsersData>
    ) {
        binding.LikesRV.layoutManager = LinearLayoutManager(mActivity)
        likesAdapter = LikesListAdapter(mActivity,
            mList,mItemClickListener)
        binding.LikesRV.adapter = likesAdapter
    }

    var mItemClickListener: UnfollowClickInterface = object : UnfollowClickInterface {
        override fun onItemClickListner(mPosition: Int, mId: String) {
            executeUnfollowApi(mPosition, mId)
        }
    }

    private fun executeUnfollowApi(mPosition: Int, mId: String) {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            followUserRequest(mPosition, mId)
        }
    }


    private fun followUserRequest(mPosition: Int, mId: String) {
        var user_id = getLoggedInUserID()
        var followUserID = mId
        Log.e(TAG, "followUserRequest: " + followUserID)
        val body = RequestBodies.FollowBody(user_id, followUserID)
        followViewModel.getFollowData(getAuthToken(), body, mActivity)
        followViewModel.followResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { followResponse ->
                            if (followResponse.status == 1 || followResponse.status == 200) {
                            } else {
                                showAlertDialog(mActivity, followResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
//                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.data?.message)
                        }
                    }

                    is Resource.Loading -> {
//                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

//    override fun onBackPressed() {
//        super.onBackPressed()
//        if(from_comment=="true"){
//            val i = Intent(mActivity, HomeActivity::class.java)
//            i.putExtra("value","fromLike")
//            startActivity(i)
//        }
//        else{
//            onBackPressed()
//        }
//    }

}