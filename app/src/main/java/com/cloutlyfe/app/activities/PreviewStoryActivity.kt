package com.cloutlyfe.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.*
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.FilterActivity.Companion.VideoUrii
import com.cloutlyfe.app.databinding.ActivityAddStoryBinding

import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_add_story.*
import java.io.File
import java.util.*


class PreviewStoryActivity : BaseActivity(),Player.EventListener  {
    lateinit var binding: ActivityAddStoryBinding
    var mBitmap: Bitmap? = null
    var exoPlayer: SimpleExoPlayer? = null
    var activity: Activity = this@PreviewStoryActivity
    var videoUri: Uri? = null
    var mLastClickkTab1: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)


        getIntentData()

        binding.imgBackIV.setOnClickListener {
            onBackPressed()

        }

        binding.imgFilterIV.setOnClickListener {
            val intent = Intent(activity, FilterActivity::class.java)
            intent.putExtra("videoUri", uri.toString())
            intent.putExtra("file", file)
            startActivityForResult(intent, 102)
//            receiveDataa.launch(intent)
        }


        addStoryLL.setOnClickListener(View.OnClickListener {
            showStroryProgressDialog(activity)
                if (SystemClock.elapsedRealtime() - mLastClickkTab1 < 4000) {
                    return@OnClickListener
                }
                mLastClickkTab1 = SystemClock.elapsedRealtime()
//                    executeMulitpleStatusImage()
        })

    }



    override fun onBackPressed() {
        super.onBackPressed()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
        finish()}



var uri:String?=null
var file: String?=null

    private fun getIntentData() {
        uri=intent.getStringExtra("data")
        file=intent.getStringExtra("file")
        initializePlayer(Uri.parse(uri))
    }


    var progressStoryDialog: Dialog? = null

    /*
    * Show Progress Dialog
    * */
    fun showStroryProgressDialog(activity: Activity?) {
        progressStoryDialog = Dialog(activity!!)
        progressStoryDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressStoryDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressStoryDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressStoryDialog!!.setCanceledOnTouchOutside(false)
        progressStoryDialog!!.setCancelable(false)
//        if (progressStoryDialog != null) progressStoryDialog!!.show()
        progressStoryDialog!!.show()
    }

    /*
      * Hide Progress Dialog
      * */
    fun dismissStroryProgressDialog() {
        if (progressStoryDialog != null && progressStoryDialog!!.isShowing) {
            progressStoryDialog?.dismiss()
        }
    }


    override fun onResume() {
        super.onResume()
        if (VideoUrii.toString() != "null" && VideoUrii.toString() != "" && VideoUrii.toString() != null) {
            binding.idExoPlayerVIew.visibility = View.VISIBLE
            binding.surfaceView.visibility = View.GONE
            binding.cameraView.visibility = View.GONE
            binding.picRL.visibility = View.GONE
            Log.e(TAG, "onResume: " + VideoUrii)
            initializePlayer(VideoUrii)
            binding.addStoryLL!!.visibility = View.VISIBLE
            binding.startttBtn.visibility = View.GONE
            binding.addImgRL.visibility = View.GONE
            binding.imgSwitchCameraIV.visibility = View.GONE
        } else {
            VideoUrii = null
        }
    }

    fun initializePlayer(link: Uri?) {
        if (!link!!.equals("")) {
//            binding.idExoPlayerVIew.visibility = View.VISIBLE

            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

            val loadControl =
                DefaultLoadControl.Builder().setBufferDurationsMs(25000, 50000, 100, 300)
                    .createDefaultLoadControl()

            val renderersFactory =
                DefaultRenderersFactory(activity).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(activity, renderersFactory).build()

            val dataSourceFactory =
                DefaultDataSourceFactory(activity, Util.getUserAgent(activity, "CloutLyfe"))

            val mediaSources =
                ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(link)
            exoPlayer!!.prepare(mediaSources)
            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
            binding.idExoPlayerVIew.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
            binding.idExoPlayerVIew.player = exoPlayer
            exoPlayer!!.playWhenReady = true
//            exoPlayer!!.seekTo(0)
            binding.idExoPlayerVIew.requestFocus()


            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {
                    if (isLoading) {
//                        showStroryProgressDialog(activity)
                    } else {
                        dismissStroryProgressDialog()
                    }
                }

                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            dismissStroryProgressDialog()
                            exoPlayer!!.setPlayWhenReady(true)
//                            progressBarVid.visibility = View.GONE
                        }
                        Player.STATE_BUFFERING -> {
//                            showStroryProgressDialog(activity)
                            exoPlayer!!.seekTo(0)
//                            progressBarVid.visibility = View.VISIBLE
                        }
                        else -> {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }

                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {
                    dismissStroryProgressDialog()
                }

                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {}
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }
    var videoThumbnail_bitmap: Bitmap? = null
//    private fun executeMulitpleStatusImage() {
//        val photoList = ArrayList<MultipartBody.Part>()
//        var videoMultipartBody: MultipartBody.Part? = null
//        var mMultipartBody: MultipartBody.Part? = null
//
//        if (videoThumbnail_bitmap != null) {
//            val requestFile1: RequestBody? =
//                convertBitmapToByteArrayUncompressed(videoThumbnail_bitmap!!)?.let {
//                    RequestBody.create(
//                        "multipart/form-data".toMediaTypeOrNull(),
//                        it
//                    )
//                }
//            videoMultipartBody = requestFile1?.let {
//                MultipartBody.Part.createFormData(
//                    "video_thumbnail",
//                    getAlphaNumericString()!!.toString() + ".png",
//                    it
//                )
//            }
//        }
//
//        if (bitmapCamera != null) {
//            val requestFile1: RequestBody? =
//                convertBitmapToByteArrayUncompressed(bitmapCamera!!)?.let {
//                    RequestBody.create(
//                        "multipart/form-data".toMediaTypeOrNull(),
//                        it
//                    )
//                }
//            mMultipartBody = requestFile1?.let {
//                MultipartBody.Part.createFormData(
//                    "status_images[]",
//                    getAlphaNumericString()!!.toString() + ".jpeg",
//                    it
//                )
//            }
//            if (mMultipartBody != null) {
//                photoList.add(mMultipartBody)
//            }
//        }
//        else {
//            // val photoList = ArrayList<MultipartBody.Part>()
//            selectedImagesAsFile.indices.forEach {
////                val file2= getFilePath(selectedImagesAsFile[it])
//                val file2 = File(selectedImagesAsFile[it].path)
//                if (selectedImagesAsFile[it].path!!.contains("mp4")) {
//                    val surveyBody: RequestBody = RequestBody.create(
//                        "video/*".toMediaTypeOrNull(),
//                        convertFileToByteArray(file2)!!
//                    )
//                    val multipartImg =
//                        MultipartBody.Part.createFormData("status_images[]", file2.path, surveyBody)
//                    photoList.add(multipartImg)
//                    Log.e(TAG, "executeMulitpleStatusImage: " + photoList)
//
//                } else {
//                    val surveyBody: RequestBody = RequestBody.create(
//                        "image/*".toMediaTypeOrNull(),
//                        convertFileToByteArray(file2)!!
//                    )
//                    val multipartImg =
//                        MultipartBody.Part.createFormData("status_images[]", file2.path, surveyBody)
//                    photoList.add(multipartImg)
//                    Log.e(TAG, "executeMulitpleStatusImage: " + photoList)
//
//                }
//            }
//        }
//
//        val user_id: RequestBody =
//            AppPrefrences().readString(activity, Constants.ID, null)!!
//                .toRequestBody("multipart/form-data".toMediaTypeOrNull())
//
//        val duration: RequestBody =
//            timeInMillisec!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
//
////        showStroryProgressDialog(activity)
//
//        val headers: MutableMap<String, String> = HashMap()
//        headers["token"] = getAuthToken()
//
//        val call = RetrofitInstance.appApi.addMultipleStatusImage(
//            headers,
//            photoList,
//            user_id,
//            duration, videoMultipartBody
//        )
//
//        call.enqueue(object : Callback<StatusModel> {
//            override fun onResponse(
//                call: Call<StatusModel>,
//                response: Response<StatusModel>
//            ) {
//                val mMultipleStatusImageModel = response.body()
//                if (mMultipleStatusImageModel?.status == 1) {
//                    is_click = false
//                    dismissStroryProgressDialog()
//                    showToast(activity, mMultipleStatusImageModel.message)
//                    onBackPressed()
//                } else if (mMultipleStatusImageModel?.status == 0) {
//                    dismissStroryProgressDialog()
//                    showAlertDialog(activity, mMultipleStatusImageModel.message)
//                } else if (mMultipleStatusImageModel?.status == 3) {
//                    dismissStroryProgressDialog()
//                    showAlertDialog(activity, mMultipleStatusImageModel.message)
//                } else {
//                    dismissStroryProgressDialog()
//                    showAlertDialog(activity, mMultipleStatusImageModel?.message)
//                }
//            }
//
//            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
//                Log.e(TAG, t.message.toString())
//                Log.e(TAG, t.message.toString())
//                dismissStroryProgressDialog()
//            }
//        })
//    }


    override fun onDestroy() {
        super.onDestroy()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onPause() {
        super.onPause()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onStop() {
        super.onStop()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

}
