package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.HomeActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.MySavedlistAdapter
import com.cloutlyfe.app.adapters.ReportAdapter
import com.cloutlyfe.app.databinding.ActivityOtherUserProfileBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.ProfilePostsLoadMoreListener
import com.cloutlyfe.app.interfaces.ReportItemClickInterface
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.utils.SpannedGridLayoutManager
import com.cloutlyfe.app.viewmodel.FollowUserViewmodel
import com.cloutlyfe.app.viewmodel.GetReportReasonsViewModel
import com.cloutlyfe.app.viewmodel.ProfileViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OtherUserProfileActivity : BaseActivity() {
    var manager: SpannedGridLayoutManager? = null
    lateinit var profileViewModel: ProfileViewModel
    lateinit var followViewModel: FollowUserViewmodel
    lateinit var binding: ActivityOtherUserProfileBinding
    var mNotificationsList: ArrayList<String> = ArrayList<String>()
    var dataList: ArrayList<HomeData> = ArrayList()
    lateinit var mProfileDataModel: HomeModel
    var mType: String = ""
    var is_blocked: String = ""

    //    var otherUserAdapter: OtherUserAdapter? = null
    var otherUserAdapter: MySavedlistAdapter? = null
    var homeData: HomeData? = null
    var myHome: HomeData? = null
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mPerPage: Int = 10
    var mPostId: String = ""
    var mOtherUserID: String = ""
    var other_user_id: String = ""
    var name: String = ""
    var value: String = ""
    var following: Int = 0
    var profileData: ProfileData? = null
    var profile_url: String? = ""
    var redirect_value: String? = ""
    var block_type: String? = ""
    var mTabType: String = "1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtherUserProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()


        binding.profileIV.setOnClickListener {
            if(!profile_url.isNullOrEmpty()){
            openPicDialog(mActivity, profile_url!!)}
        }

        if(!intent.getStringExtra("redirect_value").isNullOrEmpty()){
            redirect_value=intent.getStringExtra("redirect_value")
        }

        binding.header.imgBackIV.setOnClickListener {
            if(redirect_value=="push"){
                var intent = Intent(mActivity, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }
            else{
                onBackPressed()
            }
        }

        binding.header.optionsIV.setOnClickListener {
            if(other_user_id== AppPrefrences().readString(mActivity, Constants.ID,null)){
               binding.header.optionsIV.visibility=View.GONE
            }
            else{
                showReportBlockDialog()
            }

        }

        binding.txtMessageTV.setOnClickListener {
            if (binding.txtMessageTV.text == getString(R.string.manageP)) {
                var intent = Intent(mActivity, AllPostsActivity::class.java)
                intent.putExtra("value", "Manage Post")
                intent.putExtra("user_id", getLoggedInUserID())
                intent.putExtra("type", "3")
                startActivity(intent)
            } else {
                if(homeData!!.username=="N/A"||homeData!!.username==""||homeData!!.username==null){
                    executeCreateRoomApi(homeData, homeData?.name)
                }
                else{
                    executeCreateRoomApi(homeData, homeData?.username)
                }


            }

        }

        binding.txtFollowTV.setOnClickListener {
            if (binding.txtFollowTV.text == getString(R.string.ep)) {
                val intent = Intent(mActivity, EditProfileActivity::class.java)
                startActivity(intent)
            }
            else if((binding.txtFollowTV.text == "Unblock")){
                is_blocked="2"
                executeBlockUnblockRequestApi()

            }
            else {
                executeFollowClick()
            }

//            val intent = Intent( mActivity, EditProfileActivity::class.java)
//           startActivity(intent)
        }

        binding.cloutLL.setOnClickListener {
            val intent = Intent(mActivity, CloutCloutersActivity::class.java)
            intent.putExtra("value", "clout")
            intent.putExtra("clout", "other_user")
            intent.putExtra("other_id", mOtherUserID)
            startActivity(intent)
        }
        binding.cloutersLL.setOnClickListener {
            val intent = Intent(mActivity, CloutCloutersActivity::class.java)
            intent.putExtra("value", "clouters")
            intent.putExtra("clout", "other_user")
            intent.putExtra("other_id", mOtherUserID)
            startActivity(intent)
        }
        binding.txtPostssTV.setOnClickListener {
//            val intent = Intent( mActivity, CloutCloutersActivity::class.java)
//            intent.putExtra("value","clouters")
//            startActivity(intent)
        }

        binding.allPostsLL.setOnClickListener {
//            preventtMultipleClick()
            mTabType = "1"
            performAllPostsClick("1")

        }
        binding.videoLL.setOnClickListener {
//            preventtMultipleClick()
            mTabType = "2"
            performVideoClick("2")

        }


//        setupSpannedGridLayout()
    }
    val reportResonslist: java.util.ArrayList<DataX> = java.util.ArrayList()
    lateinit var getReportReasonViewModel: GetReportReasonsViewModel
    var dialogBR: BottomSheetDialog?=null

    private fun showReportBlockDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.block_dialog, null)
        dialogBR =
            mActivity.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogBR!!.setContentView(view)
        dialogBR!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogBR!!.setCancelable(true)
//cancel button click
        val btnBlock: TextView? = dialogBR!!.findViewById(R.id.btnBlock)
        val btnCancel: TextView? = dialogBR!!.findViewById(R.id.btnCancel)
        val txtMessageTV: TextView? = dialogBR!!.findViewById(R.id.txtMessageTV)
        txtMessageTV!!.text="Please choose an action!"
        btnCancel!!.setText("REPORT/ABUSE")

        btnBlock?.setOnClickListener {
            dialogBR!!.dismiss()
            showBlockDialog()
            }

        btnCancel.setOnClickListener {
            if (reportResonslist != null) {
                reportResonslist.clear()
            }
            getReasons()
            dialogBR!!.dismiss()
        }
        dialogBR!!.show()
    }



    private fun getReasons() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performResonsRequest()
        }
    }

    private fun performResonsRequest() {
        var user_id = getLoggedInUserID()
        val body = RequestBodies.ReportReasonsBody(
            user_id
        )
        getReportReasonViewModel.getReportResons(body)
        getReportReasonViewModel.reportReasonsResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { getReportResonse ->
                            if (getReportResonse.status == 1 || getReportResonse.status == 200) {
                                reportResonslist.addAll(getReportResonse.data)
                                showBottomReportDialog()
                            } else {
                                showAlertDialog(mActivity, getReportResonse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })

    }
    private var mReportAdapter: ReportAdapter? = null
    var dialogReport: BottomSheetDialog? = null
    var reasonId: String? = null

    private fun showBottomReportDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.report_bottomsheet, null)
        dialogReport = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogReport!!.setContentView(view)
        dialogReport!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogReport!!.setCancelable(true)
//cancel button click
        val reportRV: RecyclerView? = dialogReport!!.findViewById(R.id.reportRV)

        reportRV!!.layoutManager = LinearLayoutManager(mActivity)
        mReportAdapter = ReportAdapter(
            mActivity,
            reportResonslist, mItemClickListener
        )
        reportRV.adapter = mReportAdapter

        dialogReport!!.show()
    }


    var mItemClickListener: ReportItemClickInterface = object : ReportItemClickInterface {
        override fun onItemClickListner(mId: String) {
            reasonId = mId
            performSubmitClick()
        }
    }

    private fun performSubmitClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performSubmit()
        }
    }

    private fun mReportParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_id"] = mOtherUserID
        mMap["reasonId"] = reasonId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun performSubmit() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.reportUserRequest(mHeaders,mReportParams())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                dismissProgressDialog()
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                dialogReport!!.dismiss()
                if (mModel!!.status == 1) {
                    showToast(mActivity,mModel.message)
                } else if (mModel.status == 0) {
                    showToast(mActivity,mModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }

    private var mLastClickTab1: Long = 0

    open fun preventtMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 5000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }


    override fun onBackPressed() {
        super.onBackPressed()

    }

    override fun onResume() {
        super.onResume()
        if (mTabType == "1") {
            binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_sel))
            binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_unsel))
            mType = "1"
            mTabType = "1"
        } else {
            binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_sel))
            binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_unsel))
            mType = "2"
            mTabType = "2"
        }
        getIntentData()
    }

    private fun showBlockDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.block_dialog, null)
        var dialogPrivacy: BottomSheetDialog? =
            mActivity.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
//cancel button click
        val btnBlock: TextView? = dialogPrivacy.findViewById(R.id.btnBlock)
        val btnCancel: TextView? = dialogPrivacy.findViewById(R.id.btnCancel)
        val txtMessageTV: TextView? = dialogPrivacy.findViewById(R.id.txtMessageTV)

        if(is_blocked=="1"){
//            is_blocked="2"
            txtMessageTV!!.setText("Are you sure you want to Unblock"+" "+binding.nameTV.text+"?")
//            binding.txtFollowTV.text = "Unblock"
            btnBlock!!.text = "Unblock"
        }
        else{
//            is_blocked="1"
            txtMessageTV!!.setText("Are you sure you want to block"+" "+binding.nameTV.text+"?")
           btnBlock!!.text = "Block"
//            binding.txtFollowTV.text = "Clout"
        }

        btnBlock?.setOnClickListener {
            if(is_blocked=="1"){
                is_blocked="2"
                txtMessageTV!!.setText("Are you sure you want to Unblock"+" "+binding.nameTV.text+"?")
                binding.txtFollowTV.text = "Unblock"
                btnBlock!!.text = "Unblock"
            }
            else{
                is_blocked="1"
                binding.txtFollowTV.text = "Clout"
                txtMessageTV!!.setText("Are you sure you want to Block"+" "+binding.nameTV.text+"?")
                btnBlock!!.text = "block"
                binding.txtMessageTV.visibility=View.GONE
            }
            executeBlockUnblockRequestApi()
            dialogPrivacy.dismiss()
        }
        btnCancel?.setOnClickListener {
            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }


    private fun executeBlockUnblockRequestApi() {
        if (isNetworkAvailable(mActivity)) {
            executeBlockUnBlockRequest()
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }


    private fun mUnblockParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["blocked_user_id"] = mOtherUserID
        mMap["type"] = is_blocked
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeBlockUnBlockRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.blockUnblockRequest(mHeaders,mUnblockParams())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                dismissProgressDialog()
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel!!.status == 1) {
                    showToast(mActivity,mModel.message)
                    if(is_blocked=="1"){
                        binding.txtFollowTV.text = "Unblock"
                        binding.txtMessageTV.visibility=View.GONE
                    }
                    else{
                        binding.txtFollowTV.text = "Clout"
                        binding.txtMessageTV.visibility=View.VISIBLE
                    }
                } else if (mModel.status == 0) {
                    showToast(mActivity,mModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }


    private fun executeCreateRoomApi(mModel: HomeData?, namee: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        mHeaders["Token"] = "931cbac9c9cf192912b21ae908a90e55"
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
//        mMap["user_id"] = "10"
        if (intent.getStringExtra("other_user_id").toString().isNotEmpty() && intent.getStringExtra(
                "other_user_id"
            ) != null
        ) {
            mMap["owner_id"] = other_user_id
        } else {
            mMap["owner_id"] = mModel?.user_id
        }

        Log.e(TAG, "**PARAM**$mMap")
        showProgressDialog(mActivity)

        val call = RetrofitInstance.appApi.createRoomRequest(mHeaders, mMap)
        call.enqueue(object : Callback<CreateRoomModel> {
            override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<CreateRoomModel>,
                response: Response<CreateRoomModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mCreateRoomModel = response.body()!!
                if (mCreateRoomModel.status == 1) {
                    var mIntent = Intent(mActivity, ChatActivity::class.java)
                    if (intent.getStringExtra("other_user_id").toString()
                            .isNotEmpty() && intent.getStringExtra("other_user_id") != null
                    ) {
                        mIntent.putExtra(Constants.USER_NAME, name)
                    } else {
                        mIntent.putExtra(Constants.USER_NAME, namee)
                    }

                    mIntent.putExtra(Constants.ROOM_ID, mCreateRoomModel.room_id)
                    startActivity(mIntent)
                } else {
                    showToast(mActivity, mCreateRoomModel.message)
                }
            }
        })
    }

    private fun performVideoClick(type: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 3000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        dataList.clear()
        binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_sel))
        binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_unsel))
        executeGetProfilePostsApi(type)
    }

    private fun performAllPostsClick(type: String) {
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 3000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        dataList.clear()
        binding.postImgIV.setImageDrawable(resources.getDrawable(R.drawable.ic_img_post_sel))
        binding.postVidIV.setImageDrawable(resources.getDrawable(R.drawable.ic_video_post_unsel))
        executeGetProfilePostsApi(type)
    }


    private fun executeFollowClick() {
        increaseFollowCount()

    }

    private fun executeFollowApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            followUserRequest()
        }
    }


    private fun increaseFollowCount() {
        if (profileData!!.changefollowing == "0") {
            profileData!!.changefollowing = "1"
            binding.txtFollowTV.text = getString(R.string.fo)
            binding.txtCloutTV.text = (following + 1).toString()
            following = following + 1
            executeFollowApi()
        } else {
            binding.txtCloutTV.text = (following - 1).toString()
            following = following - 1
            profileData!!.changefollowing = "0"
            binding.txtFollowTV.text = getString(R.string.follow)
            executeFollowApi()
        }
    }

    var followUserID: String? = ""
    private fun followUserRequest() {
        var user_id = getLoggedInUserID()
        if (intent.getStringExtra("other_user_id").toString().isNotEmpty() && intent.getStringExtra(
                "other_user_id"
            ) != null
        ) {
            followUserID = mOtherUserID
        } else {
            followUserID = homeData?.user_id
        }


        Log.e(TAG, "followUserRequest: " + followUserID)
        val body = RequestBodies.FollowBody(user_id, followUserID!!)
        followViewModel.getFollowData(getAuthToken(), body, mActivity)
        followViewModel.followResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { followResponse ->
                            if (followResponse.status == 1 || followResponse.status == 200) {

//                                    showToast(activity, loginResponse.message)
//                                binding.txtFollowTV.text=getString(R.string.foll)
//                                dismissProgressDialog()
                            } else {
//                                dismissProgressDialog()
                                showAlertDialog(mActivity, followResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
//                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.data?.message)
                        }
                    }

                    is Resource.Loading -> {
//                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    private fun getIntentData() {
        if (!intent.getStringExtra("other_user_id").isNullOrEmpty()) {
            other_user_id = intent.getStringExtra("other_user_id").toString()
            name = intent.getStringExtra("name").toString()
            binding.header.txtHeadingTV.text = name
//            value = intent.getStringExtra("value").toString()
            mOtherUserID = other_user_id
            binding.nameTV.text = name

            if (intent.getStringExtra("value") == "logged_in_user") {
                binding.txtFollowTV.text = getString(R.string.ep)
                binding.txtMessageTV.text = getString(R.string.manageP)
                binding.header.optionsIV.visibility=View.GONE
            }
            setUpProfileData()

        } else {
            val gson = Gson()
            val data = intent.getStringExtra("homeData")

            homeData = gson.fromJson(
                data,
                HomeData::class.java
            )

try {
    myHome = gson.fromJson(
        data,
        HomeData::class.java
    )
}

catch (e:Exception){
    Log.e(TAG, "getIntentData: "+e.localizedMessage )
}
if(homeData!!.username=="N/A"||homeData!!.username==""||homeData!!.username==null){
    binding.header.txtHeadingTV.text = homeData!!.name
    binding.nameTV.text = homeData?.name
}
            else{
    binding.header.txtHeadingTV.text = homeData!!.username
    binding.nameTV.text = homeData?.name
            }

            mPostId = homeData?.post_id.toString()
            mOtherUserID = homeData?.user_id.toString()


            if (intent.getStringExtra("value") == "logged_in_user") {
                binding.txtFollowTV.text = getString(R.string.ep)
                binding.txtMessageTV.text = getString(R.string.manageP)
                binding.header.optionsIV.visibility=View.GONE
                Log.e(TAG, "getIntentData: "+value )
            }
            setUpProfileData()
            value = intent.getStringExtra("value")!!

        }
//        executeGetCommentsApi(mPostId)
    }

    private fun setDataOnViews() {

    }


    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        getReportReasonViewModel = ViewModelProvider(this,factory).get(GetReportReasonsViewModel::class.java)
        profileViewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        followViewModel = ViewModelProvider(this, factory).get(FollowUserViewmodel::class.java)
    }


    private fun setUpProfileData() {
//        binding.nameTV.text = homeData?.name
        executeProfileApi()
    }

    private fun executeProfileApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestProfileData()
        }
    }

    private fun RequestProfileData() {
        var user_id: String? = null
        var followUserID: String? = null
        if (intent.getStringExtra("value") == "logged_in_user") {
            user_id = getLoggedInUserID()
            followUserID = getLoggedInUserID()
        } else {
            user_id = getLoggedInUserID()
            followUserID = mOtherUserID
        }
        val body = RequestBodies.ProfileBody(user_id, followUserID)
        profileViewModel.getProfileData(getAuthToken(), body, mActivity)
        profileViewModel.profileResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { profileResponse ->
                            if (profileResponse.status == 1 || profileResponse.status == 200) {
                                profileData = profileResponse.data
                                is_blocked = profileData!!.is_blocked
                                following = Integer.parseInt(response.data.Followers)

//                                if (intent.getStringExtra("value") == "logged_in_user") {
//                                    Glide.with(mActivity)
//                                        .load(AppPrefrences().readString(mActivity,Constants.PROFILE_IMAGE,null))
//                                        .placeholder(R.drawable.placeholder_clout)
//                                        .error(R.drawable.placeholder_clout)
//                                        .into(binding.imgCoverIV)
//                                } else {
                                    Glide.with(mActivity)
                                        .load(profileResponse.data.photo)
                                        .placeholder(R.drawable.ic_profile_ph)
                                        .error(R.drawable.ic_profile_ph)
                                        .into(binding.profileIV)
                                if(mOtherUserID.isNullOrEmpty()){
                                AppPrefrences().writeString(mActivity, Constants.PROFILE_IMAGE,profileResponse.data.photo)
                                AppPrefrences().writeString(mActivity, Constants.NAME,profileResponse.data.name)}
                                if(profileResponse.data.username=="N/A"||profileResponse.data.username==""||profileResponse.data.username==null){
                                    binding.header.txtHeadingTV.text=profileResponse.data.name
                                    binding.nameTV.text=profileResponse.data.name
                                }
                                else{
                                    binding.header.txtHeadingTV.text=profileResponse.data.username
                                    binding.nameTV.text=profileResponse.data.name
                                }

                                if(intent.getStringExtra("value") == "logged_in_user"){

                                }
                                else{
                                    if (profileData!!.changefollowing == "0") {
                                        binding.txtFollowTV.text = getString(R.string.follow)
                                    } else {
                                        binding.txtFollowTV.text = getString(R.string.fo)
                                    }

                                    if(profileData!!.is_blocked=="1"){
                                        binding.txtFollowTV.text = "Unblock"
                                        binding.txtMessageTV.visibility=View.GONE
                                    }
                                }


//                                }

//                                    showToast(activity, loginResponse.message)
                                binding.txtPostsTV.text = profileResponse.postCount
                                binding.txtCloutTV.text = profileResponse.Followers
                                binding.txtCloutersTV.text = profileResponse.Followings
                                binding.txtArtistTV.text = profileResponse.data.bio


                                profile_url = profileResponse.data.photo

                                Glide.with(mActivity)
                                    .load(profileResponse.data.cover_image)
                                    .placeholder(R.drawable.placeholder_clout)
                                    .error(R.drawable.placeholder_clout)
                                    .into(binding.imgCoverIV)
                                executeGetProfilePostsApi("1")
                                dismissProgressDialog()
                            } else {
                                dismissProgressDialog()
                                showAlertDialog(mActivity, profileResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.data?.message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

//    // Sample usage from your Activity/Fragment
//    private fun setupSpannedGridLayout() {
//        val postItems: MutableList<PostItem> = mutableListOf()
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
////        postItems.add(PostItem(R.drawable.ic_dummy))
//
//        manager = SpannedGridLayoutManager(
//            object : SpannedGridLayoutManager.GridSpanLookup {
//                override fun getSpanInfo(position: Int): SpannedGridLayoutManager.SpanInfo {
//                    if (((position -4)%6 == 0) || ((position -4)%6 == 2))
//                    {
//                        if (position==postItems.size-2){
//                            return SpannedGridLayoutManager.SpanInfo(1, 1)
//                        }
//                        else{
//                            return SpannedGridLayoutManager.SpanInfo(2, 2)
//                        }
//                    } else {
//                        return SpannedGridLayoutManager.SpanInfo(1, 1)
//                    }
//                }
//            },
//            3,  // number of columns
//            1f // how big is default item
//        )
//
//        binding.savedRV.layoutManager = manager
//        otherUserAdapter = OtherUserAdapter(postItems)
//        binding.savedRV.adapter = otherUserAdapter
//    }


    // Sample usage from your Activity/Fragment
    private fun setupSpannedGridLayout(
        activity: FragmentActivity,
        dataList: ArrayList<HomeData>,
        mLoadMoreScrollListner: ProfilePostsLoadMoreListener,
        mType: String
    ) {

        manager = SpannedGridLayoutManager(
            object : SpannedGridLayoutManager.GridSpanLookup {
                override fun getSpanInfo(position: Int): SpannedGridLayoutManager.SpanInfo {
                    if (dataList.size == 1) {
                        dataList.addAll(mProfileDataModel.data)
                    }
                    if (((position - 4) % 6 == 0) || ((position - 4) % 6 == 2)) {
                        if (position == dataList.size - 2) {
                            return SpannedGridLayoutManager.SpanInfo(1, 1)
                        } else {
                            return SpannedGridLayoutManager.SpanInfo(2, 2)
                        }
                    } else {
                        return SpannedGridLayoutManager.SpanInfo(1, 1)
                    }
                }
            },
            3,  // number of columns
            1f // how big is default item
        )

        binding.savedRV.layoutManager = manager
        otherUserAdapter = MySavedlistAdapter(activity, dataList, mLoadMoreScrollListner, mType)
        binding.savedRV.adapter = otherUserAdapter
        otherUserAdapter!!.notifyDataSetChanged()
    }


    private fun executeGetProfilePostsApi(type: String) {
        mType = type
        dataList.clear()
        if (isNetworkAvailable(mActivity))
            RequestProfilePostsData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_userid"] = mOtherUserID
        mMap["type"] = mType
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestProfilePostsData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
//        showProgressDialog(activity)
        val call = RetrofitInstance.appApi.getProfilePostsRequest(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mProfileDataModel = response.body()!!
                if (mProfileDataModel.status == 1) {

                    isLoading = !mProfileDataModel.lastPage.equals(true)
                    dataList.addAll(response.body()!!.data)

                    Log.e(TAG, "RequestHomeDataAPI: " + dataList)
                    if (dataList.size == 0) {
                        binding.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                        binding.txtNoDataTV.visibility = View.VISIBLE

                    } else {
                        setupSpannedGridLayout(
                            mActivity as FragmentActivity,
                            dataList,
                            mLoadMoreScrollListner,
                            mType
                        )
                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mProfileDataModel.status == 3) {

//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else if (mProfileDataModel.status == 0) {
                    binding.txtNoDataTV.text =
                        getString(R.string.no_posts_available)
                    binding.txtNoDataTV.visibility = View.VISIBLE
//binding.txtNoDataTV.text= "No posts found"
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

//                    binding.txtNoDataTV.visibility = View.VISIBLE
//                    binding.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    var mLoadMoreScrollListner: ProfilePostsLoadMoreListener =
        object : ProfilePostsLoadMoreListener {
            override fun onLoadMoreListner(mModel: ArrayList<ProfilePostsData>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
            }
        }


    fun openPicDialog(context: Context, image_url: String) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.item_profile_img_dialog)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(true)
//        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation

        val photoView = dialog.findViewById(R.id.photoView) as PhotoView
        val window = dialog.window
        window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.setGravity(Gravity.CENTER)
        val lp = dialog.window!!.attributes
        lp.dimAmount = 0.6f

        dialog.window!!.attributes = lp
        dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.show()

        Glide.with(context)
            .load(image_url)
            .placeholder(R.drawable.ic_profile_ph)
            .into(photoView)

    }


}