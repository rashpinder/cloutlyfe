package com.cloutlyfe.app.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentUris
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.*
import android.os.StrictMode.ThreadPolicy
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.HomeActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.TrimVideoActivity
import com.cloutlyfe.app.TrimVideoActivity.Companion.trimmedVideoUri
import com.cloutlyfe.app.adapters.EditPostAdapter
import com.cloutlyfe.app.databinding.ActivityEditPostBinding
import com.cloutlyfe.app.interfaces.OptionsInterface
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.DecodeImage
import com.cloutlyfe.app.utils.FileHelper
import com.github.dhaval2404.imagepicker.ImagePicker
import com.khostul.app.utils.HandleAndroidPermissions
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
//import com.lassi.common.utils.KeyUtils
//import com.lassi.data.media.MiMedia
//import com.lassi.domain.media.LassiOption
//import com.lassi.domain.media.MediaType
//import com.lassi.presentation.builder.Lassi
//import com.services.safe4renterprise.appUtils.DecodeImage
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.URISyntaxException


class EditPostActivity : BaseActivity() {
    lateinit var binding: ActivityEditPostBinding
    var editPostAdapter: EditPostAdapter? = null
    var post_data: ProfilePostsData? = null
    var mOtheruserId: String? = null
    var mPostId: String = ""
    var uriList = ArrayList<Uri>()
    var deletedUrlList = ArrayList<String>()
    var imageList = ArrayList<Bitmap>()
    var videoThumbnail_bitmap: Bitmap? = null
    var mArrayList: ArrayList<Uri> = ArrayList<Uri>()
    var mDeletedPostIds: ArrayList<String> = ArrayList<String>()
    var mPreviousPostsList: ArrayList<DataOfPost> = ArrayList<DataOfPost>()
    var mThumbnailList: ArrayList<Uri> = ArrayList<Uri>()
    var mLatitude: String = ""
    var mLongitude: String = ""
    var mtype: String = "1"
    var count: Int = 10
    val REQUEST_CODE = 100
    private val SELECT_IMAGE = 1
    private val CAPTURE_VIDEO = 2
    var mBitmap: Bitmap? = null
    var videoFullPath: String? = null
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA

        )
    }

    companion object{
        var lati=""
        var longitu=""
        var addressLocation=""
    }

    override fun onResume() {
        super.onResume()
        if (addressLocation !=""){
            binding.editLocationET.setText(addressLocation)
            mLatitude= lati
            mLongitude= longitu
        }

        if(trimmedVideoUri.toString()!="null" && trimmedVideoUri.toString()!="" && trimmedVideoUri.toString()!=null){
            if (uriList != null && uriList.size > 0) {
                if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
                ) {
                    showToast(mActivity, "You can add a single video only")
                } else {
                    showToast(mActivity, "You can add images only")
                }
            } else {
                try {
//                val uri = data!!.getStringExtra("TRIMMED_VIDEO_URI")
//                    saveVideo(uri!!.toUri())
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(TrimVideoActivity.trimmedVideoUri.toString())
                    val time =
                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    val VideoDuration = time!!.toInt()
                    Log.e(TAG, "onActivityResult: " + VideoDuration)

//                    if (VideoDuration >= 60000) {
////                        startTrimActivity(uri)
////                        showToast(activity, "You can upload video upto 1 minute")
//                    } else {
                    videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                        TrimVideoActivity.trimmedVideoUri!!.toString(),
                        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
                    mArrayList.add(Uri.fromFile(File(TrimVideoActivity.trimmedVideoUri!!.path.toString())))
//                    mArrayList.add(addVideo(File(uri!!.toUri().path))!!)
                    imageList.add(videoThumbnail_bitmap!!)
                    setAdapter(mPreviousPostsList, mArrayList)
                    uriList.add(Uri.fromFile(File(TrimVideoActivity.trimmedVideoUri!!.path.toString())))
//                    uriList.add(addVideo(File(uri!!.toUri().path))!!)
//                    }
//            manageVideo(path) //Do whatever you want with your vid9eo
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }}

    }

    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditPostBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        if (ContextCompat.checkSelfPermission(
                this@EditPostActivity,
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                this@EditPostActivity,
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(this@EditPostActivity, PERMISSIONS, PERMISSION_ALL)
        }
        binding.header.txtHeadingTV.text = getString(R.string.edit_postt)
        getIntentData()
        binding.addPostRL.setOnClickListener {
            performAddPostlick()
        }

        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.txtUpdateTV.setOnClickListener {
            executeUpdatePostApi()
        }
        binding.locationLL.setOnClickListener {
//            showProgressDialog(activity)
            performLocationClick()
        }
    }


    var mDataModel: GetPostDetailsModel?=null
    private fun executeGetPostDetailsApi() {
        if (isNetworkAvailable(mActivity))
            RequestGetPostDetailsData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mPostParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        return mMap
    }
    private fun RequestGetPostDetailsData() {
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getPostDetailsRequest(mPostParam())
        call.enqueue(object : Callback<GetPostDetailsModel> {
            override fun onFailure(call: Call<GetPostDetailsModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetPostDetailsModel>,
                response: Response<GetPostDetailsModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mDataModel = response.body()
                if (mDataModel!!.status == 1) {
                    mPreviousPostsList.add(mDataModel!!.data)
                    setDataOnViews()
                } else if (mDataModel!!.status == 3) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                } else if (mDataModel!!.status == 2) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                } else if (mDataModel!!.status == 0) {

                } else {
//                    dismissProgressDialog()
                }
            }
        })
    }


    private var mLastClickCity: Long = 0

    private fun performLocationClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickCity < 1500) {
            return
        }
        mLastClickCity = SystemClock.elapsedRealtime()
        val mIntent = Intent(mActivity, SearchLocationActivity::class.java)
        startActivityForResult(mIntent, 567)
    }


    private fun performAddPostlick() {
        if (mArrayList.size == 10) {
            showToast(mActivity, getString(R.string.you_can_add_max_of_10image))
        }
        if (uriList != null && uriList.size > 0) {
            if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")|| uriList.get(0).path!!.contains("thumbnail")) {
                showToast(mActivity, "You can add a single video only")
            } else {
                if (mArrayList.size == 10) {
                    showToast(mActivity, getString(R.string.you_can_add_max_of_10image))
                } else {
                    performAddClick(
                    )
                }
            }
        } else {
            performAddClick()
        }
    }

    fun performAddClick() {
            Pick()
    }


    fun Pick() {
        val alertDialog = Dialog(mActivity)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtCameraTV = alertDialog.findViewById<TextView>(R.id.txtCameraTV)
        val txtCaptureVideoTV = alertDialog.findViewById<TextView>(R.id.txtCaptureVideoTV)
        val txtGalleryTV = alertDialog.findViewById<TextView>(R.id.txtGalleryTV)
        val txtPickVideoTV = alertDialog.findViewById<TextView>(R.id.txtPickVideoTV)
        val btnCancelTV = alertDialog.findViewById<TextView>(R.id.btnCancelTV)
        val videoView = alertDialog.findViewById<View>(R.id.videoView)
        val pickvideoView = alertDialog.findViewById<View>(R.id.pickvideoView)

        btnCancelTV.setOnClickListener { alertDialog.dismiss() }
        if (uriList != null && uriList.size > 0) {
            if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")|| uriList.get(0).path!!.contains("thumbnail")
            ) {
                txtCaptureVideoTV.visibility = View.GONE
                videoView.visibility = View.GONE
                pickvideoView.visibility = View.GONE
                txtPickVideoTV.visibility = View.GONE
            }
            if (uriList.toString().contains(".jpg") || uriList.toString().contains(".jpeg")|| uriList.toString()
                    .contains(".png") || uriList.toString()
                    .contains("image") || uriList.toString().contains(".gif")||uriList.toString().contains(".PNG") || uriList.toString().contains(".JPEG") || uriList.toString().contains(
                    ".JPG"
                ) || uriList.toString().contains("IMAGE") || uriList.toString().contains(".GIF")
            ) {
                txtCaptureVideoTV.visibility = View.GONE
                videoView.visibility = View.GONE
                pickvideoView.visibility = View.GONE
                txtPickVideoTV.visibility = View.GONE
            }
//                showToast(activity, "You can add a single video only")
        }


        txtCaptureVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performCaptureVideoClick()
            mtype = "2"
        }
        txtCameraTV.setOnClickListener {
            alertDialog.dismiss()
            performCameraClick()
            mtype = "1"
        }
        txtGalleryTV.setOnClickListener {
            alertDialog.dismiss()
            performGalleryClick()
            mtype = "1"
        }
        txtPickVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performPickVideoClick()
            mtype = "2"
        }
        alertDialog.show()
    }


    private fun performCameraClick() {
        ImagePicker.with(this) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)
            //Final image resolution will be less than 1080 x 1080(Optional)
            .cameraOnly().crop()
            .start(SELECT_IMAGE)
    }


    private fun performGalleryClick() {
        openGalleryForImage()
    }

    private fun openGalleryForImage() {
        val intent = Lassi(mActivity)
            .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA or CAMERA_AND_GALLERY
            .setMaxCount(count)
            .setGridSize(3)
            .setMediaType(MediaType.IMAGE) // MediaType : VIDEO IMAGE, AUDIO OR DOC
            .build()
        receiveData.launch(intent)

    }

    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
                if (!selectedMedia.isNullOrEmpty()) {
                    Log.e("imagesLoadSize", selectedMedia.size.toString())
//                    count = count - selectedMedia.size
                    count=10-(uriList.size+selectedMedia.size)
                    Log.e(TAG, "counttt: "+count)
                    for (i in 0..selectedMedia.size - 1) {
                        val uri: Uri = Uri.fromFile(File(selectedMedia.get(i).path))
                        mArrayList.add(uri)
                        uriList.add(uri)
                    }
//                    count=count-mArrayList.size
                    Log.e(TAG, "counttt: "+count )
                    setAdapter(mPreviousPostsList, mArrayList)
                }
            }
        }


    private fun performPickVideoClick() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        intent.type = "video/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select Video"),
            357
        )

    }

    private fun performCaptureVideoClick() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,60)
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0) // Quality Low
//    takeVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 5491520L) // 5MB
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, CAPTURE_VIDEO)
        }
    }


    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var selectedMediaUri: Uri? = null
        if (requestCode == 357) {
            val selectedImageUri: Uri? = data?.data
            if (selectedImageUri != null) {
                val intent = Intent(
                    mActivity,
                    TrimVideoActivity::class.java
                )
                intent.putExtra("EXTRA_VIDEO_PATH", selectedImageUri)
                startActivity(intent)
            }
        }
        else if (requestCode == 2296) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // perform action when allow permission success
                } else {
                    Toast.makeText(mActivity, "Allow permission for storage access!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
        else if (requestCode == 800 && data!=null) {
            if (uriList != null && uriList.size > 0) {
                if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")
                ) {
                    showToast(mActivity, "You can add a single video only")
                } else {
                    showToast(mActivity, "You can add images only")
                }
            } else {
                try {
                    val uri = data!!.getStringExtra(Constants.TRIMMED_VIDEO_URI)
//                    saveVideo(uri!!.toUri())
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(uri)
                    val time =
                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    val VideoDuration = time!!.toInt()
                    Log.e(TAG, "onActivityResult: " + VideoDuration)

//                    if (VideoDuration >= 60000) {
////                        startTrimActivity(uri)
////                        showToast(activity, "You can upload video upto 1 minute")
//                    } else {
                    videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                        uri!!,
                        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
//                    mArrayList.add(addVideo(File(uri!!.toUri().path))!!)
                    mArrayList.add(Uri.fromFile(File(uri.toUri().path.toString())))
                    imageList.add(videoThumbnail_bitmap!!)
                    setAdapter(mPreviousPostsList, mArrayList)
//                    uriList.add(addVideo(File(uri!!.toUri().path))!!)
                    uriList.add(Uri.fromFile(File(uri.toUri().path.toString())))
//                    }
//            manageVideo(path) //Do whatever you want with your video
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        else if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            if (data != null) {
                if (uriList != null && uriList.size > 0) {
                    if (uriList.get(0).path!!.contains(".mp4") || uriList.get(0).path!!.contains("video")|| uriList.get(0).path!!.contains("thumbmail")
                    ) {
                        showToast(mActivity, "You can add a single video only")
                    } else {
                        try {
                            selectedMediaUri = data?.data!!

                            val fileHelper = FileHelper(getApplicationContext())
                            val picturePath = fileHelper.getRealPathFromUri(selectedMediaUri)
//                        var picturePath = getRealPathFromURI(selectedMediaUri!!)
                            val bmp = BitmapFactory.decodeFile(
                                DecodeImage.decodeFile(
                                    picturePath!!,
                                    600,
                                    600
                                )
                            )
                            val bounds = BitmapFactory.Options()
                            bounds.inJustDecodeBounds = true
                            BitmapFactory.decodeFile(picturePath, bounds)
                            val opts = BitmapFactory.Options()
                            val bm = BitmapFactory.decodeFile(picturePath, opts)
                            val exif = ExifInterface(picturePath!!)
                            val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
                            val orientation =
                                if (orientString != null) Integer.parseInt(orientString) else ExifInterface.ORIENTATION_NORMAL
                            var rotationAngle = 0
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle =
                                90
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle =
                                180
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle =
                                270
                            val matrix = Matrix()
                            matrix.setRotate(
                                rotationAngle.toFloat(),
                                bm.width.toFloat() / 2,
                                bm.height.toFloat() / 2
                            )

                            val rotatedBitmap = Bitmap.createBitmap(
                                bm,
                                0,
                                0,
                                bounds.outWidth,
                                bounds.outHeight,
                                matrix,
                                true
                            )

                            val bytes = ByteArrayOutputStream()
                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                            val path = MediaStore.Images.Media.insertImage(
                                contentResolver,
                                rotatedBitmap,
                                "Clout",
                                null
                            )
//                        selectedMediaUri = activity?.let { getImageUri(it, rotatedBitmap) }
                            selectedMediaUri = Uri.parse(path)
                            uriList.add(selectedMediaUri!!)
                            mArrayList.add(selectedMediaUri!!)
                            setAdapter(mPreviousPostsList, mArrayList)
                            count=10-uriList.size
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    // Toast.makeText(activity, "Cancel", Toast.LENGTH_SHORT).show()
                } else {
                    try {
                        selectedMediaUri = data?.data!!
                        mBitmap = MediaStore.Images.Media.getBitmap(
                            getContentResolver(),
                            selectedMediaUri
                        )
                        val fileHelper = FileHelper(getApplicationContext())
                        val picturePath = fileHelper.getRealPathFromUri(selectedMediaUri)
//                        var picturePath = getRealPathFromURI(selectedMediaUri!!)
                        val bmp = BitmapFactory.decodeFile(
                            DecodeImage.decodeFile(
                                picturePath!!,
                                600,
                                600
                            )
                        )
                        val bounds = BitmapFactory.Options()
                        bounds.inJustDecodeBounds = true
                        BitmapFactory.decodeFile(picturePath, bounds)
                        val opts = BitmapFactory.Options()
                        val bm = BitmapFactory.decodeFile(picturePath, opts)
                        val exif = ExifInterface(picturePath!!)
                        val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
                        val orientation =
                            if (orientString != null) Integer.parseInt(orientString) else ExifInterface.ORIENTATION_NORMAL
                        var rotationAngle = 0
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270
                        val matrix = Matrix()
                        matrix.setRotate(
                            rotationAngle.toFloat(),
                            bm.width.toFloat() / 2,
                            bm.height.toFloat() / 2
                        )

                        val rotatedBitmap = Bitmap.createBitmap(
                            bm,
                            0,
                            0,
                            bounds.outWidth,
                            bounds.outHeight,
                            matrix,
                            true
                        )


                        val bytes = ByteArrayOutputStream()
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                        val path = MediaStore.Images.Media.insertImage(
                            contentResolver,
                            rotatedBitmap,
                            "Clout",
                            null
                        )
//                        selectedMediaUri = activity?.let { getImageUri(it, rotatedBitmap) }
                        selectedMediaUri = Uri.parse(path)

                        uriList.add(selectedMediaUri!!)
                        mArrayList.add(selectedMediaUri!!)
                        count=10-uriList.size
                        Log.e(TAG, "countt: "+count )
                        setAdapter(mPreviousPostsList, mArrayList)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

            }
        }

        else if (resultCode == RESULT_OK && requestCode == CAPTURE_VIDEO) {

            try {
                selectedMediaUri = data!!.getData()!!
                videoFullPath = getRealPathFromURI(selectedMediaUri)

                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(videoFullPath)
                val time =
                    retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                val VideoDuration = time!!.toInt()
                Log.e(TAG, "onActivityResult: "+VideoDuration)

                if (VideoDuration>60090){
                    showToast(mActivity, "You can upload video upto 1 minute")
                }
                else{
                    videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                        videoFullPath!!,
                        MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                    )
                    mArrayList.add(selectedMediaUri!!)
                    imageList.add(videoThumbnail_bitmap!!)
                    setAdapter(mPreviousPostsList, mArrayList)
                    uriList.add(selectedMediaUri!!)
                }
//            manageVideo(path) //Do whatever you want with your video
            }
            catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (getContentResolver() != null) {
            val cursor: Cursor = getContentResolver().query(uri!!, null, null, null, null)!!
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }


    @SuppressLint("NewApi")
    @Throws(URISyntaxException::class)
    fun getFilePath(uri: Uri): String? {
        var uri = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(
                mActivity,
                uri
            )
        ) {
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                )
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":").toTypedArray()
                val type = split[0]
                if ("image" == type) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(
                    split[1]
                )
            }
        }
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            if (isGooglePhotosUri(uri)) {
                return uri.lastPathSegment
            }
            val projection = arrayOf(
                MediaStore.Images.Media.DATA
            )
            var cursor: Cursor? = null
            try {
                cursor = contentResolver
                    ?.query(uri, projection, selection, selectionArgs, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    private fun executeUpdatePostApi() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                binding.txtUpdateTV.isEnabled = false
                executeEditPostRequest()
            } else {
                showToast(mActivity, getString(R.string.no_internet_connection))
            }
        }
    }

    private fun executeEditPostRequest() {
        var videoMultipartBody: MultipartBody.Part? = null
        var deleted_images:RequestBody?=null
        val descriptionList: MutableList<MultipartBody.Part> = ArrayList<MultipartBody.Part>()
//        val previousList: MutableList<MultipartBody.Part> = ArrayList<MultipartBody.Part>()
        val uriListString = ArrayList<String>()

        uriList.forEach {
            getFilePath(it)?.let { it1 -> uriListString.add(it1) }
        }

        uriListString.forEach {
            val selectedFile = File(it)
            val filePathBody: RequestBody =
                selectedFile.asRequestBody("image/*".toMediaTypeOrNull())

            descriptionList.add(
                MultipartBody.Part.createFormData(
                    "uploads[]",
                    System.currentTimeMillis().toString() + selectedFile.name,
                    filePathBody
                )
            )
        }
//if(deletedUrlList.size>0){
//       for(i in 0..deletedUrlList.size-1){
//           previousList.add(
//            MultipartBody.Part.createFormData(
//                "deleted_images[]",
//                deletedUrlList[i]
//            )
//        )}}
        var hh: String = ""
        for (i in deletedUrlList.indices) {
            hh += deletedUrlList[i]+","
            Log.e("hh", deletedUrlList[i])
        }
if(!hh.isNullOrEmpty()){
    deleted_images = hh.substring(0, hh.length - 1).toRequestBody("multipart/form-data".toMediaTypeOrNull())}
        val user_id: RequestBody =
            getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val description: RequestBody = binding.editCaptionET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val location: RequestBody = binding.editLocationET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val latitude: RequestBody = mLatitude
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val longitude: RequestBody = mLongitude
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val mPostID: RequestBody = mPostId
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val mType: RequestBody = mtype.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        Log.e(TAG, "executeEditPostRequest: "+"deletedUrlList"+deletedUrlList+"descriptionList:"+binding.editCaptionET.text.toString().trim()+"previousList:"+ "user_id:"+getLoggedInUserID()+"location"+binding.editLocationET.text.toString().trim()+"latitude"+mLatitude+"longitude"+mLongitude+"mPostID"+mPostId+"mType"+mtype)

        if (videoThumbnail_bitmap != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(videoThumbnail_bitmap!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            videoMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "video_thumbnail",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }

        showProgressDialog(mActivity)

        val headers: HashMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitInstance.appApi.editPostRequest(
            headers,
            descriptionList,
            deleted_images,
            user_id,
            mPostID,
            description,
            location,
            latitude,
            longitude,
            mType,
            videoMultipartBody
        )
        call.enqueue(object : Callback<StatusModel> {
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                dismissProgressDialog()
                val mAddPostModel = response.body()
                if (mAddPostModel!!.status == 1) {
                    showFinishAlertDialog(mActivity, mAddPostModel.message)
                } else if (mAddPostModel.status == 0) {
                    binding.txtUpdateTV.isEnabled = true
                    showAlertDialog(mActivity, mAddPostModel.message)
                } else if (mAddPostModel.status == 3) {
                    binding.txtUpdateTV.isEnabled = true
                    showAlertDialog(mActivity, mAddPostModel.message)
                } else {
                    binding.txtUpdateTV.isEnabled = true
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

        })
    }


    // - - To Show Alert Dialog
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            binding.txtUpdateTV.isEnabled = true
            alertDialog.dismiss()
//            onBackPressed()
            val intent = Intent(mActivity, HomeActivity::class.java)
            intent.putExtra("value","fromManage")
//            intent.putExtra("type",mtype)
//            intent.putExtra("user_id", getLoggedInUserID())
            startActivity(intent)
            finish()
        }
        alertDialog.show()
    }


    private fun getIntentData() {
//        val gson = Gson()
//        val data = intent.getStringExtra("data")
//        post_data = gson.fromJson(
//            data,
//            ProfilePostsData::class.java
//        )

        mPostId=intent.getStringExtra("post_id").toString()
        Log.e(TAG, "getIntentData: "+ mPostId)
        executeGetPostDetailsApi()
//        setDataOnViews()

    }

    var image: Bitmap? = null

    private fun setDataOnViews() {
        mOtheruserId = mDataModel!!.data.user_id
        mPostId = mDataModel!!.data.post_id
        binding.editLocationET.setText(mDataModel!!.data.location)
        binding.editCaptionET.setText(mDataModel!!.data.description)
        if (mDataModel!!.data!!.post_data[0].type=="1") {
            for (i in 0..mDataModel!!.data.post_data.size - 1) {
                val uri = Uri.parse(mDataModel!!.data.post_data[i].uploads)
                mArrayList.add(uri)
                uriList.add(uri)
            }
        }
        else {
//            for (i in 0..post_data!!.image.size - 1) {
                val uri = Uri.parse(mDataModel!!.data.post_data[0].uploads)
                val urii = Uri.parse(mDataModel!!.data.post_data[0].thumbnail_image)
                mThumbnailList.add(urii)
                mArrayList.add(uri)
                uriList.add(uri)
//            }
        }
        if (mArrayList.size > 0) {
            count=10-mArrayList.size
            setAdapter(mPreviousPostsList,mArrayList)
        } else {
            setAdapter(mPreviousPostsList,mThumbnailList)
        }
    }


    var mOptionsClickListener: OptionsInterface = object : OptionsInterface {
        override fun onItemClickListner(mPosition: Int, id: String) {
                    performRemoveClick(mPosition,id)
    }}

    private fun performRemoveClick(mPosition: Int, image_idd: String) {
        if (mArrayList.size > 0) {
            mArrayList.removeAt(mPosition)
            uriList.removeAt(mPosition)
            deletedUrlList.add(image_idd)
            Log.e(TAG, "performRemoveClick: "+deletedUrlList)
            count++
        }
//        if (!uriList[mPosition].toString().contains("amazonaws")){
//                uriList.removeAt(mPosition)
//            }
//            else{
//                val currentString = uriList[mPosition].toString()
//                val separated = currentString.split("amazonaws.com/").toTypedArray()
//                separated[0] // this will contain "Fruit"
//
//                separated[1] // this will contain " they taste good"
//                deletedUrlList.add(separated[1])
//                uriList.removeAt(mPosition)
//            }

//        }
        if (mThumbnailList.size > 0) {
            mThumbnailList.removeAt(mPosition)
        }

        editPostAdapter?.notifyDataSetChanged()
    }

    private fun setAdapter(mPreviousPostsList: ArrayList<DataOfPost>, list: ArrayList<Uri>) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
        binding.picsRV.layoutManager = layoutManager
        editPostAdapter = EditPostAdapter(mActivity, list, mPreviousPostsList,mOptionsClickListener)
        binding.picsRV.adapter = editPostAdapter
    }


    private fun isValidate(): Boolean {
        var flag = true
        when {
            (uriList.size <= 0) -> {
                showAlertDialog(mActivity, getString(R.string.add_image))
                flag = false
            }
        }
        return flag
    }


    private fun startTrimActivity(uri: Uri) {
//        val intent = Intent(mActivity, VideoTrimActivity::class.java)
//        intent.putExtra(Constants.EXTRA_VIDEO_PATH, FileUtils().getRealPathFromURI_API19(mActivity, uri))
//        startActivityForResult(intent, 800)
    }

    fun addVideo(videoFile: File): Uri? {
        val values = ContentValues(3)
        values.put(MediaStore.Video.Media.TITLE, "clout_video_trimmed")
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
        values.put(MediaStore.Video.Media.DATA, videoFile.absolutePath)
        Log.e("VideoPath", videoFile.absolutePath)

        var uri= Uri.parse(File(videoFile.absolutePath).toString())
        Log.e("VideoPathUri", uri.toString())

        return contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
    }


    override fun onDestroy() {
        super.onDestroy()
//        CreatePostFragment.lati =""
//        CreatePostFragment.longitu =""
//        CreatePostFragment.addressLocation =""
//        binding?.editLocationET?.setText("")
        trimmedVideoUri =null
    }

    override fun onStop() {
        super.onStop()
//        CreatePostFragment.lati =""
//        CreatePostFragment.longitu =""
//        CreatePostFragment.addressLocation =""
//        binding?.editLocationET?.setText("")
        trimmedVideoUri =null
    }
}