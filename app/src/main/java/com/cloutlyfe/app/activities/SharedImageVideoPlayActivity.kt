package com.cloutlyfe.app.activities
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray

import com.google.android.exoplayer2.trackselection.TrackSelectionArray

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout

import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.cloutlyfe.app.databinding.ActivitySharedImageVideoPlayBinding


class SharedImageVideoPlayActivity : BaseActivity(), Player.EventListener {
    lateinit var binding: ActivitySharedImageVideoPlayBinding
    //     creating a variable for exoplayer
    var exoPlayer: SimpleExoPlayer? = null

    override fun onBackPressed() {
        super.onBackPressed()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(exoPlayer!=null){
            exoPlayer!!.stop()
            exoPlayer!!.release()}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySharedImageVideoPlayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.backRL.setOnClickListener {
            onBackPressed()
        }

//        binding.imgVolumeIV.setOnClickListener {
//            performVolumeClick()
//        }
//        exoPlayerView=binding.idExoPlayerVIew
    }

    override fun onResume() {
        super.onResume()
        getIntentData()
    }

    override fun onPause() {
        super.onPause()
        if(exoPlayer!=null){
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    private fun performVolumeClick() {
        if(exoPlayer!!.volume == 0f){
            exoPlayer!!.volume = 100f
//            binding.imgVolumeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_on))
        }
        else{
            exoPlayer!!.volume = 0f
//            binding.imgVolumeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_mute))
        }
    }

    private fun getIntentData() {
        val data = intent.getStringExtra("data")
        val value = intent.getStringExtra("value")

        if(value.equals("image")){
            binding.idExoPlayerVIew.visibility=View.GONE
            binding.postImageIV.visibility=View.VISIBLE
            Glide.with(mActivity).load(data)
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into((binding.postImageIV))
        }
        else {
//            binding.idExoPlayerVIew.visibility=View.VISIBLE
            binding.postImageIV.visibility=View.GONE
            initializePlayer(data)
            }}

    fun initializePlayer(link: String?)
    {
        if (!link.isNullOrEmpty()) {
            Log.e("Videos", "***Link***" + link)
// progressBarVid.visibility = View.VISIBLE
            binding.idExoPlayerVIew.visibility = View.VISIBLE

            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode = DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

            val loadControl = DefaultLoadControl.Builder().setBufferDurationsMs(25000, 50000, 100, 300).createDefaultLoadControl()

            val renderersFactory = DefaultRenderersFactory(mActivity).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(mActivity, renderersFactory).build()

            val dataSourceFactory = DefaultDataSourceFactory(mActivity, Util.getUserAgent(mActivity, "CloutLyfe"))

            val mediaSources = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(link))
            exoPlayer!!.prepare(mediaSources)
            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
            binding.idExoPlayerVIew.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
            binding.idExoPlayerVIew.player = exoPlayer
            exoPlayer!!.playWhenReady = true
//            exoPlayer!!.seekTo(0)
            binding.idExoPlayerVIew.requestFocus()

// progressBarVid.visibility=View.GONE

//            exoPlayer!!.volume = 0f



            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {}
                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            exoPlayer!!.setPlayWhenReady(true)
//                            progressBarVid.visibility = View.GONE
                        }
                        Player.STATE_BUFFERING -> {
                            exoPlayer!!.seekTo(0)
//                            progressBarVid.visibility = View.VISIBLE
                        }
                        else ->
                        {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }
                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {}
                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {}
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }
}