package com.cloutlyfe.app.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivityChangePasswordBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.LoginPrefrences
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.ChangePasswordViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory

class ChangePasswordActivity : BaseActivity() {
    lateinit var binding: ActivityChangePasswordBinding
    lateinit var changePasswordViewModel: ChangePasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.text = getString(R.string.change_password)
        init()
        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.txtSaveTV.setOnClickListener {
            executeChangePasswordApi()
        }

    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        changePasswordViewModel =
            ViewModelProvider(this, factory).get(ChangePasswordViewModel::class.java)
    }

    private fun executeChangePasswordApi() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.no_internet_connection))
            } else {
                exceuteChnagePswdApi()
            }
    }

    private fun exceuteChnagePswdApi() {
        var old_password = binding.editOldPasswordET.text.toString().trim()
        var user_id = AppPrefrences().readString(mActivity, Constants.ID, null)
        var new_password = binding.editConfirmPasswordET.text.toString().trim()
        val body = RequestBodies.ChangePswdBody(user_id!!, old_password, new_password)
        changePasswordViewModel.changePswd(body)
        changePasswordViewModel.changePassResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { response ->
                            if (response.status == 1 || response.status == 200) {
                                if (LoginPrefrences().readString(
                                        mActivity,
                                        Constants.REMEMBER_ME,
                                        null
                                    ).equals("true")
                                ) {
                                    LoginPrefrences().writeString(
                                        mActivity, Constants.PASSWORD,
                                        new_password
                                    )
                                }
                                showToast(mActivity, response.message)
                                finish()
                            } else {
                                showAlertDialog(mActivity,response.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity,message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    /*
        * Set up validations for Sign In fields
        * */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding.editOldPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.manage_enter_old_pass))
            flag = false
        } else if (binding.editNewPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
            flag = false
        } else if (binding.editConfirmPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
            flag = false
        } else if (binding.editConfirmPasswordET.text.toString()
                .trim { it <= ' ' } != binding.editNewPasswordET.text.toString().trim { it <= ' ' }
        ) {
            showAlertDialog(mActivity, getString(R.string.password_not))
            flag = false
        }
        return flag
    }

}