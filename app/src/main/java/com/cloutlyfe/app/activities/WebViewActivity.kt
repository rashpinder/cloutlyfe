package com.cloutlyfe.app.activities

import android.os.Bundle
import android.webkit.WebViewClient
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivityWebViewBinding
import com.cloutlyfe.app.utils.Constants.Companion.PRIVACY_POLICY
import com.cloutlyfe.app.utils.Constants.Companion.TERMS_CONDITIONS


class WebViewActivity : BaseActivity() {
    lateinit var binding: ActivityWebViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (intent != null) {
            var value = intent.getStringExtra(getString(R.string.value))
            if (value.equals(getString(R.string.terms))) {
                binding.header.txtHeadingTV.text = getString(R.string.term_of_use)
                setUpWebView(TERMS_CONDITIONS)
            } else {
                binding.header.txtHeadingTV.text = getString(R.string.privacy_policy)
                setUpWebView(PRIVACY_POLICY)
            }
        }

        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }



    }

    private fun setUpWebView(mUrl : String){
        binding.mWebViewWV.webViewClient = WebViewClient()
        // this will load the url of the website
        binding.mWebViewWV.loadUrl(mUrl)
        // this will enable the javascript settings
        binding.mWebViewWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        binding.mWebViewWV.settings.setSupportZoom(true)
    }
}