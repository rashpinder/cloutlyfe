package com.cloutlyfe.app.activities
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.LoginActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivityForgotPasswordBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.ForgotPasswordViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory

class ForgotPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityForgotPasswordBinding
    lateinit var forgotPasswordViewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        performClicks()
    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        forgotPasswordViewModel = ViewModelProvider(this, factory).get(ForgotPasswordViewModel::class.java)

    }

    fun performClicks() {
        binding.backRL.setOnClickListener {
            val i = Intent(mActivity, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
        binding.txtSubmitTV.setOnClickListener {
            performSubmitClick()
        }

    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }
            !isValidEmaillId(binding.editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
        }
        return flag
    }

    private fun performSubmitClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.no_internet_connection))
            } else {
                performSubmit()
            }
    }

    private fun performSubmit() {
        var email = binding.editEmailET.text.toString().trim()
        val body = RequestBodies.ForgotPswdBody(
            email
        )
        forgotPasswordViewModel.forgotPswdUser(body)
        forgotPasswordViewModel.forgotPswdResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { forgotResponse ->
                            if (forgotResponse.status==1||forgotResponse.status==200) {
                                binding.editEmailET.setText("")
                                showDismisssforgotPasswordDialog(mActivity, forgotResponse.message)
                            }
                            else{
                                showAlertDialog(mActivity,forgotResponse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity,message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })

    }


    //  Show Alert Dialog
    fun showDismisssforgotPasswordDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            val i = Intent(mActivity, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
        alertDialog.show()
    }

}