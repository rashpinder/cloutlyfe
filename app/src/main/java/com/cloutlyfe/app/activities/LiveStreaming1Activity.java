package com.cloutlyfe.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cloutlyfe.app.Agora.ConstantsA;
import com.cloutlyfe.app.Agora.activities.BaseActivity;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces;
import com.cloutlyfe.app.R;
import com.cloutlyfe.app.databinding.ActivityLiveStreaming1Binding;
import com.cloutlyfe.app.model.GoingLiveModel;
import com.cloutlyfe.app.retrofit.API;
import com.cloutlyfe.app.retrofit.RetrofitInstance;
import com.google.gson.Gson;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;

import java.util.HashMap;
import java.util.Map;

import io.agora.rtc.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveStreaming1Activity extends BaseActivity {

    // Permission request code of any integer value
    private static final int PERMISSION_REQ_CODE = 1 << 4;
    /*
   Getting the Current Class Name
  */
    String TAG = LiveStreaming1Activity.this.getClass().getSimpleName();
    Activity mActivity = LiveStreaming1Activity.this;
    ActivityLiveStreaming1Binding binding;
    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLiveStreaming1Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
//        getSupportActionBar().hide();
        // Hide_navigation();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
//        check_permissions();
        //for high quality streaming 1080*720p
        config().setVideoDimenIndex(5);
        performCamera();
        try {
            binding.camera.toggleFacing();
        }
        catch(Exception e){

        }

        binding.imCameraSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSwitch();
            }
        });

        binding.imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.imGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performActivitySwitch();
            }
        });
        binding.imSettingk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSettingsSwitch();
            }
        });
    }

    private void performSettingsSwitch() {
        Intent i = new Intent(mActivity, SettingsActivity.class);
        startActivity(i);
    }

    private void performActivitySwitch() {
        binding.imGoLive.setClickable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.imGoLive.setClickable(true);
            }
        }, 1500);

        resetLayoutAndForward();
//        checkPermission();
    }

    private void performSwitch() {
        try {
            binding.camera.toggleFacing();
        }
        catch(Exception e){

        }
//        binding.camera.toggleFacing();
//        Log.e("toggleFacing", String.valueOf(binding.camera.toggleFacing()));
//        if (facing == LensFacing.FRONT) {
//            facing = LensFacing.BACK;
//        } else {
//            facing = LensFacing.FRONT;
//        }

    }

    @SuppressLint("ServiceCast")
    public void performCamera() {
        Log.e(TAG, "performCamera: " + "called");
        try {
            binding.camera.addCameraKitListener(new CameraKitEventListener() {
                @Override
                public void onEvent(CameraKitEvent cameraKitEvent) {
                }

                @Override
                public void onError(CameraKitError cameraKitError) {
                }

                @Override
                public void onImage(CameraKitImage cameraKitImage) {
                }

                @Override
                public void onVideo(CameraKitVideo cameraKitVideo) {

                }
            });

            //  binding.camera.setCropOutput(false);
            try {
            binding.camera.start();}
            catch (Exception e) {
                e.printStackTrace();
            }

            {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            binding.camera.stopVideo();
            binding.camera.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    //Permission for live streaming
//    private void checkPermission() {
//        boolean granted = true;
//        for (String per : PERMISSIONS) {
//            if (!permissionGranted(per)) {
//                granted = false;
//                break;
//            }
//        }
//        if (granted) {
//            performCamera();
//            resetLayoutAndForward();
//        } else {
//            requestPermissions();
//        }
//    }

//    private boolean permissionGranted(String permission) {
//        return ContextCompat.checkSelfPermission(
//                this, permission) == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermissions() {
//        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
//    }


    //    public void onRequestPermissionsResult(
//            int requestCode, @NonNull String permissions[], @NonNull int[] grantResults
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode==PERMISSION_REQ_CODE) {
//            if (grantResults!=null && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
//            ) {
//                performCamera();
////                resetLayoutAndForward();
//
//            } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
//                    permissions[0]
//            )
//            ) { // User selected the Never Ask Again Option
////                val builder = AlertDialog.Builder(context!!)
////                builder.setMessage(R.string.photoPermission)
////                        .setPositiveButton(getString(R.string.settings)) { dialog, which ->
////                        val settingsIntent =
////                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
////                    val uri = Uri.fromParts("package", (context!!).packageName, null)
////                    settingsIntent.data = uri
////                    startActivityForResult(settingsIntent, 7)
////                }
////                    .setNegativeButton(getString(R.string.not_now), null)
////                val dialog = builder.create()
////                dialog.show()
//            }
//        }
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == PERMISSION_REQ_CODE) {
//            boolean granted = true;
//            for (int result : grantResults) {
//                granted = (result == PackageManager.PERMISSION_GRANTED);
//                if (!granted) break;
//            }
//            if (granted) {
//
////                resetLayoutAndForward();
//            } else {
//                toastNeedPermissions();
//            }
//        }
//    }


//    private void toastNeedPermissions() {
//        Toast.makeText(this, R.string.need_permissions, Toast.LENGTH_LONG).show();
//    }

    private void resetLayoutAndForward() {
        executeApi();
    }


    public void gotoRoleActivity(String stream_room, String userID, String sessionUser_id) {
        ConstantsA.channelNameA = stream_room;
        int uid = (int) (System.currentTimeMillis() / 1000L);
        ConstantsA.useridA = String.valueOf(uid);

        onJoinAsBroadcaster();
    }


    public void onJoinAsBroadcaster() {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }

    public void onJoinAsAudience() {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {
        try{
        binding.camera.stop();}
        catch (Exception e){

        }
        Intent intent = new Intent();
        intent.putExtra(ConstantsA.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveStreaming2Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        performCamera();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserID());
        Log.e(TAG, "**PARAM**" + mMap);
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        API mApiInterface = RetrofitInstance.Companion.getAppApi();
        mApiInterface.executeLiveStream(getAuthToken(), mParam()).enqueue(new Callback<GoingLiveModel>() {
            @Override
            public void onResponse(Call<GoingLiveModel> call, Response<GoingLiveModel> response) {
                dismissProgressDialog();
                GoingLiveModel mModel = response.body();
                Log.e("live_streamResponse", new Gson().toJson(mModel));
                if (mModel.getStatus() == 0) {
                    showAlertDialog2(mActivity, mModel.getMessage());
                } else {
                    ConstantsA.StramID = mModel.getStreamDetail().getStream_id();
                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_USERID, getUserID());
                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_ROOMID, mModel.getStreamDetail().getStream_room());
                    if(mModel.getUserDetail().getUserName().equalsIgnoreCase("N/A")||mModel.getUserDetail().getUserName().equalsIgnoreCase("")||mModel.getUserDetail().getUserName().equalsIgnoreCase(null)){
                        CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_USERNAME, mModel.getUserDetail().getName());
                    }
                    else{
                        CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_USERNAME, mModel.getUserDetail().getUserName());
                    }

                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_ID, mModel.getStreamDetail().getStream_id());
                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.STREAM_USERPHOTO, getProfilePic());
                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.COMMENT_DISABLE, "0");
                    CloutNewPrefernces.writeString(mActivity, com.cloutlyfe.app.utils.Constants.LIKE_DISABLE, "0");

                    gotoRoleActivity(mModel.getStreamDetail().getStream_room(), getUserID(), mModel.getStreamDetail().getUser_id());

                    Log.e(TAG, "onResponse: " + getUserID());
                    Log.e(TAG, "onResponse: " + mModel.getStreamDetail().getStream_room());
                    Log.e(TAG, "onResponse: " + getUserName());
                    Log.e(TAG, "onResponse: " + mModel.getStreamDetail().getStream_id());
                    Log.e(TAG, "onResponse: " + getProfilePic());

                }
            }

            @Override
            public void onFailure(Call<GoingLiveModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog2(mActivity, t.getMessage());
            }
        });
    }


//    // we need 4 permission during creating an video so we will get that permission
//    // before start the video recording
//    public boolean check_permissions() {
//        String[] PERMISSIONS = new String[0];
//        PERMISSIONS = new String[]{
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                Manifest.permission.RECORD_AUDIO,
//                Manifest.permission.CAMERA
//        };
//
//        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestPermissions(PERMISSIONS, 2);
//            }
//        } else if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
//            performCamera();
//        } else {
//            return true;
//        }
//        return false;
//    }


//    public boolean hasPermissions(Context context, String... permissions) {
//        if (context != null && permissions != null) {
//            for (String permission : permissions) {
//                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
//                    return false;
//                } else {
//                   performCamera();
//                    return true;
//                }
//
//            }
//        }
//        return true;
//    }


    @Override
    public void onBackPressed() {
        binding.camera.destroyDrawingCache();
        binding.camera.stop();
        finish();
    }

}