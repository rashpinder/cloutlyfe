package com.cloutlyfe.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.viewpager.widget.ViewPager
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.ScreenSliderPagerAdapter
import com.cloutlyfe.app.databinding.ActivityScreenSlidePageBinding
import com.cloutlyfe.app.model.Snap
import com.cloutlyfe.app.utils.Constants.Companion.MODEL
import com.cloutlyfe.app.utils.DepthPageTransformer
import java.util.*
import kotlin.collections.ArrayList

class ScreenSlidePagerActivity : BaseActivity() {

    private var mPager: ViewPager? = null
    private var pagerAdapter: ScreenSliderPagerAdapter? = null
    var statusList= mutableListOf<Snap>()
    var statusListF= mutableListOf<Snap>()
    var position:String=""
    var snap_count: String=""
    var username:String=""
    var photo:String=""
    var user_id:String=""
    lateinit var binding: ActivityScreenSlidePageBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        binding = ActivityScreenSlidePageBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        ButterKnife.bind(this)
        getIntentValue()
        setData()
        // (statusList as ArrayList<StoriesItemV>).get(position.toInt()).user?.username
//        if(isNetworkAvailable(mActivity)){
//            executeRequestChat()
//        }else{
//            showToast(mActivity,getString(R.string.internet_connection_error))
//        }
    }
//    private fun mParam(): Map<String?, String?>? {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["user_id"] = getLoggedInUserID()
//        mMap["owner_id"] = user_id
//        Log.e(TAG, "**PARAM**$mMap")
//        return mMap
//    }
//    private fun executeRequestChat() {
//   //     showProgressDialog(mActivity)
//        val call = RetrofitClient.apiInterface.RequestChat(mParam())
//        call.enqueue(object : Callback<RequestChatModel> {
//            override fun onResponse(
//                call: Call<RequestChatModel>,
//                response: Response<RequestChatModel>
//            ) {
//                Log.e(TAG, response.body().toString())
//       //         dismissProgressDialog()
//                val mRequestChatModel = response.body()
//                if (mRequestChatModel!!.status == 1) {
//
//                }else if (mRequestChatModel!!.status == 0){
//                   showToast(mActivity,"No data Found.")
//                }else{
//                    //   showAlertDialog(requireActivity(),mGetEnvelopeModel.message)
//                }
//            }
//            override fun onFailure(call: Call<RequestChatModel>, t: Throwable) {
//                Log.e(TAG, t.message.toString())
//      //          dismissProgressDialog()
//            }
//
//        })
//    }
//
//    @OnClick(
//    //    R.id.closeIV,R.id.optionsIV
//    )
//    fun onViewClicked(view: View) {
//        when (view.id) {
//
////            R.id.closeIV -> onCloseStatusPerformed()
////            R.id.optionsIV -> onBottomSheetClick()
//
//        }
//    }

    //intent value get
    private fun getIntentValue() {
        if (intent!=null) {
            statusList = intent.getSerializableExtra("model") as ArrayList<Snap>
            position = intent!!.getStringExtra("position")!!.toString()
            snap_count= intent!!.getStringExtra("snap_count")!!.toString()
            username=intent!!.getStringExtra("username")!!.toString()
            user_id=intent!!.getStringExtra("user_id")!!.toString()
            photo=intent!!.getStringExtra("photo")!!.toString()
//            Collections.swap(statusList, 0, snap_count.toInt()-1)
            statusListF.addAll(statusList)

            //         txtUsersNameTV.setText(username)

            //   val Timestamp: Long = homeDetailItemList!!.get(position)!!.createdAt!!.toLong()
//            val Timestamp: Long = (statusList as ArrayList<SnapsItemV>).get(position.toInt()).statusdate!!.toLong()
//            val timeD = Date(Timestamp * 1000)
//            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
//            val Time: String = sdf.format(timeD)

            //           val currentDate = sdf.format(Date())

            //     findDifference(Time, currentDate);
//            var diff=currentDate.toLong()-Time.toLong()
//            val seconds = diff / 60
            //   txtTimeTV.setText(""+ statusList.get(position).lastUpdateDate!!.toLong())
//            Glide.with(mActivity!!).load(photo)
//                .placeholder(R.drawable.ic_placeholder)
//                .error(R.drawable.ic_placeholder).into(profileIV)
        }
    }


    //Setting dat on viewpager
    private fun setData() {
        mPager = findViewById<View>(R.id.pager) as ViewPager?
        for(i in statusList){
            println("status List items : "+i)
        }
        pagerAdapter = ScreenSliderPagerAdapter(mActivity,supportFragmentManager,statusList, mPager!!,photo,username,user_id)
        mPager!!.adapter = pagerAdapter
        mPager!!.setPageTransformer(true, DepthPageTransformer())

    }
}