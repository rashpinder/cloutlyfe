package com.cloutlyfe.app.activities
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivityCloutCloutersBinding
import com.cloutlyfe.app.fragments.*
import com.cloutlyfe.app.utils.*
import com.cloutlyfe.app.utils.Constants.Companion.CLOUTERS_TAG
import com.cloutlyfe.app.utils.Constants.Companion.CLOUT_TAG

class CloutCloutersActivity : BaseActivity() {
    private lateinit var homeBinding: ActivityCloutCloutersBinding
    val bundle = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeBinding = ActivityCloutCloutersBinding.inflate(layoutInflater)
        setContentView(homeBinding.root)
        homeBinding.header.txtHeadingTV.text=getString(R.string.clout_and_clouters)
        performClicks()

        if (intent.getStringExtra("value").equals("clout")){
            if(intent.getStringExtra("clout").equals("other_user")){
//                val bundle = Bundle()
                bundle.putString("other_id", intent.getStringExtra("other_id"))
                performCloutClick()
// set Fragmentclass Arguments
// set Fragmentclass Arguments
//                val fragobj = Fragmentclass()
//                fragobj.setArguments(bundle)
    }
        else{
                performCloutClick()
            }

        }
        else{
            if(intent.getStringExtra("clout").equals("other_user")){
                bundle.putString("other_id", intent.getStringExtra("other_id"))
                performCloutersClick()
            }
            else{
            performCloutersClick()
        }}
    }

    fun performClicks() {
        homeBinding.cloutLL.setOnClickListener {
            performCloutClick()
        }
        homeBinding.cloutersLL.setOnClickListener {
            performCloutersClick()
        }
        homeBinding.header.imgBackIV.setOnClickListener {
            performBackClick()
        }
    }

    private fun performCloutClick() {
        tabSelection(0)
        switch_fragment(CloutFragment(), CLOUT_TAG, false, bundle)
    }

    private fun performCloutersClick() {
        tabSelection(1)
        switch_fragment(CloutersFragment(), CLOUTERS_TAG, false, bundle)
    }

    private fun performBackClick() {
        onBackPressed()
    }

    private fun tabSelection(mPos: Int) {
        homeBinding.txtCloutTV.setTextColor(resources.getColor(R.color.black))
        homeBinding.txtCloutersTV.setTextColor(resources.getColor(R.color.black))
        homeBinding.cloutView.setBackgroundColor(resources.getColor(R.color.views_grey))
        homeBinding.cloutersView.setBackgroundColor(resources.getColor(R.color.views_grey))
        when(mPos){
            0 ->{
                homeBinding.txtCloutTV.setTextColor(resources.getColor(R.color.colorBlue))
                homeBinding.cloutView.setBackgroundColor(resources.getColor(R.color.colorBlue))
            }
            1 ->{
                homeBinding.txtCloutersTV.setTextColor(resources.getColor(R.color.colorBlue))
                homeBinding.cloutersView.setBackgroundColor(resources.getColor(R.color.colorBlue))
            }
        }
    }


    /*Switch between fragments*/
    fun switch_fragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.containerCloutCloutersFL, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

}
