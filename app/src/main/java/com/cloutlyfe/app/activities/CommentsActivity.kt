package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.CommentsAdapter
import com.cloutlyfe.app.databinding.ActivityCommentsBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.CommentLoadMoreListener
import com.cloutlyfe.app.interfaces.LikeCommentInterface
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.ui.adapter.MyCustomPagerAdapter
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.adapters.ReportAdapter
import com.cloutlyfe.app.interfaces.ReportItemClickInterface
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.viewmodel.*
import com.google.android.exoplayer2.*
import com.google.android.material.bottomsheet.BottomSheetDialog

import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray

import com.google.android.exoplayer2.trackselection.TrackSelectionArray

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout

import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

import android.R.id
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.*
import android.text.method.LinkMovementMethod
import android.view.ViewTreeObserver
import android.view.Window
import android.widget.*
import com.cloutlyfe.app.App
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.HomeActivity
import com.cloutlyfe.app.adapters.ShareBottomSheetAdapter
import com.cloutlyfe.app.fragments.HomeFragment
import com.cloutlyfe.app.interfaces.ShareInterface
import com.cloutlyfe.app.utils.*
import io.socket.client.Socket
import io.socket.emitter.Emitter


class CommentsActivity : BaseActivity(), Player.EventListener {
    lateinit var binding: ActivityCommentsBinding
    var mCommentsList: ArrayList<String> = ArrayList<String>()
    var commentsAdapter: CommentsAdapter? = null
    lateinit var mCommentsDataModel: GetCommentsModel
    private var mAdapter: ShareBottomSheetAdapter? = null
    var commentsList: ArrayList<DataXX> = ArrayList()

    //    lateinit var imageList: ArrayList<String>
//var imageList: ArrayList<ProfilePostsData> = ArrayList()
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mPerPage: Int = 10
    var mPostId: String = ""
    var mCommentId: String? = ""
    var mUserName: String? = ""
    var mUserName1: String? = ""
    var homeData: HomeData? = null
    var myCustomPagerAdapter: MyCustomPagerAdapter? = null
    lateinit var likeUnlikeViewwModel: LikeUnlikeViewModel
    lateinit var saveUnsaveViewModel: SaveUnsaveViewModel
    lateinit var likeUnlikeViewModel: LikeUnlikeCommentViewModel
    lateinit var reportViewModel: ReportViewModel
    lateinit var getReportReasonViewModel: GetReportReasonsViewModel
    val reportResonslist: ArrayList<DataX> = ArrayList()
    private var mReportAdapter: ReportAdapter? = null

//
////     creating a variable for exoplayerview.
//    var exoPlayerView: SimpleExoPlayerView? = null

    //     creating a variable for exoplayer
    var exoPlayer: SimpleExoPlayer? = null
    var reasonId: String? = null
    var mOtheruserId: String? = null
    var mName: String? = null
    var mLocation: String? = null
    var mLikeCount: String? = null
    var mCommentCount: String? = null
    var mDescription: String? = null
    var mIsLike: Int? = 0
    var mIsSave: Int? = 0
    private var mSocket: Socket? = null
    var mRoomId = ""
    val userslist: ArrayList<RoomId> = ArrayList()


    override fun onBackPressed() {
        if (!intent.getStringExtra("value").toString()
                .isNullOrEmpty() && !intent.getStringExtra("value").equals(null)
        ) {
            val i = Intent(mActivity, HomeActivity::class.java)
            startActivity(i)
            finish()
        } else {
            super.onBackPressed()
            for (i in HomeFragment.imageList.indices) {
                if (HomeFragment.imageList[i].post_id == homeData!!.post_id) {
                    HomeFragment.imageList.removeAt(i)
                    HomeFragment.imageList[i].commentCount= binding.commentTV.text.toString()
                    HomeFragment.imageList.add(i, homeData!!)
                    Constants.LAST_POSITION = i
                }
            }

            for (i in PostsFromChatActivity.imageList.indices) {
                if (PostsFromChatActivity.imageList[i].post_id == homeData!!.post_id) {
                    PostsFromChatActivity.imageList.removeAt(i)
//                    PostsFromChatActivity.imageList[i].commentCount= binding.commentTV.text.toString()
                    PostsFromChatActivity.imageList.add(i, homeData!!)
                    Constants.LAST_POSITION = i
                }
            }

            for (i in AllPostsActivity.imageList.indices) {
                if (AllPostsActivity.imageList[i].post_id == homeData!!.post_id) {
                    AllPostsActivity.imageList.removeAt(i)
//                    AllPostsActivity.imageList[i].commentCount= binding.commentTV.text.toString()
                    AllPostsActivity.imageList.add(i, homeData!!)
                    Constants.LAST_POSITION = i
                }
            }
            for (i in SavedActivity.imageList.indices) {
                if (SavedActivity.imageList[i].post_id == homeData!!.post_id) {
                    SavedActivity.imageList.removeAt(i)
//                    SavedActivity.imageList[i].commentCount= binding.commentTV.text.toString()
                    SavedActivity.imageList.add(i, homeData!!)
                    Constants.LAST_POSITION = i
                }
            }
        }


        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }


    }


    private fun wordsCount(): Int {
        val words: String = homeData!!.description!!.trim()
        if (words.isEmpty())
            return 0
        return words.split("\\s+".toRegex()).toTypedArray().size
    }

    private fun wordssCount(): Int {
        val words: String = mHomeDataModel!!.data.description!!.trim()
        if (words.isEmpty())
            return 0
        return words.split("\\s+".toRegex()).toTypedArray().size
    }


    override fun onDestroy() {
        super.onDestroy()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.sendCIV.isEnabled = true
        binding.idExoPlayerVIew.visibility = View.VISIBLE
        init()
        setUpSocketData()


//        setAdapter(mActivity, commentsList)

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
        binding.shareIV.setOnClickListener {
            performShareClick()
        }
        binding.likeLL.setOnClickListener {
            performLikeClick()
        }
        binding.likedRLL.setOnClickListener {
            performLikeRLClick()
        }
        binding.profileRL.setOnClickListener {
            performProfileClick()
        }
        binding.saveIV.setOnClickListener {
            performSaveClick()
        }
        binding.sendCIV.setOnClickListener {
            binding.sendCIV.isEnabled = false
            performAddCommentClick()
        }
        binding.imgVolumeIV.setOnClickListener {
            performVolumeClick()
        }
        binding.optionsIV.setOnClickListener {
            if (homeData?.user_id == AppPrefrences().readString(
                    mActivity,
                    Constants.ID,
                    null
                )
            ) {
                showDeleteDialog()
            } else {
                showReportDialog()
            }

        }

        binding.myProfileIV.setOnClickListener {
            val i = Intent(mActivity, OtherUserProfileActivity::class.java)
            i.putExtra("other_user_id", getLoggedInUserID())
            if(AppPrefrences().readString(
                    mActivity,
                    Constants.USERNAME,
                    null
                )=="N/A"||AppPrefrences().readString(
                    mActivity,
                    Constants.USERNAME,
                    null
                )==""|| AppPrefrences().readString(
                    mActivity,
                    Constants.USERNAME,
                    null
                )==null){
                i.putExtra(
                    "name", AppPrefrences().readString(
                        mActivity,
                        Constants.NAME,
                        null
                    )
                )
            }
            else{
                i.putExtra(
                    "name", AppPrefrences().readString(
                        mActivity,
                        Constants.USERNAME,
                        null
                    )
                )
            }


            i.putExtra("value", "logged_in_user")
            startActivity(i)
        }
//        exoPlayerView=binding.idExoPlayerVIew
    }

    private fun performLikeRLClick() {
        var mIntent = Intent(mActivity, LikesActivity::class.java)
        mIntent.putExtra("post_id", mHomeDataModel.data.post_id)
        mIntent.putExtra("from_comment", "true")
        startActivity(mIntent)
    }


    private fun setUpSocketData() {
        val app: App = application as App
        mSocket = app.getSocket()
//        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
//        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
//        mSocket!!.on("newMessage", onNewMessage)
//        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()

    }

    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }


    var mSharePostId: String? = null
    var mShareOtheruserId: String? = null
    var mOther_user_id: String? = null
    var mNamee: String? = null
    var mShareList: ArrayList<String> = ArrayList()
    var cloutsList: java.util.ArrayList<DataXXX> = java.util.ArrayList()
    lateinit var mCloutsDataModel: FollowingUsersModel


    private fun performShareClick() {
//        mSharePostId= homeData!!.post_id
//        mShareOtheruserId= homeData!!.otherID

        cloutsList.clear()
        mShareList.clear()
        executeGetCloutersApi()
    }


    private fun executeGetCloutersApi() {
        if (isNetworkAvailable(mActivity))
            RequestCloutersData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mFollowingParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["search"] = ""
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCloutersData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getFollowRequest(mHeaders, mFollowingParam())
        call.enqueue(object : Callback<FollowingUsersModel> {
            override fun onFailure(call: Call<FollowingUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<FollowingUsersModel>,
                response: Response<FollowingUsersModel>
            ) {
                Log.e(TAG, response.body().toString())
                mCloutsDataModel = response.body()!!
                if (mCloutsDataModel.status == 1) {
//                    isLoading = !mCloutsDataModel.lastPage.equals(true)
//            cloutsList.addAll(response.body()!!.data)
                    for (i in response.body()!!.data.indices) {
                        if (response.body()!!.data[i].is_delete != 1) {
                            cloutsList.add(response.body()!!.data[i])
                        }
                    }
                    Log.e(TAG, "RequestHomeDataAPI: " + cloutsList)
                    if (cloutsList.size == 0) {
                        dismissProgressDialog()
                        showToast(mActivity, "There are no users available to share post")
//                        binding!!.upArraowRL.visibility = View.GONE
//                        binding!!.txtNoDataTV.text =
//                            "No Clouters found."
//                        binding!!.txtNoDataTV.visibility = View.VISIBLE

                    } else {
                        showShareDialog()
//                        binding!!.upArraowRL.visibility = View.VISIBLE
//                        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//                        binding!!.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCloutsDataModel.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCloutsDataModel.message!!)
                } else {
                    dismissProgressDialog()
//                    binding!!.upArraowRL.visibility = View.GONE
//                    binding!!.txtNoDataTV.visibility = View.VISIBLE
//                    binding!!.txtNoDataTV.text = response.message()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        getIntentData()
    }

    override fun onPause() {
        super.onPause()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    private fun executeGetPostDetailsApi() {
        if (isNetworkAvailable(mActivity))
            RequestGetPostDetailsData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }

    lateinit var mHomeDataModel: GetPostDetailModel

    private fun mPostParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mSharePostId
        return mMap
    }

    private fun RequestGetPostDetailsData() {
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getPostDetailByPostId(mPostParam())
        call.enqueue(object : Callback<GetPostDetailModel> {
            override fun onFailure(call: Call<GetPostDetailModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetPostDetailModel>,
                response: Response<GetPostDetailModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                if (mHomeDataModel.status == 1) {
                    setDataOnViewsByPost()
                } else if (mHomeDataModel.status == 3) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                } else if (mHomeDataModel.status == 2) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                } else if (mHomeDataModel.status == 0) {
                    if (mHomeDataModel.delete_status == 5) {
                        showPostAlertDialog(mActivity, "This post is no longer available")
                    }
                } else {
//                    dismissProgressDialog()
                }
            }
        })
    }


    private fun showDeleteDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility = View.GONE

        deleteRL?.setOnClickListener {
            executeDeleteApi()
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }


    private fun executeDeleteApi() {
        if (isNetworkAvailable(mActivity))
            RequestDeleteData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.deletePostRequest(mHeaders, mDeleteParam())
        call.enqueue(object : Callback<CodeStatusModel> {
            override fun onFailure(call: Call<CodeStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CodeStatusModel>,
                response: Response<CodeStatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                if (response.body()!!.status == 1) {
                    showToast(mActivity, response.body()!!.message)

                    dismissProgressDialog()
                    onBackPressed()
                    finish()

                } else if (response.body()!!.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
                    dismissProgressDialog()
                }
            }
        })
    }


    private fun performProfileClick() {
//        Log.e("TAG", "user_id: " + homeData!!.user_id)
//        Log.e("TAG", "user_id: " + AppPrefrences().readString(mActivity, Constants.ID, null))

        if (!intent.getStringExtra("value").toString().isNullOrEmpty()) {
            if (mHomeDataModel?.data!!.user_id == AppPrefrences().readString(
                    mActivity,
                    Constants.ID,
                    null
                )
            ) {
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mHomeDataModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "logged_in_user")
                i.putExtra("other_user_id", getLoggedInUserID())
                if(AppPrefrences().readString(
                        mActivity,
                        Constants.USERNAME,
                        null
                    )=="N/A"||AppPrefrences().readString(
                        mActivity,
                        Constants.USERNAME,
                        null
                    )==""|| AppPrefrences().readString(
                        mActivity,
                        Constants.USERNAME,
                        null
                    )==null){
                    i.putExtra(
                        "name", AppPrefrences().readString(
                            mActivity,
                            Constants.NAME,
                            null
                        )
                    )
                }
                else{
                    i.putExtra(
                        "name", AppPrefrences().readString(
                            mActivity,
                            Constants.USERNAME,
                            null
                        )
                    )
                }
                startActivity(i)
//                val i = Intent(context, HomeActivity::class.java)
//                val gson = Gson()
//                val mModell = gson.toJson(mModel)
//                i.putExtra("homeData",mModell)
//                i.putExtra("value","logged_in_user")
//                context.startActivity(i)
            } else {
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mHomeDataModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                i.putExtra("other_user_id", mOtheruserId)
                if(mHomeDataModel.data.username=="N/A"||mHomeDataModel.data.username==""||mHomeDataModel.data.username==null){
                    i.putExtra("name", mHomeDataModel.data.name)
                }
                else{
                    i.putExtra("name", mHomeDataModel.data.username)
                }

                startActivity(i)
            }
        } else {
            if (homeData?.user_id == AppPrefrences().readString(
                    mActivity,
                    Constants.ID,
                    null
                )
            ) {
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(homeData)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "logged_in_user")

                startActivity(i)
//                val i = Intent(context, HomeActivity::class.java)
//                val gson = Gson()
//                val mModell = gson.toJson(mModel)
//                i.putExtra("homeData",mModell)
//                i.putExtra("value","logged_in_user")
//                context.startActivity(i)
            } else {
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(homeData)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                i.putExtra("other_user_id", mOtheruserId)
                i.putExtra("name", homeData!!.name)
                startActivity(i)
            }
        }

    }

    private fun performSaveClick() {
        if (intent.getStringExtra("value").toString()
                .isNotEmpty() && !intent.getStringExtra("value").equals(null)
        ) {
            if (mHomeDataModel!!.data.isSave.equals(0)) {
                mHomeDataModel!!.data.isSave = 1
                binding.saveIV.setImageResource(R.drawable.ic_save)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            } else {
                mHomeDataModel!!.data.isSave = 0
                binding.saveIV.setImageResource(R.drawable.ic_unsave)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            }
        } else {
            if (homeData!!.isSave.equals(0)) {
                homeData!!.isSave = 1
                binding.saveIV.setImageResource(R.drawable.ic_save)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            } else {
                homeData!!.isSave = 0
                binding.saveIV.setImageResource(R.drawable.ic_unsave)
                executeSaveUnsaveApi()
//                adapter?.notifyItemChanged(mPos!!)
            }
        }
    }

    private fun performLikeClick() {
        if (intent.getStringExtra("value").toString()
                .isNotEmpty() && !intent.getStringExtra("value").equals(null)
        ) {
            increasePushLikeeCount(mPostId)
        } else {
            increaseLikeeCount(mPostId)
        }
    }

    // this is the scroll listener of recycler view which will tell the current item number


    // this is the scroll listener of recycler view which will tell the current item number


    private fun increaseLikeeCount(
        post_id: String
    ) {
        if (homeData!!.isLike == 0) {
            binding.likeIV.setImageResource(R.drawable.ic_fav)
            homeData!!.isLike = 1
            val count = ((homeData!!.likeCount.toInt()) + 1).toString()
            homeData!!.likeCount = count
            binding.likeTV.text = homeData!!.likeCount
            if(count.toInt()>1){
                var sourceString =
                    "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + (Integer.parseInt(
                        homeData!!.likeCount
                    ) - 1) + " others" + "</b>"
                binding.likeddTV.visibility=View.VISIBLE
                binding.likeddTV.setText(Html.fromHtml(sourceString))}
            else{
                binding.likeddTV.visibility=View.VISIBLE
                var sourceString = "Liked by" + " " + "<b>" + "You" + "</b>"
                binding.likeddTV.setText(Html.fromHtml(sourceString))
            }
//Like Api Execute
            executeLikeeUnlikeApi(post_id)
        } else {
            binding.likeIV.setImageResource(R.drawable.ic_un_fav)
            homeData!!.isLike = 0
            val count = ((homeData!!.likeCount.toInt()) - 1).toString()
            homeData!!.likeCount = count
            binding.likeTV.text = homeData!!.likeCount
            if(homeData!!.likeCount.toInt().toInt()>0){
                binding.likeddTV.visibility=View.VISIBLE
                var sourceString =
                    "Liked by" + " " + "<b>" + (Integer.parseInt(
                        homeData!!.likeCount
                    )) + " others" + "</b>"
                binding.likeddTV.setText(Html.fromHtml(sourceString))}
            else{
                binding.likeddTV.visibility=View.INVISIBLE
            }

////Like Api Execute
            executeLikeeUnlikeApi(post_id)
        }
    }

    private fun increasePushLikeeCount(
        post_id: String
    ) {
        if (mHomeDataModel!!.data.isLike == 0) {
            binding.likeIV.setImageResource(R.drawable.ic_fav)
            mHomeDataModel!!.data.isLike = 1
            val count = ((mHomeDataModel!!.data.likeCount.toInt()) + 1).toString()
            mHomeDataModel!!.data.likeCount = count
            binding.likeTV.text = mHomeDataModel!!.data.likeCount
            if(count.toInt()>1){
                binding.likeddTV.visibility=View.VISIBLE
            var sourceString =
                "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + (Integer.parseInt(
                    count
                ) - 1) + " others" + "</b>"
            binding.likeddTV.setText(Html.fromHtml(sourceString))}
            else{
                binding.likeddTV.visibility=View.VISIBLE
                var sourceString = "Liked by" + " " + "<b>" + "You" + "</b>"
                binding.likeddTV.setText(Html.fromHtml(sourceString))
            }

//Like Api Execute
            executeLikeeUnlikeApi(post_id)
        } else {
            binding.likeIV.setImageResource(R.drawable.ic_un_fav)
            mHomeDataModel!!.data.isLike = 0
            val count = ((mHomeDataModel!!.data.likeCount.toInt()) - 1).toString()
            mHomeDataModel!!.data.likeCount = count
            binding.likeTV.text = mHomeDataModel!!.data.likeCount
////Like Api Execute
            if(mHomeDataModel!!.data.likeCount.toInt()>0){
                binding.likeddTV.visibility=View.VISIBLE
                var sourceString =
                    "Liked by" + " " + "<b>" + (Integer.parseInt(
                        count
                    )) + " others" + "</b>"
                binding.likeddTV.setText(Html.fromHtml(sourceString))}
            else{
                binding.likeddTV.visibility=View.INVISIBLE
            }
            executeLikeeUnlikeApi(post_id)
        }
    }


    private fun executeSaveUnsaveApi() {
        if (!mActivity.let { isNetworkAvailable(it) }) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestSaveUnsaveData()
        }
    }

    private fun RequestSaveUnsaveData() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        val body = RequestBodies.SaveUnsaveBody(user_id, post_id!!)
        saveUnsaveViewModel.saveUnsaveRequest(body)
        saveUnsaveViewModel._save_unsaveResponse.observe(
            this,
            androidx.lifecycle.Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data?.let { saveUnsaveResponse ->
                                if (saveUnsaveResponse.status == 1 || saveUnsaveResponse.status == 200) {
                                    //                                    dismissProgressDialog()
                                    //                                    showToast(mActivity, likeUnlikeResponse.message)
                                    //                                    if (saveUnsaveResponse.isSave.equals("0")){
                                    //                                        saveUnsaveResponse.isSave="0"
                                    //                                        imageList[mPos!!].isSave=0
                                    //                                        adapter?.notifyItemChanged(mPos!!)
                                    //                                    }
                                    //                                    else{
                                    //                                        saveUnsaveResponse.isSave="1"
                                    //                                        imageList[mPos!!].isSave=1
                                    //                                        adapter?.notifyItemChanged(mPos!!)
                                    //                                    }

                                } else {
                                    //                                    dismissProgressDialog()
                                }
                            }
                        }

                        is Resource.Error -> {
                            //                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(mActivity, response.message)
                            }
                        }

                        is Resource.Loading -> {
                            //                            showProgressDialog(mActivity)
                        }
                    }
                }
            })
    }


    private fun performVolumeClick() {
        if (exoPlayer!!.volume == 0f) {
            exoPlayer!!.volume = 100f
            binding.imgVolumeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_on))
        } else {
            exoPlayer!!.volume = 0f
            binding.imgVolumeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_mute))
        }
    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        likeUnlikeViewModel =
            ViewModelProvider(this, factory).get(LikeUnlikeCommentViewModel::class.java)
        likeUnlikeViewwModel = ViewModelProvider(this, factory).get(LikeUnlikeViewModel::class.java)
        saveUnsaveViewModel = ViewModelProvider(this, factory).get(SaveUnsaveViewModel::class.java)
        getReportReasonViewModel =
            factory.let {
                ViewModelProvider(
                    this,
                    it
                ).get(GetReportReasonsViewModel::class.java)
            }
        reportViewModel =
            factory.let {
                ViewModelProvider(this, it).get(ReportViewModel::class.java)
            }
    }

    private fun performAddCommentClick() {
        executeAddCommentRequest()
    }

    private fun executeAddCommentRequest() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                //   mBitmap = (saveIV!!.drawable as BitmapDrawable).bitmap
                executeAddNewCommentRequest()
            } else {
                showToast(mActivity, getString(R.string.no_internet_connection))
            }
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        mMap["comment"] = binding.editMessageET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddNewCommentRequest() {
        val call = RetrofitInstance.appApi.addCommentRequest(mParams())
        call.enqueue(object : Callback<AddCommentModel> {
            override fun onFailure(call: Call<AddCommentModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                binding.sendCIV.isEnabled = true
            }

            override fun onResponse(
                call: Call<AddCommentModel>,
                response: Response<AddCommentModel>
            ) {

                Log.e(TAG, response.body().toString())
                val mAddCommentModel = response.body()
                if (mAddCommentModel!!.status == 1) {
                    binding.editMessageET.setText("")
                    executeGetCommentsApi(mPostId)

                } else if (mAddCommentModel.status == 0) {
                    showAlertDialog(mActivity, mAddCommentModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
                binding.sendCIV.isEnabled = true
            }
        })
    }


    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.editMessageET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_add_comment))
                binding.sendCIV.isEnabled = true
                flag = false
            }
        }
        return flag
    }

    private fun getIntentData() {
        val gson = Gson()
        val data = intent.getStringExtra("homeData")
        homeData = gson.fromJson(
            data,
            HomeData::class.java
        )

        if (!intent.getStringExtra("value").toString().isNullOrEmpty()) {
            if (!intent.getStringExtra("post_id").isNullOrEmpty()) {
                mSharePostId = intent.getStringExtra("post_id")
                mShareOtheruserId = intent.getStringExtra("user_id")
                mOther_user_id = intent!!.getStringExtra("other_user_id")
                mNamee = intent!!.getStringExtra("name")
            } else {
                mSharePostId = homeData!!.post_id
                mShareOtheruserId = homeData!!.otherID
            }

            executeGetPostDetailsApi()
        } else {
            if (homeData!!.is_delete == 1) {
                showPostAlertDialog(mActivity, "This post is no longer available")
            } else {
                mOtheruserId = homeData!!.user_id
                mPostId = homeData?.post_id.toString()
                mSharePostId = homeData!!.post_id
                mShareOtheruserId = homeData!!.otherID
                if(homeData!!.username=="N/A"||homeData!!.username==""||homeData!!.username==null){
                    mName = homeData?.name
                }
                else{
                    mName = homeData?.username
                }

                mLocation = homeData?.location
                mLikeCount = homeData?.likeCount
                mCommentCount = homeData?.commentCount
                mDescription = homeData!!.description
                setDataOnViews()
            }
        }
    }

    /*
    *
    * Error Alert Dialog
    * */
    fun showPostAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            finish()
        }
        alertDialog.show()
    }


    fun makeTextViewResizable(
        tv: TextView,
        maxLine: Int,
        expandText: String,
        viewMore: Boolean
    ) {
        if (tv.tag == null) {
            tv.tag = tv.text
        }
        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val obs = tv.viewTreeObserver
                obs.removeGlobalOnLayoutListener(this)
                if (maxLine == 0) {
                    val lineEndIndex = tv.layout.getLineEnd(0)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, maxLine, expandText,
                            viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                    val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, maxLine, expandText,
                            viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                } else {
                    val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                    val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, lineEndIndex, expandText,
                            viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                }
            }
        })
    }

    private fun addClickablePartTextViewResizable(
        strSpanned: Spanned, tv: TextView,
        maxLine: Int, spanableText: String, viewMore: Boolean
    ): SpannableStringBuilder {
        val str = strSpanned.toString()
        val ssb = SpannableStringBuilder(strSpanned)
        if (str.contains(spanableText)) {
            ssb.setSpan(object : MySpannable(false) {
                override fun onClick(widget: View) {
                    if (viewMore) {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv as TextView, -1, "Read Less", false)
                    } else {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv as TextView, 2, "... Read More", true)
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
        }
        return ssb
    }

    private fun setDataOnViews() {
//        mOtheruserId= homeData!!.user_id
//        mPostId = homeData?.post_id.toString()
        binding.nameTV.text = mName
        binding.locationTV.text = mLocation
        binding.likeTV.text = mLikeCount
        binding.commentTV.text = mCommentCount
        binding.txtCaptionTV.text = mDescription
        if (wordsCount() >= 25) {
//        if( binding.txtCaptionTV.lineCount>2){
            makeTextViewResizable(binding.txtCaptionTV, 2, "... Read More", true)
        }
//        }
        if (mOtheruserId == getLoggedInUserID()) {
            binding.optionsIV.visibility = View.GONE
        } else {
            if (homeData!!.is_report == "1") {
                binding.optionsIV.visibility = View.GONE
            } else {
                binding.optionsIV.visibility = View.VISIBLE
            }
        }

        if (homeData!!.isLike == 1) {
            binding.likeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_fav))
        } else {
            binding.likeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_un_fav))
        }

        if (homeData!!.isSave == 1) {
            binding.saveIV.setImageDrawable(resources.getDrawable(R.drawable.ic_save))
        } else {
            binding.saveIV.setImageDrawable(resources.getDrawable(R.drawable.ic_unsave))
        }

        Glide.with(mActivity).load(homeData?.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((binding.profileIV))

        Glide.with(mActivity).load(
            AppPrefrences().readString(
                mActivity,
                Constants.PROFILE_IMAGE,
                null
            )
        )
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((binding.myProfileIV))

        if (homeData!!.image != null) {
            if (homeData!!.image!!.size > 1) {
                if (homeData!!.image!![0].contains(".png") || homeData!!.image!![0].contains(".jpeg") || homeData!!.image!![0].contains(
                        ".jpg"
                    ) || homeData!!.image!![0].contains("image") || homeData!!.image!![0].contains(".gif")||homeData!!.image!![0].contains(".PNG") || homeData!!.image!![0].contains(".JPEG") || homeData!!.image!![0].contains(
                        ".JPG"
                    ) || homeData!!.image!![0].contains("IMAGE") || homeData!!.image!![0].contains(".GIF")
                ) {
                    binding.viewpagerFrame.visibility = View.VISIBLE
                    binding.imgVolumeIV.visibility = View.GONE
                    myCustomPagerAdapter = MyCustomPagerAdapter(mActivity, homeData!!.image!!)
                    binding.viewPager.setAdapter(myCustomPagerAdapter)
                    if (homeData!!.image!!.size == 1) {
                        binding.indicator.visibility = View.GONE
                        binding.idExoPlayerVIew.visibility = View.GONE
                    } else {
                        binding.indicator.visibility = View.VISIBLE
                        binding.idExoPlayerVIew.visibility = View.GONE
                        binding.indicator.setViewPager(binding.viewPager)
                    }

//        binding.viewPagerIV.visibility=View.VISIBLE
//        Glide.with(mActivity).load(homeData?.image!![0])
//            .placeholder(R.drawable.ic_post_ph)
//            .error(R.drawable.ic_post_ph).centerCrop()
//            .into((binding.viewPagerIV))
                } else {
//        binding.postImageIV.visibility=View.VISIBLE
//        binding.videoView.visibility=View.VISIBLE
//        val uri: Uri = Uri.parse(homeData?.image!![0])
//        binding.videoView.setVideoURI(uri)
//        binding.videoView.start()
//        binding.videoView.setVideo(homeData?.image!![0])
                    Glide.with(mActivity).load(homeData?.thumbnail_image!![0])
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder).centerCrop()
                        .into((binding.postImageIV))
                    binding.postImageIV.visibility = View.VISIBLE
                }
            } else {
                if (homeData!!.image!![0].contains(".png") || homeData!!.image!![0].contains(".jpeg") || homeData!!.image!![0].contains(
                        ".jpg"
                    ) || homeData!!.image!![0].contains("image") || homeData!!.image!![0].contains(".gif")||homeData!!.image!![0].contains(".PNG") || homeData!!.image!![0].contains(".JPEG") || homeData!!.image!![0].contains(
                        ".JPG"
                    ) || homeData!!.image!![0].contains("IMAGE") || homeData!!.image!![0].contains(".GIF")
                ) {
                    binding.postImageIV.visibility = View.VISIBLE
                    binding.idExoPlayerVIew.visibility = View.GONE
                    Glide.with(mActivity).load(homeData?.image!![0])
                        .placeholder(R.drawable.ic_post_ph)
                        .error(R.drawable.ic_post_ph).centerCrop()
                        .into((binding.postImageIV))
                    binding.imgVolumeIV.visibility = View.GONE
                } else {
                    binding.idExoPlayerVIew.visibility = View.VISIBLE
                    initializePlayer(homeData?.image!![0])


                }

            }
        }


        if (homeData!!.likeimage != null) {
            for (i in 0..homeData!!.likeimage!!.size - 1) {
                if (homeData!!.likeimage!![i].user_id == getLoggedInUserID()) {
                    Collections.swap(homeData!!.likeimage, 0, i)
                }
            }

            if (homeData?.likeimage?.size!! >= 3) {
                Glide.with(mActivity).load(homeData?.likeimage!![0].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileOneIV))

                Glide.with(mActivity).load(homeData?.likeimage!![1].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileTwoIV))

                Glide.with(mActivity).load(homeData?.likeimage!![2].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileThreeIV))

                if(homeData!!.likeimage!![0].username=="N/A"|| homeData!!.likeimage!![0].username==""|| homeData!!.likeimage!![0].username==null){
                    mUserName=homeData!!.likeimage!![0].name
                }
                else{
                    mUserName=homeData!!.likeimage!![0].username
                }



                if (!mUserName.isNullOrEmpty()) {
                    if (homeData!!.likeimage!![0].user_id == getLoggedInUserID()) {
                        var sourceString =
                            "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + (Integer.parseInt(
                                homeData!!.likeCount
                            ) - 1) + " others" + "</b>"
                        binding.likedTV.setText(Html.fromHtml(sourceString))
                    } else {
                        var sourceString =
                            "Liked by" + " " + "<b>" + mUserName + "</b>" + " and " + "<b>" + (Integer.parseInt(
                                homeData!!.likeCount
                            ) - 1) + " others" + "</b>"
                        binding.likedTV.setText(Html.fromHtml(sourceString))
                    }
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + homeData!!.likeCount + " others" + "</b>"
                    binding.likedTV.setText(Html.fromHtml(sourceString))
                }

                binding.likeddTV.visibility = View.GONE
                binding.likedTV.visibility = View.VISIBLE
            } else if (homeData?.likeimage?.size == 2) {
                if(homeData!!.likeimage!![1].username=="N/A"|| homeData!!.likeimage!![1].username==""|| homeData!!.likeimage!![1].username==null){
                    mUserName1=homeData!!.likeimage!![1].name
                }
                else{
                    mUserName1=homeData!!.likeimage!![1].username
                }
                binding.otherUserProfileOneIV.visibility = View.GONE
                binding.otherUserProfileTwoIV.visibility = View.GONE
                binding.otherUserProfileThreeIV.visibility = View.GONE
                if (homeData!!.likeimage!![0].user_id == getLoggedInUserID()) {
                    var sourceString =
                        "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + mUserName1 + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + mUserName+ "</b>" + " and " + "<b>" + mUserName1 + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                }
                binding.likeddTV.visibility = View.VISIBLE
                binding.likedTV.visibility = View.GONE
            } else if (homeData?.likeimage?.size == 1) {
                binding.otherUserProfileOneIV.visibility = View.GONE
                binding.otherUserProfileTwoIV.visibility = View.GONE
                binding.otherUserProfileThreeIV.visibility = View.GONE
                if (homeData!!.likeimage!![0].user_id == getLoggedInUserID()) {
                    var sourceString = "Liked by" + " " + "<b>" + "You" + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + mUserName + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                }

                binding.likeddTV.visibility = View.VISIBLE
                binding.likedTV.visibility = View.GONE
            } else if (homeData?.likeimage?.size == 0) {
                binding.likedRLL.visibility = View.GONE
                binding.likeddTV.visibility = View.GONE
                binding.likedTV.visibility = View.GONE
            }
        } else {
            binding.likedRLL.visibility = View.GONE
            binding.likeddTV.visibility = View.GONE
            binding.likedTV.visibility = View.GONE
        }


        executeGetCommentsApi(mPostId)
    }


    private fun setDataOnViewsByPost() {
        mOtheruserId = mHomeDataModel.data.user_id
        mPostId = mHomeDataModel?.data!!.post_id
        if(mHomeDataModel?.data!!.username=="N/A"|| mHomeDataModel?.data!!.username==""|| mHomeDataModel?.data!!.username==null){
            binding.nameTV.text = mHomeDataModel?.data!!.name
        }
        else{
            binding.nameTV.text = mHomeDataModel?.data!!.username
        }

        binding.locationTV.text = mHomeDataModel?.data!!.location
        binding.likeTV.text = mHomeDataModel?.data!!.likeCount
        binding.commentTV.text = mHomeDataModel?.data!!.commentCount

        binding.txtCaptionTV.text = mHomeDataModel!!.data.description
        if (wordssCount() >= 25) {
//        if( binding.txtCaptionTV.lineCount>2){
            makeTextViewResizable(binding.txtCaptionTV, 2, "... Read More", true)
        }


        if (mHomeDataModel.data.user_id == getLoggedInUserID()) {
            binding.optionsIV.visibility = View.GONE
        } else {
            if (mHomeDataModel.data.is_report == "1") {
                binding.optionsIV.visibility = View.GONE
            } else {
                binding.optionsIV.visibility = View.VISIBLE
            }
        }
if(!intent.getStringExtra("other_user_id").isNullOrEmpty()){
        if (intent.getStringExtra("other_user_id") == getLoggedInUserID()) {
            binding.optionsIV.visibility = View.GONE
        }
        else {
            if (mHomeDataModel.data.is_report == "1") {
                binding.optionsIV.visibility = View.GONE
            } else {
                binding.optionsIV.visibility = View.VISIBLE
            }
        }}

        if (mHomeDataModel!!.data.isLike == 1) {
            binding.likeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_fav))
        } else {
            binding.likeIV.setImageDrawable(resources.getDrawable(R.drawable.ic_un_fav))
        }

        if (mHomeDataModel!!.data.isSave == 1) {
            binding.saveIV.setImageDrawable(resources.getDrawable(R.drawable.ic_save))
        } else {
            binding.saveIV.setImageDrawable(resources.getDrawable(R.drawable.ic_unsave))
        }

        Glide.with(mActivity).load(mHomeDataModel?.data!!.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((binding.profileIV))

        Glide.with(mActivity).load(
            AppPrefrences().readString(
                mActivity,
                Constants.PROFILE_IMAGE,
                null
            )
        )
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((binding.myProfileIV))


        if (mHomeDataModel!!.data.image != null) {
            if (mHomeDataModel!!.data.image!!.size > 1) {
                if (mHomeDataModel!!.data.image!![0].contains(".png") || mHomeDataModel!!.data.image!![0].contains(
                        ".jpeg"
                    ) || mHomeDataModel!!.data.image!![0].contains(".jpg") || mHomeDataModel!!.data.image!![0].contains(
                        "image"
                    ) || mHomeDataModel.data.image[0].contains(".gif")||homeData!!.image!![0].contains(".PNG") || homeData!!.image!![0].contains(".JPEG") || homeData!!.image!![0].contains(
                        ".JPG"
                    ) || homeData!!.image!![0].contains("IMAGE") || homeData!!.image!![0].contains(".GIF")
                ) {
                    binding.viewpagerFrame.visibility = View.VISIBLE
                    binding.imgVolumeIV.visibility = View.GONE
                    myCustomPagerAdapter =
                        MyCustomPagerAdapter(mActivity, mHomeDataModel.data.image)
                    binding.viewPager.setAdapter(myCustomPagerAdapter)
                    if (mHomeDataModel!!.data.image!!.size == 1) {
                        binding.indicator.visibility = View.GONE
                        binding.idExoPlayerVIew.visibility = View.GONE
                    } else {
                        binding.indicator.visibility = View.VISIBLE
                        binding.idExoPlayerVIew.visibility = View.GONE
                        binding.indicator.setViewPager(binding.viewPager)
                    }

//        binding.viewPagerIV.visibility=View.VISIBLE
//        Glide.with(mActivity).load(homeData?.image!![0])
//            .placeholder(R.drawable.ic_post_ph)
//            .error(R.drawable.ic_post_ph).centerCrop()
//            .into((binding.viewPagerIV))
                } else {
//        binding.postImageIV.visibility=View.VISIBLE
//        binding.videoView.visibility=View.VISIBLE
//        val uri: Uri = Uri.parse(homeData?.image!![0])
//        binding.videoView.setVideoURI(uri)
//        binding.videoView.start()
//        binding.videoView.setVideo(homeData?.image!![0])
                    Glide.with(mActivity).load(mHomeDataModel?.data!!.thumbnail_image!![0])
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder).centerCrop()
                        .into((binding.postImageIV))
                    binding.postImageIV.visibility = View.VISIBLE
                }
            } else {
                if (mHomeDataModel!!.data.image!![0].contains(".png") || mHomeDataModel!!.data.image!![0].contains(
                        ".jpeg"
                    ) || mHomeDataModel!!.data.image!![0].contains(".jpg") || mHomeDataModel!!.data.image!![0].contains(
                        "image"
                    )|| mHomeDataModel.data.image[0].contains(".gif")||homeData!!.image!![0].contains(".PNG") || homeData!!.image!![0].contains(".JPEG") || homeData!!.image!![0].contains(
                        ".JPG"
                    ) || homeData!!.image!![0].contains("IMAGE") || homeData!!.image!![0].contains(".GIF")
                ) {
                    binding.postImageIV.visibility = View.VISIBLE
                    binding.idExoPlayerVIew.visibility = View.GONE
                    Glide.with(mActivity).load(mHomeDataModel?.data!!.image!![0])
                        .placeholder(R.drawable.ic_post_ph)
                        .error(R.drawable.ic_post_ph).centerCrop()
                        .into((binding.postImageIV))
                    binding.imgVolumeIV.visibility = View.GONE
                } else {
                    binding.idExoPlayerVIew.visibility = View.VISIBLE
                    initializePlayer(mHomeDataModel?.data!!.image!![0])
                }
            }
        }

        if (mHomeDataModel!!.data.likeimage != null) {
            for (i in 0..mHomeDataModel!!.data.likeimage!!.size - 1) {
                if (mHomeDataModel!!.data.likeimage!![i].user_id == getLoggedInUserID()) {
                    Collections.swap(mHomeDataModel!!.data.likeimage, 0, i)
                }
            }
            if (mHomeDataModel?.data!!.likeimage?.size!! >= 3) {
                Glide.with(mActivity).load(mHomeDataModel?.data!!.likeimage!![0].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileOneIV))

                Glide.with(mActivity).load(mHomeDataModel?.data!!.likeimage!![1].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileTwoIV))

                Glide.with(mActivity).load(mHomeDataModel?.data!!.likeimage!![2].image)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((binding.otherUserProfileThreeIV))

                if(mHomeDataModel!!.data.likeimage!![0].username=="N/A"|| mHomeDataModel!!.data.likeimage!![0].username==""|| mHomeDataModel!!.data.likeimage!![0].username==null){
                    mUserName=mHomeDataModel!!.data.likeimage!![0].name
                }
                else{
                    mUserName=mHomeDataModel!!.data.likeimage!![0].username
                }

                if (!mHomeDataModel!!.data.likeimage!![0].username.isNullOrEmpty()) {
                    if (mHomeDataModel!!.data.likeimage!![0].user_id == getLoggedInUserID()) {
                        var sourceString =
                            "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + (Integer.parseInt(
                                mHomeDataModel!!.data.likeCount
                            ) - 1) + " others" + "</b>"
                        binding.likedTV.setText(Html.fromHtml(sourceString))
                    } else {
                        var sourceString =
                            "Liked by" + " " + "<b>" + mUserName + "</b>" + " and " + "<b>" + (Integer.parseInt(
                                mHomeDataModel!!.data.likeCount
                            ) - 1) + " others" + "</b>"
                        binding.likedTV.setText(Html.fromHtml(sourceString))
                    }
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + mHomeDataModel!!.data.likeCount + " others" + "</b>"
                    binding.likedTV.setText(Html.fromHtml(sourceString))
                }

//            var sourceString = "Liked by" +" "+"<b>"+ mHomeDataModel!!.data.likeimage!![0].username+"</b>"+" and "+ "<b>"+mHomeDataModel!!.data.likeCount+" others"+"</b>"
//            binding.likedTV.setText(Html.fromHtml(sourceString))
                binding.likeddTV.visibility = View.GONE
                binding.likedTV.visibility = View.VISIBLE
            } else if (mHomeDataModel?.data!!.likeimage?.size == 2) {
                if(mHomeDataModel!!.data.likeimage!![1].username=="N/A"|| mHomeDataModel!!.data.likeimage!![1].username==""|| mHomeDataModel!!.data.likeimage!![1].username==null){
                    mUserName1=mHomeDataModel!!.data.likeimage!![1].name
                }
                else{
                    mUserName1=mHomeDataModel!!.data.likeimage!![1].username
                }
                if(mHomeDataModel!!.data.likeimage!![0].username=="N/A"|| mHomeDataModel!!.data.likeimage!![0].username==""|| mHomeDataModel!!.data.likeimage!![0].username==null){
                    mUserName=mHomeDataModel!!.data.likeimage!![0].name
                }
                else{
                    mUserName=mHomeDataModel!!.data.likeimage!![0].username
                }

                binding.otherUserProfileOneIV.visibility = View.GONE
                binding.otherUserProfileTwoIV.visibility = View.GONE
                binding.otherUserProfileThreeIV.visibility = View.GONE
                if (mHomeDataModel!!.data.likeimage!![0].user_id == getLoggedInUserID()) {
                    var sourceString =
                        "Liked by" + " " + "<b>" + "You" + "</b>" + " and " + "<b>" + mUserName1 + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + mUserName + "</b>" + " and " + "<b>" +mUserName1 + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                }

                binding.likeddTV.visibility = View.VISIBLE
                binding.likedTV.visibility = View.GONE
            } else if (mHomeDataModel?.data!!.likeimage?.size == 1) {
                if(mHomeDataModel!!.data.likeimage!![0].username=="N/A"|| mHomeDataModel!!.data.likeimage!![0].username==""|| mHomeDataModel!!.data.likeimage!![0].username==null){
                    mUserName=mHomeDataModel!!.data.likeimage!![0].name
                }
                else{
                    mUserName=mHomeDataModel!!.data.likeimage!![0].username
                }
                binding.otherUserProfileOneIV.visibility = View.GONE
                binding.otherUserProfileTwoIV.visibility = View.GONE
                binding.otherUserProfileThreeIV.visibility = View.GONE
                if (mHomeDataModel!!.data.likeimage!![0].user_id == getLoggedInUserID()) {
                    var sourceString = "Liked by" + " " + "<b>" + "You" + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                } else {
                    var sourceString =
                        "Liked by" + " " + "<b>" + mUserName + "</b>"
                    binding.likeddTV.setText(Html.fromHtml(sourceString))
                }
                binding.likeddTV.visibility = View.VISIBLE
                binding.likedTV.visibility = View.GONE
            } else if (mHomeDataModel?.data!!.likeimage?.size == 0) {
                binding.likedRLL.visibility = View.GONE
                binding.likeddTV.visibility = View.GONE
                binding.likedTV.visibility = View.GONE
            }
        } else {
            binding.likedRLL.visibility = View.INVISIBLE
            binding.likeddTV.visibility = View.INVISIBLE
            binding.likedTV.visibility = View.INVISIBLE
        }
        executeGetCommentsApi(mPostId)
    }

    fun initializePlayer(link: String?) {
        if (!link.isNullOrEmpty()) {
            Log.e("Videos", "***Link***" + link)
// progressBarVid.visibility = View.VISIBLE
            binding.idExoPlayerVIew.visibility = View.VISIBLE

            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

            val loadControl =
                DefaultLoadControl.Builder().setBufferDurationsMs(25000, 50000, 100, 300)
                    .createDefaultLoadControl()

            val renderersFactory =
                DefaultRenderersFactory(mActivity).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(mActivity, renderersFactory).build()

            val dataSourceFactory =
                DefaultDataSourceFactory(mActivity, Util.getUserAgent(mActivity, "CloutLyfe"))

            val mediaSources =
                ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(link))
            exoPlayer!!.prepare(mediaSources)
            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
            binding.idExoPlayerVIew.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
            binding.idExoPlayerVIew.player = exoPlayer
            exoPlayer!!.playWhenReady = true
//            exoPlayer!!.seekTo(0)
            binding.idExoPlayerVIew.requestFocus()

// progressBarVid.visibility=View.GONE

//            exoPlayer!!.volume = 0f


            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {}
                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            exoPlayer!!.setPlayWhenReady(true)
//                            progressBarVid.visibility = View.GONE
                        }
                        Player.STATE_BUFFERING -> {
                            exoPlayer!!.seekTo(0)
//                            progressBarVid.visibility = View.VISIBLE
                        }
                        else -> {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }

                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {}
                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {}
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }


    private fun setAdapter(
        mActivity: Activity,
        commentsList: ArrayList<DataXX>,
        mLoadMoreScrollListner: CommentLoadMoreListener
    ) {
        commentsAdapter = CommentsAdapter(
            mActivity,
            commentsList,
            mLoadMoreScrollListner,
            mLikeClickListener,
            mPostId,
            mOtheruserId
        )
        binding.commentsRV.layoutManager = LinearLayoutManager(mActivity)
        binding.commentsRV.adapter = commentsAdapter
    }


    private fun executeGetCommentsApi(post_id: String?) {
//        commentsList.clear()
        if (isNetworkAvailable(mActivity))
            RequestCommentsData(post_id)
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        mMap["last_commentid"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCommentsData(post_id: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getCommentRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetCommentsModel> {
            override fun onFailure(call: Call<GetCommentsModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetCommentsModel>,
                response: Response<GetCommentsModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                commentsList.clear()
                mCommentsDataModel = response.body()!!
                if (mCommentsDataModel.status == 1) {
                    isLoading = !mCommentsDataModel.lastPage.equals(true)
                    commentsList.addAll(response.body()!!.data)
                    if (homeData != null) {
                        homeData!!.commentCount = commentsList.size.toString()
                    }
//                    else{
//                        mHomeDataModel.data.commentCount=commentsList.size.toString()
//                    }

                    binding.commentTV.setText(commentsList.size.toString())

                    Log.e(TAG, "RequestHomeDataAPI: " + commentsList)
                    if (commentsList.size == 0) {

                        binding.upArraowRL.visibility = View.GONE
//                        binding.txtNoDataTV.text =
//                            getString(R.string.no_posts_available)

                    } else {
                        binding.upArraowRL.visibility = View.VISIBLE
                        setAdapter(mActivity, commentsList, mLoadMoreScrollListner)
//                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCommentsDataModel.status == 3) {

//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

                    binding.upArraowRL.visibility = View.GONE
//                    binding.txtNoDataTV.visibility = View.VISIBLE
//                    binding.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    var mLoadMoreScrollListner: CommentLoadMoreListener = object : CommentLoadMoreListener {
        override fun onLoadMoreListner(mModel: ArrayList<DataXX>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
        }
    }


    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreSavedRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        binding.mProgressRL.visibility = View.VISIBLE
        val call = RetrofitInstance.appApi.getCommentRequest(mHeaders, mLoadMoreParam())
        call.enqueue(object : Callback<GetCommentsModel> {
            override fun onFailure(call: Call<GetCommentsModel>, t: Throwable) {
                dismissProgressDialog()
                binding.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<GetCommentsModel>,
                response: Response<GetCommentsModel>
            ) {
                binding.mProgressRL.visibility = View.GONE
                val mGetSavedModel = response.body()
                if (mGetSavedModel!!.status == 1) {
                    mGetSavedModel.data.let {
                        commentsList.addAll<DataXX>(
                            it
                        )
                    }
                    commentsAdapter?.notifyDataSetChanged()
                    isLoading = !mGetSavedModel.lastPage.equals(true)
                } else if (mGetSavedModel.status == 0) {
//                    showToast(mActivity, mGetSavedModel.message)
                }
            }
        })
    }


    var mLikeClickListener: LikeCommentInterface = object : LikeCommentInterface {
        override fun onItemClickListner(
            mPosition: Int,
            comment_id: String,
            likeIV: ImageView,
            postId: String,
            commentBy: String
        ) {
            mCommentId = comment_id
            var mPos = mPosition
            var mimg: ImageView = likeIV
//            if(getLoggedInUserID()==commentBy){
            deleteCommentApi(mPos)
//        }
//            increaseLikeCount(mPos!!, mimg, mCommentId)
        }
    }


    private fun deleteCommentApi(mPos: Int) {
        if (isNetworkAvailable(mActivity))
            RequestDeleteComment(mPos)
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteCommentParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["comment_id"] = mCommentId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteComment(mPos: Int) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.deleteCommentRequest(mHeaders, mDeleteCommentParam())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                if (response.body()!!.status == 1) {
//                    showToast(mActivity, response.body()!!.message)
                    commentsList.removeAt(mPos)
                    binding.commentTV.setText(commentsList.size.toString())
                    commentsAdapter!!.notifyDataSetChanged()
                    executeGetCommentsApi(mPostId)
                } else if (response.body()!!.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

                }
            }
        })
    }


    private fun increaseLikeCount(
        position: Int,
        imageView: ImageView,
        comment_id: String
    ) {
        if (commentsList[position].isLike == 0) {
            imageView.setImageResource(R.drawable.ic_comment_like)
            commentsList[position].isLike = 1

//            val count = ((commentsList[position].likeCount.toInt()) + 1).toString()
//            commentsList[position].likeCount = count
//            likeTextView.text = imageList[position].likeCount
//Like Api Execute
            executeLikeUnlikeApi(position, comment_id, imageView)
        } else {
            imageView.setImageResource(R.drawable.ic_like)
//            imageView.setColorFilter(ContextCompat.getColor(mActivity, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);

            commentsList[position].isLike = 0
//            val count = ((commentsList[position].likeCount.toInt()) - 1).toString()
//            commentsList[position].likeCount = count
//            likeTextView.text = commentsList[position].likeCount
////Like Api Execute
            executeLikeUnlikeApi(position, comment_id, imageView)
        }
    }


    private fun executeLikeeUnlikeApi(
        post_id: String
    ) {
        if (!mActivity.let { isNetworkAvailable(it) }) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestLikeeUnlikeData(post_id)
        }
    }

    private fun RequestLikeeUnlikeData(
        post_id: String
    ) {
        var user_id = getLoggedInUserID()
        var post_id = post_id
        val body = RequestBodies.LikeUnlikeBody(user_id, post_id)
        likeUnlikeViewwModel.likeUnlikeRequest(body)
        likeUnlikeViewwModel._likeUnlikeResponse.observe(
            this,
            androidx.lifecycle.Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            response.data?.let { likeUnlikeResponse ->
                                if (likeUnlikeResponse.status == 1 || likeUnlikeResponse.status == 200) {
                                } else {
                                    //                                    dismissProgressDialog()
                                }
                            }
                        }

                        is Resource.Error -> {
                            //                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(mActivity, response.message)
                            }
                        }

                        is Resource.Loading -> {
                            //                            showProgressDialog(mActivity)
                        }
                    }
                }
            })
    }


    private fun executeLikeUnlikeApi(
        mPosition: Int,
        comment_id: String,
        likeIV: ImageView
    ) {
        if (!mActivity.let { isNetworkAvailable(it) }) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestLikeUnlikeData(mPosition, comment_id, likeIV)
        }
    }

    private fun RequestLikeUnlikeData(
        mPosition: Int,
        comment_id: String,
        likeIV: ImageView
    ) {
        var user_id = getLoggedInUserID()
        var comment_id = comment_id
        val body = RequestBodies.LikeUnlikeCommentBody(user_id, comment_id)
        likeUnlikeViewModel.likeUnlikeCommentRequest(body)
        likeUnlikeViewModel._likeUnlikeResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { likeUnlikeResponse ->
                            if (likeUnlikeResponse.status == 1 || likeUnlikeResponse.status == 200) {
                            } else {
                                //                                    dismissProgressDialog()
                            }
                        }
                    }

                    is Resource.Error -> {
                        //                            dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        //                            showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    private fun showReportDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.report_dilaog, null)

        var dialogPrivacy: BottomSheetDialog? =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
//cancel button click
        val reportLL: LinearLayout? = dialogPrivacy.findViewById(R.id.reportLL)

        reportLL?.setOnClickListener {
            if (reportResonslist != null) {
                reportResonslist.clear()
            }
            getReasons()

            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }

    private fun getReasons() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performResonsRequest()
        }
    }

    private fun performResonsRequest() {
        var user_id = getLoggedInUserID()
        val body = RequestBodies.ReportReasonsBody(
            user_id
        )
        getReportReasonViewModel.getReportResons(body)
        getReportReasonViewModel.reportReasonsResponse.observe(
            this,
            androidx.lifecycle.Observer { event ->
                event.getContentIfNotHandled()?.let { response ->
                    when (response) {
                        is Resource.Success -> {
                            dismissProgressDialog()
                            response.data?.let { getReportResonse ->
                                if (getReportResonse.status == 1 || getReportResonse.status == 200) {
                                    reportResonslist.addAll(getReportResonse.data)
                                    showBottomReportDialog()
                                } else {
                                    showAlertDialog(mActivity, getReportResonse.message)

                                }
                            }
                        }

                        is Resource.Error -> {
                            dismissProgressDialog()
                            response.message?.let { message ->
                                showAlertDialog(mActivity, message)
                            }
                        }

                        is Resource.Loading -> {
                            showProgressDialog(mActivity)
                        }
                    }
                }
            })

    }

    var dialogReport: BottomSheetDialog? = null
    private fun showBottomReportDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.report_bottomsheet, null)
        dialogReport =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogReport!!.setContentView(view)
        dialogReport!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogReport!!.setCancelable(true)
//cancel button click
        val reportRV: RecyclerView? = dialogReport!!.findViewById(R.id.reportRV)

        reportRV!!.layoutManager = LinearLayoutManager(mActivity)
        mReportAdapter = ReportAdapter(
            mActivity,
            reportResonslist, mItemClickListener
        )
        reportRV.adapter = mReportAdapter

//        reportRV?.setOnClickListener {
//            dialogReport.dismiss()
//        }
        dialogReport!!.show()
    }


    var mItemClickListener: ReportItemClickInterface = object : ReportItemClickInterface {
        override fun onItemClickListner(mId: String) {
            reasonId = mId
            performSubmitClick()
        }
    }


    private fun performSubmitClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performSubmit()
        }
    }

    private fun performSubmit() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        var reasonId = reasonId
        var reportedId = mOtheruserId
        var reportType = "1"
        var reportedBy = getLoggedInUserID()
        val body = RequestBodies.ReportBody(
            user_id, post_id.toString(), reasonId!!, reportedId!!, reportType, reportedBy!!
        )
        reportViewModel.addReportRequest(body)
        reportViewModel._reportResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { forgotResponse ->
                            if (forgotResponse.status == 1 || forgotResponse.status == 200) {
                                dialogReport!!.dismiss()
                                binding.optionsIV.visibility = View.GONE
//                    setTopSwipeRefresh()
//                    executeHomeApi()
                                showAlertDialog(mActivity, forgotResponse.message)
                            } else {
                                dialogReport!!.dismiss()
                                showAlertDialog(mActivity, forgotResponse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })

    }


    private fun showShareDialog() {
        dismissProgressDialog()
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.share_bottomsheet, null)
        var dialogReport: BottomSheetDialog? =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)

        dialogReport!!.setContentView(view)
        dialogReport.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogReport.setCancelable(true)
//cancel button click
        val bottomsheetRV: RecyclerView? = dialogReport.findViewById(R.id.bottomsheetRV)
        val txtSendTV: TextView? = dialogReport.findViewById(R.id.txtSendTV)
        val editSearchET: EditText? = dialogReport.findViewById(R.id.editSearchET)

        editSearchET!!.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                filter(s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
//        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//        list.add("Grave Yard")
//        list.add("Danel Bros")
        mAdapter = ShareBottomSheetAdapter(mActivity, cloutsList, mOnItemCheckListener)

        bottomsheetRV?.layoutManager = LinearLayoutManager(mActivity)
        bottomsheetRV?.adapter = mAdapter

        txtSendTV?.setOnClickListener {
            dialogReport.dismiss()
            performSendClick()
        }
        dialogReport.show()
    }


    fun filter(text: String?) {
        val temp: MutableList<DataXXX> = ArrayList()
        for (d in cloutsList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.name.toLowerCase().contains(text.toString())) {
                temp.add(d)
            }
        }
        //update recyclerview
        mAdapter!!.updateList(temp)
    }


    var mOnItemCheckListener: ShareBottomSheetAdapter.OnItemCheckListener = object :
        ShareBottomSheetAdapter.OnItemCheckListener {
        override fun onItemCheck(item: DataXXX?) {
            mShareList.add(item!!.user_id)

        }

        override fun onItemUncheck(item: DataXXX?) {
            mShareList.remove(item!!.user_id)

        }
    }


    private fun isValidateShare(): Boolean {
        var flag = true
        when {
            (mShareList.size <= 0) -> {
                showAlertDialog(mActivity, getString(R.string.share_list))
                flag = false
            }
        }
        return flag
    }

    private fun performSendClick() {
        preventMultipleClick()
        if (isNetworkAvailable(mActivity)) {
            if (isValidateShare()) {
                executeSendPostRequest()
            }
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }

    /*
    * Execute api param
    * */
    private fun mShareParam(): MutableMap<String?, String?> {
        val s = TextUtils.join(", ", mShareList)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["message"] = ""
        mMap["room_id"] = ""
        mMap["other_id"] = s
        mMap["post_id"] = mSharePostId
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendPostRequest() {
        val call = RetrofitInstance.appApi.sharePostRequest(mShareParam())
        call.enqueue(object : Callback<SendMessageModel> {
            override fun onFailure(call: Call<SendMessageModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<SendMessageModel>,
                response: Response<SendMessageModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel?.status == 1) {
                    showAlertDialog(mActivity, mModel.message)
                    userslist.addAll(mModel.room_ids)
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    for (i in userslist.indices) {
                        mSocket!!.emit("newMessage", userslist[i].room_id, mOjectString)
                    }

//                    var mIntent = Intent(activity, ChatActivity::class.java)
//                    mIntent.putExtra(Constants.USER_NAME,  mModel.data.username)
//                    mIntent.putExtra(Constants.ROOM_ID, mModel.data.room_id)
//                    startActivity(mIntent)
//                    mMsgArrayList!!.add(mModel.data)

                } else if (mModel?.status == 0) {
                    showToast(mActivity, mModel.message)
                }
            }
        })
    }


//    override fun onPrepared() {
//        Handler(Looper.getMainLooper()).postDelayed({
//            binding.videoView.visibility = View.GONE
//        },300)
//
//    }

}