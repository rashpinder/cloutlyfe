package com.cloutlyfe.app.activities

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.GoLiveRVAdapter
import com.cloutlyfe.app.databinding.ActivityGoLiveBinding
import com.cloutlyfe.app.model.CloutHomeRVModel

class GoLiveActivity : BaseActivity() {
    var mLiveStreamAdapter: GoLiveRVAdapter? = null
    lateinit var binding: ActivityGoLiveBinding
    var mList: ArrayList<CloutHomeRVModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGoLiveBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))
        mList.add(CloutHomeRVModel(R.drawable.ic_post_ph))

        binding.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        setLiveUsersRVAdapter()
    }


    fun setLiveUsersRVAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        binding.LikesRV.layoutManager = layoutManager
        mLiveStreamAdapter = GoLiveRVAdapter(mActivity, mList)
        binding.LikesRV.adapter = mLiveStreamAdapter

    }
}