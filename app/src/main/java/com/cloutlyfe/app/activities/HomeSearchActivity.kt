package com.cloutlyfe.app.activities

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.HomeSearchAdapter
import com.cloutlyfe.app.databinding.ActivityHomeSearchBinding

import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.ItemClicklistenerInterface
import com.cloutlyfe.app.model.SearchData
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.SearchViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory
import java.util.*


class HomeSearchActivity : BaseActivity() {
    lateinit var binding: ActivityHomeSearchBinding
    var mhomeSearchList: ArrayList<String> = ArrayList<String>()
    var homeSearchAdapter: HomeSearchAdapter? = null
    lateinit var searchViewModel: SearchViewModel
    var data_list = mutableListOf<SearchData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        sesrchTextFunc()

//        executeApi()

//        setAdapter()

        binding.imgBackIV.setOnClickListener {
            onBackPressed()
        }
    }


    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        searchViewModel = ViewModelProvider(this, factory).get(SearchViewModel::class.java)
    }

    //text write check
    private fun sesrchTextFunc() {
        binding.editSearchET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                binding.txtNoDataTV.visibility=View.GONE
                binding.homeSearchRV.visibility=View.GONE
                if(charSequence.length==0){
                    if (data_list!=null){
                        data_list.clear()
                    }}
//                    binding.homeSearchRV.visibility=View.GONE
//                }
//                else{
//                    binding.homeSearchRV.visibility=View.VISIBLE
//                    executeApi(charSequence.toString())
//                }
            }

            override fun afterTextChanged(editable: Editable) {
                val string = editable.trim().toString()

                if(string.length == 0){
                    data_list.clear()
                    binding.homeSearchRV.visibility=View.GONE
                }
                else{
                    binding.homeSearchRV.visibility=View.VISIBLE
//                    val handler = Handler(Looper.getMainLooper())
//                    handler.postDelayed(Runnable {
                        executeApi(string)
//                    }, 1500)
                }


//                if ((string.length>0) && (string.length.toString()!=""))
//                {
////                    finalExit=false
////                    txCancelClear.setText(getString(R.string.search))
////                    canExit=false
//                    if (data_list.size > 1) {
////                        homeSearchAdapter.filter!!.filter(string)
//                        binding.homeSearchRV.visibility=View.VISIBLE
//                    }
//                }
//
//                else
//                {
//                    finalExit=false
//                    txCancelClear.setText(getString(R.string.cancel))
//                    canExit=true
//                    binding.homeSearchRV.visibility=View.VISIBLE
//                    val handler = Handler(Looper.getMainLooper())
//                    handler.postDelayed(Runnable {
//                        executeApi(string)
//                    }, 4000)


//                }
//                else{
//                    if (data_list!=null){
//                        data_list.clear()
//                    }
//                    binding.homeSearchRV.visibility=View.GONE
//                }
            }
        })
    }


    private fun executeApi(string: String) {
        binding.txtNoDataTV.visibility= View.GONE
        if (data_list!=null){
            data_list.clear()
        }
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.no_internet_connection))
        } else {
            executeSearchApi(string)
        }
    }

    private fun executeSearchApi(string: String) {
        var user_id = getLoggedInUserID()
        var search = string
        var perPage = "10000"
        var lastUserId = ""
        val body = RequestBodies.SearchHomeBody(
            user_id,
            search,
            lastUserId,
            perPage
        )
        searchViewModel.searchData(getAuthToken(),body,mActivity)

        searchViewModel.resultResponse.observe(this, androidx.lifecycle.Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { response ->
                            if (response.status==1||response.status==200) {
                                binding.txtNoDataTV.visibility= View.GONE
                                binding.homeSearchRV.visibility= View.VISIBLE
//                                if(binding.editSearchET.text.equals("")||binding.editSearchET.text==null){
                                if (search.length==0){
                                    data_list.clear()
                                    binding.homeSearchRV.visibility= View.GONE
                                }
//                            }
                                else{
                                data_list.addAll(response.data)
                                setAdapter(response.data)}
                            }
                            else{
                                binding.homeSearchRV.visibility= View.GONE

//                                if (data_list!=null&&data_list.size>0)
//                                {
//                                    data_list.clear()
//                                }
                                binding.txtNoDataTV.visibility= View.VISIBLE
                            }
                        }
                    }

                    is Resource.Error -> {
                       dismissProgressDialog()
                        response.message?.let { message ->
                            showToast(mActivity,message)

                        }
                    }

                    is Resource.Loading -> {
                        // Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    var mItemClickListener: ItemClicklistenerInterface = object : ItemClicklistenerInterface {
        override fun onItemClickListner(mPosition: Int, userId: String) {
//            showEditDeleteDialog()
        }
    }

    private fun setAdapter(data: List<SearchData>) {
        binding.homeSearchRV.layoutManager = LinearLayoutManager(mActivity)
        homeSearchAdapter = HomeSearchAdapter(mActivity,
            data as MutableList<SearchData>,mItemClickListener)
        binding.homeSearchRV.adapter = homeSearchAdapter

    }





}