package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.hardware.*
import android.hardware.Camera
import android.media.*
import android.net.Uri
import android.os.*
import android.os.StrictMode.VmPolicy
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.view.ViewTreeObserver.OnTouchModeChangeListener
import android.widget.LinearLayout
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.motion.widget.OnSwipe
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.FilterActivity.Companion.VideoUrii
import com.cloutlyfe.app.adapters.ImageAdapter
import com.cloutlyfe.app.databinding.ActivityAddStoryBinding
import com.cloutlyfe.app.interfaces.OptionsInterface
import com.cloutlyfe.app.model.ImageModel
import com.cloutlyfe.app.model.StatusModel
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.OnSwipeTouchListener
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.iammert.library.cameravideobuttonlib.CameraVideoButton
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import com.wonderkiln.camerakit.*
import kotlinx.android.synthetic.main.activity_add_story.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.util.*
import kotlin.math.roundToInt
import kotlin.math.sqrt


class AddStoryActivity : BaseActivity(), Camera.PictureCallback, SurfaceHolder.Callback,
    SensorEventListener, Player.EventListener{
    lateinit var binding: ActivityAddStoryBinding
    var imageList = ArrayList<ImageModel>()
    var count: Int = 10
    var surfaceHolder: SurfaceHolder? = null
    var start_time: Long? = null
    var end_time: Long? = null
    var pos: Int? = 0
    var camera: Camera? = null
    var is_click: Boolean? = false
    var isFrontCamOpen = true
    var cameraId = 1
    var imageAdapter: ImageAdapter? = null
    var mBitmap: Bitmap? = null
    val selectedImagesAsFile = ArrayList<Uri>()
    var bitmapCamera: Bitmap? = null
    var bitmapCameraa: Bitmap? = null
    var exoPlayer: SimpleExoPlayer? = null
    var activity: Activity = this@AddStoryActivity
    var videoUri: Uri? = null
    var is_recording = false
    var zoomFactor = 1.0f
    var uid = 0
    var audio: MediaPlayer? = null
    var running = false
    var ll: LinearLayout? = null
    var videoThumbnail_bitmap: Bitmap? = null
    var videoUrl: String? = null
    var file: File? = null
    var timeInMillisec: String? = null
    private var listener: CameraVideoButton.ActionListener? = null


//    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//        when (event!!.actionMasked) {
//            MotionEvent.ACTION_DOWN -> {
//                showToast(this@AddStoryActivity, "DOWNNNNNN")
//            }
//            MotionEvent.ACTION_CANCEL,
//            MotionEvent.ACTION_UP -> {
//                showToast(this@AddStoryActivity, "UPPPPPPP")
//            }
//            MotionEvent.ACTION_MOVE -> {
//                showToast(this@AddStoryActivity, "action_move")
//                return true
//            }
//
//        }
//
//        return false
//    }



    private var mInitialX = 0f
    private var mInitialY = 0f
    private val mZoomThreshold = 50f // Adjust this value to suit your needs


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        binding.cameraView.addCameraKitListener(object : CameraKitEventListener {
            override fun onEvent(cameraKitEvent: CameraKitEvent) {}
            override fun onError(cameraKitError: CameraKitError) {}
            override fun onImage(cameraKitImage: CameraKitImage) {}
            override fun onVideo(cameraKitVideo: CameraKitVideo) {}
        })
        releaseCamera()
        binding.cameraView.start()
        binding.cameraView.facing = 1

        makeDir()
        surfaceHolder = binding.surfaceView.getHolder();
        surfaceHolder!!.addCallback(this)
        surfaceHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        binding.startttBtn.setVideoDuration(60000)
        binding.startttBtn.enableVideoRecording(true)
        binding.startttBtn.enablePhotoTaking(true)

        binding.addImgRL.setOnClickListener {
            performAddStatuslick()
        }

        binding.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.cameraView.setOnTouchListener(object : OnSwipeTouchListener(mActivity) {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                return super.onTouch(v, event)
                when (event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)

//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "DOWNNNNNN")
                    }
                    MotionEvent.ACTION_CANCEL,
                    MotionEvent.ACTION_UP -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)
//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "UPPPPPPP")
                    }
                    MotionEvent.ACTION_MOVE -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)
//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "action_move")
                        return true
                    }
            }
                return super.onTouch(v, event)
        }})


  binding.startttBtn.setOnTouchListener(object : OnSwipeTouchListener(mActivity) {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                return super.onTouch(v, event)
                when (event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)
//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "DOWNNNNNN")
                    }
                    MotionEvent.ACTION_CANCEL,
                    MotionEvent.ACTION_UP -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)
//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "UPPPPPPP")
                    }
                    MotionEvent.ACTION_MOVE -> {
                        binding.cameraView.setPinchToZoom(true)
                        binding.cameraView.setZoom(0.2f)
//                        scaleGestureDetector.onTouchEvent(event)
                        showToast(this@AddStoryActivity, "action_move")
                        return true
                    }
            }
                return super.onTouch(v, event)
        }})

        binding.imgFilterIV.setOnClickListener {
            val intent = Intent(activity, FilterActivity::class.java)
            if (videoUri != null) {
                intent.putExtra("videoUri", videoUri.toString())
                intent.putExtra("file", file!!.path)
                startActivityForResult(intent, 102)
            }

            if (!imageUri.isNullOrEmpty()) {
                val args = Bundle()
                args.putSerializable(
                    "selectedImagesArrayList",
                    selectedImagesArrayList as ArrayList<String>
                )
                intent.putExtra("bundle", args)
                intent.putExtra("imageUri", imageUri)
                intent.putExtra("position", pos)
                intentLauncher.launch(intent)
            }

            if (!filePath.isNullOrEmpty()) {
                intent.putExtra("filePath", filePath)
                intentLauncher.launch(intent)

            }
        }

        surfaceView.getHolder().setFormat(PixelFormat.RGBA_8888)

        addStoryLL.setOnClickListener(View.OnClickListener {
            showStroryProgressDialog(activity)
            is_click = true
            if (imageList!!.size > 0 || bitmapCamera != null || selectedImagesAsFile != null) {
                if (SystemClock.elapsedRealtime() - mLastClickkTab1 < 4000) {
                    return@OnClickListener
                }
                mLastClickkTab1 = SystemClock.elapsedRealtime()
                if (is_click == true) {
                    executeMulitpleStatusImage()
                }
            } else {
                showAlertDialog(activity, "Please select Image to add Story!")
            }
        })

        imgSwitchCameraIV.setOnClickListener(View.OnClickListener {
            if (isFrontCamOpen) {
                releaseCamera()
                cameraId = 0
                isFrontCamOpen = false
            } else {
                releaseCamera()
                cameraId = 1
                isFrontCamOpen = true
            }
            if (cameraView != null) {
                cameraView.toggleFacing()
            }
        })

        if (surfaceView != null) {
            setViewVisibility(R.id.startBtnRL, View.VISIBLE)
            setViewVisibility(R.id.surfaceView, View.VISIBLE)
            surfaceHolder = surfaceView!!.holder
            surfaceHolder!!.addCallback(this@AddStoryActivity)
        }

        binding.startttBtn.actionListener = object : CameraVideoButton.ActionListener {
            override fun onStartRecord() {
                start_time = Calendar.getInstance().getTimeInMillis()
                binding.cameraView.visibility = View.VISIBLE
                binding.surfaceView.visibility = View.GONE
                binding.addImgRL.visibility = View.GONE
                binding.imgSwitchCameraIV.visibility = View.GONE
                binding.cameraView.start()
                makeDir()
                Start_or_Stop_Recording()
            }

            override fun onEndRecord() {
                end_time = Calendar.getInstance().getTimeInMillis()
                val diff = end_time!! - start_time!!
                val duration = diff
                timeInMillisec = duration.toString()
                binding.idExoPlayerVIew.visibility = View.VISIBLE
                binding.surfaceView.visibility = View.GONE
                binding.cameraView.visibility = View.GONE
                binding.picRL.visibility = View.GONE
                var uri: Uri? = null
                uri = FileProvider.getUriForFile(
                    this@AddStoryActivity,
                    "com.cloutlyfe.app" + ".provider",
                    file!!
                )
                videoUri = uri
                initializePlayer(uri)
                try {
                    if (binding.cameraView != null) {
                        binding.cameraView.stopVideo()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                binding.addStoryLL!!.visibility = View.VISIBLE
                binding.startttBtn.visibility = View.GONE
                binding.addImgRL.visibility = View.GONE
                binding.imgSwitchCameraIV.visibility = View.GONE
                running = false
                imgFilterIV.visibility = View.VISIBLE
            }

            override fun onDurationTooShortError() {

            }

            override fun onSingleTap() {
                binding.cameraView.visibility = View.GONE
                binding.surfaceView.visibility = View.VISIBLE
                binding.startttBtn.visibility = View.GONE
                binding.addImgRL.visibility = View.GONE
                binding.imgSwitchCameraIV.visibility = View.GONE
                if (isFrontCamOpen) {
                    releaseCamera()
                    cameraId = 1
                    startCamera(1)
                    isFrontCamOpen = false
                } else {
                    releaseCamera()
                    cameraId = 0
                    startCamera(0)
                    isFrontCamOpen = true
                }
                setupSurfaceHolder()

                Log.v("TEST", "Take photo here")
            }
        }


}
    private val scaleGestureDetector: ScaleGestureDetector by lazy {
        ScaleGestureDetector(mActivity, ScaleListener())
    }

    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        private var scaleFactor = 1f

        override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
            return true
        }

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            scaleFactor *= detector.scaleFactor
            scaleFactor = 5f
            binding.cameraView.setZoom(scaleFactor)
            // Apply the zoom factor to your camera or video recorder here
            // ...
            return true
        }

        override fun onScaleEnd(detector: ScaleGestureDetector) {
            scaleFactor = 1f
        }
    }


    // Function to calculate the distance between two fingers
    private fun getDistance(event: MotionEvent): Float {
        val x1 = event.getX(0)
        val y1 = event.getY(0)
        val x2 = event.getX(1)
        val y2 = event.getY(1)
        val dx = x2 - x1
        val dy = y2 - y1
        return sqrt(dx * dx + dy * dy)
    }

//    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var textureView: TextureView? = null



//    private fun initScaleGestureDetector() {
//        scaleGestureDetector = ScaleGestureDetector(
//            mActivity,
//            object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
//                private var scaleFactor = 1.0f
//                override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
//                    scaleFactor = 1.0f
//                    return true
//                }
//
//                override fun onScale(detector: ScaleGestureDetector): Boolean {
//                    val newScaleFactor = scaleFactor * detector?.scaleFactor!!
//                    binding.idExoPlayerVIew.scaleX = newScaleFactor.coerceIn(0.5f, 2.0f)
//                    binding.idExoPlayerVIew.scaleY = newScaleFactor.coerceIn(0.5f, 2.0f)
//                    scaleFactor = newScaleFactor
//                    return true
//                }
//            })}

//        binding.surfaceView.setOnTouchListener { _, event ->
//            scaleGestureDetector?.onTouchEvent(event)
//            true
//        }



//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        val result = gestureDetector?.onTouchEvent(event!!)
//        return if (!result!!) {
//            super.onTouchEvent(event)
//        } else true
//    }

    private var gestureDetector: GestureDetector? = null
    private var imageUri: String? = ""
    override fun onResume() {
        super.onResume()
        if (VideoUrii.toString() != "null" && VideoUrii.toString() != "" && VideoUrii.toString() != null) {
            Log.e(TAG, "onResume: " + VideoUrii)
            binding.addStoryLL.visibility = View.VISIBLE
            binding.startttBtn.visibility = View.GONE
            binding.addImgRL.visibility = View.GONE
            binding.imgSwitchCameraIV.visibility = View.GONE
            binding.idExoPlayerVIew.visibility = View.VISIBLE
            binding.surfaceView.visibility = View.GONE
            binding.cameraView.visibility = View.GONE
            binding.picRL.visibility = View.GONE
            initializePlayer(VideoUrii)
            videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                VideoUrii.toString(),
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
            )
        } else {
            VideoUrii = null
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val result = data!!.getStringExtra("data")
            // do something with the result
        }
    }


    var progressStoryDialog: Dialog? = null

    /*
    * Show Progress Dialog
    * */
    fun showStroryProgressDialog(activity: Activity?) {
        progressStoryDialog = Dialog(activity!!)
        progressStoryDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressStoryDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressStoryDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressStoryDialog!!.setCanceledOnTouchOutside(false)
        progressStoryDialog!!.setCancelable(false)
        if (progressStoryDialog != null)
            progressStoryDialog!!.show()
    }

    /*
      * Hide Progress Dialog
      * */
    fun dismissStroryProgressDialog() {
        if (!isFinishing()) {
            if (progressStoryDialog != null && progressStoryDialog!!.isShowing) {
                progressStoryDialog?.dismiss()
            }
        }

    }

    fun initializePlayer(link: Uri?) {
        if (!link!!.equals("")) {
            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
            val renderersFactory =
                DefaultRenderersFactory(activity).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(activity, renderersFactory).build()

            val dataSourceFactory =
                DefaultDataSourceFactory(activity, Util.getUserAgent(activity, "CloutLyfe"))

            val mediaSources =
                ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(link)
            exoPlayer!!.prepare(mediaSources)
            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
            binding.idExoPlayerVIew.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
            binding.idExoPlayerVIew.player = exoPlayer
            exoPlayer!!.playWhenReady = true
            binding.idExoPlayerVIew.requestFocus()


            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {

                }

                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            dismissStroryProgressDialog()
                            exoPlayer!!.setPlayWhenReady(true)
                        }
                        Player.STATE_BUFFERING -> {
                            showStroryProgressDialog(activity)
                            exoPlayer!!.seekTo(0)
                        }
                        else -> {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }

                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {
                    dismissStroryProgressDialog()
                }

                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {}
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }

    fun Start_or_Stop_Recording() {
        makeDir()
        is_recording = true
        file = null
        file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "myvideo" + System.currentTimeMillis() + ".mp4"
        )
        selectedImagesAsFile.add(Uri.fromFile(file))
        binding.cameraView.captureVideo(file)
        videoUrl = (Uri.fromFile(file).toString())
        videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
            videoUrl.toString(),
            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
        )
    }

    private fun makeDir() {
        val dir = File(getApplicationContext().externalCacheDir, "myvideo" + ".mp4")

        try {
            if (!dir.exists()) {
                println("Directory created")
                dir.mkdir()
            } else {
                println("Directory is not created")
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun performAddStatuslick() {
        if (imageList!!.size == 10) {
            showToast(activity, getString(R.string.you_can_add_max_of_10image))
        } else {
            performAddClick()
        }
    }

    fun performAddClick() {
        performGalleryClick()
    }

    private fun performGalleryClick() {
        openGalleryForImage()
    }

    private fun openGalleryForImage() {
        val intent = Lassi(activity)
            .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA or CAMERA_AND_GALLERY
            .setMaxCount(count)
            .setGridSize(3)
            .setMediaType(MediaType.IMAGE) // MediaType : VIDEO IMAGE, AUDIO OR DOC
            .build()
        receiveData.launch(intent)
    }


    private var selectedImagesArrayList: ArrayList<String> = ArrayList()
    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                timeInMillisec = "8000"
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
                if (!selectedMedia.isEmpty()) {
                    startttBtn.visibility = View.GONE
                    startBtnRL.visibility = View.GONE
                    imgFilterIV.visibility = View.VISIBLE
                    addStoryLL!!.visibility = View.VISIBLE
                    imgSwitchCameraIV.visibility = View.GONE
                    imgCapturedIV!!.visibility = View.VISIBLE
                    Log.e("imagesLoadSize", selectedMedia.toString())
//                    count = count - selectedMedia.size
                    count = 10 - (imageList.size + selectedMedia.size)

                    if (selectedImagesArrayList.isNotEmpty()) {
                        selectedImagesArrayList.clear()
                    }
                    if (selectedMedia.isNotEmpty()) {

                        for (i in 0 until selectedMedia.size) {
                            val ImageModel = ImageModel()
                            ImageModel.setImage(selectedMedia.get(i).path)
                            imageList.add(ImageModel)
                            selectedImagesAsFile.add(Uri.fromFile(File(imageList!![i].getImage())))
                            selectedImagesArrayList.add(Uri.fromFile(selectedMedia[i].path?.let { it1 ->
                                File(
                                    it1
                                )
                            }).toString())
                        }
                    }

                    Log.e(TAG, "counttt: " + count)
                    setImageList()
                }
            }
        }

    var mOptionsClickListener: OptionsInterface = object : OptionsInterface {
        override fun onItemClickListner(mPosition: Int, postId: String) {
            performRemoveClick(mPosition)
        }
    }

    private fun performRemoveClick(mPosition: Int) {
        if (imageList.size == 1) {
            imageList.removeAt(mPosition)
            selectedImagesAsFile.removeAt(mPosition)
            count++
            startBtnRL.visibility = View.VISIBLE
            imgSwitchCameraIV.visibility = View.VISIBLE
            picRL!!.visibility = View.GONE
            addStoryLL!!.visibility = View.GONE
            startBtnRL!!.visibility = View.VISIBLE
            imgCapturedIV!!.visibility = View.VISIBLE
            surfaceView.visibility = View.VISIBLE
            binding.startttBtn.visibility = View.VISIBLE
            binding.imgSwitchCameraIV.visibility = View.VISIBLE
            binding.imgFilterIV.visibility = View.GONE

        } else if (imageList.size > 0) {
            try {
                imgCapturedIV!!.visibility = View.VISIBLE
                imageList.removeAt(mPosition)
                selectedImagesAsFile.removeAt(mPosition)
                count++
                Glide.with(activity)
                    .load(imageList!!.get(0).getImage())
                    .placeholder(R.color.grey)
                    .transition(DrawableTransitionOptions.withCrossFade(500))
                    .into(imgCapturedIV)
                imageList!![0].setSelected(true)
            } catch (e: IndexOutOfBoundsException) {

            }

        } else {

        }
        imageAdapter?.notifyDataSetChanged()
    }

    override fun onSensorChanged(event: SensorEvent?) {

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    // Add image in selectedImageList
    fun selectImage(position: Int) {
        pos = position
        picRL!!.visibility = View.VISIBLE
        Glide.with(activity)
            .load(imageList.get(position).getImage())
            .placeholder(R.color.grey)
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(imgCapturedIV)
        imageList[position].setSelected(true)
        imageUri = selectedImagesArrayList.get(position)
        imageAdapter!!.notifyDataSetChanged()
    }

    @JvmName("setImageList1")
    fun setImageList() {
        val layoutManager =
            LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView!!.layoutManager = layoutManager

        for (item in imageList!!) {
            // body of loop
            println("*********************** Image List ************************* " + item)
        }
        imageAdapter = ImageAdapter(
            applicationContext,
            imageList,
            selectedImagesArrayList,
            mOptionsClickListener
        )
        binding.recyclerView.adapter = imageAdapter
        Handler().postDelayed({
            selectImage(pos!!)
            imageList[pos!!].setSelected(true)
        }, 1500)


        imageAdapter!!.setOnItemClickListener(object : ImageAdapter.StatusOnItemClickListener {
            override fun onItemClick(position: Int, v: View?) {
                Log.e(TAG, "onItemClick: " + "called")
                picRL.visibility = View.GONE
                imgSwitchCameraIV.visibility = View.GONE
                startBtnRL.visibility = View.GONE
                try {
                    selectImage(position)
                } catch (ed: IndexOutOfBoundsException) {
                    ed.printStackTrace()
                }
            }
        })
    }

    private fun setViewVisibility(id: Int, visibility: Int) {
        val view = findViewById<View>(id)
        if (view != null) {
            view.visibility = visibility
        }
    }

    private fun setupSurfaceHolder() {
        setViewVisibility(R.id.startBtnRL, View.VISIBLE)
        setViewVisibility(R.id.surfaceView, View.VISIBLE)
        captureImage()
    }

    fun captureImage() {
        if (camera != null) {
            val parameters: Camera.Parameters = camera!!.getParameters()
            val focusModes: List<String> = parameters.getSupportedFocusModes()
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)
            } else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
            }
            camera!!.setParameters(parameters)
            camera!!.setDisplayOrientation(90)

            camera!!.takePicture(null, null, this@AddStoryActivity)
        }
    }

    private fun startCamera(cameraid: Int) {
        try {
            camera = Camera.open(cameraid)
            camera!!.setDisplayOrientation(90)
            camera!!.setPreviewDisplay(surfaceHolder)
            camera!!.startPreview()

        } catch (e: IOException) {
            camera!!.release()
            camera = null
            e.printStackTrace()
        }
    }

    fun resetCamera() {
        if (surfaceHolder!!.surface == null) {
            // Return if preview surface does not exist
            return
        }
        if (camera != null) {
            // Stop if preview surface is already running.
            camera!!.stopPreview()
            try {
                camera!!.setDisplayOrientation(90)
                // Set preview display
                camera!!.setPreviewDisplay(surfaceHolder)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // Start the camera preview...
            camera!!.startPreview()
        }
    }

    private fun releaseCamera() {
        if (camera != null) {
            camera!!.stopPreview()
            camera!!.release()
            camera = null
        }
    }

    var onPictureTakenByteArry: ByteArray? = null
    var filePath: String? = ""

    override fun onPictureTaken(bytes: ByteArray, camera: Camera?) {
        timeInMillisec = "8000"
        binding.surfaceView.visibility = View.GONE
        binding.imgFilterIV.visibility = View.VISIBLE
        binding.recyclerView!!.visibility = View.GONE
        startBtnRL.visibility = View.GONE
        addImgRL.visibility = View.GONE
        imgSwitchCameraIV.visibility = View.GONE
        imgCapturedIV!!.visibility = View.VISIBLE
        picRL!!.visibility = View.VISIBLE
        addStoryLL!!.visibility = View.VISIBLE

        bitmapCameraa = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        if (isFrontCamOpen) {
            val matrix = Matrix()
            matrix.setRotate(
                90f,
                bitmapCameraa!!.width.toFloat() / 2,
                bitmapCameraa!!.height.toFloat() / 2
            )
            val scaledBitmap = Bitmap.createScaledBitmap(
                bitmapCameraa!!,
                (bitmapCameraa!!.width.toFloat() / 2).roundToInt(),
                (bitmapCameraa!!.height.toFloat() / 2).roundToInt(), true
            )

            bitmapCamera = Bitmap.createBitmap(
                scaledBitmap,
                0,
                0,
                scaledBitmap.width,
                scaledBitmap.height,
                matrix,
                true
            )

        } else {
            val matrix = Matrix()
            matrix.setRotate(
                -90f,
                bitmapCameraa!!.width.toFloat() / 2,
                bitmapCameraa!!.height.toFloat() / 2
            )
            matrix.postScale(-1.0f, 1.0f)
            val scaledBitmap = Bitmap.createScaledBitmap(
                bitmapCameraa!!,
                (bitmapCameraa!!.width.toFloat() / 2).roundToInt(),
                (bitmapCameraa!!.height.toFloat() / 2).roundToInt(), true
            )

            bitmapCamera = Bitmap.createBitmap(
                scaledBitmap,
                0,
                0,
                scaledBitmap.width,
                scaledBitmap.height,
                matrix,
                true
            )
        }
        // Create a file with the byte array
        val file = File.createTempFile("image", ".jpg")
        val fileOutputStream = FileOutputStream(file)
        // Write the bitmap to the file
        bitmapCamera!!.compress(Bitmap.CompressFormat.JPEG, 60, fileOutputStream)
        fileOutputStream.close()

// Get the path of the file
        filePath = file.absolutePath
        // selectedImagesAsFile.add( getImageUriFromBitmap(this,bitmap))
        imgCapturedIV!!.setImageBitmap(bitmapCamera)
        // save images to gallery
        saveToGallery(activity, bitmapCamera!!, "CloutLyfe")

    }

    var mLastClickkTab1: Long = 0

    fun saveToGallery(context: Context, bitmap: Bitmap, albumName: String) {
        val filename = "${System.currentTimeMillis()}.png"
        val write: (OutputStream) -> Boolean = {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                put(
                    MediaStore.MediaColumns.RELATIVE_PATH,
                    "${Environment.DIRECTORY_DCIM}/$albumName"
                )
            }

            context.contentResolver.let {
                it.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)?.let { uri ->
                    it.openOutputStream(uri)?.let(write)
                }
            }
        } else {
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                    .toString() + File.separator + albumName
            val file = File(imagesDir)
            if (!file.exists()) {
                file.mkdir()
            }
            val image = File(imagesDir, filename)
            write(FileOutputStream(image))
        }
    }

    private fun executeMulitpleStatusImage() {
        showStroryProgressDialog(activity)
        val photoList = ArrayList<MultipartBody.Part>()
        var videoMultipartBody: MultipartBody.Part? = null
        var mMultipartBody: MultipartBody.Part? = null
        if (videoThumbnail_bitmap != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(videoThumbnail_bitmap!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            videoMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "video_thumbnail",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }
        if (bitmapCamera != null) {
            val requestFile1: RequestBody? =
                convertBitmapToByteArrayUncompressed(bitmapCamera!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "status_images[]",
                    getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
            if (mMultipartBody != null) {
                photoList.add(mMultipartBody)
            }
        }

        if (!onPicTakenimageUri.isNullOrEmpty()) {
            val uri: Uri = onPicTakenimageUri!!.toUri()
            val iStream: InputStream = contentResolver.openInputStream(uri!!)!!
            val inputData: ByteArray = getBytes(iStream)!!
            val requestFile1: RequestBody =
                inputData.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "status_images[]",
                    getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }

            if (mMultipartBody != null) {
                photoList.add(mMultipartBody)
            }
        } else {
            if (VideoUrii.toString() != "null" && VideoUrii.toString() != "" && VideoUrii.toString() != null) {
                val uri: Uri = File(VideoUrii.toString()).toUri()
                val iStream: InputStream = contentResolver.openInputStream(uri!!)!!
                val inputData: ByteArray = getBytes(iStream)!!
                val requestFile1: RequestBody =
                    inputData.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                val multipartImg = requestFile1?.let {
                    MultipartBody.Part.createFormData(
                        "status_images[]",
                        getAlphaNumericString()!!.toString() + ".mp4",
                        it
                    )
                }
                photoList.add(multipartImg!!)
            } else {
                selectedImagesAsFile.indices.forEach {
//                val file2= getFilePath(selectedImagesAsFile[it])
                    val file2 = File(selectedImagesAsFile[it].path)
                    Log.e(TAG, "executeMulitpleStatusImage: " + selectedImagesAsFile[it].path)
                    if (selectedImagesAsFile[it].path!!.contains("mp4")) {
                        val surveyBody: RequestBody = RequestBody.create(
                            "video/*".toMediaTypeOrNull(),
                            convertFileToByteArray(file2)!!
                        )
                        val multipartImg =
                            MultipartBody.Part.createFormData(
                                "status_images[]",
                                file2.path,
                                surveyBody
                            )
                        photoList.add(multipartImg)
                    } else {
                        val surveyBody: RequestBody = RequestBody.create(
                            "image/*".toMediaTypeOrNull(),
                            convertFileToByteArray(file2)!!
                        )
                        val multipartImg =
                            MultipartBody.Part.createFormData(
                                "status_images[]",
                                file2.path,
                                surveyBody
                            )
                        photoList.add(multipartImg)
                    }
                }
            }
        }
        val user_id: RequestBody =
            AppPrefrences().readString(activity, Constants.ID, null)!!
                .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val duration: RequestBody =
            timeInMillisec!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val headers: MutableMap<String, String> = HashMap()
        headers["token"] = getAuthToken()
        val call = RetrofitInstance.appApi.addMultipleStatusImage(
            headers,
            photoList,
            user_id,
            duration, videoMultipartBody
        )
        call.enqueue(object : Callback<StatusModel> {
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                val mMultipleStatusImageModel = response.body()
                if (mMultipleStatusImageModel?.status == 1) {
                    is_click = false
                    dismissStroryProgressDialog()
                    showToast(activity, mMultipleStatusImageModel.message)
                    onBackPressed()
                } else if (mMultipleStatusImageModel?.status == 0) {
                    dismissStroryProgressDialog()
                    showAlertDialog(activity, mMultipleStatusImageModel.message)
                } else if (mMultipleStatusImageModel?.status == 3) {
                    dismissStroryProgressDialog()
                    showAlertDialog(activity, mMultipleStatusImageModel.message)
                } else {
                    dismissStroryProgressDialog()
                    showAlertDialog(activity, mMultipleStatusImageModel?.message)
                }
            }

            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissStroryProgressDialog()
            }
        })
    }

    /* code to convert image Uri to ByteArray */
    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray? {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 1024
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
//        startCamera()
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        resetCamera()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
//        releaseCamera()
    }

    override fun onDestroy() {
        if (camera != null) {
            camera!!.release()
        }
        VideoUrii = null
        binding.cameraView.facing = CameraKit.Constants.FACING_FRONT
        super.onDestroy()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onPause() {
        super.onPause()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }


    fun convertFileToByteArray(f: File): ByteArray? {
        val inputStream: InputStream = FileInputStream(f)
        var byteArray: ByteArray? = null
        try { /*from w  ww.  j  a  v a 2  s .c  om*/
            val b = ByteArray(1024)
            val os = ByteArrayOutputStream()
            var c: Int
            while (inputStream.read(b).also { c = it } != -1) {
                os.write(b, 0, c)
            }
            byteArray = os.toByteArray()

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return byteArray
    }

    var onPicTakenimageUri: String? = ""
    private var intentLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 777) {
                if (!selectedImagesArrayList.isNullOrEmpty()) {
                    selectedImagesArrayList.clear()
                    startttBtn.visibility = View.GONE
                    startBtnRL.visibility = View.GONE
                    addStoryLL!!.visibility = View.VISIBLE
                    imgSwitchCameraIV.visibility = View.GONE
                    imgCapturedIV!!.visibility = View.VISIBLE

                    val mIntentData: Intent? = result.data
                    Log.e(TAG, "launcher: " + result.data)
                    val poss = mIntentData?.getIntExtra("position", 0)
                    val updatedImageUri = mIntentData?.getStringExtra("updatedImageUri")!!
                    val args = mIntentData.getBundleExtra("bundle")
                    selectedImagesArrayList =
                        args!!.getSerializable("selectedImagesArrayList") as ArrayList<String>

                    imageUri = Uri.parse(updatedImageUri).toString()
//                    var image12=Uri.parse(updatedImageUri)
//                    Handler().postDelayed({
                    Glide.with(activity).load(imageUri).into(binding.imgCapturedIV)
//                    }, 100)

//                selectedImagesArrayList.add(pos,updatedImageUri)

                    val ImageModel = ImageModel()
                    ImageModel.setImage(updatedImageUri)
                    imageList.set(poss!!, ImageModel)
                    selectedImagesAsFile.set(poss, Uri.parse(updatedImageUri))
                    selectedImagesArrayList.set(poss, updatedImageUri)
                    Log.e(TAG, "counttt: " + count)
                    setImageList()
                } else {
                    bitmapCamera = null
                    val mIntentData: Intent? = result.data
                    val capturedImageUri = mIntentData?.getStringExtra("updatedImageUrii")!!
                    onPicTakenimageUri = Uri.parse(capturedImageUri).toString()
                    Glide.with(activity).load(onPicTakenimageUri).into(binding.imgCapturedIV)
                    timeInMillisec = "8000"
                    binding.surfaceView.visibility = View.GONE
                    binding.imgFilterIV.visibility = View.VISIBLE
                    binding.recyclerView!!.visibility = View.GONE
                    startBtnRL.visibility = View.GONE
                    addImgRL.visibility = View.GONE
                    imgSwitchCameraIV.visibility = View.GONE
                    imgCapturedIV!!.visibility = View.VISIBLE
                    picRL!!.visibility = View.VISIBLE
                    addStoryLL!!.visibility = View.VISIBLE
                    binding.imgFilterIV!!.visibility = View.GONE
                }
            }
        }

    override fun onBackPressed() {
        super.onBackPressed()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onStop() {
        super.onStop()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }



}
