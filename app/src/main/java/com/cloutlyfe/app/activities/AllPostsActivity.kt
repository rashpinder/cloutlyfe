package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.App
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.ReportAdapter
import com.cloutlyfe.app.adapters.ShareBottomSheetAdapter
import com.cloutlyfe.app.databinding.*
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.fragments.HomeFragment
import com.cloutlyfe.app.interfaces.*
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.ui.adapter.SavedPostsFeedAdapter
import com.cloutlyfe.app.ui.view.CenterLayoutManager
import com.cloutlyfe.app.ui.view.VideoViews
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import io.socket.client.Socket
import io.socket.emitter.Emitter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class AllPostsActivity : BaseActivity() {
    lateinit var binding: ActivityAllPostsBinding
//    private var adapter: SavedAdapter? = null

    val layoutManager = LinearLayoutManager(mActivity)
    val list: ArrayList<String> = ArrayList()
    val reportResonslist: ArrayList<DataX> = ArrayList()
    private var adapter: SavedPostsFeedAdapter? = null
    private var mReportAdapter: ReportAdapter? = null
    private var mAdapter: ShareBottomSheetAdapter? = null
    lateinit var homeViewModel: HomeViewModel
    lateinit var reportViewModel: ReportViewModel
    lateinit var likeUnlikeViewModel: LikeUnlikeViewModel
    lateinit var getReportReasonViewModel: GetReportReasonsViewModel
    lateinit var saveUnsaveViewModel: SaveUnsaveViewModel
    lateinit var mHomeDataModel: HomeModel
    var viewtype: String? = null
    var reasonId: String? = null
    var mPostId: String? = null
    var post_id: String? = null
    var title: String? = null
    var user_id: String? = null
    var mType: String? = null
    var mPos: Int? = 0
    var mPositionn: Int? = 0
    var vvInfoo: VideoViews? = null
    var imgVol: ImageView? = null
    var mOtheruserId: String? = null
//    var imageList: ArrayList<ProfilePostsData> = ArrayList()
//    var imageList: ArrayList<HomeData> = ArrayList()
    var imageHomeList: ArrayList<HomeData> = ArrayList()
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mPerPage: Int = 2000

    private var mSocket: Socket? = null
    var mRoomId = ""
    val userslist: ArrayList<RoomId> = ArrayList()

    companion object{
        var imageList: ArrayList<HomeData> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAllPostsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if(intent.getStringExtra("value").equals("Manage Post")){
            binding.header.txtHeadingTV.text=getString(R.string.manage_post)
        }
        else{
            binding.header.txtHeadingTV.text=getString(R.string.all_posts)
        }
        post_id = intent.getStringExtra("post_id")
        mType = intent.getStringExtra("type")
        user_id = intent.getStringExtra("user_id")
        init()
        setUpSocketData()


        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.allPostRV.addOnScrollListener(
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        //Dragging
                    } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    }
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    //here we find the current item number
                    val lManager = binding.allPostRV.layoutManager as LinearLayoutManager?
                    var firstElementPosition = lManager!!.findFirstCompletelyVisibleItemPosition()
                    Log.e(TAG, "onScrollStateChanged: " + firstElementPosition)
                    if (firstElementPosition == -1) {
                        firstElementPosition = 0
                    }
                    if (Constants.POSTS_SOUND) {
//                        showToast(mActivity,"true")
                        if (firstElementPosition >= 0) {
                            if(imageList.size>0){
                            imageList[firstElementPosition].vol = true
                            (binding.allPostRV.layoutManager?.findViewByPosition(
                                firstElementPosition
                            ))?.findViewById<VideoViews>(
                                R.id.vvInfo
                            )?.unmute()
                            (binding.allPostRV.layoutManager?.findViewByPosition(
                                firstElementPosition
                            ))?.findViewById<ImageView>(
                                R.id.imgVolumeIV
                            )?.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_on))
                        }}
                    } else {

                        if (firstElementPosition >= 0) {
                            if(imageList.size>0){
                            imageList[firstElementPosition].vol = false
                            (binding.allPostRV.layoutManager?.findViewByPosition(
                                firstElementPosition
                            ))?.findViewById<VideoViews>(
                                R.id.vvInfo
                            )?.mute()
                            (binding.allPostRV.layoutManager?.findViewByPosition(
                                firstElementPosition
                            ))?.findViewById<ImageView>(
                                R.id.imgVolumeIV
                            )?.setImageDrawable(resources.getDrawable(R.drawable.ic_volume_mute))
//                            showToast(mActivity,"false")
                        }}
                    }

                }
            })
    }



    private fun setUpSocketData() {
        val app: App = application as App
        mSocket = app.getSocket()
//        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
//        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
//        mSocket!!.on("newMessage", onNewMessage)
//        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()

    }

    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }



    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        homeViewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        getReportReasonViewModel = ViewModelProvider(this, factory).get(GetReportReasonsViewModel::class.java)
        reportViewModel = ViewModelProvider(this, factory).get(ReportViewModel::class.java)
        likeUnlikeViewModel = ViewModelProvider(this, factory).get(LikeUnlikeViewModel::class.java)
        saveUnsaveViewModel = ViewModelProvider(this, factory).get(SaveUnsaveViewModel::class.java)
    }


    override fun onResume() {
        super.onResume()
        if(intent.getStringExtra("value").equals("Manage Post")){
            executeAllPostsApi(post_id)
        }
        else{
            executeAllPostsApi(post_id)
        }

        Constants.POSTS_SOUND =true
        Constants.SOUND_VALUE ="all_post"

        if (Constants.LAST_POSITION != -1){
            if (binding!!.allPostRV.handingVideoHolder != null) binding!!.allPostRV.handingVideoHolder.playVideo()
            binding!!.allPostRV.adapter!!.notifyDataSetChanged()
            initView()
            Constants.LAST_POSITION=-1
        }
    }

    private fun executeAllPostsApi(post_id: String?) {
        if (isNetworkAvailable(mActivity))
            requestPostsData(post_id)
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_userid"] = user_id
        mMap["type"] = mType
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun requestPostsData(post_id: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getProfilePostsRequest(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                if (imageList != null) {
                    imageList.clear()
                }
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                if (mHomeDataModel.status == 1) {
                    dismissProgressDialog()
                    isLoading = !mHomeDataModel.lastPage.equals(true)
                    imageList.addAll(response.body()!!.data)

                    Log.e(TAG, "RequestHomeDataAPI: " + imageList)
                    initView()
                    if (imageList.size == 0) {
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                    } else {
                        binding.txtNoDataTV.visibility = View.GONE
                        for (i in 0..imageList.size - 1) {
                            if (imageList.get(i).post_id == post_id) {
                                binding.allPostRV.scrollToPosition(i)
//                                Collections.swap(imageList, 0, i)
                            }
                        }

                    }}
                else if (mHomeDataModel.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mHomeDataModel.message!!)
                } else {
                    dismissProgressDialog()
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.txtNoDataTV.text = response.body()!!.message
                }
            }
        })
    }


    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: java.util.ArrayList<String>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
        }
    }

//
//    private fun mLoadMoreParam(): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["user_id"] = getLoggedInUserID()
//        mMap["page_no"] = mPageNo.toString()
//        mMap["par_page"] = mPerPage.toString()
//        Log.e(TAG, "**PARAM**$mMap")
//        return mMap
//    }
//
//
//    private fun executeMoreSavedRequest() {
//        val mHeaders: MutableMap<String, String> = HashMap()
//        mHeaders["Token"] = getAuthToken()
//        binding.mProgressRL.visibility = View.VISIBLE
//        val call = RetrofitInstance.appApi.getProfilePostsRequest(mHeaders, mLoadMoreParam())
//        call.enqueue(object : Callback<HomeModel> {
//            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
//                dismissProgressDialog()
//                binding.mProgressRL.visibility = View.GONE
//            }
//
//            override fun onResponse(
//                call: Call<HomeModel>,
//                response: Response<HomeModel>
//            ) {
//                binding.mProgressRL.visibility = View.GONE
//                val mGetSavedModel = response.body()
//                if (mGetSavedModel!!.status == 1) {
//                    mGetSavedModel.data.let {
//                        imageList.addAll<HomeData>(
//                            it
//                        )
//                    }
//                    for (i in 0..imageList.size - 1) {
//                        if (imageList.get(i).post_id == post_id) {
//                            binding.allPostRV.smoothScrollToPosition(i)
////                                Collections.swap(imageList, 0, i)
//                        }
//
//                        adapter?.notifyDataSetChanged()
//                        isLoading = !mGetSavedModel.lastPage.equals(true)
//                    }}
//                else if (mGetSavedModel.status == 0) {
////                    showToast(mActivity, mGetSavedModel.message)
//                }
//            }
//        })
//    }



    private fun executeLikeUnlikeApi(
        mPosition: Int,
        post_id: String,
        likeIV: ImageView,
        likeTV: TextView
    ) {
        if (!mActivity.let { isNetworkAvailable(it) }) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestLikeUnlikeData(mPosition, post_id, likeIV, likeTV)
        }
    }

    private fun RequestLikeUnlikeData(
        mPosition: Int,
        post_id: String,
        likeIV: ImageView,
        likeTV: TextView
    ) {
        var user_id = getLoggedInUserID()
        var post_id = post_id
        val body = RequestBodies.LikeUnlikeBody(user_id, post_id)
        likeUnlikeViewModel.likeUnlikeRequest(body)
        likeUnlikeViewModel._likeUnlikeResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { likeUnlikeResponse ->
                            if (likeUnlikeResponse.status == 1 || likeUnlikeResponse.status == 200) {
                            } else {
                                //                                    dismissProgressDialog()
                            }
                        }
                    }

                    is Resource.Error -> {
                        //                            dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        //                            showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    // this is the scroll listener of recycler view which will tell the current item number


    // this is the scroll listener of recycler view which will tell the current item number


    private fun increaseLikeCount(
        position: Int,
        imageView: ImageView,
        post_id: String,
        likeTextView: TextView
    ) {
        if (imageList[position].isLike == 0) {
            imageView.setImageResource(R.drawable.ic_fav)
            imageList[position].isLike = 1
            val count = ((imageList[position].likeCount.toInt()) + 1).toString()
            imageList[position].likeCount = count
            likeTextView.text = imageList[position].likeCount
//Like Api Execute
            executeLikeUnlikeApi(position, post_id, imageView, likeTextView)
        } else {
            imageView.setImageResource(R.drawable.ic_un_fav)
            imageList[position].isLike = 0
            val count = ((imageList[position].likeCount.toInt()) - 1).toString()
            imageList[position].likeCount = count
            likeTextView.text = imageList[position].likeCount
////Like Api Execute
            executeLikeUnlikeApi(position, post_id, imageView, likeTextView)
        }
    }


    private fun executeSaveUnsaveApi() {
        if (!mActivity.let { isNetworkAvailable(it) }) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestSaveUnsaveData()
        }
    }

    private fun RequestSaveUnsaveData() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        val body = RequestBodies.SaveUnsaveBody(user_id, post_id!!)
        saveUnsaveViewModel.saveUnsaveRequest(body)
        saveUnsaveViewModel._save_unsaveResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { saveUnsaveResponse ->
                            if (saveUnsaveResponse.status == 1 || saveUnsaveResponse.status == 200) {

                            } else {
                                //                                    dismissProgressDialog()
                            }
                        }
                    }

                    is Resource.Error -> {
                        //                            dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        //                            showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    private fun showReportDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.report_dilaog, null)

        var dialogPrivacy: BottomSheetDialog? =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogPrivacy.setCancelable(true)
//cancel button click
        val reportLL: LinearLayout? = dialogPrivacy.findViewById(R.id.reportLL)

        reportLL?.setOnClickListener {
            if (reportResonslist != null) {
                reportResonslist.clear()
            }
            getReasons()

            dialogPrivacy.dismiss()
        }
        dialogPrivacy.show()
    }


    var mItemClickListener: ReportItemClickInterface = object : ReportItemClickInterface {
        override fun onItemClickListner(mId: String) {
            reasonId = mId
            performSubmitClick()
        }
    }

    var mLikeClickListener: LikeunlikeInterface = object : LikeunlikeInterface {
        override fun onItemClickListner(
            mPosition: Int,
            post_id: String,
            likeIV: ImageView,
            likeTV: TextView
        ) {
            mPostId = post_id
            mPos = mPosition
            var mimg: ImageView = likeIV
            var mlikeTV: TextView = likeTV
            increaseLikeCount(mPos!!, mimg, mPostId!!, mlikeTV)
        }
    }

    var mSaveClickListener: SaveUnsaveInterface = object : SaveUnsaveInterface {
        override fun onItemClickListner(mPosition: Int, post_id: String, saveIV: ImageView) {
            mPostId = post_id
            mPos = mPosition
            var msaveImg: ImageView = saveIV
            if (imageList[mPos!!].isSave.equals(0)) {
                imageList[mPos!!].isSave = 1
                msaveImg.setImageResource(R.drawable.ic_save)
                executeSaveUnsaveApi()

            } else {
                imageList[mPos!!].isSave = 0
                msaveImg.setImageResource(R.drawable.ic_unsave)
                executeSaveUnsaveApi()
            }

        }

    }
    private fun getReasons() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performResonsRequest()
        }
    }

    private fun performResonsRequest() {
        var user_id = getLoggedInUserID()
        val body = RequestBodies.ReportReasonsBody(
            user_id
        )
        getReportReasonViewModel.getReportResons(body)
        getReportReasonViewModel.reportReasonsResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { getReportResonse ->
                            if (getReportResonse.status == 1 || getReportResonse.status == 200) {
                                reportResonslist.addAll(getReportResonse.data)
                                showBottomReportDialog()
                            } else {
                                showAlertDialog(mActivity, getReportResonse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })

    }

    private fun performSubmitClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            performSubmit()
        }
    }

    private fun performSubmit() {
        var user_id = getLoggedInUserID()
        var post_id = mPostId
        var reasonId = reasonId
        var reportedId = mOtheruserId
        var reportType = "1"
        var reportedBy = getLoggedInUserID()
        val body = RequestBodies.ReportBody(
            user_id, post_id.toString(), reasonId!!, reportedId!!, reportType, reportedBy!!
        )
        reportViewModel.addReportRequest(body)
        reportViewModel._reportResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { forgotResponse ->
                            if (forgotResponse.status == 1 || forgotResponse.status == 200) {
                             imageList[mDelPos!!].is_report="1"
//                    setTopSwipeRefresh()
//                    executeHomeApi()
                                adapter!!.notifyItemChanged(mDelPos!!)
                                showAlertDialog(mActivity, forgotResponse.message)
                            } else {
                                showAlertDialog(mActivity, forgotResponse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })

    }

    private fun initView() {
        binding.allPostRV.layoutManager = CenterLayoutManager(mActivity)
        binding.allPostRV.smoothScrollBy(0, 1)
        binding.allPostRV.smoothScrollBy(0, -1)
        adapter =
            mActivity.let {
                SavedPostsFeedAdapter(
                    it,
                    mShareClickListner,
                    mLikeClickListener,
                    mSaveClickListener,
                    mOptionsClickListener,
                    mVolumeClickListener,
                    imageList,mLoadMoreScrollListner
                )
            }
        binding.allPostRV.adapter = adapter

        if (Constants.LAST_POSITION != -1) {
            binding!!.allPostRV.scrollToPosition(Constants.LAST_POSITION)
        }
    }




//    var mShareClickListner: ShareInterface = object : ShareInterface {
//        override fun onItemClickListner(mPosition: Int, postId: String, userId: String) {
//            showShareDialog()
//        }
//    }


    var mVolumeClickListener: VolumeInterface =
        object : VolumeInterface {
            override fun onItemClickListner(
                mPosition: Int,
                imgVolumeIV: ImageView,
                vvInfo: VideoViews,
                sound: Boolean
            ) {
                imgVol = imgVolumeIV
                vvInfoo = vvInfo
                mPositionn = mPosition
                if (Constants.POSTS_SOUND.equals(true)) {
//                for (i in 0..imageList.size-1){
                    imgVolumeIV.setImageResource(R.drawable.ic_volume_mute)
                    vvInfo.mute()
                    imageList[mPositionn!!].vol = false
                    Constants.POSTS_SOUND = false
//            }
                } else {
//                for (i in 0..imageList.size-1){
                    vvInfo.unmute()
                    imgVolumeIV.setImageResource(R.drawable.ic_volume_on)
                    imageList[mPositionn!!].vol = false
                    Constants.POSTS_SOUND = true
//                }
                }
            }
        }

    var mDelPos: Int? = 0
    var mOptionsClickListener: PostsMenuClickListenerInterface =
        object : PostsMenuClickListenerInterface {
            override fun onItemClickListner(
                mPosition: Int,
                mModel: HomeData,
                post_id: String,
                otherUserId: String,
                loggedInUser: Boolean
            ) {
                mPostId = post_id
                mDelPos = mPosition
                mOtheruserId = otherUserId
                if(mOtheruserId== AppPrefrences().readString(mActivity, Constants.ID,null)){
                    showDeleteDialog(mModel)
                }
                else{
                    showReportDialog()
                }

            }
        }

    private fun showDeleteHomeDialog(mModel: HomeData) {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)

        if(intent.getStringExtra("value").equals("Manage Post")){
        editPostRL!!.visibility=View.VISIBLE
        }
            else{
            editPostRL!!.visibility=View.GONE
        }

        deleteRL?.setOnClickListener {
            executeDeleteApi()
            dialogDelete.dismiss()
        }

        editPostRL?.setOnClickListener {
            var i = Intent( mActivity, EditPostActivity::class.java)
//            val gson = Gson()
//            val mModell = gson.toJson(mModel)
            i.putExtra("post_id",mModel.post_id)
//            i.putExtra("data", mModell)
            startActivity(i)
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }

    private fun showDeleteDialog(mModel: HomeData) {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)

        if(intent.getStringExtra("value").equals("Manage Post")){
        editPostRL!!.visibility=View.VISIBLE
        }
            else{
            editPostRL!!.visibility=View.GONE
        }

        deleteRL?.setOnClickListener {
            executeDeleteApi()
            dialogDelete.dismiss()
        }

        editPostRL?.setOnClickListener {
            var i = Intent( mActivity, EditPostActivity::class.java)
            i.putExtra("post_id",mModel.post_id)
//            val gson = Gson()
//            val mModell = gson.toJson(mModel)
//            i.putExtra("data", mModell)
            startActivity(i)
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }


    private fun executeDeleteApi() {
        if (isNetworkAvailable(mActivity))
            RequestDeleteData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.deletePostRequest(mHeaders, mDeleteParam())
        call.enqueue(object : Callback<CodeStatusModel> {
            override fun onFailure(call: Call<CodeStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CodeStatusModel>,
                response: Response<CodeStatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                if (response.body()!!.status == 1) {
                    showToast(mActivity, response.body()!!.message)
//                    vvInfoo!!.release()
//                    if(vvInfoo?.mediaPlayer!=null){
                    vvInfoo?.mediaPlayer=null
                    vvInfoo?.mediaPlayer?.stop()
                    vvInfoo?.mediaPlayer?.release()
//                }
                    if (binding.allPostRV.handingVideoHolder != null)
                        binding.allPostRV.handingVideoHolder.stopVideo()
                    executeAllPostsApi(post_id)
//                    imageList.removeAt(mDelPos!!)
//                    adapter!!.notifyItemChanged(mDelPos!!)
//                    adapter!!.notifyDataSetChanged()
//                    adapter!!.notifyItemChanged(mHomeDelPos!!, imageList.size)
                    dismissProgressDialog()

                } else if (response.body()!!.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
                    dismissProgressDialog()
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        if (binding.allPostRV.handingVideoHolder != null) binding.allPostRV.handingVideoHolder
            .stopVideo()
    }

    override fun onStop() {
        super.onStop()
        if (binding.allPostRV.handingVideoHolder != null) binding.allPostRV.handingVideoHolder
            .stopVideo()

    }

    override fun onDestroy() {
        super.onDestroy()

    }
    private fun showBottomReportDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.report_bottomsheet, null)
        var dialogReport: BottomSheetDialog? =
            BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogReport!!.setContentView(view)
        dialogReport.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogReport.setCancelable(true)
//cancel button click
        val reportRV: RecyclerView? = dialogReport.findViewById(R.id.reportRV)

        reportRV!!.layoutManager = LinearLayoutManager(mActivity)
        mReportAdapter = ReportAdapter(
            mActivity,
            reportResonslist, mItemClickListener
        )
        reportRV.adapter = mReportAdapter

//        reportRV?.setOnClickListener {
//            dialogReport.dismiss()
//        }
        dialogReport.show()
    }


    var mSharePostId: String? = null
    var mShareOtheruserId: String? = null
    var mShareList: ArrayList<String> = ArrayList()
    var cloutsList: java.util.ArrayList<DataXXX> = java.util.ArrayList()
    lateinit var mCloutsDataModel: FollowingUsersModel


    var mShareClickListner: ShareInterface = object : ShareInterface {
        override fun onItemClickListner(mPosition: Int, post_id: String, userId: String) {
            mSharePostId= post_id
            mShareOtheruserId= userId
            cloutsList.clear()
            mShareList.clear()
            executeGetCloutersApi()
        }
    }


    private fun executeGetCloutersApi() {
        if (isNetworkAvailable(mActivity))
            RequestCloutersData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mFollowingParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["search"] = ""
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCloutersData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getFollowRequest(mHeaders, mFollowingParam())
        call.enqueue(object : Callback<FollowingUsersModel> {
            override fun onFailure(call: Call<FollowingUsersModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<FollowingUsersModel>,
                response: Response<FollowingUsersModel>
            ) {
                Log.e(TAG, response.body().toString())
                mCloutsDataModel = response.body()!!
                if (mCloutsDataModel.status == 1) {
//                    isLoading = !mCloutsDataModel.lastPage.equals(true)
//                    cloutsList.addAll(response.body()!!.data)
                    for (i in response.body()!!.data.indices){
                        if(response.body()!!.data[i].is_delete!=1){
                            cloutsList.add(response.body()!!.data[i])
                        }

                    }
                    Log.e(TAG, "RequestHomeDataAPI: " + cloutsList)
                    if (cloutsList.size == 0) {
                        dismissProgressDialog()
                        showToast(mActivity,"There are no users available to share post")

                    } else {
                        showShareDialog()
//                        binding!!.upArraowRL.visibility = View.VISIBLE
//                        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//                        binding!!.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCloutsDataModel.status == 3) {
                    dismissProgressDialog()
//                    showAccountDisableAlertDialog(mActivity, mCloutsDataModel.message!!)
                } else {
                    dismissProgressDialog()
//                    binding!!.upArraowRL.visibility = View.GONE
//                    binding!!.txtNoDataTV.visibility = View.VISIBLE
//                    binding!!.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    fun filter(text: String?) {
        val temp: MutableList<DataXXX> = ArrayList()
        for (d in cloutsList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.name.toLowerCase().contains(text.toString())) {
                temp.add(d)
            }
        }
        //update recyclerview
        mAdapter!!.updateList(temp)
    }

    var dialogsghare: BottomSheetDialog?=null
    private fun showShareDialog() {
        dismissProgressDialog()
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.share_bottomsheet, null)
        dialogsghare = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)

        dialogsghare!!.setContentView(view)
        dialogsghare!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogsghare!!.setCancelable(true)
//cancel button click
        val bottomsheetRV: RecyclerView? = dialogsghare!!.findViewById(R.id.bottomsheetRV)
        val txtSendTV: TextView? = dialogsghare!!.findViewById(R.id.txtSendTV)
        val editSearchET: EditText? = dialogsghare!!.findViewById(R.id.editSearchET)

        editSearchET!!.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                filter(s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())
            }
        })
//        setAdapter(activity!!, cloutsList, mLoadMoreScrollListner)
//        list.add("Grave Yard")
//        list.add("Danel Bros")
        mAdapter = ShareBottomSheetAdapter(mActivity, cloutsList,mOnItemCheckListener)

        bottomsheetRV?.layoutManager = LinearLayoutManager(mActivity)
        bottomsheetRV?.adapter = mAdapter

        txtSendTV?.setOnClickListener {
            performSendClick()
        }
        dialogsghare!!.show()
    }

    var mOnItemCheckListener: ShareBottomSheetAdapter.OnItemCheckListener = object :
        ShareBottomSheetAdapter.OnItemCheckListener {
        override fun onItemCheck(item: DataXXX?) {
            mShareList.add(item!!.user_id)

        }

        override fun onItemUncheck(item: DataXXX?) {
            mShareList.remove(item!!.user_id)

        }
    }

    private fun isValidate(): Boolean {
        var flag = true
        when {
            (mShareList.size <= 0) -> {
                showAlertDialog(mActivity, getString(R.string.share_list))
                flag = false
            }
        }
        return flag
    }

    private fun performSendClick() {
        preventMultipleClick()
        if (isNetworkAvailable(mActivity)) {
            if (isValidate()) {
                executeSendPostRequest()
            }
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }

    /*
    * Execute api param
    * */
    private fun mShareParam(): MutableMap<String?, String?> {
        val s = TextUtils.join(", ", mShareList)
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] =getLoggedInUserID()
        mMap["message"] =""
        mMap["room_id"] = ""
        mMap["other_id"] = s
        mMap["post_id"] = mSharePostId
        mMap["type"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendPostRequest() {
        val call = RetrofitInstance.appApi.sharePostRequest(mShareParam())
        call.enqueue(object : Callback<SendMessageModel> {
            override fun onFailure(call: Call<SendMessageModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<SendMessageModel>,
                response: Response<SendMessageModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel?.status == 1) {
                    dialogsghare!!.dismiss()
                    showAlertDialog(mActivity,mModel.message)
                    userslist.addAll(mModel.room_ids)
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    for (i in userslist.indices) {
                        mSocket!!.emit("newMessage", userslist[i].room_id, mOjectString)}

//                    var mIntent = Intent(activity, ChatActivity::class.java)
//                    mIntent.putExtra(Constants.USER_NAME,  mModel.data.username)
//                    mIntent.putExtra(Constants.ROOM_ID, mModel.data.room_id)
//                    startActivity(mIntent)
//                    mMsgArrayList!!.add(mModel.data)

                } else if (mModel?.status == 0) {
                    showToast(mActivity, mModel.message)
                }
            }
        })
    }
}
