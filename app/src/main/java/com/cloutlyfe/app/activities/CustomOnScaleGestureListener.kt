package com.cloutlyfe.app.activities

import android.view.GestureDetector
import android.view.ScaleGestureDetector


class CustomOnScaleGestureListener : GestureDetector.SimpleOnGestureListener() {
    private var scaleFactor = 1.0f
    private val maxScaleFactor = 2.0f
    private val minScaleFactor = 1.0f

    fun onScale(detector: ScaleGestureDetector): Boolean {
        scaleFactor *= detector.scaleFactor
        scaleFactor = Math.max(minScaleFactor, Math.min(scaleFactor, maxScaleFactor))

        // Implement zoom here
        return true
    }

    fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
        return true
    }

    fun onScaleEnd(detector: ScaleGestureDetector?) {}
}