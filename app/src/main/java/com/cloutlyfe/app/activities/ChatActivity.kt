package com.cloutlyfe.app.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.Window
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.*
import com.cloutlyfe.app.TrimVideoActivity.Companion.trimmedVideoUri
import com.cloutlyfe.app.adapters.ChatMessagesAdapter
import com.cloutlyfe.app.databinding.ActivityChatBinding
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants.Companion.ID
import com.cloutlyfe.app.utils.Constants.Companion.LAST_MSG
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_IMAGE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_SHARE_STORY_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_VIDEO_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.OTHERUSERID
import com.cloutlyfe.app.utils.Constants.Companion.POST_UNAVAILABLE_LEFT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.POST_UNAVAILABLE_RIGHT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_IMAGE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_SHARE_STORY_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_VIDEO_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.ROOM_ID
import com.cloutlyfe.app.utils.Constants.Companion.USER_NAME
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import com.khostul.app.utils.HandleAndroidPermissions
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*


class ChatActivity : BaseActivity() {
    lateinit var binding: ActivityChatBinding
    lateinit var mChatMessagesAdapter: ChatMessagesAdapter
    var mMsgArrayList: ArrayList<AllMessage> = ArrayList()
    var imagesList: ArrayList<Uri> = ArrayList()
    var thumbnailList: ArrayList<Uri> = ArrayList()
    var mPageNo: Int = 1
    var mLastId: Int = 0
    var mPerPage: Int = 10
    var mIsPagination = true
    var mRoomId = ""
    var documentType = "1"
    var redirect_value = ""
    var otherUserId = ""
    var mSocket: Socket? = null
    var mOtherBlocked:String=""
    var mIsBlocked:String=""
    var mProfileBitmap: Bitmap? = null
    private val CAPTURE_VIDEO = 2
    var videoThumbnail_bitmap: Bitmap? = null
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA

        )
    }


    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (ContextCompat.checkSelfPermission(
                this@ChatActivity,
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                this@ChatActivity,
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(this@ChatActivity, PERMISSIONS, PERMISSION_ALL)
        }
        binding.sendRL.setOnClickListener {
            if (isNetworkAvailable(mActivity)) {
                if (isValidate()) {
                    if (SystemClock.elapsedRealtime() - mLastClickkTab1 < 4000) {
                        return@setOnClickListener
                    }
                    mLastClickkTab1 = SystemClock.elapsedRealtime()
                        executeSendMsgRequest()
                }
            } else {
                showToast(mActivity, getString(R.string.no_internet_connection))
            }
//            performSendClick()
        }

        binding.closeImageIV.setOnClickListener {
            performCrossClick()
        }

        binding.imgCameraIV.setOnClickListener {
//            showToast(mActivity,"Coming soon....")
            performAddProfileImgClick()
//            performSendClick()
        }


        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        getIntentData()
        setEditFocus()
        setDataOnWidgets()
        setTopSwipeRefresh()

    }

    fun getImageUri(Context: Activity?, inImage: Bitmap): Uri {
        val tempFile = File.createTempFile("temprentpk", ".png")
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val bitmapData = bytes.toByteArray()

        val fileOutPut = FileOutputStream(tempFile)
        fileOutPut.write(bitmapData)
        fileOutPut.flush()
        fileOutPut.close()
        return Uri.fromFile(tempFile)
    }
    var thumnailUri:Uri?=null
    override fun onResume() {
        super.onResume()
        if(trimmedVideoUri.toString()!="null" && trimmedVideoUri.toString()!="" && trimmedVideoUri.toString()!=null){
        try {
            val retriever = MediaMetadataRetriever()
            retriever.setDataSource(TrimVideoActivity.trimmedVideoUri.toString())
            val time =
                retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            val VideoDuration = time!!.toInt()
            Log.e(TAG, "onActivityResult: " + VideoDuration)

//            videoFullPath = getRealPathFromURI(trimmedVideoUri)

            videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                TrimVideoActivity.trimmedVideoUri!!.toString(),
                MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
            )

      thumnailUri=getImageUri(mActivity, videoThumbnail_bitmap!!)

            imagesList.add(trimmedVideoUri!!)
            thumbnailList.add(thumnailUri!!)
            mProfileBitmap=videoThumbnail_bitmap

            if(mProfileBitmap!=null){
                binding.imageLL.visibility=View.VISIBLE
                binding.closeImageIV.visibility=View.VISIBLE}
            Glide.with(mActivity).load(mProfileBitmap)
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into(binding.imgSelectedImageIV)

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }}


 private fun performCrossClick(){
     binding.imageLL.visibility=View.GONE
     binding.closeImageIV.visibility=View.GONE
     mProfileBitmap=null
     imagesList.removeAt(0)
         thumbnailList.clear()
 }

    private fun performAddProfileImgClick() {
                Pick()
    }

    fun Pick() {
        val alertDialog = Dialog(mActivity)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.item_camera_gallery_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtCameraTV = alertDialog.findViewById<TextView>(R.id.txtCameraTV)
        val txtCaptureVideoTV = alertDialog.findViewById<TextView>(R.id.txtCaptureVideoTV)
        val txtGalleryTV = alertDialog.findViewById<TextView>(R.id.txtGalleryTV)
        val txtPickVideoTV = alertDialog.findViewById<TextView>(R.id.txtPickVideoTV)
        val btnCancelTV = alertDialog.findViewById<TextView>(R.id.btnCancelTV)
        val videoView = alertDialog.findViewById<View>(R.id.videoView)
        val pickvideoView = alertDialog.findViewById<View>(R.id.pickvideoView)
txtGalleryTV.setText("Capture or pick Image from gallery")
        txtCameraTV.visibility=View.GONE

        btnCancelTV.setOnClickListener {
            alertDialog.dismiss() }

        txtCaptureVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performCaptureVideoClick()
        }

        txtCameraTV.setOnClickListener {
            alertDialog.dismiss()
            onSelectImageClick()
        }
        txtGalleryTV.setOnClickListener {
            alertDialog.dismiss()
            onSelectImageClick()
        }
        txtPickVideoTV.setOnClickListener {
            alertDialog.dismiss()
            performPickVideoClick()
        }
        alertDialog.show()
    }


    private fun performPickVideoClick() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        intent.type = "video/*"
        startActivityForResult(
            Intent.createChooser(intent, "Select Video"),
            357
        )
    }
    //    private val mVideoFile: File? = null
//    private val mThumbnailFile: File? = null
    private fun performCaptureVideoClick() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,60)
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0) // Quality Low
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, CAPTURE_VIDEO)
        }
    }


    private fun performGalleryClick() {
        openGalleryForImage()
    }

    private fun openGalleryForImage() {
        val intent = Lassi(mActivity)
            .with(LassiOption.CAMERA_AND_GALLERY) // choose Option CAMERA or CAMERA_AND_GALLERY
            .setMaxCount(1)
            .setMediaType(MediaType.IMAGE)
            .build()
        receiveData.launch(intent)
    }

    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as java.util.ArrayList<MiMedia>
                if (!selectedMedia .isNullOrEmpty()) {
                    Log.e("imagesLoadSize", selectedMedia.size.toString())

                    for (i in 0..selectedMedia.size-1) {
                        val uri: Uri  = Uri.fromFile(File(selectedMedia.get(i).path))
                    }
                }
            }
        }



    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start(200)
    }

    var videoFullPath: String? = null
    var selectedMediaUri: Uri?=null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        imagesList.clear()
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            if (requestCode==200){
                val uri: Uri = data?.data!!
                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                mProfileBitmap = selectedImage
                if(mProfileBitmap!=null){
                binding.imageLL.visibility=View.VISIBLE
                binding.closeImageIV.visibility=View.VISIBLE}
                Glide.with(mActivity).load(mProfileBitmap)
                    .placeholder(R.drawable.ic_post_ph)
                    .error(R.drawable.ic_post_ph)
                    .into(binding.imgSelectedImageIV)
                imagesList.add(uri)
            }
            else if (requestCode === 357) {
                val selectedImageUri: Uri = data!!.data!!
                if (selectedImageUri != null) {
                    val intent = Intent(
                        mActivity,
                        TrimVideoActivity::class.java
                    )
                    intent.putExtra("EXTRA_VIDEO_PATH", selectedImageUri)
                    startActivity(intent)
                }
            }
            else if (resultCode == RESULT_OK && requestCode == CAPTURE_VIDEO) {
                try {
                    selectedMediaUri = data!!.getData()!!
                    videoFullPath = getRealPathFromURI(selectedMediaUri)
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(videoFullPath)
                    val time =
                        retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    val VideoDuration = time!!.toInt()
                    Log.e(TAG, "onActivityResult: "+VideoDuration)

                    if (VideoDuration>60090){
                        showToast(mActivity, "You can upload video upto 1 minute")
                    }
                    else{
                        imagesList.add(Uri.parse(videoFullPath))
                        videoThumbnail_bitmap = ThumbnailUtils.createVideoThumbnail(
                            videoFullPath!!,
                            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND
                        )


                        thumnailUri=getImageUri(mActivity, videoThumbnail_bitmap!!)

                        thumbnailList.add(thumnailUri!!)

                        mProfileBitmap=videoThumbnail_bitmap

                        if(mProfileBitmap!=null){
                            binding.imageLL.visibility=View.VISIBLE
                            binding.closeImageIV.visibility=View.VISIBLE}
                        Glide.with(mActivity).load(mProfileBitmap)
                            .placeholder(R.drawable.ic_post_ph)
                            .error(R.drawable.ic_post_ph)
                            .into(binding.imgSelectedImageIV)
                    }
//            manageVideo(path) //Do whatever you want with your video
                }
                catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }



    fun getRealPathFromURI(uri: Uri?): String? {
        var path = ""
        if (getContentResolver() != null)
        {
            val cursor: Cursor = getContentResolver().query(uri!!, null, null, null, null)!!
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }


    var roomId: String? = ""
    var msg_id: String? = ""
    var mPos: Int? = 0


    var mDeleteChatInterface: DeleteChatInterface = object : DeleteChatInterface {
        override fun onItemClickListner(mPosition: Int, id: String, room_id: String) {
            mPos = mPosition
            msg_id = id
            roomId = room_id
            executeDeleteApi()
        }
    }


    private fun executeDeleteApi() {
        if (isNetworkAvailable(mActivity))
            RequestDeleteData()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["id"] = msg_id
        mMap["room_id"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteData() {
//        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.deleteChatRequest(mDeleteParam())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
//                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                Log.e(TAG, response.body().toString())
//                dismissProgressDialog()
                if (response.body()!!.status == 1) {
                    var model=DeleteMsgDataClass(mMsgArrayList.get(mPos!!).id,mMsgArrayList.get(mPos!!).message,mMsgArrayList.get(mPos!!).room_id,mMsgArrayList.get(mPos!!).document_type,"1",mMsgArrayList.get(mPos!!).user_id,
                        mPos.toString()
                    )
                    // perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(model)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    mSocket!!.emit("newMessage", mRoomId, mOjectString)

//                    showToast(mActivity, response.body()!!.message)
                    mMsgArrayList.removeAt(mPos!!)
                    mChatMessagesAdapter.notifyDataSetChanged()
                   } else if (response.body()!!.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {

                }
            }
        })
    }

    private fun seenMsgApi() {
        if (isNetworkAvailable(mActivity))
            executeSeenMsgesRequest()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))

    }

    /*
       * Execute api param
       * */
    private fun mSeenMsgParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["room_id"] = mRoomId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSeenMsgesRequest() {
        val call = RetrofitInstance.appApi.seenMessageRequest(mSeenMsgParam())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, "*****Msg****" + "failure")
            }

            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel?.status == 1) {
                    Log.e(TAG, "*****Msg****" + "Msg Seen")
                } else if (mModel?.status == 0) {
                    showToast(mActivity, mModel.message)
                }
            }
        })
    }


    /*
    * Set Edit Message Focus
    * */
    private fun setEditFocus() {
        binding.editMsgET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {

                binding.chatDataRV.scrollToPosition(mMsgArrayList.size - 1)
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }


    private fun setTopSwipeRefresh() {
        mMsgArrayList.clear()
        binding.mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.black))
        binding.mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true) {
                Log.e(ContentValues.TAG, "setTopSwipeRefresh: "+mPageNo )
                ++mPageNo
                mPerPage=10+mPerPage
                getAllMsgesData()
            } else {
                binding.mSwipeRefreshSR.isRefreshing = false
            }

        }
    }

    private fun getIntentData() {
        if (intent != null) {
            mRoomId = intent.getStringExtra(ROOM_ID).toString()
            redirect_value = intent.getStringExtra("redirect_value").toString()
            if (intent.getStringExtra(OTHERUSERID) != null) {
                otherUserId = intent.getStringExtra(OTHERUSERID)!!
            }

            binding.header.txtHeadingTV.text = intent.getStringExtra(USER_NAME)
//            binding.txtOtherUsernameTV.setText(intent.getStringExtra(USER_NAME))
//            Log.e("TAG","un"+txtOtherUsernameTV)

        }

//        Handler(Looper.getMainLooper()).postDelayed({
            setUpSocketData()
//        }, 1000)

    }


    private fun setUpSocketData() {
        val app: App = application as App
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
//        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("leaveChat", onUserLeft)

        Handler(Looper.getMainLooper()).postDelayed({
            mSocket!!.connect()
        }, 1000)



        getAllMsgesData()

    }

    private fun getAllMsgesData() {
        if (isNetworkAvailable(mActivity))
            executeGetAllMegesRequest()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun performCameraClick() {

    }

    override fun onBackPressed() {
        seenMsgApi()
        if(redirect_value=="push"){
            var intentt = Intent(mActivity, HomeActivity::class.java)
            startActivity(intentt)
        }
        else{
            if (mMsgArrayList != null && mMsgArrayList.size > 0) {
                var mIntent: Intent = Intent()
                mIntent.putExtra(LAST_MSG, mMsgArrayList.get(mMsgArrayList.size - 1).message)
                setResult(808, mIntent)
                super.onBackPressed()
            } else {
                super.onBackPressed()
            }
        }

    }
    var mLastClickkTab1:Long=0
    var is_click:Boolean?=false

//    private fun performSendClick() {
//        preventMultipleClick()
//
//
//    }


    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding.editMsgET.text.toString().trim { it <= ' ' } == "" && mProfileBitmap==null) {
            showAlertDialog(mActivity, "Please enter message or select image/video to share.")
            flag = false
        }
//        if (binding.editMsgET.text.toString().trim { it <= ' ' } == "" ) {
//            flag = false
//        }
        return flag
    }

    val selectedImagesAsFile= java.util.ArrayList<Uri>()

    /*
    * Execute api param
    * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["message"] = binding.editMsgET.text.toString().trim()
        mMap["room_id"] = mRoomId
//        mMap["chat_images"] = mRoomId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendMsgRequest() {
        var profileMultipartBody: MultipartBody.Part? = null
        val headers: java.util.HashMap<String, String> = java.util.HashMap()
        headers["Token"] = getAuthToken()
        Log.e(TAG, "tokenn: " + getAuthToken())

        val user_id: RequestBody =
            getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val message: RequestBody =
            binding.editMsgET.text.toString().trim().toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val room_id: RequestBody =
            mRoomId.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val document_type: RequestBody =
            documentType.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        val multipleImages = arrayOfNulls<MultipartBody.Part>(imagesList.size)
        val multipleThumbnailImages = arrayOfNulls<MultipartBody.Part>(thumbnailList.size)
if(!imagesList.isNullOrEmpty()){
        for (i in imagesList.indices) {
            val file = File(imagesList[0].path)
            val surveyBody = RequestBody.create("image/*".toMediaTypeOrNull(), file)
            multipleImages[0] = MultipartBody.Part.createFormData(
                "chat_images[]",
                System.currentTimeMillis().toString()+file.name,
                surveyBody
            )
        }
    documentType="2"
}
        if(!thumbnailList.isNullOrEmpty()){
            documentType="3"
        for (i in thumbnailList.indices) {
            val file = File(thumbnailList[0].path)
            val surveyBody = RequestBody.create("image".toMediaTypeOrNull(), file)
            multipleThumbnailImages[0] = MultipartBody.Part.createFormData(
                "chat_images_thumb[]",
                System.currentTimeMillis().toString()+file.name,
                surveyBody
            )
        }}
        binding.mProgressPB.visibility = View.VISIBLE
        binding.imgSendIV.visibility = View.GONE
        binding.editMsgET.isEnabled = false

        val call =  RetrofitInstance.appApi.sendMessageRequest(headers,user_id,message,room_id,document_type,multipleImages,multipleThumbnailImages)
//        val call = RetrofitInstance.appApi.sendMessageRequest(mParam())
        call.enqueue(object : Callback<SendMessageNewModel> {
            override fun onFailure(call: Call<SendMessageNewModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                binding.mProgressPB.visibility = View.GONE
                binding.imgSendIV.visibility = View.VISIBLE
                binding.editMsgET.isEnabled = true
            }

            override fun onResponse(
                call: Call<SendMessageNewModel>,
                response: Response<SendMessageNewModel>
            ) {
                Log.e(TAG, response.body().toString())
                trimmedVideoUri=null
                binding.imageLL.visibility=View.GONE
                binding.mProgressPB.visibility=View.GONE
                binding.closeImageIV.visibility=View.GONE
                binding.imgSendIV.visibility = View.VISIBLE
                binding.editMsgET.isEnabled = true
                mProfileBitmap=null
                val mModel = response.body()
                if (mModel?.status == 1) {
                    if(mModel.data.chat_images=="" || mModel.data.chat_images.isNullOrEmpty()){
                    mModel.data.viewType = RIGHT_VIEW_HOLDER}
                    else if(mModel.data.chat_images.contains(".png")|| mModel.data.chat_images.contains(".jpeg")|| mModel.data.chat_images.contains(".jpg")|| mModel.data.chat_images.contains("image")|| mModel.data.chat_images!!.contains(".gif")||mModel.data.chat_images!!.contains(".JPEG") || mModel.data.chat_images!!.contains(".PNG") || mModel.data.chat_images!!.contains(".GIF")){
                        mModel.data.viewType = RIGHT_IMAGE_VIEW_HOLDER
                    }
                    else{
                        mModel.data.viewType = RIGHT_IMAGE_VIEW_HOLDER
                    }

                    // perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    mSocket!!.emit("newMessage", mRoomId, mOjectString)
                    mMsgArrayList.add(mModel.data)
                    mChatMessagesAdapter.notifyDataSetChanged()
                    scrollToBottom()
                    binding.editMsgET.setText("")
                } else if (mModel?.status == 0) {
                    showToast(mActivity, mModel.message)
                }
            }
        })
    }


    private fun mGetMsgesParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["authToken"] = getAuthToken()
        mMap["room_id"] = mRoomId
        mMap["per_page"] = "" + mPerPage
//        mMap["page_no"] = "" + mPageNo
        mMap["last_id"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMegesRequest() {
        binding.mSwipeRefreshSR.isRefreshing = true
        val call = RetrofitInstance.appApi.getOnetoOneChatRequest(mGetMsgesParam())
        call.enqueue(object : Callback<GetOneToOneChatNewModel> {
            override fun onFailure(call: Call<GetOneToOneChatNewModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                binding.mSwipeRefreshSR.isRefreshing = false
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetOneToOneChatNewModel>,
                response: Response<GetOneToOneChatNewModel>
            ) {
                Log.e(TAG, response.body().toString())
                binding.mSwipeRefreshSR.isRefreshing = false
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel?.status == 1) {
                    mMsgArrayList.clear()
                    if (mModel.all_messages.size < 10) {
                        mIsPagination = false
                    }
                    mOtherBlocked=mModel.receiver_detail.other_blocked
                    mIsBlocked=mModel.receiver_detail.is_blocked
                    if(mOtherBlocked=="1"){
                        binding.editMsgET.visibility=View.GONE
                        binding.imgCameraIV.visibility=View.GONE
                        binding.sendRL.visibility=View.GONE
                        binding.blockTV.visibility=View.VISIBLE
                        binding.blockTV.text=intent.getStringExtra(USER_NAME)+" "+"has blocked you"+"!"
                    }
                    else if(mIsBlocked=="1"){
                        binding.editMsgET.visibility=View.GONE
                        binding.imgCameraIV.visibility=View.GONE
                        binding.sendRL.visibility=View.GONE
                        binding.blockTV.visibility=View.VISIBLE
                        binding.blockTV.text="You blocked"+" "+intent.getStringExtra(USER_NAME)+"!"
                    }
                    else{
                        binding.editMsgET.visibility=View.VISIBLE
                        binding.imgCameraIV.visibility=View.VISIBLE
                        binding.sendRL.visibility=View.VISIBLE
                        binding.blockTV.visibility=View.GONE
                    }


                    for (i in 0..mModel.all_messages.size - 1) {
                        if (mModel.all_messages.get(i).user_id.equals(
                                AppPrefrences().readString(
                                    mActivity,
                                    ID,
                                    null
                                )
                            )
                        ) {
                            if(mModel.all_messages[i].status_details!=null){
                                if(mModel.all_messages[i].status_details?.status_image!=null && mModel.all_messages[i].status_details?.status_image!!.contains(".png")|| mModel.all_messages[i].status_details?.status_image!!.contains(".jpeg")|| mModel.all_messages[i].status_details!!.status_image.contains(".jpg")|| mModel.all_messages[i].status_details?.status_image!!.contains("image")|| mModel.all_messages[i].status_details?.status_image!!.contains(".gif")|| mModel.all_messages[i].status_details?.status_image!!.contains(".GIF")||mModel.all_messages[i].status_details?.status_image!!.contains(".JPEG")||mModel.all_messages[i].status_details?.status_image!!.contains(".PNG")){
                                    mModel.all_messages.get(i).viewType =
                                        RIGHT_SHARE_STORY_VIEW_HOLDER}
                                else if(mModel.all_messages[i].status_details?.status_image!=null && mModel.all_messages[i].status_details?.status_image!!.contains(".mp4") || mModel.all_messages[i].status_details?.status_image!!.contains("video") || mModel.all_messages[i].status_details?.status_image!!.contains(".3gp")|| mModel.all_messages[i].status_details?.status_image!!.contains(".MP4")){
                                    mModel.all_messages.get(i).viewType =
                                        RIGHT_SHARE_STORY_VIEW_HOLDER
                                }
                            }
                            else if(mModel.all_messages[i].is_delete_status=="1" && mModel.all_messages[i].status_id!="0" ){
                                mModel.all_messages.get(i).viewType =
                                    RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER
                            }

                            else if (mModel.all_messages[i].post_details!!.is_delete == "1" && mModel.all_messages[i].message == "" && mModel.all_messages[i].chat_images.isNullOrEmpty()) {
                                mModel.all_messages.get(i).viewType =
                                    POST_UNAVAILABLE_RIGHT_VIEW_HOLDER
                            }

                            else if (!mModel.all_messages[i].chat_images.isNullOrEmpty()){
                                    if(mModel.all_messages[i].chat_images!=null && mModel.all_messages[i].chat_images!!.contains(".png")|| mModel.all_messages[i].chat_images!!.contains(".jpeg")|| mModel.all_messages[i].chat_images!!.contains(".jpg")|| mModel.all_messages[i].chat_images!!.contains("image")|| mModel.all_messages[i].chat_images!!.contains(".gif")|| mModel.all_messages[i].chat_images!!.contains(".GIF")||mModel.all_messages[i].chat_images!!.contains(".JPEG")||mModel.all_messages[i].chat_images!!.contains(".PNG")){
                                mModel.all_messages.get(i).viewType = RIGHT_IMAGE_VIEW_HOLDER}
                                else if(mModel.all_messages[i].chat_images!=null && mModel.all_messages[i].chat_images!!.contains(".mp4") || mModel.all_messages[i].chat_images!!.contains("video") || mModel.all_messages[i].chat_images!!.contains(".3gp")|| mModel.all_messages[i].chat_images!!.contains(".MP4")){
                                    mModel.all_messages.get(i).viewType =
                                        RIGHT_IMAGE_VIEW_HOLDER
                                }
                            }
                            else {
                                if (mModel.all_messages[i].post_details!!.thumbnail_image.size > 0) {
                                    mModel.all_messages.get(i).viewType =
                                        RIGHT_VIDEO_POST_VIEW_HOLDER
                                } else if (mModel.all_messages[i].post_details!!.image.size == 0 && mModel.all_messages[i].post_details!!.thumbnail_image.size == 0) {
                                    mModel.all_messages.get(i).viewType = RIGHT_VIEW_HOLDER
                                } else if (mModel.all_messages[i].post_details!!.thumbnail_image.size == 0) {
                                    mModel.all_messages.get(i).viewType = RIGHT_POST_VIEW_HOLDER
                                } else {
                                    Log.e(TAG, "value: " + "right")
                                    mModel.all_messages.get(i).viewType = RIGHT_VIEW_HOLDER
                                }
                            }
                        } else {
                            if(mModel.all_messages[i].status_details!=null){
                                if(mModel.all_messages[i].status_details?.status_image!=null && mModel.all_messages[i].status_details?.status_image!!.contains(".png")|| mModel.all_messages[i].status_details?.status_image!!.contains(".jpeg")|| mModel.all_messages[i].status_details!!.status_image.contains(".jpg")|| mModel.all_messages[i].status_details?.status_image!!.contains("image")|| mModel.all_messages[i].status_details?.status_image!!.contains(".gif")|| mModel.all_messages[i].status_details?.status_image!!.contains(".GIF")||mModel.all_messages[i].status_details?.status_image!!.contains(".JPEG")||mModel.all_messages[i].status_details?.status_image!!.contains(".PNG")){
                                    mModel.all_messages.get(i).viewType =
                                        LEFT_SHARE_STORY_VIEW_HOLDER}
                                else if(mModel.all_messages[i].status_details?.status_image!=null && mModel.all_messages[i].status_details?.status_image!!.contains(".mp4") || mModel.all_messages[i].status_details?.status_image!!.contains("video") || mModel.all_messages[i].status_details?.status_image!!.contains(".3gp")|| mModel.all_messages[i].status_details?.status_image!!.contains(".MP4")){
                                    mModel.all_messages.get(i).viewType =
                                        LEFT_SHARE_STORY_VIEW_HOLDER
                                }
                            }
                            else if(mModel.all_messages[i].is_delete_status=="1" && mModel.all_messages[i].status_id!="0") {
                                mModel.all_messages.get(i).viewType =
                                    LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER
                            }

                            else if (mModel.all_messages[i].post_details!!.is_delete == "1" && mModel.all_messages[i].message == "" && mModel.all_messages[i].chat_images.isNullOrEmpty()) {
                                mModel.all_messages.get(i).viewType =
                                    POST_UNAVAILABLE_LEFT_VIEW_HOLDER
                            }
                            else if(mModel.all_messages[i].chat_images!=null){
                                if (mModel.all_messages[i].chat_images!!.contains(".png")|| mModel.all_messages[i].chat_images!!.contains(".jpeg")|| mModel.all_messages[i].chat_images!!.contains(".jpg")|| mModel.all_messages[i].chat_images!!.contains("image") || mModel.all_messages[i].chat_images!!.contains(".gif") || mModel.all_messages[i].chat_images!!.contains(".GIF")||mModel.all_messages[i].chat_images!!.contains(".JPEG")||mModel.all_messages[i].chat_images!!.contains(".PNG")){
                                mModel.all_messages.get(i).viewType = LEFT_IMAGE_VIEW_HOLDER
                            }
                               else if(mModel.all_messages[i].chat_images!!.contains(".mp4") || mModel.all_messages[i].chat_images!!.contains("video") || mModel.all_messages[i].chat_images!!.contains(".3gp")||mModel.all_messages[i].chat_images!!.contains(".MP4")){
                                    mModel.all_messages.get(i).viewType =
                                        LEFT_IMAGE_VIEW_HOLDER
                                }
                            }
                            else {
                                if (mModel.all_messages[i].post_details!!.thumbnail_image.size > 0) {
                                    mModel.all_messages.get(i).viewType =
                                        LEFT_VIDEO_POST_VIEW_HOLDER
                                } else if (mModel.all_messages[i].post_details!!.image.size == 0 && mModel.all_messages[i].post_details!!.thumbnail_image.size == 0) {
                                    mModel.all_messages.get(i).viewType = LEFT_VIEW_HOLDER
                                } else if (mModel.all_messages[i].post_details!!.thumbnail_image.size == 0) {
                                    mModel.all_messages.get(i).viewType = LEFT_POST_VIEW_HOLDER
                                } else {
                                    Log.e(TAG, "value: " + "left")
                                    mModel.all_messages.get(i).viewType = LEFT_VIEW_HOLDER
                                }
                            }
                        }
                    }

                    mMsgArrayList.addAll(0, mModel.all_messages)
//                        mMsgArrayList!!.sortedWith(compareBy({ it!!.created }))


                    //     mMsgArrayList!!.reverse()
                    for (i in 0..mMsgArrayList.size - 1) {
                        mMsgArrayList.sortBy { it.creation_date }
                        println(
                            " ************ msa array list ************* : " + mMsgArrayList.get(
                                i
                            ).creation_date
                        )
                    }
//                        Collections.reverse(mMsgArrayList)
                    if (mModel.receiver_detail.user_id != null) {
                        otherUserId = mModel.receiver_detail.user_id
                    }
                    mChatMessagesAdapter.notifyDataSetChanged()

                    if (mPageNo == 1) {
                        binding.chatDataRV.scrollToPosition(mMsgArrayList.size - 1)
                    }
                } else if (mModel!!.status == 0) {
                    mChatMessagesAdapter.notifyDataSetChanged()
                } else {
                    println("data :  " + mModel.message)
                }
            }
        }
        )
    }


    private fun setDataOnWidgets() {
        mChatMessagesAdapter = ChatMessagesAdapter(mActivity, mMsgArrayList, mDeleteChatInterface)
        binding.chatDataRV.isNestedScrollingEnabled = false
        val mLayoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        binding.chatDataRV.layoutManager = mLayoutManager
        binding.chatDataRV.itemAnimator = DefaultItemAnimator()
        binding.chatDataRV.adapter = mChatMessagesAdapter

        if (binding.editMsgET.hasFocus()) {
            binding.chatDataRV.scrollToPosition(mMsgArrayList.size - 1)
        }

        binding.chatDataRV.scrollToPosition(mMsgArrayList.size - 1)
    }


    private fun scrollToBottom() {
        binding.chatDataRV.scrollToPosition(mChatMessagesAdapter.itemCount - 1)
    }


    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable {
//                showToast(this@ChatActivity, "connect")
                Log.e(TAG, "connecting")
//                mSocket!!.connect()
            })
    }

    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable {
//                mSocket?.connect()
                Log.e(TAG, "Error disconnect")
                Log.e(TAG, "Error disconnect: ${it.get(0).toString()}" )
            })
    }

    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting")
        mSocket?.connect()
            Log.e(TAG, "Error connecting: ${it.get(0).toString()}" )
//            showToast(this@ChatActivity, "onConnectError")
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        trimmedVideoUri=null
        mSocket?.off()
        mSocket?.disconnect()
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
//           showToast(this@ChatActivity, "newMEssageCalled")
            seenMsgApi()
            try {
                Log.e(TAG, "****MessageObject****" + args.get(1).toString())
                val mGson = Gson()
                val mChatMessageData: AllMessage =
                    mGson.fromJson(args.get(1).toString(), AllMessage::class.java)

                if (mChatMessageData.user_id.equals(
                        AppPrefrences().readString(
                            mActivity,
                            ID,
                            null
                        )
                    )
                ) {
                    if(mChatMessageData.status_details!=null){
                        if(mChatMessageData.status_details?.status_image!=null && mChatMessageData.status_details?.status_image!!.contains(".png")|| mChatMessageData.status_details?.status_image!!.contains(".jpeg")|| mChatMessageData.status_details?.status_image!!.contains(".jpg")|| mChatMessageData.status_details?.status_image!!.contains("image")|| mChatMessageData.status_details?.status_image!!.contains(".gif")|| mChatMessageData.status_details?.status_image!!.contains(".GIF")||mChatMessageData.status_details?.status_image!!.contains(".JPEG")||mChatMessageData.status_details?.status_image!!.contains(".PNG")){
                            mChatMessageData.viewType =
                                RIGHT_SHARE_STORY_VIEW_HOLDER}
                        else if(mChatMessageData.status_details?.status_image!=null && mChatMessageData.status_details?.status_image!!.contains(".mp4") || mChatMessageData.status_details?.status_image!!.contains("video") || mChatMessageData.status_details?.status_image!!.contains(".3gp")|| mChatMessageData.status_details?.status_image!!.contains(".MP4")){
                            mChatMessageData.viewType =
                                RIGHT_SHARE_STORY_VIEW_HOLDER
                        }
                        mMsgArrayList.add(mChatMessageData)
                    }

                    else if(mChatMessageData.post_details!=null){
                    if (mChatMessageData.post_details.is_delete == "1" && mChatMessageData.message == "" && mChatMessageData.chat_images.isNullOrEmpty()) {
                        mChatMessageData.viewType =
                            POST_UNAVAILABLE_RIGHT_VIEW_HOLDER
                    }
                    else if (!mChatMessageData.chat_images.isNullOrEmpty()){
                        if(!mChatMessageData.chat_images.isNullOrEmpty() && mChatMessageData.chat_images.contains(".png")|| mChatMessageData.chat_images.contains(".jpeg")|| mChatMessageData.chat_images.contains(".jpg")|| mChatMessageData.chat_images!!.contains("image")|| mChatMessageData.chat_images!!.contains(".gif")|| mChatMessageData.chat_images!!.contains(".GIF")|| mChatMessageData.chat_images!!.contains(".JPEG")|| mChatMessageData.chat_images!!.contains(".PNG")){
                            mChatMessageData.viewType = RIGHT_IMAGE_VIEW_HOLDER}
                        else if(!mChatMessageData.chat_images.isNullOrEmpty() && mChatMessageData.chat_images.contains(".mp4") || mChatMessageData.chat_images.contains("video")|| mChatMessageData.chat_images.contains(".3gp")||mChatMessageData.chat_images.contains(".MP4")){
                            mChatMessageData.viewType =
                                RIGHT_IMAGE_VIEW_HOLDER
                        }
                    }
                    else {
                        if (mChatMessageData.post_details.thumbnail_image.size > 0) {
                            mChatMessageData.viewType =
                                RIGHT_VIDEO_POST_VIEW_HOLDER
                        } else if (mChatMessageData.post_details.image.size == 0 && mChatMessageData.post_details.thumbnail_image.size == 0) {
                            mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                        } else if (mChatMessageData.post_details.thumbnail_image.size == 0) {
                            mChatMessageData.viewType = RIGHT_POST_VIEW_HOLDER
                        } else {
                            Log.e(TAG, "value: " + "right")
                            mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                        }
                    }
                }
                    else{
                         if (!mChatMessageData.chat_images.isNullOrEmpty()){
                            if(!mChatMessageData.chat_images.isNullOrEmpty() && mChatMessageData.chat_images.contains(".png")|| mChatMessageData.chat_images.contains(".jpeg")|| mChatMessageData.chat_images.contains(".jpg")|| mChatMessageData.chat_images!!.contains("image")|| mChatMessageData.chat_images!!.contains(".gif")||mChatMessageData.chat_images!!.contains(".GIF")||mChatMessageData.chat_images!!.contains(".JPEG")||mChatMessageData.chat_images!!.contains(".PNG")){
                                mChatMessageData.viewType = RIGHT_IMAGE_VIEW_HOLDER}
                            else if(!mChatMessageData.chat_images.isNullOrEmpty() && mChatMessageData.chat_images.contains(".mp4") || mChatMessageData.chat_images.contains("video")|| mChatMessageData.chat_images!!.contains(".3gp")||mChatMessageData.chat_images.contains(".MP4")){
                                mChatMessageData.viewType =
                                    RIGHT_IMAGE_VIEW_HOLDER
                            }}
                             else{
                                mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                            }
                    }
                    mMsgArrayList.add(mChatMessageData)
                }

                else {
                    if(mChatMessageData.status_details!=null){
                        if(mChatMessageData.status_details?.status_image!=null && mChatMessageData.status_details?.status_image!!.contains(".png")|| mChatMessageData.status_details?.status_image!!.contains(".jpeg")|| mChatMessageData.status_details?.status_image!!.contains(".jpg")|| mChatMessageData.status_details?.status_image!!.contains("image")|| mChatMessageData.status_details?.status_image!!.contains(".gif")|| mChatMessageData.status_details?.status_image!!.contains(".GIF")||mChatMessageData.status_details?.status_image!!.contains(".JPEG")||mChatMessageData.status_details?.status_image!!.contains(".PNG")){
                            mChatMessageData.viewType =
                                LEFT_SHARE_STORY_VIEW_HOLDER}
                        else if(mChatMessageData.status_details?.status_image!=null && mChatMessageData.status_details?.status_image!!.contains(".mp4") || mChatMessageData.status_details?.status_image!!.contains("video") || mChatMessageData.status_details?.status_image!!.contains(".3gp")|| mChatMessageData.status_details?.status_image!!.contains(".MP4")){
                            mChatMessageData.viewType =
                                LEFT_SHARE_STORY_VIEW_HOLDER
                        }
                        mMsgArrayList.add(mChatMessageData)
                    }
                    else if(mChatMessageData.is_delete=="1"){
                        mMsgArrayList.removeAt(mChatMessageData.mPos.toInt())
                    }
                     else if(mChatMessageData.post_details!=null){
                    if (mChatMessageData.post_details.is_delete == "1" && mChatMessageData.message == "" && mChatMessageData.chat_images.isNullOrEmpty()) {
                        mChatMessageData.viewType =
                            POST_UNAVAILABLE_LEFT_VIEW_HOLDER
                    }
                    else if(!mChatMessageData.chat_images.isNullOrEmpty()){
                        if (mChatMessageData.chat_images.contains(".png")|| mChatMessageData.chat_images.contains(".jpeg")|| mChatMessageData.chat_images.contains(".jpg")|| mChatMessageData.chat_images!!.contains("image") || mChatMessageData.chat_images!!.contains(".gif")|| mChatMessageData.chat_images!!.contains(".GIF")|| mChatMessageData.chat_images!!.contains(".JPEG")|| mChatMessageData.chat_images!!.contains(".PNG")){
                            mChatMessageData.viewType = LEFT_IMAGE_VIEW_HOLDER
                        }
                        else if(mChatMessageData.chat_images.contains(".mp4") || mChatMessageData.chat_images.contains("video") || mChatMessageData.chat_images!!.contains(".3gp")||mChatMessageData.chat_images.contains(".MP4")){
                            mChatMessageData.viewType =
                                LEFT_IMAGE_VIEW_HOLDER
                        }
                        mMsgArrayList.add(mChatMessageData)
                    }
                    else {
                        if (mChatMessageData.post_details.thumbnail_image.size > 0) {
                            mChatMessageData.viewType =
                                LEFT_VIDEO_POST_VIEW_HOLDER
                        } else if (mChatMessageData.post_details.image.size == 0 && mChatMessageData.post_details.thumbnail_image.size == 0) {
                            mChatMessageData.viewType = LEFT_VIEW_HOLDER
                        } else if (mChatMessageData.post_details.thumbnail_image.size == 0) {
                            mChatMessageData.viewType = LEFT_POST_VIEW_HOLDER
                        } else {
                            Log.e(TAG, "value: " + "left")
                            mChatMessageData.viewType = LEFT_VIEW_HOLDER
                        }
                    }
                        mMsgArrayList.add(mChatMessageData)

                     }
                    else{
                        if(!mChatMessageData.chat_images.isNullOrEmpty()){
                            if (mChatMessageData.chat_images.contains(".png")|| mChatMessageData.chat_images.contains(".jpeg")|| mChatMessageData.chat_images.contains(".jpg")|| mChatMessageData.chat_images!!.contains("image") || mChatMessageData.chat_images!!.contains(".gif")||mChatMessageData.chat_images!!.contains(".GIF")||mChatMessageData.chat_images!!.contains(".JPEG")||mChatMessageData.chat_images!!.contains(".PNG")){
                                mChatMessageData.viewType = LEFT_IMAGE_VIEW_HOLDER
                            }
                            else if(mChatMessageData.chat_images.contains(".mp4") || mChatMessageData.chat_images.contains("video") || mChatMessageData.chat_images!!.contains(".3gp")||mChatMessageData.chat_images.contains(".MP4")){
                                mChatMessageData.viewType =
                                    LEFT_IMAGE_VIEW_HOLDER
                            }}
                        else{
                            mChatMessageData.viewType = LEFT_VIEW_HOLDER
                        }
                        mMsgArrayList.add(mChatMessageData)
                    }
                }

                mChatMessagesAdapter.notifyDataSetChanged()
                binding.editMsgET.setText("")
                scrollToBottom()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
                scrollToBottom()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }

    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }

    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }

//    private val onUpdateChat= Emitter.Listener {args->
//        runOnUiThread(
//            Runnable {
//                val mGson = Gson()
//                val mChatMessageData: AllMessage = mGson.fromJson(args.get(1).toString(), AllMessage::class.java)
//                mMsgArrayList!!.add(mChatMessageData)
//                mChatMessagesAdapter!!.notifyDataSetChanged()
//            }
//        )
//    }

}