package com.cloutlyfe.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.SearchLocationAdapter
import com.cloutlyfe.app.databinding.ActivitySearchLocationBinding
import com.cloutlyfe.app.fragments.CreatePostFragment
import com.cloutlyfe.app.interfaces.LocationtemClickListner
import com.cloutlyfe.app.interfaces.PlacesLatLongModel
import com.cloutlyfe.app.model.EasyLocationProvider
import com.cloutlyfe.app.model.PlacesModel
import com.cloutlyfe.app.model.PredictionsItem
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants.Companion.GOOGLE_PLACES_LAT_LONG
import com.cloutlyfe.app.utils.Constants.Companion.GOOGLE_PLACES_SEARCH
import com.cloutlyfe.app.utils.Constants.Companion.PLACES_API_KEY
import com.cloutlyfe.app.utils.Constants.Companion.dismissProgressDialog
import com.cloutlyfe.app.utils.Constants.Companion.showProgressDialog
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class SearchLocationActivity : AppCompatActivity() {
    var binding: ActivitySearchLocationBinding? = null
    var TAG = this@SearchLocationActivity.javaClass.simpleName

    val mLocationArrayList: ArrayList<PredictionsItem?>? = ArrayList()
    var mAdapter: SearchLocationAdapter? = null

    var easyLocationProvider: EasyLocationProvider? = null
    var isCurrentLocation = true
    var mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION

    var addressCount = 0
    var latitude = 0.0
    var longitude = 0.0
    var locationAddress = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchLocationBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        init()
        setUpSearch()
    }

    fun init() {
        binding!!.imgCancelIV.setOnClickListener {
            performCloseClick()
        }

        binding!!.currentLocationLL.setOnClickListener {
            if (checkPermission()) {
                getLocationLatLong()
            } else {
//                if (Build.VERSION.SDK_INT <= 28) {
                requestPermission28()
//                } else if (Build.VERSION.SDK_INT >= 29) {
//                    requestPermission29()
//                }
//                else if (Build.VERSION.SDK_INT > 29) {
//                    showLocationSettingAndroid11Dialog(mActivity)
//                }
//            }
//            showProgressDialog(this)
//            getLocationLatLong()

            }
        }
    }
    /*
     */
    /*********
     * Support for Marshmallows Version
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    /*
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED*/
    /* && mAccessBackgroundP == PackageManager.PERMISSION_GRANTED*/ /*;
    }



    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation*/
    /*,mAccessBackground*/ /*}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getLocationLatLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();
                }
                return;
            }

        }
    }*/
    /*********
     * Support for Marshmallows Version
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     */
    private fun checkPermission(): Boolean {
//        if (Build.VERSION.SDK_INT <= 28) {
//            val mlocationFineP = ContextCompat.checkSelfPermission(this, mAccessFineLocation)
//            val mlocationCourseP =
//                ContextCompat.checkSelfPermission(this, mAccessCourseLocation)
//            return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
//        } else if (Build.VERSION.SDK_INT >= 29) {
            val mlocationFineP = ContextCompat.checkSelfPermission(this, mAccessFineLocation)
            val mlocationCourseP =
                ContextCompat.checkSelfPermission(this, mAccessCourseLocation)
            return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
//        }
//        return false
    }

    val REQUEST_PERMISSION_CODE = 919
    private fun requestPermission28() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    private fun requestPermission29() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(mAccessFineLocation, mAccessCourseLocation),
            REQUEST_PERMISSION_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.size <= 0) {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.e(TAG, "User interaction was cancelled.")
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // can be schedule in this way also
                    //  Utils.scheduleJob(this, LocationUpdatesService.class);9
                    //doing this way to communicate via messenger
                    // Start service and provide it a way to communicate with this class.
                    getLocationLatLong()
                } else {
                    // Permission denied.
                    Toast.makeText(this, getString(R.string.permision_msg), Toast.LENGTH_LONG)
                        .show()
                }
                return
            }
        }
    }
//    fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<String?>?,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode) {
//            REQUEST_PERMISSION_CODE -> {
//                if (grantResults.size <= 0) {
//                    // If user interaction was interrupted, the permission request is cancelled and you
//                    // receive empty arrays.
//                    Log.e(TAG, "User interaction was cancelled.")
//                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // can be schedule in this way also
//                    //  Utils.scheduleJob(this, LocationUpdatesService.class);9
//                    //doing this way to communicate via messenger
//                    // Start service and provide it a way to communicate with this class.
//                    getLocationLatLong()
//                } else {
//                    // Permission denied.
//                    Toast.makeText(this, getString(R.string.permision_msg), Toast.LENGTH_LONG)
//                        .show()
//                }
//                return
//            }
//        }
//    }


    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(this)
            .setInterval(3000)
            .setFastestInterval(1000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                    Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                    if (isCurrentLocation) {
                        //dynamically
                        isCurrentLocation = false
                        latitude = mlatitude
                        longitude = mlongitude
                        if (latitude != null && longitude != null) {
                            getUserAddressFromLatLong(latitude, longitude)
                        }
                    }
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }

    fun getUserAddressFromLatLong(mlatitude: Double, mlongitude: Double) {
        try {

            val addresses: List<Address>
            val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
            addresses = geocoder.getFromLocation(
                mlatitude,
                mlongitude,
                1
            )!! // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            var city: String = ""
            var country: String = ""
            var code: String = ""


            if (addresses != null && addresses.size > 0) {
                if (addresses[0].locality != null) {
                    city = addresses[0].locality
                }

                if (addresses[0].countryName != null) {
                    country = addresses[0].countryName
                }
                if (addresses[0].countryCode != null) {
                    code = addresses[0].countryCode
                }

                locationAddress = "$city, $country, $code"
                Log.e("locatttt", "$city,$country,$code")
//                if (addressCount == 0) {
//                    addressCount = 1

                CreatePostFragment.addressLocation = locationAddress.toString()
                CreatePostFragment.lati = latitude.toString()
                CreatePostFragment.longitu = longitude.toString()
                EditPostActivity.addressLocation = locationAddress.toString()
                EditPostActivity.lati = latitude.toString()
                EditPostActivity.longitu = longitude.toString()
//                    dismissProgressDialog()
                val intentReturn = Intent()
                intentReturn.putExtra("latitude", latitude.toString())
                intentReturn.putExtra("longitude", longitude.toString())
                intentReturn.putExtra("locationAddress", locationAddress.toString())
                setResult(RESULT_OK, intentReturn)
                finish()
//                }
                Log.e(TAG, "$city,$country,$code")
            }
//            dismissProgressDialog()
        } catch (e: Exception) {
            dismissProgressDialog()
            Log.e(TAG, "**Error**" + e.message)
        }
    }

    private fun setUpSearch() {
        binding!!.editSearchET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 0) {
                    binding!!.imgCancelIV.visibility = View.VISIBLE
                } else {
                    binding!!.imgCancelIV.visibility = View.GONE
                }
            }
        })


        binding!!.editSearchET.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchLocation()
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun searchLocation() {
        if (isNetworkAvailable(this)) {
            executePlacesRequest()
        } else {
            showToast(this, getString(R.string.no_internet_connection))
        }
    }

    private fun performCloseClick() {
        binding?.editSearchET?.setText("")
        binding?.imgCancelIV?.visibility = View.GONE
        mLocationArrayList?.clear()
        mAdapter?.notifyDataSetChanged()
        binding?.noDataTV?.visibility = View.GONE
    }


    private fun executePlacesRequest() {
        showProgressDialog(this)
        val mSearhText = binding?.editSearchET?.text.toString().trim { it <= ' ' }
        val mApiUrl: String =
            GOOGLE_PLACES_SEARCH + "input=" + mSearhText + "&fields=name,address_component,place_id&key=" + PLACES_API_KEY

        Log.e(TAG, "executePlacesRequestMAP: " + mApiUrl)
        val mApiInterface: Call<PlacesModel>? =
            RetrofitInstance.apiInterface1.searchPlacesRequest(mApiUrl)
        mApiInterface?.enqueue(object : Callback<PlacesModel> {
            override fun onResponse(call: Call<PlacesModel>, response: Response<PlacesModel>) {
                dismissProgressDialog()
                var mPlacesModel = response.body()
                mLocationArrayList?.clear()
                if (mPlacesModel?.status == "OK") {
                    mLocationArrayList?.addAll(mPlacesModel.predictions!!)
                    if (mLocationArrayList!!.size > 0) {
                        setAdapter()
                        binding?.noDataTV?.visibility = View.GONE
                    } else {
                        binding?.noDataTV?.visibility = View.VISIBLE
                    }
                } else {
                    binding?.noDataTV?.visibility = View.VISIBLE
                    showToast(this@SearchLocationActivity, getString(R.string.lnf))
                }
            }

            override fun onFailure(call: Call<PlacesModel>, t: Throwable) {
                dismissProgressDialog()
                binding?.noDataTV?.visibility = View.VISIBLE
                Log.e(TAG, "**Error**" + t.message)
            }
        })
    }


    private fun setAdapter() {
        mAdapter = SearchLocationAdapter(this, mLocationArrayList, mPlayDatesItemClickListner)
        binding?.locationsRV?.layoutManager = LinearLayoutManager(this)
        binding?.locationsRV?.setHasFixedSize(true)
        binding?.locationsRV?.adapter = mAdapter
    }

    var mPlayDatesItemClickListner: LocationtemClickListner = object : LocationtemClickListner {
        override fun onItemClickListner(mModel: PredictionsItem) {
//            super.LoaonItemClickListner(mModel)
            getPlaceLatLong(mModel)
        }
    }


    private fun getPlaceLatLong(mModel: PredictionsItem) {
        if (isNetworkAvailable(this)) {
            executePlaceLatLongRequest(mModel)
        } else {
            showToast(this, getString(R.string.no_internet_connection))
        }
    }

    private fun executePlaceLatLongRequest(mModel: PredictionsItem) {
        showProgressDialog(this)
        val mApiUrl: String =
            GOOGLE_PLACES_LAT_LONG + mModel.placeId + "&fields=name,geometry&types=establishment" + "&key=" + PLACES_API_KEY
        val mApiInterface: Call<PlacesLatLongModel>? =
            RetrofitInstance.apiInterface1.searchPlacesLatLongRequest(mApiUrl)
        mApiInterface?.enqueue(object : Callback<PlacesLatLongModel> {
            override fun onResponse(
                call: Call<PlacesLatLongModel>,
                response: Response<PlacesLatLongModel>
            ) {
                dismissProgressDialog()
                val mPlacesLatLongModel = response.body()
                if (mPlacesLatLongModel?.status == "OK") {
                    Log.e(TAG, "onResponse: " + mPlacesLatLongModel)
                    CreatePostFragment.addressLocation =
                        mPlacesLatLongModel.result!!.name.toString()
                    CreatePostFragment.lati =
                        mPlacesLatLongModel.result.geometry!!.location!!.lat.toString()
                    CreatePostFragment.longitu =
                        mPlacesLatLongModel.result.geometry!!.location!!.lng.toString()

                    EditPostActivity.addressLocation = mPlacesLatLongModel.result.name.toString()
                    EditPostActivity.lati =
                        mPlacesLatLongModel.result.geometry!!.location!!.lat.toString()
                    EditPostActivity.longitu =
                        mPlacesLatLongModel.result.geometry!!.location!!.lng.toString()
                    val intentReturn = Intent()
                    intentReturn.putExtra(
                        "latitude",
                        mPlacesLatLongModel.result.geometry!!.location!!.lat.toString()
                    )
                    intentReturn.putExtra(
                        "longitude",
                        mPlacesLatLongModel.result.geometry!!.location!!.lng.toString()
                    )
                    intentReturn.putExtra(
                        "locationAddress",
                        mPlacesLatLongModel.result.name.toString()
                    )
                    setResult(RESULT_OK, intentReturn)
                    finish()


                } else {
                    showAlertDialog(
                        this@SearchLocationActivity,
                        getString(R.string.lnf)
                    )
                }
            }

            override fun onFailure(call: Call<PlacesLatLongModel>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG, "**Error**" + t.message)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (easyLocationProvider != null) {
            easyLocationProvider!!.removeUpdates()
            lifecycle.removeObserver(easyLocationProvider!!)
        }
    }


    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    /*
    * Toast Message
    * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    /*
    *
    * Error Alert Dialog
    * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

}