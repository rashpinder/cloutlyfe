package com.cloutlyfe.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.ContentResolver
import android.content.ContentValues.TAG
import android.content.Intent
import android.database.Cursor
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.TrimVideoActivity
import com.cloutlyfe.app.activities.GPUImageFilterTools.createFilterForType
import com.cloutlyfe.app.activities.GPUImageFilterTools.filtersListForImage
import com.cloutlyfe.app.activities.GPUImageFilterTools.names
import com.cloutlyfe.app.databinding.ActivityFilterBinding
import com.daasuu.mp4compose.FillMode
import com.daasuu.mp4compose.Rotation
import com.daasuu.mp4compose.composer.Mp4Composer
import com.daasuu.mp4compose.filter.GlFilter
import com.daasuu.mp4compose.filter.GlFilterGroup
import com.daasuu.mp4compose.filter.GlMonochromeFilter
import com.daasuu.mp4compose.filter.GlVignetteFilter
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter
import java.io.File
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class FilterActivity : BaseActivity() {
    private var activity = this@FilterActivity
    lateinit var binding: ActivityFilterBinding
    var exoPlayer: SimpleExoPlayer? = null
    private var mp4Composer: Mp4Composer? = null
    private var videoUri: Uri? = null
    private var videoUrll: String? = null
    private var glFilter: GlFilter = GlFilterGroup(GlMonochromeFilter(), GlVignetteFilter())
    var videoUri2: Uri? = null
    private var selectedImagesArrayList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getIntentData()

        binding.doneIV.setOnClickListener {
            if (videoUrll.isNullOrEmpty()) {
                if(!isFinishing){
                    showProgressDialogF(activity)}
                val savedImageFileName =
                    "IMG" + SimpleDateFormat("yyyyMM_dd_HH_mm_ss").format(Date()) + ".jpg"
                val savedImagePath = "CustomImageFilters" + "/" +savedImageFileName
                binding.imageIV.saveToPictures("CustomImageFilters", savedImageFileName, null)
                val updatedImageUri = Uri.fromFile(File(getAndroidPicturesFolder()!!.absolutePath + "/" + savedImagePath))
                Log.i(TAG, "getIntentData: $savedImagePath::::$updatedImageUri")
                Handler().postDelayed({
                    dismissProgressDialogf()
                ImageUrii=updatedImageUri.path
                val intent = Intent()
                val args = Bundle()
                args.putSerializable("selectedImagesArrayList", selectedImagesArrayList as java.util.ArrayList<String>)
                intent.putExtra("bundle", args)
                intent.putExtra("data", ImageUrii.toString())
                intent.putExtra("position", pos)
                Log.e(TAG, "onCreate: "+pos )
                intent.putExtra("updatedImageUri",updatedImageUri.toString())
                intent.putExtra("updatedImageUrii",updatedImageUri.toString())
                setResult(777, intent)
                finish() }, 3000)
            }

            else {
                VideoUrii = videoUri2
                videoUrllFile = videoUrll


//                val intent = Intent()
//                intent.putExtra("data", videoUri2.toString())
//                intent.putExtra("file", videoUrll)
//                setResult(102, intent)
                finish()
            }
            //finishing activity

//            mGetMessageLauncher.launch(videoUri2.toString())
        }
    }


    private fun getAndroidPicturesFolder(): File? {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
    }

    var imageUri: String? = ""
    var capturedimageUri: String? = ""

    companion object {
        var VideoUrii: Uri? = null
        var videoUrllFile: String? = null
        var ImageUrii: String? = null
//        var selectedImagesArrayListt: ArrayList<String>? = null
    }
var pos:Int=0
    var filePath:String?=null
    private fun getIntentData() {
        if (intent != null) {
             val videoUrl = intent.getStringExtra("videoUri")
            filePath = intent.getStringExtra("filePath")
            pos = intent.getIntExtra("position",0)
            videoUrll = intent.getStringExtra("videoUri")
            imageUri = intent.getStringExtra("imageUri")
            capturedimageUri = intent.getStringExtra("capturedimageUri")
            videoUrll = intent.getStringExtra("file")
            if(!videoUrl.isNullOrEmpty()){
            if (videoUrl.contains(".mp4")) {
                binding.exoplayer.visibility = View.VISIBLE
                binding.imageIV.visibility = View.GONE
                setAdapter()
                videoUri = Uri.parse(videoUrl)
                try {
                    @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                        DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

                    val renderersFactory =
                        DefaultRenderersFactory(activity).setExtensionRendererMode(
                            extensionRendererMode
                        )

                    exoPlayer = SimpleExoPlayer.Builder(activity, renderersFactory).build()

                    val dataSourceFactory =
                        DefaultDataSourceFactory(activity, Util.getUserAgent(activity, "CloutLyfe"))

                    val mediaSources = ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(videoUri!!)
                    exoPlayer!!.prepare(mediaSources)
                    exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
//                    exoPlayer!!.addListener(this)
                    binding.exoplayer.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
                    binding.exoplayer.player = exoPlayer
                    exoPlayer!!.playWhenReady = true
//            exoPlayer!!.seekTo(0)
                    binding.exoplayer.requestFocus()
                } catch (e: Exception) {
                    Log.e("TAG", "Error : $e")
                }
            }}

            else if(!filePath.isNullOrEmpty()){
                binding.exoplayer.visibility = View.GONE
                binding.imageIV.visibility = View.VISIBLE
                setAdapter2()
                val bitmap = BitmapFactory.decodeFile(filePath)
                binding.imageIV.setImage(bitmap)
            }
//            else if(!capturedimageUri.isNullOrEmpty()){
//                binding.exoplayer.visibility = View.GONE
//                binding.imageIV.visibility = View.VISIBLE
//                setAdapter2()
//                binding.imageIV.setImage(Uri.parse(capturedimageUri))
//            }

            else {
                val args = intent.getBundleExtra("bundle")
                selectedImagesArrayList = args!!.getSerializable("selectedImagesArrayList") as ArrayList<String>
                videoUrll=null
                binding.exoplayer.visibility = View.GONE
                binding.imageIV.visibility = View.VISIBLE
                setAdapter2()
                binding.imageIV.setImage(Uri.parse(imageUri))
            }
        }
    }

    private fun setAdapter() {
        val filters = FilterType.values()
        val filterAdapter = FilterAdapter(::clickListenerInterface, activity, filters)
        binding.filterRV.adapter = filterAdapter
    }

    private fun setAdapter2() {
        val filterAdapter =
            FilterAdapter2(::clickListenerInterface2, activity, filtersListForImage, names)
        binding.filterRV.adapter = filterAdapter
    }

    private fun clickListenerInterface(filterType: FilterType) {
        glFilter = FilterType.createGlFilter(filterType, this)
        val customFilterVideo = getVideoFilePath().toString()
        startVideoFilterProcess(glFilter, customFilterVideo)
    }

    private fun clickListenerInterface2(filterType: GPUImageFilterTools.FilterType) {
        createFilterForType(activity, filterType)
        switchFilterTo(createFilterForType(activity, filterType))
    }

    private fun startVideoFilterProcess(resultedFilter: GlFilter, customFilterVideo: String) {
        exoPlayer!!.playWhenReady = false
        val videoWatchedTime = exoPlayer!!.currentPosition
        binding.progressBar.visibility = View.VISIBLE
        mp4Composer = null
        mp4Composer = Mp4Composer(videoUrll!!, customFilterVideo)
            .rotation(Rotation.ROTATION_90)
            .size(720, 720)
            .fillMode(FillMode.PRESERVE_ASPECT_FIT).videoBitrate((0.25 * 30 * 720 * 720).roundToInt())
            .filter(resultedFilter)
            .rotation(Rotation.NORMAL)
            .listener(object : Mp4Composer.Listener {
                override fun onProgress(progress: Double) {
                    Log.d(TAG, "onProgress = $progress")
                }

                override fun onCurrentWrittenVideoTime(timeUs: Long) {
                    Log.d(TAG, "onCurrentWrittenVideoTime = $timeUs")
                }

                override fun onCompleted() {
                    Log.d(TAG, "onCompleted()")
                    runOnUiThread {
                        mp4Composer = null
                        binding.progressBar.visibility = View.GONE
                        videoUri2 = Uri.parse(customFilterVideo)
                        try {
                            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                            val renderersFactory =
                                DefaultRenderersFactory(activity).setExtensionRendererMode(
                                    extensionRendererMode
                                )

                            exoPlayer = SimpleExoPlayer.Builder(activity, renderersFactory).build()

                            val dataSourceFactory = DefaultDataSourceFactory(
                                activity,
                                Util.getUserAgent(activity, "CloutLyfe")
                            )

                            val mediaSources = ProgressiveMediaSource.Factory(dataSourceFactory)
                                .createMediaSource(videoUri2!!)
                            exoPlayer!!.prepare(mediaSources)
                            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
//                    exoPlayer!!.addListener(this)
                            binding.exoplayer.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
                            binding.exoplayer.player = exoPlayer
                            exoPlayer!!.playWhenReady = true
//            exoPlayer!!.seekTo(0)
                            binding.exoplayer.requestFocus()
                        } catch (e: Exception) {
                            Log.e("TAG", "Error : $e")
                        }
                    }
                }

                override fun onCanceled() {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(activity, "Canceled", Toast.LENGTH_SHORT).show()
                    Log.d(TAG, "onCanceled")
                }

                override fun onFailed(exception: java.lang.Exception?) {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show()
                    Log.e(TAG, "onFailed()", exception)
                }
            })
            .start()
    }

    private fun getVideoFilePath(): String? {
        return getAndroidMoviesFolder()!!.absolutePath + "/" + SimpleDateFormat("yyyyMM_dd_HH_mm_ss").format(
            Date()
        ) + "_cloutLyfe.mp4"
    }

    private fun getAndroidMoviesFolder(): File? {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
    }

    private fun switchFilterTo(filter: GPUImageFilter) {
        if (binding.imageIV.filter == null || binding!!.imageIV.filter.javaClass != filter.javaClass) {
            binding.imageIV.filter = filter
        }
        binding.imageIV.requestRender()
    }

    override fun onBackPressed() {
        if (videoUri.toString().contains(".mp4")) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
            VideoUrii = videoUri
            videoUrllFile = filePath
        }
        finish()
    }

    override fun onStop() {
        super.onStop()
        dismissProgressDialogf()
    }

    var progressDialogF: Dialog? = null
    /*
    * Show Progress Dialog
    * */
    fun showProgressDialogF(mActivity: Activity?) {
        progressDialogF = Dialog(mActivity!!)
        progressDialogF!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialogF!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialogF!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialogF!!.setCanceledOnTouchOutside(false)
        progressDialogF!!.setCancelable(false)
//        if (progressDialog != null) progressDialog!!.show()
//            if (!isFinishing()) {
                progressDialogF?.show()
//    }
    }
    /*
      * Hide Progress Dialog
      * */
    fun dismissProgressDialogf() {
        if (!isFinishing()) {
            if (progressDialogF != null && progressDialogF!!.isShowing) {
                progressDialogF?.dismiss()
            }}
    }

    override fun onDestroy() {
        super.onDestroy()
        dismissProgressDialogf()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onPause() {
        super.onPause()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }
}