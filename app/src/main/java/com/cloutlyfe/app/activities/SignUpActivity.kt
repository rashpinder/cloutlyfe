package com.cloutlyfe.app.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.LoginActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivitySignUpBinding
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Resource
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.viewmodel.SignUpViewModel
import com.cloutlyfe.app.viewmodel.ViewModelProviderFactory

class SignUpActivity : BaseActivity() {
    lateinit var binding: ActivitySignUpBinding

    private var checkUncheck: Int = 0
    lateinit var signUpViewModel: SignUpViewModel
    var deviceTokenUser: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getDeviceTokenUser()
        init()
//        binding.txtTermsTV.setMovementMethod(LinkMovementMethod.getInstance())
        binding.txtTermsTV.makeLinks(
            Pair(getString(R.string.terms_andconditions), View.OnClickListener {
                val url = "https://cloutlyfe.online/CloutLyfe/TermsAndConditions.html"
//                val url = "https://cloutlyfe.online/CloutLyfe/TermsAndConditions.html"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            })
        )
        performClicks()
    }

    private fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
// use this to change the link color
                    textPaint.color = textPaint.linkColor
// toggle below value to enable/disable
// the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            val startIndexOfLink = this.text.toString().indexOf(link.first)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }


    //Device token
    fun getDeviceTokenUser() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful) {
                deviceTokenUser = it.result!!
                Log.e("CLOUTLYFE", "Device token ::" + deviceTokenUser)
            } else {
                Log.e("CLOUTLYFE", "Device token fetch issue")
                return@OnCompleteListener
            }
        })

    }


    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        signUpViewModel = ViewModelProvider(this, factory).get(SignUpViewModel::class.java)
    }

    fun performClicks() {
//        binding.txtTermsTV.setOnClickListener {
//            performTermsTextClick()
//        }
        binding.imgEyeIV.setOnClickListener {
            clickOnshowHidePwd()
        }
        binding.txtSignUpTV.setOnClickListener {
            performSignUpClick()
        }
        binding.txtLogInTV.setOnClickListener {
            performLogInClick()
        }
        binding.imgTermsIV.setOnClickListener {
            performTermsClick()
        }
    }


//    private fun performTermsTextClick() {
//        val openURL = Intent(Intent.ACTION_VIEW)
//        openURL.data = Uri.parse("http://161.97.132.85/CloutLyfe/TermsAndConditions.html")
//        startActivity(openURL)
//    }

    private fun performTermsClick() {
        if (checkUncheck == 0) {
            binding.imgTermsIV.setBackgroundResource(R.drawable.ic_check)
            checkUncheck++
        } else {
            binding.imgTermsIV.setBackgroundResource(R.drawable.ic_uncheck)
            checkUncheck--
        }
    }

    private fun clickOnshowHidePwd() {
        if (binding.editPasswordET.getTransformationMethod()
                .equals(PasswordTransformationMethod.getInstance())
        ) {
            binding.imgEyeIV.setImageResource(R.drawable.ic_unhide_pwd)

            //Show Password
            binding.editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            binding.editPasswordET.setSelection(binding.editPasswordET.length())
        } else {
            binding.imgEyeIV.setImageResource(R.drawable.ic_hide_pwd)
            //Hide Password
            binding.editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance())
            binding.editPasswordET.setSelection(binding.editPasswordET.length())

        }
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.editUserNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_username))
                flag = false
            }

            binding.editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_name))
                flag = false
            }

            binding.editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }

            !isValidEmaillId(binding.editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }

            binding.editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }

            binding.editPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }

            checkUncheck == 0 -> {
                showAlertDialog(mActivity, getString(R.string.agree_terms))
                flag = false
            }
        }
        return flag
    }

    private fun performLogInClick() {
        val i = Intent(mActivity, LoginActivity::class.java)
        startActivity(i)

    }

    private fun performSignUpClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.no_internet_connection))
            } else {
                executeSignUpApi()
            }
    }

    //Api execution
    private fun executeSignUpApi() {
        var userName = binding.editUserNameET.text.toString().trim()
        var name = binding.editNameET.text.toString().trim()
        var email = binding.editEmailET.text.toString().trim()
        val password = binding.editPasswordET.text.toString().trim()
        val deviceToken = deviceTokenUser
        val deviceType = "2"
        val location = ""
        val body = RequestBodies.SignUpBody(
            userName,
            name,
            email,
            password,
            deviceToken,
            deviceType,
            location
        )
        signUpViewModel.signUpUser(body)
        signUpViewModel.signUpResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        dismissProgressDialog()
                        response.data?.let { signupResponse ->
                            if (signupResponse.status == 1 || signupResponse.status == 200) {
                                showOpenActivityAlertDialog(mActivity, signupResponse.message)
//                                finish()
//                                showToast(mActivity,signupResponse.message)
//                                AppPrefrences().writeBoolean(mActivity,Constants.ISLOGIN, true)
//                                AppPrefrences().writeString(mActivity, Constants.ID, signupResponse.data.user_id)
//                                AppPrefrences().writeString(mActivity, Constants.NAME, signupResponse.data.name)
//                                AppPrefrences().writeString(mActivity, Constants.BIO, signupResponse.data.bio)
//                                AppPrefrences().writeString(mActivity, Constants.BLOCK_USER, signupResponse.data.block_user)
//                                AppPrefrences().writeString(mActivity, Constants.CHANGE_STATUS, signupResponse.data.changeStatus)
//                                AppPrefrences().writeString(mActivity, Constants.COUNTRY, signupResponse.data.country)
//                                AppPrefrences().writeString(mActivity, Constants.VERIFIED, signupResponse.data.verified)
//                                AppPrefrences().writeString(mActivity, Constants.FB_ID, signupResponse.data.facebook_id)
//                                AppPrefrences().writeString(mActivity, Constants.EMAIL, signupResponse.data.email)
//                                AppPrefrences().writeString(mActivity, Constants.AUTHTOKEN, signupResponse.data.auth_token)
//                                AppPrefrences().writeString(mActivity, Constants.GOOGLE_ID, signupResponse.data.google_id)
//                                AppPrefrences().writeString(mActivity, Constants.GOOGLE_ID, signupResponse.data.twitter_id)
//                                AppPrefrences().writeString(mActivity, Constants.PASSWORD, signupResponse.data.password)
//                                AppPrefrences().writeString(mActivity, Constants.DEVICETOKEN, signupResponse.data.device_token)
//                                AppPrefrences().writeString(mActivity, Constants.DEVICETYPE,signupResponse.data.device_type)
//                                AppPrefrences().writeString(mActivity, Constants.PHONE, signupResponse.data.mobile_number)
//                                AppPrefrences().writeString(mActivity, Constants.COVER_IMAGE, signupResponse.data.cover_image)
//                                Intent(mActivity, HomeActivity::class.java).also { startActivity(it) }
//                                finish()
                            } else {
                                showAlertDialog(mActivity, signupResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialog()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialog(mActivity)
                    }
                }
            }
        })
    }
}