package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.ViewsBottomSheetAdapter
import com.cloutlyfe.app.interfaces.ProfileClickInterface
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.bottomsheet.BottomSheetDialog
import jp.shts.android.storiesprogressview.StoriesProgressView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class StatusActivity : BaseActivity(), StoriesProgressView.StoriesListener,Player.EventListener {
    var selectedImageList: ArrayList<String>? = ArrayList()
    var selectedImageCounterList: ArrayList<String>? = ArrayList()
    var selectedStatusList: ArrayList<String>? = ArrayList()
    var selectedtimeList: ArrayList<String>? = ArrayList()
    var dataList: ArrayList<StatusViewedData>? = ArrayList()
    var viewsBottomsheet:LinearLayout?=null
    var exoPlayer: SimpleExoPlayer? = null

    // on below line we are creating variable for
    // our press time and time limit to display a story.
    var pressTime = 0L
    var limit = 500L
    var count:String? = "0"

    // on below line we are creating variables for
    // our progress bar view and image view .
    private var storiesProgressView: StoriesProgressView? = null
    private var image: ImageView? = null
    private var profileIV: ImageView? = null
    private var bottomDeleteLL: LinearLayout? = null
    private var ViewsLL: LinearLayout? = null
    private var idExoPlayerVIew: PlayerView? = null
    private var txtUsersNameTV: TextView? = null
    private var txtTimeTV: TextView? = null
    private var closeIV: ImageView? = null
//    private var stories: StoriesProgressView? = null
    private var reverse: View? = null
    private var skip: View? = null
    private var mViewsAdapter: ViewsBottomSheetAdapter? = null
    var statusID :String?=null
    // on below line we are creating a counter
    // for keeping count of our stories.
    private var counter = 0
//    private var duration_counter = 0
    private var status_images_counter = 0
    private var counter_status_id = 0
    private var counter_time = 0
    private var mStatusId:String?=""
    private var mModel: StatusImageModel?=null

    private var statusDetailList: ArrayList<MyStory?>? = ArrayList()
//    private var durationList: ArrayList<Long>? = ArrayList()
    private var durationList: ArrayList<String>? = ArrayList()

    private var statusSnapDetailList: ArrayList<Snap?>? = ArrayList()

    // on below line we are creating a new method for adding touch listener
    private val onTouchListener = OnTouchListener { v, event -> // inside on touch method we are
        // getting action on below line.
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {

                // on action down when we press our screen
                // the story will pause for specific time.
                pressTime = System.currentTimeMillis()

                // on below line we are pausing our indicator.
                storiesProgressView!!.pause()
                return@OnTouchListener false
            }
            MotionEvent.ACTION_UP -> {

                // in action up case when user do not touches
                // screen this method will skip to next image.
                val now = System.currentTimeMillis()

                // on below line we are resuming our progress bar for status.
                storiesProgressView!!.resume()

                // on below line we are returning if the limit < now - presstime
                return@OnTouchListener limit < now - pressTime
            }
        }
        false
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        image=findViewById(R.id.image)
        profileIV=findViewById(R.id.profileIV)
        txtUsersNameTV=findViewById(R.id.txtUsersNameTV)
        ViewsLL=findViewById(R.id.ViewsLL)
        txtTimeTV=findViewById(R.id.txtTimeTV)
        closeIV=findViewById(R.id.closeIV)
        reverse=findViewById(R.id.reverse)
        skip=findViewById(R.id.skip)
        bottomDeleteLL=findViewById(R.id.bottomDeleteLL)
        idExoPlayerVIew=findViewById(R.id.idExoPlayerVIew)
        viewsBottomsheet= findViewById(R.id.viewsBottomsheet)
closeIV!!.setOnClickListener {
    onBackPressed()
}
        ViewsLL!!.setOnClickListener {
            storiesProgressView!!.pause()
            showBottomViewDialog()
}
    bottomDeleteLL!!.setOnClickListener {
        storiesProgressView!!.pause()
        showDeleteDialog()
    }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (exoPlayer != null) {
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    private fun executeDeleteApi() {
        if (isNetworkAvailable(mActivity))
            RequestDeleteStatus()
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mDeleteParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["status_id"] = mStatusId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestDeleteStatus() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialogg(mActivity)
        val call = RetrofitInstance.appApi.deleteStatusRequest(mHeaders, mDeleteParam())
        call.enqueue(object : Callback<CodeStatusModel> {
            override fun onFailure(call: Call<CodeStatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialogg()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CodeStatusModel>,
                response: Response<CodeStatusModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialogg()
                if (response.body()!!.status == 1) {
                    showToast(mActivity, response.body()!!.message)
                    dismissProgressDialogg()
                    onBackPressed()
                    finish()

                } else if (response.body()!!.status == 3) {
                    dismissProgressDialogg()
                    storiesProgressView!!.resume()
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
                    dismissProgressDialogg()
                    storiesProgressView!!.resume()
                }
            }
        })
    }


    private fun executeGetViewedUsersListApi(statusId: String) {
        mStatusId=statusId
        if (isNetworkAvailable(mActivity!!)){
            requestGetViewedStatusList()
        }
        else{
            showToast(mActivity, getString(R.string.no_internet_connection))
        }}


    private fun mUpdateViewParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["status_id"] = mStatusId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun requestGetViewedStatusList() {
        val call = RetrofitInstance.appApi.getStatusViewUserDetail(mUpdateViewParam())
        call.enqueue(object : Callback<GetStatusViewedUsersListModel> {
            override fun onFailure(call: Call<GetStatusViewedUsersListModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialogg()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetStatusViewedUsersListModel>,
                response: Response<GetStatusViewedUsersListModel>
            ) {
                Log.e(TAG, response.body().toString())
                dataList!!.clear()
                count="0"
                if (response.body()!!.status == 1) {
                    count= response.body()!!.is_count
                    dataList!!.addAll(response.body()!!.data)
                } else if (response.body()!!.status == 3) {

                } else {
                }
            }
        })
    }

var dialogViews: BottomSheetDialog? = null

    private fun showBottomViewDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.story_view_bottomsheet, null)
        dialogViews = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogViews!!.setContentView(view)
        dialogViews!!.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogViews!!.setCancelable(true)
        dialogViews!!.setOnCancelListener(
            DialogInterface.OnCancelListener {
                storiesProgressView!!.resume()
            }
        )
//cancel button click
        val bottomsheetRV: RecyclerView? = dialogViews!!.findViewById(R.id.bottomsheetRV)
        var txtCountTV: TextView?= dialogViews!!.findViewById(R.id.txtCountTV)
        val txtNoDataTV:TextView?= dialogViews!!.findViewById(R.id.txtNoDataTV)

        if(count=="0"){
            txtNoDataTV!!.visibility=View.VISIBLE
        }
        else{
            txtNoDataTV!!.visibility=View.GONE
        }
        txtCountTV!!.setText(count!!)
        bottomsheetRV!!.layoutManager = LinearLayoutManager(mActivity)
        mViewsAdapter = ViewsBottomSheetAdapter(
            mActivity,
            dataList,mItemClickListener
        )
        bottomsheetRV!!.adapter = mViewsAdapter
        dialogViews!!.show()
    }


    private fun showDeleteDialog() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.edit_delete_dialog, null)
        var dialogDelete: BottomSheetDialog? = BottomSheetDialog(mActivity, R.style.BottomSheetDialog)
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
        dialogDelete.setOnCancelListener(
            DialogInterface.OnCancelListener {
                storiesProgressView!!.resume()
            }
        )

//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility=View.GONE

        deleteRL?.setOnClickListener {
            if(statusDetailList!!.get((statusDetailList!!.size) - 1)!!.snaps!!.size !=0) {
                statusID = statusDetailList!!.get((statusDetailList!!.size) - 1)!!.snaps!!.get(0)!!.status_id
                executeDeleteApi()
            }
            else{
                dialogDelete.dismiss()
            }

        }
        dialogDelete.show()
    }



    override fun onResume() {
        super.onResume()
        // hit api
        executeStatusDetailData()
    }

    /*
         * Hide Progress Dialog
         * */
    fun dismissProgressDialogg() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog?.dismiss()
        }
    }


    /*
    * Show Progress Dialog
    * */
    fun showProgressDialogg(mActivity: Activity?) {
        progressDialog = mActivity?.let { Dialog(it) }
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
//        if (progressDialog != null) progressDialog!!.show()
        if(mActivity!=null){
            progressDialog!!.show()}
    }

    override fun onNext() {
        if(exoPlayer!=null){
            exoPlayer!!.pause()
            exoPlayer!!.release()
        }
        if(!selectedImageList!![++status_images_counter].contains("mp4")){
//            showProgressDialogg(mActivity)
            storiesProgressView!!.pause()
//            storiesProgressView!!.setStoryDuration(5000L)
            idExoPlayerVIew!!.visibility = View.GONE
            image!!.visibility = View.VISIBLE
            Glide.with(applicationContext)
                .load(selectedImageList!![++counter])
                .placeholder(R.color.grey)
                .error(R.color.grey)
                .listener(object : RequestListener<Drawable?> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        storiesProgressView!!.resume()
//                        dismissProgressDialogg()
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        storiesProgressView!!.resume()
//                        dismissProgressDialogg()
                        return false
                    }
                })
                .into(image!!)
//        Glide.with(applicationContext)
//            .load(selectedImageList!![++counter])
//            .placeholder(R.color.grey)
//            .thumbnail(0.01f)
//            .into(image!!)

        }
        else{
//            durr= durationList!!.get(++duration_counter).toLong()
//            storiesProgressView!!.setStoryDuration(durr)
            idExoPlayerVIew!!.visibility = View.VISIBLE
            image!!.visibility = View.GONE
            initializePlayer(selectedImageList!![++counter])
        }
        val today = Date()
        val timestamp: Long = selectedtimeList!![++counter_time].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date: Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date: Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    txtTimeTV!!.text = "Just now"
                }
                else{
                    txtTimeTV!!.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    txtTimeTV!!.text = "$minutes minutes ago"
                }
                else{
                    txtTimeTV!!.text = "Just now"
                }
            }
        }
        else if(hours.toInt() > 0){
            txtTimeTV!!.text = "$hours hours ago"
        }
        executeGetViewedUsersListApi(selectedStatusList!![++counter_status_id])
    }

    override fun onPrev() {
        if(exoPlayer!=null){
            exoPlayer!!.pause()
            exoPlayer!!.release()
        }
        // this method id called when we move to previous story.
        // on below line we are decreasing our counter
        if (status_images_counter - 1 < 0) return
        Log.e(TAG, "onPrev: "+selectedImageList.toString())
        Log.e(TAG, "onPrev: "+counter.toString() )
        if(!selectedImageList!![--status_images_counter].contains("mp4")){
//            showProgressDialogg(mActivity)
//            --duration_counter
//           storiesProgressView!!.setStoryDuration(5000L)
            idExoPlayerVIew!!.visibility = View.GONE
            image!!.visibility = View.VISIBLE
            Glide.with(applicationContext)
                .load(selectedImageList!![--counter])
                .placeholder(R.color.grey)
                .error(R.color.grey)
                .listener(object : RequestListener<Drawable?> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
//                        dismissProgressDialogg()
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
//                        dismissProgressDialogg()
                        return false
                    }
                })
                .into(image!!)
//        Glide.with(applicationContext)
//            .load(selectedImageList!![--counter])
//            .placeholder(R.color.grey)
//            .thumbnail(0.01f)
////            .centerCrop()
//            .into(image!!)
        }
        else{
//            durr= durationList!!.get(--duration_counter).toLong()
            idExoPlayerVIew!!.visibility = View.VISIBLE
            image!!.visibility = View.GONE
//            storiesProgressView!!.setStoryDuration(durr)
            initializePlayer(selectedImageList!![--counter])
        }
        val today = Date()
        val timestamp: Long = selectedtimeList!![--counter_time].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date: Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date: Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    txtTimeTV!!.text = "Just now"
                }
                else{
                    txtTimeTV!!.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    txtTimeTV!!.text = "$minutes minutes ago"
                }
                else{
                    txtTimeTV!!.text = "Just now"
                }
            }
        }
        else if(hours.toInt() > 0){
            txtTimeTV!!.text = "$hours hours ago"
        }
        executeGetViewedUsersListApi(selectedStatusList!![--counter_status_id])
    }

    override fun onComplete() {
        // when the stories are completed this method is called.
        // in this method we are moving back to initial main activity.
        //    finishAffinity()
        onBackPressed()
    }



    override fun onDestroy() {
        super.onDestroy()
        if(exoPlayer!=null){
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }


    override fun onStop() {
        super.onStop()
        if(exoPlayer!=null){
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

    override fun onPause() {
        super.onPause()
        if(exoPlayer!=null){
            exoPlayer!!.stop()
            exoPlayer!!.release()
        }
    }

//    override fun onDestroy() {
//        // in on destroy method we are destroying
//        // our stories progress view.
//        storiesProgressView!!.destroy()
//        super.onDestroy()
//    }

    fun getDataMehod(){
        /*  latest code */
//showProgressDialog(mActivity)
        // on below line we are initializing our variables.
        storiesProgressView = findViewById(R.id.stories) as StoriesProgressView
        // on below line we are setting the total count for our stories.
        storiesProgressView!!.setStoriesCount(selectedImageList!!.size)
        // on below line we are calling a method for set

        val objects: LongArray = al.toTypedArray().toLongArray()
        for (obj in objects){
            storiesProgressView!!.setStoriesCountWithDurations(objects)
        }

//        storiesProgressView!!.setStoryDuration(durationList!!.get(duration_counter).toLong())
        // on story listener and passing context to it.
        storiesProgressView!!.setStoriesListener(this)
        // below line is use to start stories progress bar.



        if (selectedImageList!![status_images_counter].contains("mp4")){
            idExoPlayerVIew!!.visibility = View.VISIBLE
            image!!.visibility = View.GONE
            initializePlayer(selectedImageList!![counter])
        }
        else{

//            showProgressDialogg(mActivity)
//            storiesProgressView!!.setStoryDuration(5000L)
            idExoPlayerVIew!!.visibility = View.GONE
            image!!.visibility = View.VISIBLE

            Glide.with(applicationContext)
                .load(selectedImageList!![counter])
                .placeholder(R.color.grey)
                .error(R.color.grey)
                .listener(object : RequestListener<Drawable?> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        storiesProgressView!!.startStories(counter)
//                        dismissProgressDialogg()
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
//                        dismissProgressDialogg()
                        return false
                    }
                })
                .into(image!!)

//            Glide.with(applicationContext)
//                .load(selectedImageList!![counter])
//                .placeholder(R.color.grey)
//                .thumbnail(0.01f)
//                .into(image!!)
        }

        val today = Date()
        val timestamp: Long = selectedtimeList!![counter_time].toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val status_date: Date?=sdf.parse(formattedDate)

        val format_today=sdf.format(today)
        val today_date: Date?=sdf.parse(format_today)
        println("************* status  date ****************** "+formattedDate+"\n today_date "+format_today)
        val diff: Long = today!!.time - status_date!!.time
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(diff)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(diff)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(diff)
        println("Hours =$hours\n Minutes =$minutes\n seconds =$seconds")
        if(hours.toInt() == 0){
            if(minutes.toInt() == 0){
                if(seconds.toInt()<60) {
                    txtTimeTV!!.text = "Just now"
                }
                else{
                    txtTimeTV!!.text = ""
                }
            }
            else if (minutes.toInt() > 0){
                if(minutes.toInt()<60){
                    txtTimeTV!!.text = "$minutes minutes ago"
                }
                else{
                    txtTimeTV!!.text = "Just now"
                }
            }
        }
        else if(hours.toInt() > 0){
            txtTimeTV!!.text = "$hours hours ago"
        }

        executeGetViewedUsersListApi(selectedStatusList!![counter_status_id])
        // below is the view for going to the previous story.
        // initializing our previous view.
        val reverse: View = findViewById(R.id.reverse)

        // adding on click listener for our reverse view.
        reverse.setOnClickListener { // inside on click we are
            // reversing our progress view.
            storiesProgressView!!.reverse()
        }

        // on below line we are calling a set on touch
        // listener method to move towards previous image.
        reverse.setOnTouchListener(onTouchListener)

        // on below line we are initializing
        // view to skip a specific story.
        val skip: View = findViewById(R.id.skip)
        skip.setOnClickListener { // inside on click we are
            // skipping the story progress view.
            storiesProgressView!!.skip()
        }
        // on below line we are calling a set on touch
        // listener method to move to next story.
        skip.setOnTouchListener(onTouchListener)
        //     }
    }
    var boolean:Boolean=true
    var durr:Long=0
    fun initializePlayer(link: String?)
    {
        if (!link.isNullOrEmpty()) {
            Log.e("Videos", "***Link***" + link)
// progressBarVid.visibility = View.VISIBLE
            idExoPlayerVIew!!.visibility = View.VISIBLE
            image!!.visibility = View.GONE

            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode = DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER

            val loadControl = DefaultLoadControl.Builder().setBufferDurationsMs(25000, 50000, 100, 300).createDefaultLoadControl()

            val renderersFactory = DefaultRenderersFactory(mActivity).setExtensionRendererMode(extensionRendererMode)

            exoPlayer = SimpleExoPlayer.Builder(mActivity, renderersFactory).build()


            val dataSourceFactory = DefaultDataSourceFactory(mActivity, Util.getUserAgent(mActivity, "CloutLyfe"))

            val mediaSources = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(
                Uri.parse(link))
            exoPlayer!!.prepare(mediaSources)
//            exoPlayer!!.repeatMode = Player.REPEAT_MODE_ALL
            exoPlayer!!.addListener(this)
            idExoPlayerVIew!!.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
            idExoPlayerVIew!!.setShutterBackgroundColor(Color.TRANSPARENT)
            idExoPlayerVIew!!.player = exoPlayer
            exoPlayer!!.playWhenReady = true
            idExoPlayerVIew!!.requestFocus()


            exoPlayer!!.addListener(object : Player.EventListener {
                override fun onTimelineChanged(
                    timeline: Timeline,
                    manifest: Any?,
                    reason: Int
                ) {
                }

                override fun onTracksChanged(
                    trackGroups: TrackGroupArray,
                    trackSelections: TrackSelectionArray
                ) {
                }

                override fun onLoadingChanged(isLoading: Boolean) {
                    if(isLoading){
//                        showProgressDialogg(mActivity)
                        storiesProgressView?.pause()
                    }
                    else{
//                        dismissProgressDialogg()
                        storiesProgressView?.resume()
                    }
                }
                override fun onPlayerStateChanged(
                    playWhenReady: Boolean,
                    playbackState: Int
                ) {
                    when (playbackState) {
                        Player.STATE_READY -> {
                            if(boolean){
                                boolean=false
                                storiesProgressView!!.startStories(counter)
                            }
                            else{
                                exoPlayer!!.playWhenReady = true
                                storiesProgressView?.resume()
                            }
//                            dismissProgressDialogg()

//                            storiesProgressView!!.startStories(); // <- start progress

                        }
                        Player.STATE_BUFFERING -> {
                            storiesProgressView?.pause()
//                            exoPlayer!!.seekTo(0)
                        }
                        Player.STATE_IDLE -> {

                        }

                        else ->
                        {
                            exoPlayer!!.retry()
                        }
                    }
                }

                override fun onRepeatModeChanged(repeatMode: Int) {

                }
                override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
                override fun onPlayerError(error: ExoPlaybackException) {}
                override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
                override fun onSeekProcessed() {

                }
            })
        } else {
            Log.e(TAG, "initializePlayer: No video url foundL")
        }

    }
    val al: MutableList<Long> = ArrayList()

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeStatusDetailData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["token"] = getAuthToken()
        showProgressDialogg(mActivity)
        val call = RetrofitInstance.appApi.getStories(mHeaders, mParam())
        call.enqueue(object : Callback<GetStoriesModel> {
            override fun onResponse(
                call: Call<GetStoriesModel>,
                response: Response<GetStoriesModel>
            ) {
                Log.e(TAG, response.body().toString())
                statusDetailList?.clear()
                statusSnapDetailList?.clear()
                selectedImageList?.clear()
                selectedImageCounterList?.clear()
                selectedStatusList?.clear()
                selectedtimeList?.clear()
                durationList?.clear()
                al.clear()
                dismissProgressDialogg()
                val mGetStatusDetailModel = response.body()
                if (mGetStatusDetailModel!!.status == 1) {
                    statusDetailList?.addAll(mGetStatusDetailModel.data.myStories)
                    statusSnapDetailList?.addAll(statusDetailList!![0]!!.snaps)
                        for(i in statusSnapDetailList?.indices!!){
                            var statusImages: String? =statusSnapDetailList!![i]!!.status_image
                            var statusId: String? =statusSnapDetailList!![i]!!.status_id
                            var time: String? =statusSnapDetailList!![i]!!.statusdate
                            var durationn: String? =statusSnapDetailList!![i]!!.duration
                            selectedImageList!!.add(statusImages!!)
                            selectedImageCounterList!!.add(statusImages!!)
                            selectedStatusList!!.add(statusId!!)
                            selectedtimeList!!.add(time!!)
                            durationList!!.add(durationn!!)
                            al.add(i,durationn.toLong())
                        }

                    if(statusSnapDetailList!!.size!=0) {
                        Glide.with(mActivity!!).load(statusDetailList!!.get(0)!!.user!!.photo)
                            .placeholder(R.drawable.ic_profile_ph)
                            .error(R.drawable.ic_profile_ph).into(profileIV!!)
                        txtUsersNameTV!!.setText(statusDetailList!!.get(0)!!.user!!.name)
                        getDataMehod()
                        /*  latest code */
//                        showProgressDialog(mActivity)
                        // on below line we are initializing our variables.


                    }
                    else{
                        dismissProgressDialogg()
                        showToast(mActivity,"No story added yet")
                        finish()
                    }
                }else if (mGetStatusDetailModel!!.status == 3){
                    dismissProgressDialogg()
                    showAlertDialog(mActivity, mGetStatusDetailModel.message!!)
                }else{
                    dismissProgressDialogg()
                    //   showAlertDialog(requireActivity(),mGetEnvelopeModel.message)

                }
            }
            override fun onFailure(call: Call<GetStoriesModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialogg()
            }

        })
    }

    var mItemClickListener: ProfileClickInterface = object : ProfileClickInterface {
        override fun onItemClickListner(
            mPosition: Int,
            value: String,
            other_user_id: String,
            name: String
        ) {
            val i = Intent(mActivity, OtherUserProfileActivity::class.java)
            i.putExtra("value", value)
            i.putExtra("other_user_id", other_user_id)
            i.putExtra("name", name)
           startActivity(i)
            dialogViews!!.dismiss()
        }
    }
}