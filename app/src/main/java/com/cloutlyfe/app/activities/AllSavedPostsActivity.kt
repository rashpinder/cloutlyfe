package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.cloutlyfe.app.BaseActivity
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.AllSavedAdapter
import com.cloutlyfe.app.databinding.ActivityAllSavedPostsBinding
import com.cloutlyfe.app.interfaces.LoadMoreScrollListner
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.HomeModel
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.viewmodel.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created By Rashpinder
 */
class AllSavedPostsActivity : BaseActivity() {

    lateinit var binding: ActivityAllSavedPostsBinding
    var allSavedAdapter: AllSavedAdapter? = null
    var imageList: ArrayList<HomeData> = ArrayList()
    var thumbnailList: ArrayList<String> = ArrayList()
    var list: ArrayList<String> = ArrayList()
    lateinit var savedViewModel: HomeViewModel
    var mPageNo: Int = 1
    var mPerPage: Int = 2000
    var isLoading: Boolean = true
    lateinit var mHomeDataModel: HomeModel
    lateinit var mImageDataModel: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAllSavedPostsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.text = getString(R.string.saved_posts)

        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        if (imageList != null) {
            imageList.clear()
        }
        if (list != null) {
            list.clear()
        }
        if (thumbnailList != null) {
            thumbnailList.clear()
        }
        executeSavedApi()
    }


    private fun executeSavedApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            RequestSavedData()
        }
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestSavedData() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.savedRequest(mHeaders, mParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                if (mHomeDataModel.status == 1) {
                    dismissProgressDialog()
                    isLoading = !mHomeDataModel.lastPage.equals(true)
                    imageList.addAll(response.body()!!.data)
                    for (i in 0..imageList.size - 1) {
                        if (imageList[i].thumbnail_image==null || imageList[i].thumbnail_image!!.isEmpty()) {
                            thumbnailList.add("")
                        }
                        else{
                            thumbnailList.add(imageList[i].thumbnail_image!![0])
                        }
                        if (imageList[i].image!!.size > 0) {
                            list.add(imageList[i].image!![0])
                        } else if (imageList[i].image!!.size == 1) {
                            list.add(imageList[i].image!![i])
                        }
                        else {
                            list.add(imageList[i].image!![i])
                        }
                    }
                    setAdapter(imageList, list,thumbnailList)
                    Log.e(TAG, "RequestHomeDataAPI: " + imageList)
                    if (imageList.size == 0) {
                        binding.savedRV.visibility=View.GONE
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text =
                            getString(R.string.not_saved)
                    } else {
                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mHomeDataModel.status == 3) {
//                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                } else {
                    dismissProgressDialog()
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.savedRV.visibility=View.GONE
                    binding.txtNoDataTV.text = getString(R.string.not_saved)
                }
            }
        })
    }
    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: java.util.ArrayList<String>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
        }
    }


    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreSavedRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        binding.mProgressRL.visibility = View.VISIBLE
        val call = RetrofitInstance.appApi.savedRequest(mHeaders, mLoadMoreParam())
        call.enqueue(object : Callback<HomeModel> {
            override fun onFailure(call: Call<HomeModel>, t: Throwable) {
                dismissProgressDialog()
                binding.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<HomeModel>,
                response: Response<HomeModel>
            ) {
                binding.mProgressRL.visibility = View.GONE
                val mGetSavedModel = response.body()
                if (mGetSavedModel!!.status == 1) {
                    mGetSavedModel.data.let {
                        imageList.addAll<HomeData>(
                            it
                        )
                    }
                    for (i in 0..imageList.size - 1) {
                        if (imageList[i].thumbnail_image==null || imageList[i].thumbnail_image!!.isEmpty()) {
                            thumbnailList.add("bjhbvhfb")
                        }
                        else{
                            thumbnailList.add(imageList[i].thumbnail_image.toString())
                        }
                        if (imageList[i].image!!.size > 0) {
                            list.add(imageList[i].image!![0])
                        } else if (imageList[i].image!!.size == 1) {
                            list.add(imageList[i].image!![i])
                        }
                        else {
                            list.add(imageList[i].image!![i])
                        }
                    }

                    allSavedAdapter?.notifyDataSetChanged()
                    isLoading = !mGetSavedModel.lastPage.equals(true)
                } else if (mGetSavedModel.status == 0) {
//                    showToast(mActivity, mGetSavedModel.message)
                }
            }
        })
    }


    private fun setAdapter(
        imageList: ArrayList<HomeData>,
        list: ArrayList<String>,
        thumbnailList: ArrayList<String>
    ) {
            val adapter = AllSavedAdapter(mActivity, imageList, list,thumbnailList, mLoadMoreScrollListner)
            val layoutManager = GridLayoutManager(mActivity, 3)
            binding.savedRV.layoutManager = layoutManager
            binding.savedRV.adapter = adapter
    }
}
