package com.cloutlyfe.app.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.ReplyCommentAdapter
import com.cloutlyfe.app.databinding.ActivityReplyBinding
import com.cloutlyfe.app.interfaces.CommentLoadMoreListener
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ReplyActivity : BaseActivity() {
    lateinit var binding: ActivityReplyBinding
    var mCommentsList: ArrayList<String> = ArrayList<String>()
    var commentsAdapter: ReplyCommentAdapter? = null
    lateinit var mCommentsDataModel: GetCommentsModel
    var commentsList: ArrayList<DataXX> = ArrayList()
    var replyList: ArrayList<ReplyComment> = ArrayList()
    lateinit var imageList: ArrayList<String>
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mPerPage: Int = 10
    var mPostId: String = ""
    var mCommentId: String = ""
    var commentData: DataXX? = null
//    lateinit var likeUnlikeViewModel: LikeUnlikeCommentViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReplyBinding.inflate(layoutInflater)
        setContentView(binding.root)
//        init()
        getIntentData()
//        setAdapter(mActivity, commentsList)

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
        binding.sendCIV.setOnClickListener {
            performAddCommentClick()
        }

    }

//    private fun init() {
//        val repository = AppRepository()
//        val factory = ViewModelProviderFactory(application, repository)
//        likeUnlikeViewModel = ViewModelProvider(this, factory).get(LikeUnlikeCommentViewModel::class.java)
//    }
    private fun performAddCommentClick() {
        executeAddCommentRequest()
    }

    private fun executeAddCommentRequest() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                //   mBitmap = (saveIV!!.drawable as BitmapDrawable).bitmap
                executeAddNewCommentRequest()
            } else {
                showToast(mActivity, getString(R.string.no_internet_connection))
            }
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        mMap["comment"] = binding.editMessageET.text.toString()
        mMap["comment_id"] = mCommentId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddNewCommentRequest() {
        val call = RetrofitInstance.appApi.addReplyCommentRequest(mParams())
        call.enqueue(object : Callback<AddCommentModel> {
            override fun onFailure(call: Call<AddCommentModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            override fun onResponse(
                call: Call<AddCommentModel>,
                response: Response<AddCommentModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mAddCommentModel = response.body()
                if (mAddCommentModel!!.status == 1) {
                    binding.editMessageET.setText("")
                    commentsList.clear()
                    executeGetCommentsApi(mPostId)

                } else if (mAddCommentModel.status == 0) {
                    showAlertDialog(mActivity, mAddCommentModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }


    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.editMessageET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_add_comment))
                flag = false
            }
        }
        return flag
    }

    private fun getIntentData() {
        val gson = Gson()
        val data = intent.getStringExtra("CommentsData")
        commentData = gson.fromJson(
            data,
            DataXX::class.java
        )

        setDataOnViews()
        executeGetCommentsApi(mPostId)
    }


    private fun setDataOnViews() {
        mPostId = commentData?.post_id.toString()
        mCommentId = commentData?.comment_id.toString()

        Glide.with(mActivity).load(commentData?.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((binding.myProfileIV))
    }

    private fun setAdapter(
        mActivity: Activity,
        commentsList: ArrayList<DataXX>,
        mLoadMoreScrollListner: CommentLoadMoreListener,
        replyList: ArrayList<ReplyComment>
    ) {
        commentsAdapter = ReplyCommentAdapter(mActivity, commentsList, mLoadMoreScrollListner,replyList)
        binding.commentsRV.layoutManager = LinearLayoutManager(mActivity)
        binding.commentsRV.adapter = commentsAdapter
    }


    private fun executeGetCommentsApi(post_id: String?) {
        commentsList.clear()
        replyList.clear()
        if (isNetworkAvailable(mActivity))
            RequestCommentsData(post_id)
        else
            showToast(mActivity, getString(R.string.no_internet_connection))
    }


    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["post_id"] = mPostId
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        mMap["last_commentid"] = ""
        mMap["comment_id"] = mCommentId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun RequestCommentsData(post_id: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getReplyCommentRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetCommentsModel> {
            override fun onFailure(call: Call<GetCommentsModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetCommentsModel>,
                response: Response<GetCommentsModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mCommentsDataModel = response.body()!!
                if (mCommentsDataModel.status == 1) {
                    dismissProgressDialog()
                    isLoading = !mCommentsDataModel.lastPage.equals(true)
                    commentsList.addAll(response.body()!!.data)

                    Log.e(TAG, "RequestHomeDataAPI: " + commentsList)
                    if (commentsList.size == 0) {

//                        binding.txtNoDataTV.visibility = View.VISIBLE
//                        binding.txtNoDataTV.text =
//                            getString(R.string.no_posts_available)

                    } else {
                        for (i in 0..commentsList.size - 1) {
                            replyList.addAll(commentsList[i].reply_comment)
                        }
                        setAdapter(mActivity, commentsList, mLoadMoreScrollListner,replyList)
//                        binding.txtNoDataTV.visibility = View.GONE
                    }
                } else if (mCommentsDataModel.status == 3) {
//                    showAccountDisableAlertDialog(mActivity, mCommentsDataModel.message!!)
                } else {
                    dismissProgressDialog()
//                    binding.txtNoDataTV.visibility = View.VISIBLE
//                    binding.txtNoDataTV.text = response.message()
                }
            }
        })
    }


    var mLoadMoreScrollListner: CommentLoadMoreListener = object : CommentLoadMoreListener {
        override fun onLoadMoreListner(mModel: ArrayList<DataXX>) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreSavedRequest()
//            }
        }
    }


    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        mMap["last_commentid"] = ""
        mMap["comment_id"] = mCommentId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreSavedRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        binding.mProgressRL.visibility = View.VISIBLE
        val call = RetrofitInstance.appApi.getCommentRequest(mHeaders, mLoadMoreParam())
        call.enqueue(object : Callback<GetCommentsModel> {
            override fun onFailure(call: Call<GetCommentsModel>, t: Throwable) {
                dismissProgressDialog()
                binding.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<GetCommentsModel>,
                response: Response<GetCommentsModel>
            ) {
                binding.mProgressRL.visibility = View.GONE
                val mGetSavedModel = response.body()
                if (mGetSavedModel!!.status == 1) {
                    mGetSavedModel.data.let {
                        commentsList.addAll<DataXX>(
                            it
                        )
                    }
                    commentsAdapter?.notifyDataSetChanged()
                    isLoading = !mGetSavedModel.lastPage.equals(true)
                } else if (mGetSavedModel.status == 0) {
//                    showToast(mActivity, mGetSavedModel.message)
                }
            }
        })
    }



}