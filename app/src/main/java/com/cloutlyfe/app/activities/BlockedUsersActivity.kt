package com.cloutlyfe.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View

import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.R

import com.cloutlyfe.app.adapters.BlockAdapter

import com.cloutlyfe.app.interfaces.ItemClicklistenerInterface

import java.util.*

import com.cloutlyfe.app.databinding.ActivityBlockedUsersBinding
import com.cloutlyfe.app.model.BlockUsersListModel
import com.cloutlyfe.app.model.StatusModel
import com.cloutlyfe.app.model.UserData
import com.cloutlyfe.app.retrofit.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BlockedUsersActivity : BaseActivity() {
    lateinit var binding: ActivityBlockedUsersBinding
    var mBlockList: ArrayList<UserData> = ArrayList()
    var blockAdapter: BlockAdapter? = null
    lateinit var blockViewModel: StatusModel
     var mPos:Int= 0
     var blocked_user_id:String= ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBlockedUsersBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.setText(getString(R.string.bu))
        executeBlockRequestApi()
        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

    }

    private fun executeBlockRequestApi() {
        if (isNetworkAvailable(mActivity)) {
            executeBlockRequest()
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeBlockRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getAllblockUsersRequest(mHeaders,mParams())
        call.enqueue(object : Callback<BlockUsersListModel> {
            override fun onFailure(call: Call<BlockUsersListModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<BlockUsersListModel>,
                response: Response<BlockUsersListModel>
            ) {
                dismissProgressDialog()
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel!!.status == 1) {
                    mBlockList.addAll(mModel.data)
                    setAdapter(mBlockList)
                    if (mBlockList.size > 0) {
                        binding.txtNoDataTV.visibility = View.GONE
                    } else {
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text = mModel.message
                    }
                } else if (mModel.status == 0) {
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.txtNoDataTV.text = mModel.message
//                    showAlertDialog(mActivity, mModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }


    private fun executeBlockUnblockRequestApi() {
        if (isNetworkAvailable(mActivity)) {
            executeBlockUnBlockRequest()
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }


    private fun mUnblockParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["blocked_user_id"] = blocked_user_id
        mMap["type"] = "2"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeBlockUnBlockRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.blockUnblockRequest(mHeaders,mUnblockParams())
        call.enqueue(object : Callback<StatusModel> {
            override fun onFailure(call: Call<StatusModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusModel>,
                response: Response<StatusModel>
            ) {
                dismissProgressDialog()
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel!!.status == 1) {
                    mBlockList.removeAt(mPos)
                    blockAdapter!!.notifyDataSetChanged()
                    if (mBlockList.size > 0) {
                        binding.txtNoDataTV.visibility = View.GONE
                    } else {
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text = "No Blocked users found"
                    }
                } else if (mModel.status == 0) {
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.txtNoDataTV.text = "No Blocked users found"
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }



    var mItemClickListener: ItemClicklistenerInterface = object : ItemClicklistenerInterface {
        override fun onItemClickListner(mPosition: Int, userId: String) {
            mPos= mPosition
            blocked_user_id= userId
            executeBlockUnblockRequestApi()
        }
    }

    private fun setAdapter(mBlockList: ArrayList<UserData>) {
        binding.blockRV.layoutManager = LinearLayoutManager(mActivity)
        blockAdapter = BlockAdapter(mActivity,
           mBlockList,mItemClickListener)
        binding.blockRV.adapter = blockAdapter

    }





}