package com.cloutlyfe.app.activities

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.cloutlyfe.app.R
import com.cloutlyfe.app.adapters.NotificationAdapter
import com.cloutlyfe.app.databinding.ActivityNotificationsBinding
import com.cloutlyfe.app.fragments.HomeFragment
import com.cloutlyfe.app.interfaces.LoadMoreScrollListner
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.retrofit.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationsActivity : BaseActivity() {
    lateinit var binding: ActivityNotificationsBinding
    var mNotificationsList: ArrayList<NotificationData> = ArrayList<NotificationData>()
    var mList: ArrayList<Notification> = ArrayList<Notification>()
    var notificationListAdapter: NotificationAdapter? = null

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.header.txtHeadingTV.text = getString(R.string.noti)
        executeNotificationsRequestApi()
//        setAdapter()
        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }
    }

    private fun executeNotificationsRequestApi() {
        if (isNetworkAvailable(mActivity)) {
            //   mBitmap = (saveIV!!.drawable as BitmapDrawable).bitmap
            executeNotificationRequest()
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeNotificationRequest() {
        showProgressDialog(mActivity)
        val call = RetrofitInstance.appApi.getNotificationRequest(mParams())
        call.enqueue(object : Callback<NotificationModel> {
            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<NotificationModel>,
                response: Response<NotificationModel>
            ) {
                dismissProgressDialog()
                Log.e(TAG, response.body().toString())
                val mNotificationsModel = response.body()
                if (mNotificationsModel!!.status == 1) {

                    mNotificationsList.addAll(mNotificationsModel.data)
                    for (i in 0..mNotificationsModel.data.size - 1) {
                        mList.addAll(mNotificationsModel.data[i].notification)
                    }
                    setAdapter(mList, mNotificationsList)
                    if (mNotificationsList.size > 0) {
                        binding.txtNoDataTV.visibility = View.GONE
                    } else {
                        binding.txtNoDataTV.visibility = View.VISIBLE
                        binding.txtNoDataTV.text = mNotificationsModel.message
                    }
                } else if (mNotificationsModel.status == 0) {
                    binding.txtNoDataTV.visibility = View.VISIBLE
                    binding.txtNoDataTV.text = mNotificationsModel.message
//                    showAlertDialog(mActivity, mNotificationsModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }
        })
    }


    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: java.util.ArrayList<String>) {
            if (isLoading) {
                Log.e(TAG, "isLoadingBoolean: " + isLoading)
                ++mPageNo
                binding!!.mProgressRL.visibility = View.VISIBLE
                Handler().postDelayed({
                    executeMoreNotificationDataRequest()
                }, 1500)

            }
        }
    }



    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["page_no"] = mPageNo.toString()
        mMap["par_page"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreNotificationDataRequest() {
        val call = RetrofitInstance.appApi.getNotificationRequest(mLoadMoreParam())
        call.enqueue(object : Callback<NotificationModel> {
            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                dismissProgressDialog()
                binding!!.mProgressRL.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<NotificationModel>,
                response: Response<NotificationModel>
            ) {
                binding!!.mProgressRL.visibility = View.GONE
                val mNotificationsModel = response.body()
                if (mNotificationsModel!!.status == 1) {
                    mNotificationsModel.data.let {
                        mNotificationsList.addAll<NotificationData>(
                            it
                        )
                    }
                    for (i in 0..mNotificationsModel.data.size - 1) {
                        mList.addAll(mNotificationsModel.data[i].notification)
                    }

                    if (mNotificationsList.size == 0) {
                        binding!!.txtNoDataTV.visibility = View.VISIBLE
                        binding!!.txtNoDataTV.text =
                            getString(R.string.no_posts_available)
                    } else {
                        binding!!.txtNoDataTV.visibility = View.GONE
                        notificationListAdapter?.notifyDataSetChanged()
                        isLoading = mNotificationsModel.lastPage == "FALSE"
                    }

                    Log.e(TAG, "isLoading: " + isLoading)
                } else if (mNotificationsModel.status == 0) {
//                    showToast(mActivity, mNotificationsModel.message)
                }
            }
        })
    }

    private fun setAdapter(
        mNotificationsList: ArrayList<Notification>,
        mList: ArrayList<NotificationData>
    ) {
        Log.e(TAG, "list size: " + mNotificationsList.size)

        binding.notifcationRV.layoutManager = LinearLayoutManager(mActivity)
        notificationListAdapter = NotificationAdapter(
            mActivity,
            mNotificationsList, mList
        )
        binding.notifcationRV.adapter = notificationListAdapter

//            notificationListAdapter = NotificationAdapter(mActivity,mNotificationsList,mList)
//            binding.notifcationRV.adapter = notificationListAdapter
    }


}