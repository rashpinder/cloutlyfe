package com.cloutlyfe.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.databinding.ActivityEditProfileBinding
import com.cloutlyfe.app.model.EditProfileModel
import com.cloutlyfe.app.retrofit.RetrofitInstance
import com.cloutlyfe.app.utils.Constants.Companion.BIO
import com.cloutlyfe.app.utils.Constants.Companion.COVER_IMAGE
import com.cloutlyfe.app.utils.Constants.Companion.NAME
import com.cloutlyfe.app.utils.Constants.Companion.PROFILE_IMAGE
import com.cloutlyfe.app.utils.Constants.Companion.USERNAME
import com.github.dhaval2404.imagepicker.ImagePicker
import com.khostul.app.utils.HandleAndroidPermissions
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class EditProfileActivity : BaseActivity() {
    lateinit var binding: ActivityEditProfileBinding
    var value: String? = ""
    var previous_cover_image: String? = ""
    var mProfileBitmap: Bitmap? = null
    var mCoverBitmap: Bitmap? = null
    internal var PERMISSION_ALL = 1

    private val PERMISSIONS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.CAMERA
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA

        )
    }
    private val readImagePermission =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) Manifest.permission.READ_MEDIA_IMAGES else Manifest.permission.READ_EXTERNAL_STORAGE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (ContextCompat.checkSelfPermission(
                this@EditProfileActivity,
                readImagePermission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            //permission granted
        } else {
            //permission not granted
        }

        if (!HandleAndroidPermissions.hasPermissions(
                this@EditProfileActivity,
                *PERMISSIONS
            )
        ) {
            ActivityCompat.requestPermissions(this@EditProfileActivity, PERMISSIONS, PERMISSION_ALL)
        }
        binding.header.txtHeadingTV.text = getString(R.string.edit_profile)
        getProfileDetail()
        binding.header.imgBackIV.setOnClickListener {
            onBackPressed()
        }

        binding.txtSaveTV.setOnClickListener {
            executeEditProfileApi()
//            onBackPressed()
        }

        binding.imgCrossTV.setOnClickListener {
            mCoverBitmap=null
            previous_cover_image="1"
            binding.imageIV.setImageDrawable(resources.getDrawable(R.drawable.placeholder_clout))
            binding.imgCrossTV.visibility= View.GONE
        }

        binding.profileFL.setOnClickListener {
            performAddProfileImgClick()
            value = "profile"
        }
        binding.uploadImagesRL.setOnClickListener {
            performAddCoverImgClick()
            value = "cover"
        }
//        binding.privacyRL.setOnClickListener {
//            val i = Intent(mActivity, WebViewActivity::class.java)
//            i.putExtra(getString(R.string.value),getString(R.string.terms))
//            startActivity(i)
//        }
//        binding.termsRL.setOnClickListener {
//            val i = Intent(mActivity, WebViewActivity::class.java)
//            i.putExtra(getString(R.string.value),getString(R.string.terms))
//            startActivity(i)
//        }
//        binding.LogoutRL.setOnClickListener {
//            onBackPressed()
//        }

    }

    override fun onResume() {
        super.onResume()

    }
    private fun performAddCoverImgClick() {
            onSelectCoverImageClick()
    }


    private fun getProfileDetail() {
        Glide.with(mActivity).load(AppPrefrences().readString(mActivity, PROFILE_IMAGE, null))
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(binding.imgProfileIV)

        Glide.with(mActivity).load(AppPrefrences().readString(mActivity, COVER_IMAGE, null))
            .placeholder(R.drawable.placeholder_clout)
            .error(R.drawable.placeholder_clout)
            .into(binding.imageIV)

if(AppPrefrences().readString(mActivity, COVER_IMAGE, null)==null || AppPrefrences().readString(mActivity, COVER_IMAGE, null)==""){
    previous_cover_image=""
    binding.imgCrossTV.visibility=View.GONE
}
        else if(AppPrefrences().readString(mActivity, COVER_IMAGE, null)=="1"){
    previous_cover_image="1"
    binding.imgCrossTV.visibility=View.GONE
        }
        else{
    Glide.with(mActivity).load(AppPrefrences().readString(mActivity, COVER_IMAGE, null))
        .placeholder(R.drawable.placeholder_clout)
        .error(R.drawable.placeholder_clout)
        .into(binding.imageIV)
    previous_cover_image=AppPrefrences().readString(mActivity, COVER_IMAGE, null)
    binding.imgCrossTV.visibility=View.VISIBLE
        }

        binding.editNameET.setText(AppPrefrences().readString(mActivity, NAME, null))
        binding.editUserNameET.setText(AppPrefrences().readString(mActivity, USERNAME, null))
        binding.editBioET.setText(AppPrefrences().readString(mActivity, BIO, null))

        showCursorAtEnd(binding.editUserNameET)
        showCursorAtEnd(binding.editNameET)
        showCursorAtEnd(binding.editBioET)
    }


    fun showCursorAtEnd(mEditText: EditText) {
        mEditText.setSelection(mEditText.text.toString().length)
    }

    private fun performAddProfileImgClick() {
            onSelectImageClick()
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start(200)
    }


    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectCoverImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .crop(100F, 50F)
            .start(202)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK

            if (requestCode==200){
                val uri: Uri = data?.data!!
//            if (value.equals("profile")) {
                // Use Uri object instead of File to avoid storage permissions
//                binding.imgProfileIV.setImageURI(uri)
                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                mProfileBitmap = selectedImage
                Glide.with(mActivity).load(mProfileBitmap)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into(binding.imgProfileIV)
//                binding.imgProfileIV.setImageBitmap(mProfileBitmap)

//if(mProfileBitmap!=null){
//                binding.imgProfileIV.setImageBitmap(mProfileBitmap)}
//                if(mCoverBitmap!=null){
//                binding.imageIV.setImageBitmap(mCoverBitmap)}

//            }
        }
           else if (requestCode== 202) {
                previous_cover_image=""
                val uri1: Uri = data?.data!!
//                if (value.equals("cover")) {
//                    binding.imageIV.setImageURI(uri1)
                    val imageStream1 = contentResolver.openInputStream(uri1)
                    val selectedImage1 = BitmapFactory.decodeStream(imageStream1)
                    mCoverBitmap = selectedImage1
                Glide.with(mActivity).load(mCoverBitmap)
                    .placeholder(R.drawable.placeholder_clout)
                    .error(R.drawable.placeholder_clout)
                    .into(binding.imageIV)
                binding.imgCrossTV.visibility= View.VISIBLE
//                binding.imageIV.setImageBitmap(mCoverBitmap)
//                    value = ""
////                }
//
//
//                if(mProfileBitmap!=null){
//                binding.imgProfileIV.setImageBitmap(mProfileBitmap)}
//                if(mCoverBitmap!=null){
//                binding.imageIV.setImageBitmap(mCoverBitmap)}

            }
        }
        else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun isValidate(): Boolean {
        var flag = true
        when {
//            (mProfileBitmap== null) -> {
//                showAlertDialog(mActivity, getString(R.string.add_profile))
//                flag = false
//            }
//            (mCoverBitmap== null) -> {
//                showAlertDialog(mActivity, getString(R.string.add_cover))
//                flag = false
//            }
            binding.editNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_name))
                flag = false
            }
            binding.editUserNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_username))
                flag = false
            }
//            binding.editBioET.text.toString().trim { it <= ' ' } == "" -> {
//                showAlertDialog(mActivity, getString(R.string.enter_bio))
//                flag = false
//            }
        }
        return flag
    }

    private fun executeEditProfileApi() {
//        binding.txtPostTV.isEnabled=false
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeEditProfileRequest()
            } else {
                showToast(mActivity, getString(R.string.no_internet_connection))
            }
        }
    }

    private fun executeEditProfileRequest() {
        var profileMultipartBody: MultipartBody.Part? = null
        var coverPhotoMultipartBody: MultipartBody.Part? = null

        val user_id: RequestBody =
            getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val previous_cover_image: RequestBody =
            previous_cover_image!!.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val name: RequestBody = binding.editNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val username: RequestBody = binding.editUserNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val status: RequestBody = "0".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val bio: RequestBody = binding.editBioET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        if(mProfileBitmap!=null){
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mProfileBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
        profileMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "photo",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }}
            if(mCoverBitmap!=null){
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mCoverBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
                coverPhotoMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "cover_photo",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }

        showProgressDialog(mActivity)

        val headers: HashMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        Log.e(TAG, "tokenn: " + getAuthToken())
        val call = RetrofitInstance.appApi.edituserProfileRequest(
            headers,
            previous_cover_image,
            user_id,
            name,username,
            status,
            bio,
            profileMultipartBody,
            coverPhotoMultipartBody
        )

        call.enqueue(object : Callback<EditProfileModel> {
            override fun onResponse(
                call: Call<EditProfileModel>,
                response: Response<EditProfileModel>
            ) {
                dismissProgressDialog()
                val mAddPostModel = response.body()
                if (mAddPostModel!!.status == 1) {
                    AppPrefrences().writeString(mActivity, PROFILE_IMAGE, mAddPostModel.data.user_image)
                    AppPrefrences().writeString(mActivity, COVER_IMAGE, mAddPostModel.data.cover_image)
                    AppPrefrences().writeString(mActivity, NAME, mAddPostModel.data.name)
                    AppPrefrences().writeString(mActivity, USERNAME, mAddPostModel.data.username)
                    AppPrefrences().writeString(mActivity, BIO, mAddPostModel.data.bio)

                    showFinishAlertDialog(mActivity, mAddPostModel.message)
                } else if (mAddPostModel.status == 0) {
//                    binding.txtPostTV.isEnabled=true
                    showAlertDialog(mActivity, mAddPostModel.message)
                } else if (mAddPostModel.status == 3) {
//                    binding.txtPostTV.isEnabled=true
                    showAlertDialog(mActivity, mAddPostModel.message)
                } else {
//                    binding.txtPostTV.isEnabled=true
                    showAlertDialog(mActivity, getString(R.string.server_error))
                }
            }

            override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

        })
    }

    // - - To Show Alert Dialog
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
//                binding.txtPostTV.isEnabled=true
            alertDialog.dismiss()
//                val intent = Intent(mActivity, HomeActivity::class.java)
//                startActivity(intent)
            finish()
        }
        alertDialog.show()
    }
}