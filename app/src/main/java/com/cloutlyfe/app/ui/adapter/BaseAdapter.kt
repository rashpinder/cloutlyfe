package com.cloutlyfe.app.ui.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.mode.Feed
import kotlin.collections.ArrayList


abstract class BaseAdapter<T>(activity: Context?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    protected var list: MutableList<T>
    override fun getItemCount(): Int {
        return list.size
    }

    @Synchronized
    fun add(t: T) {
        list.add(t)
        sort()
        val position = list.indexOf(t)
        notifyItemInserted(position)
    }

    @Synchronized
    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    @Synchronized
    fun addPosition(t: T, position: Int) {
        var position = position
        list.add(position, t)
        sort()
        position = list.indexOf(t)
        notifyItemInserted(position)
    }

    @Synchronized
    fun removerPosition(position: Int) {
        val t = list[position]
        list.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeRemoved(position, list.size)
    }

    @Synchronized
    fun updatePosition(t: T, position: Int) {
        list[position] = t
        notifyItemChanged(position)
        notifyItemRangeChanged(position, list.size)
    }

    val data: ArrayList<T>
        get() = list as ArrayList<T>

    @Synchronized
    fun sort() {
    }

    //    protected Activity activity;
    //    protected LayoutInflater inflater;
    init {
//        this.activity = activity;
        list = ArrayList()
        //        this.inflater = activity.getLayoutInflater();
    }
}
