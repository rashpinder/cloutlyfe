package com.cloutlyfe.app.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.cloutlyfe.app.R

class CameraAnimation : AppCompatImageView {
    //    AnimationDrawable frameAnimation;
    constructor(context: Context) : super(context) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(context)
    }

    private fun initView(context: Context) {
        setImageResource(R.drawable.ic_add)
        //        frameAnimation = (AnimationDrawable) getDrawable();
    } //    public void start() {
    //        frameAnimation.start();
    //    }
    //
    //    public void stop() {
    //        frameAnimation.stop();
    //    }
}
