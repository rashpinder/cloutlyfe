package com.cloutlyfe.app.ui.view

import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.util.AttributeSet
import android.view.Surface
import android.view.TextureView
import android.view.TextureView.SurfaceTextureListener
import android.widget.RelativeLayout
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.ui.adapter.FeedAdapter
import com.danikula.videocache.HttpProxyCacheServer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.Exception

class VideoViews : RelativeLayout, SurfaceTextureListener {
    private var video: String? = null
    private var  value: Boolean? = true
    open var mediaPlayer: MediaPlayer? = null
    private var isPlaying = false
    private var onCompletionListener: OnCompletionListener? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }


//    fun setOnInfoListerner(): Boolean {
//        return isPlaying
//    }

    fun setOnCompletionListener(onCompletionListener: OnCompletionListener?) {
        this.onCompletionListener = onCompletionListener
    }

//    fun setVideo(video: Video?) {
//        this.video = video
//    }
    fun setVideo(video: String?) {
        this.video = video
    }

    override fun onViewRemoved(child: View?) {
        super.onViewRemoved(child)
        stop()
    }

     fun init(context: Context) {
        inflate(context, R.layout.layout_video, this)
        val textureView = findViewById<TextureView>(R.id.surfaceVie)
        textureView.surfaceTextureListener = this
    }
    fun reset(){
        mediaPlayer?.reset()
    }

    override fun onSurfaceTextureAvailable(surfaceTexture: SurfaceTexture, i: Int, i1: Int) {
        try {
            mediaPlayer = MediaPlayer()
            mediaPlayer?.setSurface(Surface(surfaceTexture))
            if(!video.isNullOrEmpty()){
                try {
//                    CoroutineScope(Dispatchers.IO).launch {
                        val proxy: HttpProxyCacheServer = App.getProxy(context)
                        val proxyUrl = proxy.getProxyUrl(video)
                        mediaPlayer?.setDataSource(proxyUrl)
//                    }

                }
                catch (e:Exception){
//                    CoroutineScope(Dispatchers.IO).launch {
                        val proxy: HttpProxyCacheServer = App.getProxy(context)
                        val proxyUrl = proxy.getProxyUrl(video)
                        mediaPlayer?.setDataSource(proxyUrl)
//                    }
                }
           }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onSurfaceTextureSizeChanged(surfaceTexture: SurfaceTexture, i: Int, i1: Int) {}
    override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean {
        return false
    }

    override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {}

    fun play(onPreparedListener: OnPreparedListener) {
        if (isPlaying) return
        try {
            mediaPlayer?.setOnPreparedListener { mediaPlayerr ->
                mediaPlayerr.seekTo(0)
                mediaPlayerr.isLooping = true
                mediaPlayerr.start()
                onPreparedListener.onPrepared()
            }
            mediaPlayer?.prepare()
//            try {
//                mediaPlayer?.prepareAsync()
//            }
//            catch (e:Exception){
//                mediaPlayer?.setOnPreparedListener { mediaPlayerr ->
//                    mediaPlayerr.seekTo(0)
//                    mediaPlayerr.isLooping = true
//                    mediaPlayerr.start()
//                    onPreparedListener.onPrepared()
//            }
//                mediaPlayer?.prepareAsync()
//            }

//            Handler(Looper.getMainLooper()).postDelayed(500) {
//                try{
//
//                }
//                catch (e:IllegalStateException){
//
//                }
//
//            }


        } catch (e: Exception) {
            Log.e(TAG, "play: "+e.printStackTrace())
//            mediaPlayer?.setOnPreparedListener { mediaPlayerr ->
//                mediaPlayerr.seekTo(0)
//                mediaPlayerr.isLooping = true
//                mediaPlayerr.start()
////                onPreparedListener.onPrepared()
//            }
//            if (mediaPlayer==null){
//            mediaPlayer?.prepare()}
//
//            mediaPlayer!!.prepare()
//            mediaPlayer!!.prepareAsync()
//            isPlaying = false
//            mediaPlayer?.stop()
//            mediaPlayer?.release()
//            mediaPlayer?.start()
//            mediaPlayer!!.prepareAsync()
//            onPreparedListener.onPrepared()
//            e.printStackTrace()
        }
    }

    fun stopPlayBack(){
//        if (mediaPlayer!=null){
            Log.e("TAG", "onScrolled: "+"stopplayback called" )
//            mediaPlayer!!.stop()
//            mediaPlayer!!.prepare()
//    }
    }

    fun stop() {
        if (!isPlaying) return
        isPlaying = false
        if(mediaPlayer!=null){
            try{
        mediaPlayer?.pause()
        mediaPlayer?.stop()
                mediaPlayer=null
            }
        catch (e:IllegalStateException){
            mediaPlayer=null
        }
    }
    }


    fun release(){
        try{
        mediaPlayer?.release()
            mediaPlayer=null
        }
        catch(e:NullPointerException){
            mediaPlayer=null
        }
    }


    interface OnCompletionListener {
        fun onCompletion()
    }

    public interface OnPreparedListener {
        fun onPrepared()
    }


    fun mute() {
        try{
        if(mediaPlayer!=null){
//            SOUND=false
            mediaPlayer?.setVolume(0f, 0f)}}
        catch (e:IllegalStateException){
            Log.e("TAG", "onScrolled: "+e.printStackTrace())
        }
    }

    fun unmute() {
        try{
        if(mediaPlayer!=null){
//            SOUND=true
            mediaPlayer?.setVolume(1f, 1f)}}
        catch (e:IllegalStateException){
            Log.e("TAG", "onScrolled: "+e.printStackTrace())
        }

    }

    private fun setVolume(amount: Int) {
        val max = 100
        val numerator: Double = if (max - amount > 0) Math.log((max - amount).toDouble()) else 0.0
        val volume = (1 - numerator / Math.log(max.toDouble())).toFloat()
        if(mediaPlayer!=null){
            mediaPlayer!!.setVolume(volume, volume)}
    }
}
