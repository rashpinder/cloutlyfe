package com.cloutlyfe.app.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R


class MyCustomPagerAdapter(var mActivity: Context, var images: ArrayList<String>): PagerAdapter() {
    //  var layoutInflater: LayoutInflater? = null

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): View {
        Log.e("PagerAdapter", "instantiateItem: "+images, )

        val layoutInflater = LayoutInflater.from(mActivity)
        val itemView: View = layoutInflater.inflate(R.layout.slide_images_item, container, false)
        val imageView: ImageView = itemView.findViewById(R.id.ivInfo)
        Glide.with(mActivity)
            .load(images.get(position))
            .placeholder(R.drawable.ic_post_ph)
            .error(R.drawable.ic_post_ph)
            .into(imageView)
//           imageView.setImageResource(images.get(position))
        container.addView(itemView)


        //listening to image click
        imageView.setOnClickListener(View.OnClickListener {
//            Toast.makeText(
//                mActivity,
//                "you clicked image " + (position + 1),
//                Toast.LENGTH_LONG
//            ).show()
        })
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
//        super.destroyItem(container, position, `object`)
        container.removeView(`object` as LinearLayout)
    }
}
