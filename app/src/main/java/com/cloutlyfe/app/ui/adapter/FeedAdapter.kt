package com.cloutlyfe.app.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.CommentsActivity
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.*
import com.cloutlyfe.app.mode.Feed
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.ui.view.CameraAnimation
import com.cloutlyfe.app.ui.view.VideoViews
import com.cloutlyfe.app.ui.view.VideoViews.OnPreparedListener
import com.cloutlyfe.app.utils.Constants
import com.google.gson.Gson
import com.hoanganhtuan95ptit.autoplayvideorecyclerview.VideoHolder
import de.hdodenhof.circleimageview.CircleImageView
import me.relex.circleindicator.CircleIndicator
import pl.droidsonroids.gif.GifImageView
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by HoangAnhTuan on 1/21/2018.
 */
class FeedAdapter(
    var context: Context,
    var mShareClickListener: ShareInterface,
    var mLikeClickListener: LikeunlikeInterface,
    var mSaveClickListener: SaveUnsaveInterface,
    var mOptionsClickListener: HomeMenuClickListenerInterface,
    var mVolumeClickListener: VolumeInterface,
    var imageList: ArrayList<HomeData>,
    var mLoadMoreScrollListner: LoadMoreScrollListner,
    var mCallHomeApiInterface: CallHomeApiInterface?,
    var mApiStatus: String?,
    var mCurrentDate: String?
) : BaseAdapter<Feed?>(
    context
) {
    var myCustomPagerAdapter: MyCustomPagerAdapter? = null
    var feed: String? = null
    var loggedInUser: Boolean? = true

    override fun getItemViewType(position: Int): Int {
        Log.e("Position", "getItemViewType: " + position)
        var listmags: ArrayList<String>? = null
        if( !imageList.get(position).image.isNullOrEmpty()){
        if (imageList.get(position).image!!.size != 0) {
            listmags = imageList.get(position).image
            feed = listmags!![0]
        }
            if (feed!!.contains(".png") || feed!!.contains(".jpg")||feed!!.contains(".jpeg") || feed!!.contains(".JPEG") || feed!!.contains(".PNG") || feed!!.contains(".GIF") || feed!!.contains("image") || feed!!.contains(".gif")) {
                return PHOTO_M1
            } else if (feed!!.contains(".mp4") ||  feed!!.contains(".MP4") ||feed!!.contains("video")|| feed!!.contains("3gp")) {
                return VIDEO_M1
            } else {
                return CAUGHT_UP
            }
        }
        else{
            return position
        }


    }

//    override fun onViewRecycled(holder: MediaViewHolder) {
//        super.onViewRecycled(holder)
//        val videoItem = mediaItems.getOrNull(holder.bindingAdapterPosition)
//        holder.releasePlayer(videoItem)
//    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun onBindPhoto11Holder(holder: Photo11Holder, feed: ArrayList<String>, position: Int) {
        var mModel = imageList[position]

        if ((imageList[position].isLike == 0)) {
            holder.likeIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_un_fav))
        } else if ((imageList[position].isLike == 1)) {
            holder.likeIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_fav))
        }
if(imageList[position].user_id==AppPrefrences().readString(context,Constants.ID,null)){
    holder.optionsIV.visibility=View.GONE
}
        else{
    if(imageList[position].is_report=="1"){
        holder.optionsIV.visibility=View.GONE
    }
    else{
        holder.optionsIV.visibility=View.VISIBLE
    }
        }


//        if(imageList[position].is_already_report){
//         holder.optionsIV.visibility=View.GONE
//        }
//        else{
//            holder.optionsIV.visibility=View.VISIBLE
//        }

        if ((imageList.get(position).isSave == 0)) {
            holder.saveIV.setImageResource(R.drawable.ic_unsave)
        } else if ((imageList.get(position).isSave == 1)) {
            holder.saveIV.setImageResource(R.drawable.ic_save)
        }
        holder.likeTV.text = imageList.get(position).likeCount.toString()

        holder.txtCaptionTV.text = imageList.get(position).description

        if (!imageList.get(position).commentCount.equals(null)) {
            holder.commentTV.text = imageList.get(position).commentCount
        } else {
            holder.commentTV.text = "0"
        }
        if(imageList[position].user_id==AppPrefrences().readString(context,Constants.ID,null)){
            Glide.with(context)
                .load(AppPrefrences().readString(context,Constants.PROFILE_IMAGE,null))
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(holder.profileIV)
        }
        else{
            Glide.with(context)
                .load(imageList.get(position).photo)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(holder.profileIV)
        }

        if(mModel.username=="N/A" || mModel.username==""|| mModel.username==null){
            holder.nameTV.text = imageList.get(position).name
        }
        else{
            holder.nameTV.text = imageList.get(position).username}

        holder.locationTV.text = imageList.get(position).location
        myCustomPagerAdapter = MyCustomPagerAdapter(context, feed)
        holder.viewPager.adapter = myCustomPagerAdapter
        if (feed.size == 1) {
            holder.indicator.visibility = View.GONE
        } else {
            holder.indicator.visibility = View.VISIBLE
            holder.indicator.setViewPager(holder.viewPager)
        }

        var padding = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._14sdp)
        var height = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._340sdp)
        if (position == 0) {
            holder.parentLayout.setPadding(padding, 100, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        } else {
//        if(position != 0){
            holder.parentLayout.setPadding(padding, 0, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        }
        if (position == imageList.size - 1) {
            var bottom = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._26sdp)
            val layoutParams = (holder.bottomLayout.layoutParams as? ViewGroup.MarginLayoutParams)
            layoutParams?.bottomMargin = bottom
            holder.bottomLayout.layoutParams = layoutParams
        } else {
            var bottom = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._26sdp)
            val layoutParams = (holder.bottomLayout.layoutParams as? ViewGroup.MarginLayoutParams)
            layoutParams?.bottomMargin = bottom
            holder.bottomLayout.layoutParams = layoutParams
        }

        holder.commentsLL.setOnClickListener(View.OnClickListener {
            val i = Intent(context, CommentsActivity::class.java)
            val gson = Gson()
            val mModell = gson.toJson(mModel)
            i.putExtra("homeData", mModell)
            i.putExtra("position", position)
            context.startActivity(i)
        })
        holder.shareIV.setOnClickListener(View.OnClickListener {
            mShareClickListener.onItemClickListner(holder.adapterPosition,mModel.post_id,mModel.user_id)
        })


        holder.likeLL.setOnClickListener(View.OnClickListener {
            mLikeClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id,
                holder.likeIV,
                holder.likeTV
            )
        })

        holder.saveIV.setOnClickListener(View.OnClickListener {
            mSaveClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id, holder.saveIV
            )
        })
        holder.optionsIV.setOnClickListener(View.OnClickListener {
            mOptionsClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id,
                imageList.get(position).user_id,
                loggedInUser!!
            )
        })
        holder.profileRL.setOnClickListener(View.OnClickListener {
            Log.e("TAG", "user_id: " + imageList[position].user_id)
            Log.e("TAG", "user_id: " + AppPrefrences().readString(context, Constants.ID, null))
            if (imageList[position].user_id == AppPrefrences().readString(
                    context,
                    Constants.ID,
                    null
                )
            ) {
                val i = Intent(context, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mModel)
                i.putExtra("homeData", mModell)
                i.putExtra("redirect_value","home")
                i.putExtra("value", "logged_in_user")
                context.startActivity(i)
//                val i = Intent(context, HomeActivity::class.java)
//                val gson = Gson()
//                val mModell = gson.toJson(mModel)
//                i.putExtra("homeData",mModell)
//                i.putExtra("value","logged_in_user")
//                context.startActivity(i)
            } else {
                val i = Intent(context, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                i.putExtra("redirect_value","home")
                i.putExtra("other_user_id", imageList.get(position).otherID)
                context.startActivity(i)
            }

        })

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun onBindCaught11Holder(holder: Caught11Holder, position: Int) {
        var padding = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._16sdp)
        var height = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._340sdp)
        if (position == 0) {
            holder.parentLayout.setPadding(padding, 100, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        } else {
//        if(position != 0){
            holder.parentLayout.setPadding(padding, 0, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        }
    }

    private fun onBindVideo11Holder(
        holder: DemoVideoHolder,
        feed: ArrayList<String>,
        position: Int,
        videoThumbnail: ArrayList<String>?
    ) {
        var mModel = imageList.get(position)

//        if (imageList.get(position).user_id.equals(
//                AppPrefrences().readString(
//                    context,
//                    Constants.ID,
//                    null
//                )
//            )
//        ) {
//            holder.optionsIV.visibility = View.VISIBLE
//            loggedInUser=true
//        } else {
//            holder.optionsIV.visibility = View.VISIBLE
//            loggedInUser=false
//        }
//        if(imageList[position].is_already_report){
//            holder.optionsIV.visibility=View.GONE
//        }
//        else{
//            holder.optionsIV.visibility=View.VISIBLE
//        }
        if(imageList[position].user_id==AppPrefrences().readString(context,Constants.ID,null)){
            holder.optionsIV.visibility=View.GONE
        }
        else{
            if(imageList[position].is_report=="1"){
                holder.optionsIV.visibility=View.GONE
            }
            else{
                holder.optionsIV.visibility=View.VISIBLE
            }
        }

//        if(imageList[position].is_report=="1"){
//            holder.optionsIV.visibility=View.GONE
//        }
//        else{
//            holder.optionsIV.visibility=View.VISIBLE
//        }
        if ((imageList.get(position).isLike == 0)) {
            holder.likeIV.setImageResource(R.drawable.ic_un_fav)
        } else if ((imageList.get(position).isLike == 1)) {
            holder.likeIV.setImageResource(R.drawable.ic_fav)
        }

        if ((imageList.get(position).isSave == 0)) {
            holder.saveIV.setImageResource(R.drawable.ic_unsave)
        } else if ((imageList.get(position).isSave == 1)) {
            holder.saveIV.setImageResource(R.drawable.ic_save)
        }
if(feed.size>0){
    holder.vvInfo.init(context)
        holder.vvInfo.setVideo(feed[0])}

//        if(SOUND.equals(true)) {
//            holder.imgVolumeIV.setImageResource(R.drawable.ic_volume_on)
//            holder.vvInfo.unmute()
//        } else{
//            holder.imgVolumeIV.setImageResource(R.drawable.ic_volume_mute)
//            holder.vvInfo.mute()
//        }
        holder.txtCaptionTV.text = imageList.get(position).description
        if(mModel.username=="N/A" || mModel.username==""|| mModel.username==null){
            holder.nameTV.text = imageList.get(position).name
        }
        else{
            holder.nameTV.text = imageList.get(position).username}


//        holder.nameTV.text = imageList.get(position).username
        holder.locationTV.text = imageList.get(position).location
        holder.likeTV.text = imageList.get(position).likeCount.toString()
        if (!imageList.get(position).commentCount.equals(null)) {
            holder.commentTV.text = imageList.get(position).commentCount
        } else {
            holder.commentTV.text = "0"
        }
        if(imageList[position].user_id==AppPrefrences().readString(context,Constants.ID,null)){
            Glide.with(context)
                .load(AppPrefrences().readString(context,Constants.PROFILE_IMAGE,null))
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(holder.profileIV)
        }
        else{
        Glide.with(context)
            .load(imageList.get(position).photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(holder.profileIV)}

        if (!videoThumbnail!!.equals("") && !videoThumbnail.equals(null) && videoThumbnail.size!=0) {
            Glide.with(context)
                .load(videoThumbnail[0])
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.ivInfo)
        }


        var padding = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._14sdp)
        var height = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._340sdp)
        if (position == 0) {
            holder.parentLayout.setPadding(padding, 100, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        } else {
//        if(position != 0){
            holder.parentLayout.setPadding(padding, 0, padding, 0)
            holder.parentLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.layoutParams
            params.height = height
            holder.parentLayout.layoutParams = params
        }
        if (position == imageList.size - 1) {
            var bottom = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._26sdp)
            val layoutParams = (holder.bottomLayout.layoutParams as? ViewGroup.MarginLayoutParams)
            layoutParams?.bottomMargin = bottom
            holder.bottomLayout.layoutParams = layoutParams
        } else {
            var bottom = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._26sdp)
            val layoutParams = (holder.bottomLayout.layoutParams as? ViewGroup.MarginLayoutParams)
            layoutParams?.bottomMargin = bottom
            holder.bottomLayout.layoutParams = layoutParams
        }
//        if (holder.layoutPosition == 0) {
//            holder.parentLayout.setPadding(padding, 100, padding, 0)
//        }
        holder.likeLL.setOnClickListener(View.OnClickListener {
            mLikeClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id,
                holder.likeIV, holder.likeTV
            )
        })

        holder.saveIV.setOnClickListener(View.OnClickListener {
            mSaveClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id,
                holder.saveIV
            )
        })

        holder.shareIV.setOnClickListener(View.OnClickListener {
            mShareClickListener.onItemClickListner(
                holder.adapterPosition,
                mModel.post_id,
                mModel.user_id
            )
        })

        holder.imgVolumeIV.setOnClickListener(View.OnClickListener {

//            var sound:Int=getVolume()

//            if(imageList.get(position).vol==true) {
//                holder.imgVolumeIV.setImageResource(R.drawable.ic_volume_on)
//                holder.vvInfo.unmute()
//            } else{
//                holder.imgVolumeIV.setImageResource(R.drawable.ic_volume_mute)
//                holder.vvInfo.mute()
//            }
//
            mVolumeClickListener.onItemClickListner(
                holder.adapterPosition, holder.imgVolumeIV, holder.vvInfo,
                imageList[position].vol
            )
        })

        holder.commentsLL.setOnClickListener(View.OnClickListener {
            val i = Intent(context, CommentsActivity::class.java)
            val gson = Gson()
            val mModell = gson.toJson(mModel)
            i.putExtra("homeData", mModell)
            context.startActivity(i)
//            holder.vvInfo.release()
        })
        holder.profileRL.setOnClickListener(View.OnClickListener {
            Log.e("TAG", "user_id: " + imageList[position].user_id)
            Log.e("TAG", "user_id: " + AppPrefrences().readString(context, Constants.ID, null))
            if (imageList[position].user_id == AppPrefrences().readString(
                    context,
                    Constants.ID,
                    null
                )
            ) {
                val i = Intent(context, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mModel)
                i.putExtra("homeData", mModell)
                i.putExtra("redirect_value","home")
                i.putExtra("value", "logged_in_user")
                context.startActivity(i)
            } else {
                val i = Intent(context, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                i.putExtra("redirect_value","home")
                i.putExtra("other_user_id", imageList.get(position).otherID)
                context.startActivity(i)
            }

        })

        holder.optionsIV.setOnClickListener(View.OnClickListener {
            mOptionsClickListener.onItemClickListner(
                holder.adapterPosition,
                imageList.get(position).post_id,
                imageList.get(position).user_id,
                loggedInUser!!
            )
        })
    }


    fun getVolume(): Int {
        val audio = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        val currentVolume = audio!!.getStreamVolume(AudioManager.STREAM_MUSIC)
        return currentVolume
    }


    private val screenWight: Int
        private get() {
            var displayMetrics = DisplayMetrics()
            displayMetrics = context.resources.displayMetrics
            return displayMetrics.widthPixels
        }

    open class DemoVideoHolder(itemView: View) : VideoHolder(itemView) {
        var vvInfo: VideoViews
        var profileIV: ImageView
        var locationTV: TextView
        var nameTV: TextView
        var ivInfo: ImageView
        var parentLayout: LinearLayout
        var likeLL: LinearLayout
        var bottomLayout: LinearLayout
        var optionsIV: ImageView
        var imgVolumeIV: ImageView
        var likeIV: ImageView
        var saveIV: ImageView
        var shareIV: ImageView
        var likeTV: TextView
        var commentTV: TextView
        var txtCaptionTV: TextView
        var commentsLL: LinearLayout
        var profileRL: RelativeLayout
        var ivCameraAnimation: CameraAnimation? = null
        override fun getVideoLayout(): View {
            return vvInfo
        }

        override fun playVideo() {
            ivInfo.visibility = View.VISIBLE
            vvInfo.play(object : OnPreparedListener {
                override fun onPrepared() {
                    Handler(Looper.getMainLooper()).postDelayed({
                        ivInfo.visibility = View.GONE
                    }, 300)

                }
            })
        }

        override fun stopVideo() {
            ivInfo.visibility = View.VISIBLE
            vvInfo.stop()
//            vvInfo.setVideo(null)

//            if (vvInfo.isFocusable){
//
//            }
//            else{
//                vvInfo.release()
//            }
            vvInfo.release()
        }

        init {
            vvInfo = itemView.findViewById(R.id.vvInfo)
            txtCaptionTV = itemView.findViewById(R.id.txtCaptionTV)
            commentsLL = itemView.findViewById(R.id.commentsLL)
            imgVolumeIV = itemView.findViewById(R.id.imgVolumeIV)
            likeTV = itemView.findViewById(R.id.likeTV)
            likeLL = itemView.findViewById(R.id.likeLL)
            likeIV = itemView.findViewById(R.id.likeIV)
            optionsIV = itemView.findViewById(R.id.optionsIV)
            profileIV = itemView.findViewById(R.id.profileIV)
            locationTV = itemView.findViewById(R.id.locationTV)
            ivInfo = itemView.findViewById(R.id.ivInfo)
            nameTV = itemView.findViewById(R.id.nameTV)
            parentLayout = itemView.findViewById(R.id.parentLayout)
            bottomLayout = itemView.findViewById(R.id.bottomLayout)
            commentTV = itemView.findViewById(R.id.commentTV)
            saveIV = itemView.findViewById(R.id.saveIV)
            shareIV = itemView.findViewById(R.id.shareIV)
            profileRL = itemView.findViewById(R.id.profileRL)
        }
    }

    override fun getItemCount(): Int {
        return imageList.size
    }




    open inner class PhotoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var viewPager: ViewPager
        var indicator: CircleIndicator
        var parentLayout: LinearLayout
        var commentsLL: LinearLayout
        var shareIV: ImageView
        var optionsIV: ImageView
        var profileRL: RelativeLayout
        var bottomLayout: LinearLayout
        var likeLL: LinearLayout
        var profileIV: CircleImageView
        var likeIV: ImageView
        var locationTV: TextView
        var nameTV: TextView
        var likeTV: TextView
        var imgVolumeIV: ImageView
        var saveIV: ImageView
        var commentTV: TextView
        var txtCaptionTV: TextView

        init {
            profileIV = itemView.findViewById(R.id.profileIV)
            txtCaptionTV = itemView.findViewById(R.id.txtCaptionTV)
            saveIV = itemView.findViewById(R.id.saveIV)
            locationTV = itemView.findViewById(R.id.locationTV)
            nameTV = itemView.findViewById(R.id.nameTV)
            viewPager = itemView.findViewById(R.id.viewPager)
            profileRL = itemView.findViewById(R.id.profileRL)
            shareIV = itemView.findViewById(R.id.shareIV)
            commentsLL = itemView.findViewById(R.id.commentsLL)
            indicator = itemView.findViewById(R.id.indicator)
            parentLayout = itemView.findViewById(R.id.parentLayout)
            optionsIV = itemView.findViewById(R.id.optionsIV)
            bottomLayout = itemView.findViewById(R.id.bottomLayout)
            likeTV = itemView.findViewById(R.id.likeTV)
            imgVolumeIV = itemView.findViewById(R.id.imgVolumeIV)
            likeLL = itemView.findViewById(R.id.likeLL)
            likeIV = itemView.findViewById(R.id.likeIV)
            commentTV = itemView.findViewById(R.id.commentTV)
        }
    }

    open inner class CaughtUpHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var gif: GifImageView
        var parentLayout: LinearLayout

        init {
            gif = itemView.findViewById(R.id.gif)
            parentLayout = itemView.findViewById(R.id.parentLayout)
        }
    }

    inner class Photo11Holder(itemView: View) : PhotoHolder(itemView)
    inner class Video11Holder(itemView: View) : DemoVideoHolder(itemView)
    inner class Caught11Holder(itemView: View) : CaughtUpHolder(itemView)

    companion object {
        private const val PHOTO_M1 = 0
        private const val VIDEO_M1 = 2
        private const val CAUGHT_UP = 3
        private var screenWight = 0
    }

    init {
        Companion.screenWight = screenWight
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            PHOTO_M1 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false)
                Photo11Holder(view)
            }
            VIDEO_M1 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
                Video11Holder(view)
            }
            else -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_caught_up, parent, false)
                Caught11Holder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        Log.e("Position", "BindViewHoldwer: " + position)
//        Log.e("Position", "BindViewHoldwer: " + mApiStatus)
        val tsLong = System.currentTimeMillis() / 1000
        mCurrentDate = tsLong.toString()

//        val offset: Int = TimeZone.getDefault().rawOffset + TimeZone.getDefault().dstSavings
//        mCurrentDate= (System.currentTimeMillis() - offset).toString()

        var listmags: ArrayList<String>? = null
        listmags = imageList.get(position).image

        var listthumbnails: ArrayList<String>? = null
        listthumbnails = imageList.get(position).thumbnail_image

        if (imageList.size % 6 == 0 && imageList.size - 1 == position) {
            imageList[position].image?.let { mLoadMoreScrollListner.onLoadMoreListner(it) }
            Log.e("Position", "BindViewHoldwer: " + mApiStatus)
        }

        else if(imageList.size-1 == position && mApiStatus=="2"){
            mCallHomeApiInterface?.onScroll("2", mCurrentDate!!,true)
            Log.e("Position", "BindViewHoldwer: " + mApiStatus)
        }

        else if(imageList.size-1 == position && mApiStatus=="3"){
//            mCallHomeApiInterface?.onScroll("3",imageList[position].created_at,true)
            mCallHomeApiInterface?.onScroll("3", mCurrentDate!!,true)
            Log.e("Position", "BindViewHoldwer: " + mApiStatus)
        }

        else if(imageList.size-1 == position && mApiStatus=="0"){
            Log.e("Position", "BindViewHoldwer: " + mApiStatus)
        }

        if (holder is Video11Holder) {
            listmags?.let { onBindVideo11Holder(holder, it, position, listthumbnails) }
        } else if (holder is Photo11Holder) {
            onBindPhoto11Holder(holder, listmags!!, position)
        } else if (holder is Caught11Holder) {
            onBindCaught11Holder(holder, position)
        }
        else{
            onBindCaught11Holder(holder as Caught11Holder, position)
        }
    }

    var mStatus:String?=""
    fun setData(mSt:String){
        mApiStatus=mSt

    }
}