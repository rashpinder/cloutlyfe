package com.cloutlyfe.app

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.activities.BaseActivity
import com.cloutlyfe.app.activities.ForgotPasswordActivity
import com.cloutlyfe.app.activities.SignUpActivity
import com.cloutlyfe.app.databinding.ActivityLoginBinding
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.twitterConfig.AlertDialogManager
import com.cloutlyfe.app.twitterConfig.ConnectionDetector
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.LoginPrefrences
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.viewmodel.*
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.json.JSONObject
import retrofit2.Call
import java.net.URL
import java.util.*


class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding
    protected val REQUEST_CHECK_SETTINGS = 0x1
    private var checkUncheck: Int = 0
    var deviceTokenUser: String = ""
    var mCallbackManager: CallbackManager? = null
    protected var mGoogleApiClient: GoogleApiClient? = null
    lateinit var loginViewModel: LoginViewModel
    lateinit var googleLoginViewModel: GoogleLoginViewModel
    lateinit var twitterLoginViewModel: TwitterLoginModel
    lateinit var fbLoginViewModel: FacebookLoginViewModel
    var google_email: String? = null
    var google_id: String? = null
    var google_name: String? = null
    private lateinit var twitterAuthClient: TwitterAuthClient

    var mGoogleSignInClient: GoogleSignInClient? = null
    var GoogleProfilePicture = ""
    protected val GOOGLE_SIGN_IN = 12324
    private var mFirebaseAuth: FirebaseAuth? = null
    var fbId: String? = null
    var fbFirstName: String? = null
    var fbLastName: String? = null
    var gFirstName: String? = null
    var gLastName: String? = null
    var fbEmail: String? = ""
    var value: Boolean = true
    private var fbProfilePicture: URL? = null
    var fbSocialUserserName: String? = null
    var FacebookProfilePicture = ""

    // Progress dialog
    var pDialog: ProgressDialog? = null

    // Twitter
    private val twitter: Twitter? = null
//    private val requestToken: RequestToken? = null

    // Shared Preferences
    private val mSharedPreferences: SharedPreferences? = null

    // Internet Connection detector
    private val cd: ConnectionDetector? = null

    // Alert Dialog Manager
    var alert = AlertDialogManager()
    var authClient: TwitterAuthClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                    resources.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)
                )
            )
            .debug(true)
            .build()
        Twitter.initialize(config)
        twitterAuthClient = TwitterAuthClient()

//        twitterInitialisation()
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()
        getDeviceTokenUser()
        init()
        if (LoginPrefrences().readString(mActivity, Constants.REMEMBER_ME, null).equals("true")) {
            binding.editEmailET.setText(
                LoginPrefrences().readString(
                    mActivity,
                    Constants.EMAIL,
                    null
                )
            )
            binding.editPasswordET.setText(
                LoginPrefrences().readString(
                    mActivity,
                    Constants.PASSWORD,
                    null
                )
            )
            LoginPrefrences().writeString(mActivity, Constants.REMEMBER_ME, "true")
            binding.imgRememberMeIV.setBackgroundResource(R.drawable.ic_check)
        } else {
            binding.editEmailET.setText("")
            binding.editPasswordET.setText("")
            LoginPrefrences().writeString(mActivity, Constants.REMEMBER_ME, "false")
            binding.imgRememberMeIV.setBackgroundResource(R.drawable.ic_uncheck)
        }

        TwitterCore.getInstance().getSessionManager().clearActiveSession()
        authClient = TwitterAuthClient()

        performClicks()
    }

//    private fun loginWithTwitter() {
//        if (!isNetworkAvailable(mActivity)) {
//            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
//        } else {
//            loginWithTwitterApi()
//        }
//    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        loginViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        googleLoginViewModel =
            ViewModelProvider(this, factory).get(GoogleLoginViewModel::class.java)
        fbLoginViewModel =
            ViewModelProvider(this, factory).get(FacebookLoginViewModel::class.java)
        twitterLoginViewModel =
            ViewModelProvider(this, factory).get(TwitterLoginModel::class.java)
    }

    private fun twitterInitialisation() {
        //twitter
        //This code must be entering before the setContentView to make the twitter login work...
        val config = TwitterConfig.Builder(this)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(
                TwitterAuthConfig(
                    resources.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                    resources.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)
                )
            )
            .debug(true)
            .build()
        Twitter.initialize(config)
        twitterAuthClient = TwitterAuthClient()
    }

    private fun twitterLogin() {
        val twitterSession = TwitterCore.getInstance().sessionManager.activeSession
        if (twitterSession == null) {
            twitterAuthClient.authorize(mActivity, object : Callback<TwitterSession?>() {

                override fun success(result: Result<TwitterSession?>) {
                    val twitterSessionData: TwitterSession? = result.data
                    getTwitterData(twitterSessionData)
                }

                override fun failure(exception: TwitterException?) {
                    if (exception.toString().contains("Authorize failed")) {

                    } else {
//                        showToast(
//                            this@LoginActivity,
//                            "For Twitter login , you must have Twitter application installed on your device !! "
//                        )
                    }

                    Log.e("TAG", "failure: ", exception)

                }
            })
        } else {
            getTwitterData(twitterSession)
        }
    }

    private fun getTwitterData(twitterSession: TwitterSession?) {
        val twitterApiClient = TwitterApiClient(twitterSession)
        val getUserCall: Call<User> =
            twitterApiClient.accountService.verifyCredentials(true, false, true)
        getUserCall.enqueue(object : Callback<User?>() {
            override fun failure(exception: TwitterException) {
                Log.e("Twitter", "Failed to get user data " + exception.message)
            }

            override fun success(result: Result<User?>?) {

                var firstName: String
                var lastName: String
                val user: User? = result?.data
                val twitter_id = user?.idStr.toString()
                val email = user?.email ?: ""
                val userImg = user?.profileImageUrl
                try {
                    firstName = user?.name?.split(" ")?.get(0).toString()
                    lastName = user?.name?.split(" ")?.get(1).toString()
                } catch (e: java.lang.Exception) {
                    firstName = user?.name.toString()
                    lastName = ""
                }
                executeTwitterApi(firstName,lastName, twitter_id,email, userImg.toString())
            }
        })

    }


    private fun executeTwitterApi(firstName: String,lastName: String, twitter_id:String,email:String,userImg:String) {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            executeTwitterLoginApi(firstName,lastName, twitter_id,email,userImg)
        }
    }

    private fun executeTwitterLoginApi(firstName: String,lastName: String, twitter_id:String,email:String,userImg:String) {
        var name = firstName + " " + lastName
        var email = email
        val twitter_id = twitter_id
        val deviceToken = deviceTokenUser
        val deviceType = "2"
        val user_image= userImg
        val body = RequestBodies.TwitterLoginBody(
            name,
            email!!, twitter_id!!, deviceToken, deviceType,user_image
        )

        twitterLoginViewModel.twitterLoginRequest(mActivity, body)
        twitterLoginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { loginResponse ->
                            if (loginResponse.status == 1 || loginResponse.status == 200) {
                                showToast(mActivity, loginResponse.message)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISLOGIN, true)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISSIMPLELOGIN, false)
                                LoginPrefrences().writeString(
                                    mActivity, Constants.EMAIL,
                                    binding.editEmailET.text.toString()
                                )
                                LoginPrefrences().writeString(
                                    mActivity, Constants.PASSWORD,
                                    binding.editPasswordET.text.toString()
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                CloutNewPrefernces.writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.NAME,
                                    loginResponse.data.name
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.USERNAME,
                                    loginResponse.data.username
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BIO,
                                    loginResponse.data.bio
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BLOCK_USER,
                                    loginResponse.data.block_user
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.CHANGE_STATUS,
                                    loginResponse.data.changeStatus
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COUNTRY,
                                    loginResponse.data.country
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.VERIFIED,
                                    loginResponse.data.verified
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.FB_ID,
                                    loginResponse.data.facebook_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.EMAIL,
                                    loginResponse.data.email
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.AUTHTOKEN,
                                    loginResponse.data.auth_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.google_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.twitter_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PASSWORD,
                                    loginResponse.data.password
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETOKEN,
                                    loginResponse.data.device_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETYPE,
                                    loginResponse.data.device_type
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PHONE,
                                    loginResponse.data.mobile_number
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COVER_IMAGE,
                                    loginResponse.data.cover_image
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PROFILE_IMAGE,
                                    loginResponse.data.user_image
                                )
                                Intent(
                                    mActivity,
                                    HomeActivity::class.java
                                ).also { startActivity(it) }
                                finish()
                            } else {
                                dismissProgressDialogg()
                                showAlertDialog(mActivity, loginResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialogg()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialogg(mActivity)
                    }
                }
            }
        })
    }



    //Device token
    fun getDeviceTokenUser() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful) {
                deviceTokenUser = it.result!!
                Log.e("CLOUTLYFE", "Device token ::" + deviceTokenUser)
            } else {
                Log.e("CLOUTLYFE", "Device token fetch issue")
                return@OnCompleteListener
            }
        })

    }


    fun performClicks() {
        binding.imgEyeIV.setOnClickListener {
            clickOnshowHidePwd()
        }
        binding.txtSignUpTV.setOnClickListener {
            performSignUpClick()
        }
        binding.txtLogInTV.setOnClickListener {
            performLogInClick()
        }
        binding.rememberMeRL.setOnClickListener {
            performRememberMeClick()
        }

        binding.txtForgotPassTV.setOnClickListener {
            performForgetPasswordClick()
        }
        binding.imgGoogleIV.setOnClickListener {
            performGoogleClick()
        }
        binding.imgFbIV.setOnClickListener {
            performFbClick()
        }

        binding.imgTwitterIV.setOnClickListener {
            performTwitterClick()
        }
    }

    private fun performTwitterClick() {
        twitterLogin()
    }

    private fun performFbClick() {
        preventMultipleClick()
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {

//            FacebookSdk.sdkInitialize()
//            FacebookSdk.sdkInitialize(applicationContext)
//            try{
//            LoginManager.getInstance().logOut()}
//            catch(e:Exception){
//                e.printStackTrace()
//            }


//            AppEventsLogger.activateApp(application)
            mCallbackManager = CallbackManager.Factory.create()
            loginWithFacebook()
        }
    }


    private fun loginWithFacebook() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance()
                .logInWithReadPermissions(this@LoginActivity,Arrays.asList("public_profile", "email"))
            LoginManager.getInstance()
                .registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Log.d("onSuccess: ", loginResult.accessToken.token)
                        getFacebookData(loginResult)
                    }

                    override fun onCancel() {
                        Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                    }

                    override fun onError(error: FacebookException) {
                        Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                })
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }

    private fun getFacebookData(loginResult: LoginResult) {
        showProgressDialogg(mActivity)
        val graphRequest = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, response ->
            dismissProgressDialogg()
            try {
                if (`object`!!.has("id")) {
                    fbId = `object`.getString("id")
                    Log.e("LoginActivity", "id$fbId")
                }
                //check permission first userName
                if (`object`.has("first_name")) {
                    fbFirstName = `object`.getString("first_name")
                    Log.e("LoginActivity", "first_name$fbFirstName")
                }
                //check permisson last userName
                if (`object`.has("last_name")) {
                    fbLastName = `object`.getString("last_name")
                    Log.e("LoginActivity", "last_name$fbLastName")
                }
                //check permisson email
                if (`object`.has("email")) {
                    fbEmail = `object`.getString("email")
                    Log.e("LoginActivity", "email$fbEmail")
                }
                else{
                    Log.e("LoginActivity", "email$fbEmail")
                }
                val jsonObject = JSONObject(`object`.getString("picture"))
                if (jsonObject != null) {
                    val dataObject = jsonObject.getJSONObject("data")
                    Log.e("Loginactivity", "json oject get picture$dataObject")
                    fbProfilePicture =
                        URL("https://graph.facebook.com/$fbId/picture?width=500&height=500")
                    Log.e("LoginActivity", "json object=>$`object`")
                }
                FacebookProfilePicture = if (fbProfilePicture != null) {
                    fbProfilePicture.toString()
                } else {
                    ""
                }
                fbSocialUserserName = "$fbFirstName $fbLastName"

                if(!fbEmail.equals("")){
                    executeLoginWithFbApi()
                } else{
                    showEmailAlertDialog(mActivity)
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        val bundle = Bundle()
        Log.e("LoginActivity", "bundle set")
        bundle.putString("fields", "id, first_name, last_name,email,picture")
        graphRequest.parameters = bundle
        graphRequest.executeAsync()
    }


    fun showEmailAlertDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.email_alert_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val editEmailET = alertDialog.findViewById<TextView>(R.id.editEmailET)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)

        fbEmail=editEmailET.getText().toString().trim()

        btnDismiss.setOnClickListener {
            if(editEmailET.getText().toString().trim()!=null && editEmailET.getText().toString().trim()!=""){
                fbEmail=editEmailET.getText().toString().trim()
                alertDialog.dismiss()
                executeLoginWithFbApi()}
            else{
                showToast(mActivity,"Please enter email address")
            }
        }
        alertDialog.show()
    }

    private fun executeLoginWithFbApi() {
        var name = fbSocialUserserName
        var email = fbEmail
        Log.e(TAG, "executeLoginWithFbApi: "+fbEmail )
        val facebook_id = fbId
        val deviceToken = deviceTokenUser
        val deviceType = "2"
        val user_image = FacebookProfilePicture
        val body = RequestBodies.FacebookLoginBody(
            name!!,
            email!!, facebook_id!!, deviceToken, deviceType, user_image
        )

        Log.e(TAG, "executeLoginWithFbApi: "+ name!!+
            email!!+  "fb_id:"+facebook_id!!+ "dev_token:"+deviceToken+ "dev_tyep:"+deviceType+"user_img:"+ user_image)

        fbLoginViewModel.facebookLoginRequest(mActivity, body)
        fbLoginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { loginResponse ->
                            if (loginResponse.status == 1 || loginResponse.status == 200) {
                                showToast(mActivity, loginResponse.message)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISLOGIN, true)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISSIMPLELOGIN, false)
                                LoginPrefrences().writeString(
                                    mActivity, Constants.EMAIL,
                                    binding.editEmailET.text.toString()
                                )
                                LoginPrefrences().writeString(
                                    mActivity, Constants.PASSWORD,
                                    binding.editPasswordET.text.toString()
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                CloutNewPrefernces.writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.NAME,
                                    loginResponse.data.name
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.USERNAME,
                                    loginResponse.data.username
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BIO,
                                    loginResponse.data.bio
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BLOCK_USER,
                                    loginResponse.data.block_user
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.CHANGE_STATUS,
                                    loginResponse.data.changeStatus
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COUNTRY,
                                    loginResponse.data.country
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.VERIFIED,
                                    loginResponse.data.verified
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.FB_ID,
                                    loginResponse.data.facebook_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.EMAIL,
                                    loginResponse.data.email
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.AUTHTOKEN,
                                    loginResponse.data.auth_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.google_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.twitter_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PASSWORD,
                                    loginResponse.data.password
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETOKEN,
                                    loginResponse.data.device_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETYPE,
                                    loginResponse.data.device_type
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PHONE,
                                    loginResponse.data.mobile_number
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COVER_IMAGE,
                                    loginResponse.data.cover_image
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PROFILE_IMAGE,
                                    loginResponse.data.user_image
                                )
                                Intent(
                                    mActivity,
                                    HomeActivity::class.java
                                ).also { startActivity(it) }
                                finish()
                            } else {
                                dismissProgressDialogg()
                                showAlertDialog(mActivity, loginResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialogg()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialogg(mActivity)
                    }
                }
            }
        })
    }


    private fun performGoogleClick() {
        preventMultipleClick()
        val signInIntent: Intent = mGoogleSignInClient!!.getSignInIntent()
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            google_email = account?.email
            google_id = account?.id
            google_name = account?.displayName

            val name = account?.displayName

            val idx = name?.lastIndexOf(' ')
            if (idx == -1) {
//                Toast.makeText(mActivity, "Invalid full name", Toast.LENGTH_LONG).show()
                return
            }

            gFirstName = idx?.let { name?.substring(0, it) }
            gLastName = name?.substring(idx?.plus(1)!!)
            Log.e("SPLITED NAME", gFirstName + " - " + gLastName)

            GoogleProfilePicture = if (account?.photoUrl != null) {
                account.photoUrl.toString()
                //                    mBase64GoogleImage=convertUrlToBase64(GoogleProfilePicture);
            } else {
                ""
                //                    mBase64GoogleImage="";
            }
            executeGoogleApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
            Toast.makeText(mActivity, "Failed", Toast.LENGTH_LONG)
        }
    }

    val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            if (mCallbackManager != null) {
                // Pass the activity result back to the Facebook SDK
                mCallbackManager!!.onActivityResult(1111, result.resultCode , result.data )
            }
            // Handle the Intent
            //do stuff here
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        mCallbackManager!!.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        Log.e(TAG, "onActivityResult: "+"dhcvhjcvhc" )
        twitterAuthClient.onActivityResult(requestCode, resultCode, data)

        if (mCallbackManager != null) {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager!!.onActivityResult(requestCode, resultCode, data)
        }


        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val completedTask: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(completedTask)
            }
//            TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE ->
//                authClient!!.onActivityResult(requestCode, resultCode, data)
//        }

        }}


    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
        if (mGoogleApiClient != null) mGoogleApiClient!!.connect()
    }


    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
//        mGoogleApiClient!!.disconnect()
//        dismissProgressDialog()
    }

    override fun onRestart() {
        super.onRestart()
        Log.e(TAG, "onRestart")
    }

    private fun executeGoogleApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.no_internet_connection))
        } else {
            executeGoogleLoginApi()
        }
    }

    private fun executeGoogleLoginApi() {
        var name = gFirstName + " " + gLastName
        var email = google_email
        val google_id = google_id
        val deviceToken = deviceTokenUser
        val deviceType = "2"
        val user_image = GoogleProfilePicture
        val body = RequestBodies.GoogleLoginBody(
            name,
            email!!, google_id!!, deviceToken, deviceType,user_image
        )
        googleLoginViewModel.googleLoginRequest(mActivity, body)
        googleLoginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { loginResponse ->
                            if (loginResponse.status == 1 || loginResponse.status == 200) {
                                showToast(mActivity, loginResponse.message)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISLOGIN, true)
                                LoginPrefrences().writeString(
                                    mActivity, Constants.EMAIL,
                                    binding.editEmailET.text.toString()
                                )
                                LoginPrefrences().writeString(
                                    mActivity, Constants.PASSWORD,
                                    binding.editPasswordET.text.toString()
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                CloutNewPrefernces.writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.NAME,
                                    loginResponse.data.name
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.USERNAME,
                                    loginResponse.data.username
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BIO,
                                    loginResponse.data.bio
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BLOCK_USER,
                                    loginResponse.data.block_user
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.CHANGE_STATUS,
                                    loginResponse.data.changeStatus
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COUNTRY,
                                    loginResponse.data.country
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.VERIFIED,
                                    loginResponse.data.verified
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.FB_ID,
                                    loginResponse.data.facebook_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.EMAIL,
                                    loginResponse.data.email
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.AUTHTOKEN,
                                    loginResponse.data.auth_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.google_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.twitter_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PASSWORD,
                                    loginResponse.data.password
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETOKEN,
                                    loginResponse.data.device_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETYPE,
                                    loginResponse.data.device_type
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PHONE,
                                    loginResponse.data.mobile_number
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COVER_IMAGE,
                                    loginResponse.data.cover_image
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PROFILE_IMAGE,
                                    loginResponse.data.user_image
                                )
                                Intent(
                                    mActivity,
                                    HomeActivity::class.java
                                ).also { startActivity(it) }
                                finish()
                            } else {
                                dismissProgressDialogg()
                                showAlertDialog(mActivity, loginResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialogg()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialogg(mActivity)
                    }
                }
            }
        })
    }


    /*
    * Show Progress Dialog
    * */
    fun showProgressDialogg(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
//        if (progressDialog != null) progressDialog!!.show()
        if(mActivity!=null){
            progressDialog!!.show()}
    }
    /*
      * Hide Progress Dialog
      * */
    fun dismissProgressDialogg() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog?.dismiss()
        }
    }




    private fun performRememberMeClick() {
        if (checkUncheck == 0) {
            binding.imgRememberMeIV.setBackgroundResource(R.drawable.ic_check)
            checkUncheck++
            LoginPrefrences().writeString(mActivity, Constants.REMEMBER_ME, "true")
            LoginPrefrences().writeString(
                mActivity, Constants.EMAIL,
                binding.editEmailET.text.toString()
            )
            LoginPrefrences().writeString(
                mActivity, Constants.PASSWORD,
                binding.editPasswordET.text.toString()
            )
        } else {
            binding.imgRememberMeIV.setBackgroundResource(R.drawable.ic_uncheck)
            LoginPrefrences().writeString(mActivity, Constants.REMEMBER_ME, "false")
            LoginPrefrences().writeString(
                mActivity, Constants.EMAIL, ""
            )
            LoginPrefrences().writeString(
                mActivity, Constants.PASSWORD,
                ""
            )
            checkUncheck--
        }
    }

    private fun clickOnshowHidePwd() {
        if (binding.editPasswordET.getTransformationMethod()
                .equals(PasswordTransformationMethod.getInstance())
        ) {
            binding.imgEyeIV.setImageResource(R.drawable.ic_unhide_pwd)
            //Show Password
            binding.editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            binding.editPasswordET.setSelection(binding.editPasswordET.length())
        } else {
            binding.imgEyeIV.setImageResource(R.drawable.ic_hide_pwd)
            //Hide Password
            binding.editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
            binding.editPasswordET.setSelection(binding.editPasswordET.length())
        }
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            binding.editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_email))
                flag = false
            }

            !isValidEmaillId(binding.editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }

            binding.editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
        }
        return flag
    }

    private fun performForgetPasswordClick() {
        val i = Intent(mActivity, ForgotPasswordActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun performLogInClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.no_internet_connection))
            } else {
                executeLoginApi()
            }
    }

    private fun executeLoginApi() {
        var email = binding.editEmailET.text.toString().trim()
        val password = binding.editPasswordET.text.toString().trim()
        val deviceToken = deviceTokenUser
        val deviceType = "2"
        val body = RequestBodies.LoginBody(
            email,
            password, deviceToken, deviceType
        )

        loginViewModel.loginUser(mActivity, body)
        loginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { loginResponse ->
                            if (loginResponse.status == 1 || loginResponse.status == 200) {
                                showToast(mActivity, loginResponse.message)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISLOGIN, true)
                                AppPrefrences().writeBoolean(mActivity, Constants.ISSIMPLELOGIN, true)
                                LoginPrefrences().writeString(
                                    mActivity, Constants.EMAIL,
                                    binding.editEmailET.text.toString()
                                )
                                LoginPrefrences().writeString(
                                    mActivity, Constants.PASSWORD,
                                    binding.editPasswordET.text.toString()
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                CloutNewPrefernces.writeString(
                                    mActivity,
                                    Constants.ID,
                                    loginResponse.data.user_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.NAME,
                                    loginResponse.data.name
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.USERNAME,
                                    loginResponse.data.username
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BIO,
                                    loginResponse.data.bio
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.BLOCK_USER,
                                    loginResponse.data.block_user
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.CHANGE_STATUS,
                                    loginResponse.data.changeStatus
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COUNTRY,
                                    loginResponse.data.country
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.VERIFIED,
                                    loginResponse.data.verified
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.FB_ID,
                                    loginResponse.data.facebook_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.EMAIL,
                                    loginResponse.data.email
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.AUTHTOKEN,
                                    loginResponse.data.auth_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.google_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.GOOGLE_ID,
                                    loginResponse.data.twitter_id
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PASSWORD,
                                    loginResponse.data.password
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETOKEN,
                                    loginResponse.data.device_token
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.DEVICETYPE,
                                    loginResponse.data.device_type
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PHONE,
                                    loginResponse.data.mobile_number
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.COVER_IMAGE,
                                    loginResponse.data.cover_image
                                )
                                AppPrefrences().writeString(
                                    mActivity,
                                    Constants.PROFILE_IMAGE,
                                    loginResponse.data.user_image
                                )
                                Intent(
                                    mActivity,
                                    HomeActivity::class.java
                                ).also { startActivity(it) }
                                finish()
                            } else {
                                dismissProgressDialogg()
                                showAlertDialog(mActivity, loginResponse.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        dismissProgressDialogg()
                        response.message?.let { message ->
                            showAlertDialog(mActivity, response.message)
                        }
                    }

                    is Resource.Loading -> {
                        showProgressDialogg(mActivity)
                    }
                }
            }
        })
    }


    private fun performSignUpClick() {
        val i = Intent(mActivity, SignUpActivity::class.java)
        startActivity(i)
    }
}