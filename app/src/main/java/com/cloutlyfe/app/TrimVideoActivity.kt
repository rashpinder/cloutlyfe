package com.cloutlyfe.app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.lb.video_trimmer_library.interfaces.VideoTrimmingListener
import com.cloutlyfe.app.databinding.ActivityTrimVideoBinding
import java.io.File

class TrimVideoActivity : AppCompatActivity(), VideoTrimmingListener {
lateinit var binding: ActivityTrimVideoBinding
    var TAG="TrimmerActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrimVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
//
//        if(File(Utils.outputPath+"/TrimmedMusistVideo").exists())
//        {
//            FileUtils.deleteDirectory(File(Utils.outputPath+"/TrimmedMusistVideo"))
//            Log.e(TAG, "initComponents: file deleted  at" + Utils.outputPath+"/TrimmedMusistVideo")
//
//
//
//        }
//        else{
//            Log.e(TAG, "initComponents: file not available at")
//        }

//        if(File(Environment.DIRECTORY_MOVIES+ File.separator+"CompressedVideo").exists())
//        {
////            FileUtils.deleteDirectory(File(Environment.DIRECTORY_MOVIES+"/CompressedVideo"))
//            Log.e(TAG, "initComponents: file deleted  at" + Environment.DIRECTORY_MOVIES+ File.separator+"CompressedVideo")
//
//        }
//        else
//        {
//            Log.e(TAG, "initComponents: file not available at")
//
//        }

        val extraIntent = intent
        if (extraIntent != null) {
            val path: Uri = extraIntent.getParcelableExtra("EXTRA_VIDEO_PATH")!!
//            val path: Uri? = extraIntent?.getParcelableExtra(Constants.EXTRA_VIDEO_PATH)
//            val file = File(path.toString())
//            if (file.exists()) {
//                showToast(this,"exists")
//            }
//            else
//            {
//                showToast(this," not exists")
//
//            }


            binding.videoTrimmerView.setMaxDurationInMs (60000)

            binding.videoTrimmerView.setOnK4LVideoListener(this)
//            val fileName = "trimmedVideo_${System.currentTimeMillis()}.mp4"
            val fileName = "trimmedVideo.mp4"
//            val trimmedVideoFile = File(Utils.outputPath+"/TrimmedMusistVideo", fileName)
             val trimmedVideoFile = File(externalCacheDir, fileName)
            binding.videoTrimmerView.setDestinationFile(trimmedVideoFile)
            binding.videoTrimmerView.setVideoURI(path)
            binding.videoTrimmerView.setVideoInformationVisibility(true)

        } else {
//            showToast(this, "Something went wrong.")
        }
    }

    override fun onTrimStarted() {
        binding.trimmingProgressView.visibility = View.VISIBLE
    }

    override fun onFinishedTrimming(uri: Uri?) {
        binding.trimmingProgressView.visibility = View.GONE
        trimmedVideoUri=uri
        val returnIntent = Intent()
        returnIntent.putExtra("TRIMMED_VIDEO_URI",uri.toString())
        setResult(800, returnIntent)
        finish()
    }

    override fun onErrorWhileViewingVideo(what: Int, extra: Int) {
        binding.trimmingProgressView.visibility = View.GONE
        Toast.makeText(this@TrimVideoActivity, "error while previewing video", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onVideoPrepared() {

    }

companion object{
    var trimmedVideoUri:Uri?=null
}

}

