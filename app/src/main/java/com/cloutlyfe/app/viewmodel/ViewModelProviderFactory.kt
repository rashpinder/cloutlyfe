package com.cloutlyfe.app.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cloutlyfe.app.repository.AppRepository

class ViewModelProviderFactory(
    val app: Application,
    val appRepository: AppRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(SignUpViewModel::class.java)) {
            return SignUpViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(GoogleLoginViewModel::class.java)) {
            return GoogleLoginViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(FacebookLoginViewModel::class.java)) {
            return FacebookLoginViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(TwitterLoginModel::class.java)) {
            return TwitterLoginModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ChangePasswordViewModel::class.java)) {
            return ChangePasswordViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java)) {
            return ForgotPasswordViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(LogOutViewModel::class.java)) {
            return LogOutViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(GetReportReasonsViewModel::class.java)) {
            return GetReportReasonsViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(ReportViewModel::class.java)) {
            return ReportViewModel(app, appRepository) as T
        }
          if (modelClass.isAssignableFrom(LikeUnlikeViewModel::class.java)) {
            return LikeUnlikeViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {
            return EditProfileViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
            return NotificationViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(SaveUnsaveViewModel::class.java)) {
            return SaveUnsaveViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(LikeUnlikeCommentViewModel::class.java)) {
            return LikeUnlikeCommentViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(FollowUserViewmodel::class.java)) {
            return FollowUserViewmodel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(RemoveUserViewModel::class.java)) {
            return RemoveUserViewModel(app, appRepository) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }

}