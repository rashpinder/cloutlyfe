package com.cloutlyfe.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.SearchModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Constants.Companion.showAuthDismissDialog
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class SearchViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _resultResponse = MutableLiveData<Event<Resource<SearchModel>>>()
    val resultResponse:LiveData<Event<Resource<SearchModel>>> = _resultResponse


    fun searchData(authToken: String, body: RequestBodies.SearchHomeBody, mActivity: Activity) = viewModelScope.launch {
        search(authToken,body,mActivity)
    }

    private suspend fun search(authToken: String, body: RequestBodies.SearchHomeBody, mActivity: Activity
    ) {
        _resultResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.searchData(authToken,body)
                if (response.code()==200)
                {
                    _resultResponse.postValue(handleResponse(response))}
                else
                {
                    showAuthDismissDialog(mActivity,getApplication<App>().getString(R.string.authorization_failure))
                }

                Log.e("Check","iff"+response)
            } else {
                _resultResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<SearchModel>): Event<Resource<SearchModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}