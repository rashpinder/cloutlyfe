package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.CodeStatusModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException


class ChangePasswordViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {
    private val _changePassResponse = MutableLiveData<Event<Resource<CodeStatusModel>>>()
    val changePassResponse: LiveData<Event<Resource<CodeStatusModel>>> = _changePassResponse


    fun changePswd(body: RequestBodies.ChangePswdBody) = viewModelScope.launch {
        changePassword(body)
    }

    private suspend fun changePassword(body: RequestBodies.ChangePswdBody) {
        _changePassResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response =appRepository.changePasssword(body)
                if (response.code()!=500)
                {
                    _changePassResponse.postValue(handleResponse(response))
                }
                else
                {
                    _changePassResponse.postValue(Event(Resource.Error(getApplication<App>().getString(
                        R.string.server_error))))
                }
            } else {
                _changePassResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))
            }
        } catch (t: Throwable) {
            Log.e("ISSUE",""+t.message)
            when (t) {
                is IOException -> {
                    _changePassResponse.postValue(
                        Event(Resource.Error(
                            ""+t.message
                        ))

                    )
                }
                else -> {
                    _changePassResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<CodeStatusModel>): Event<Resource<CodeStatusModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}