package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.GetReportResonsModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class GetReportReasonsViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _reportResonsResponse = MutableLiveData<Event<Resource<GetReportResonsModel>>>()
    val reportReasonsResponse: LiveData<Event<Resource<GetReportResonsModel>>> = _reportResonsResponse


    fun getReportResons(body: RequestBodies.ReportReasonsBody) = viewModelScope.launch {
        getResons(body)
    }

    private suspend fun getResons(body: RequestBodies.ReportReasonsBody) {
        _reportResonsResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.getReportData(body)
                _reportResonsResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                _reportResonsResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _reportResonsResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _reportResonsResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<GetReportResonsModel>): Event<Resource<GetReportResonsModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}