package com.cloutlyfe.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.ProfileModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Constants.Companion.showAuthDismissDialog
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import kotlinx.coroutines.launch

import retrofit2.Response
import java.io.IOException

class ProfileViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _profileResponse = MutableLiveData<Event<Resource<ProfileModel>>>()
    val profileResponse:LiveData<Event<Resource<ProfileModel>>> = _profileResponse


    fun getProfileData(authToken: String,body: RequestBodies.ProfileBody, activity: Activity) = viewModelScope.launch {
        profileData(authToken,body,activity)
    }

    private suspend fun profileData(
        authToken: String,
        body: RequestBodies.ProfileBody,
        activity: Activity
    ) {
        _profileResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.profileData(authToken,body)
                Log.e("Check","iff1"+response.code())
                if (response.code()==200)
                {
                    _profileResponse.postValue(handleResponse(response))}
                else
                {
                    showAuthDismissDialog(activity,getApplication<App>().getString(R.string.authorization_failure))
                }

            } else {
                _profileResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _profileResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _profileResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<ProfileModel>): Event<Resource<ProfileModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}