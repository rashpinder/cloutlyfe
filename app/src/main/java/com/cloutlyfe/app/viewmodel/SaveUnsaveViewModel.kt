package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.SaveUnsaveModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class SaveUnsaveViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val saveUnsaveResponse = MutableLiveData<Event<Resource<SaveUnsaveModel>>>()
    val _save_unsaveResponse: LiveData<Event<Resource<SaveUnsaveModel>>> = saveUnsaveResponse


    fun saveUnsaveRequest(body: RequestBodies.SaveUnsaveBody) = viewModelScope.launch {
        saveUnsave(body)
    }

    private suspend fun saveUnsave(body: RequestBodies.SaveUnsaveBody) {
        saveUnsaveResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.saveUnsaveData(body)
                saveUnsaveResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                saveUnsaveResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    saveUnsaveResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    saveUnsaveResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<SaveUnsaveModel>): Event<Resource<SaveUnsaveModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}