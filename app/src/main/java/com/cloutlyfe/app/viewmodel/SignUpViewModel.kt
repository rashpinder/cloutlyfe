package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.SignUpModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class SignUpViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _signUpResponse = MutableLiveData<Event<Resource<SignUpModel>>>()
    val signUpResponse: LiveData<Event<Resource<SignUpModel>>> = _signUpResponse


    fun signUpUser(body: RequestBodies.SignUpBody) = viewModelScope.launch {
        signUp(body)
    }

    private suspend fun signUp(body: RequestBodies.SignUpBody) {
        _signUpResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<App>())) {
                val response = appRepository.signUpUser(body)
                _signUpResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                _signUpResponse.postValue(Event(Resource.Error(getApplication<App>().getString(
                    R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _signUpResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _signUpResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<SignUpModel>): Event<Resource<SignUpModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}