package com.cloutlyfe.app.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.cloutlyfe.app.repository.AppRepository

class HomeViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

//    private val _homeResponse = MutableLiveData<Event<Resource<HomeModel>>>()
//    val homeResponse:LiveData<Event<Resource<HomeModel>>> = _homeResponse
//
//
//    fun homeGetData(authToken: String,body: RequestBodies.HomeBody, activity: Activity) = viewModelScope.launch {
//        homeData(authToken,body,activity)
//    }
//
//    private suspend fun homeData(
//        authToken: String,
//        body: RequestBodies.HomeBody,
//        activity: Activity
//    ) {
//        _homeResponse.postValue(Event(Resource.Loading()))
//        try {
//            if (hasInternetConnection(getApplication<App>())) {
//                val response = appRepository.savedData(authToken,body)
//                Log.e("Check","iff1"+response.code())
//                if (response.code()==200)
//                {
//                    _homeResponse.postValue(handleResponse(response))
//                }
//                else
//                {
//                    showAuthDismissDialog(activity,getApplication<App>().getString(R.string.authorization_failure))
//                }
//
//            } else {
//                _homeResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))
//
//                Log.e("Check","else")
//            }
//        } catch (t: Throwable) {
//            when (t) {
//                is IOException -> {
//                    _homeResponse.postValue(
//                        Event(Resource.Error(
//                            getApplication<App>().getString(
//                                R.string.network_failure
//                            )
//                        ))
//                    )
//                }
//                else -> {
//                    _homeResponse.postValue(
//                        Event(Resource.Error(
//                            getApplication<App>().getString(
//                                R.string.conversion_error
//                            )
//                        ))
//                    )
//                }
//            }
//        }
//    }
//
//    private fun handleResponse(response: Response<HomeModel>): Event<Resource<HomeModel>>? {
//        if (response.isSuccessful) {
//            response.body()?.let { resultResponse ->
//                return Event(Resource.Success(resultResponse))
//            }
//        }
//        return Event(Resource.Error(response.message()))
//    }
}