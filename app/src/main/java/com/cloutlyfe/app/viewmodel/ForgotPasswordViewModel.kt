package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.StatusModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class ForgotPasswordViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _forgotPswdResponse = MutableLiveData<Event<Resource<StatusModel>>>()
    val forgotPswdResponse: LiveData<Event<Resource<StatusModel>>> = _forgotPswdResponse


    fun forgotPswdUser(body: RequestBodies.ForgotPswdBody) = viewModelScope.launch {
        forgotPswd(body)
    }

    private suspend fun forgotPswd(body: RequestBodies.ForgotPswdBody) {
        _forgotPswdResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.forgotPassword(body)
                _forgotPswdResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                _forgotPswdResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _forgotPswdResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _forgotPswdResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<StatusModel>): Event<Resource<StatusModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}