package com.cloutlyfe.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.SignUpModel
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class TwitterLoginModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {
    private val _loginResponse = MutableLiveData<Event<Resource<SignUpModel>>>()
    val loginResponse: LiveData<Event<Resource<SignUpModel>>> = _loginResponse


    fun twitterLoginRequest(mActivity: Activity, body: RequestBodies.TwitterLoginBody) = viewModelScope.launch {
        login(mActivity,body)
    }

    private suspend fun login(mActivity: Activity, body: RequestBodies.TwitterLoginBody) {
        _loginResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<App>())) {
                val response = appRepository.twitterLoginUser(body)
                if (response.code()!=500)
                {
                    _loginResponse.postValue(handleResponse(response))
                }
                else
                {
                    _loginResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.server_error))))
                }
            } else {
                _loginResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))
            }
        } catch (t: Throwable) {
            Log.e("ISSUE",""+t.message)
            when (t) {
                is IOException -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            ""+t.message
                        ))

                    )
                }
                else -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<SignUpModel>): Event<Resource<SignUpModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}