package com.cloutlyfe.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cloutlyfe.app.App
import com.cloutlyfe.app.R
import com.cloutlyfe.app.repository.AppRepository
import com.cloutlyfe.app.utils.Constants.Companion.hasInternetConnection
import com.cloutlyfe.app.utils.Event
import com.cloutlyfe.app.utils.Resource
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.model.LikeUnlikeModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class LikeUnlikeCommentViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val likeUnlikeResponse = MutableLiveData<Event<Resource<LikeUnlikeModel>>>()
    val _likeUnlikeResponse: LiveData<Event<Resource<LikeUnlikeModel>>> = likeUnlikeResponse


    fun likeUnlikeCommentRequest(body: RequestBodies.LikeUnlikeCommentBody) = viewModelScope.launch {
        likeUnlike(body)
    }

    private suspend fun likeUnlike(body: RequestBodies.LikeUnlikeCommentBody) {
        likeUnlikeResponse.postValue(Event(Resource.Loading()))
        try {
            if (hasInternetConnection(getApplication<App>())) {
                val response = appRepository.likeUnlikeCommentData(body)
                likeUnlikeResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                likeUnlikeResponse.postValue(Event(Resource.Error(getApplication<App>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    likeUnlikeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    likeUnlikeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<App>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<LikeUnlikeModel>): Event<Resource<LikeUnlikeModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}