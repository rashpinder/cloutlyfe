package com.cloutlyfe.app.retrofit

import ApiClients
import com.cloutlyfe.app.dataobject.RequestBodies
import com.cloutlyfe.app.interfaces.PlacesLatLongModel
import com.cloutlyfe.app.model.*
import com.cloutlyfe.app.model.cloudre.CloudRecordingResponse
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface API {

    companion object {
        fun getApi(): API {
            return ApiClients.getClient
        }
    }
    @POST("login.php")
    suspend fun loginUser(@Body body: RequestBodies.LoginBody): Response<SignUpModel>

    @POST("googleLogin.php")
    suspend fun googleLoginRequest(@Body body: RequestBodies.GoogleLoginBody): Response<SignUpModel>

    @POST("facebookLogin.php")
    suspend fun fbLoginRequest(@Body body: RequestBodies.FacebookLoginBody): Response<SignUpModel>

    @POST("twitterLogin.php")
    suspend fun twitterLoginRequest(@Body body: RequestBodies.TwitterLoginBody): Response<SignUpModel>

    @POST("signUp.php")
    suspend fun signUpUser(@Body body: RequestBodies.SignUpBody): Response<SignUpModel>

    @POST("forgetPassword.php")
    suspend fun forgotPswd(@Body body: RequestBodies.ForgotPswdBody): Response<StatusModel>

    @POST("addReport.php")
    suspend fun reportRequest(@Body body: RequestBodies.ReportBody): Response<StatusModel>

    @POST("getAllReportReasons.php")
    suspend fun getReportReasonsRequest(@Body body: RequestBodies.ReportReasonsBody): Response<GetReportResonsModel>

    @POST("ChangePassword.php")
    suspend fun changePswd(@Body body: RequestBodies.ChangePswdBody): Response<CodeStatusModel>

    @POST("LikeAndUnlike.php")
    suspend fun likeUnlikeRequest(@Body body: RequestBodies.LikeUnlikeBody): Response<LikeUnlikeModel>

    @POST("savePost.php")
    suspend fun saveUnsaveRequest(@Body body: RequestBodies.SaveUnsaveBody): Response<SaveUnsaveModel>

    @POST("logout.php")
    suspend fun logOut(
        @Header("Token") mAuthorization: String?,
        @Body body: RequestBodies.LogoutBody
    ): Response<StatusModel>


//    @Multipart
//    @POST("AddPost.php")
//    fun createPostRequest(
//        @HeaderMap Token: HashMap<String,String>?,
//        @Part list: List<MultipartBody.Part>?,
//        @Part("user_id") user_id: RequestBody?,
//        @Part("bio") bio: RequestBody?,
//        @Part("location") location: RequestBody?,
//        @Part("latitude") latitude: RequestBody?,
//        @Part("longitude") longitude: RequestBody?
//    ): Call<StatusModel>


    @Multipart
    @POST("AddPostv2.php")
    fun createPostRequest(
        @HeaderMap Token: HashMap<String,String>?,
        @Part list: List<MultipartBody.Part>?,
        @Part("user_id") user_id: RequestBody?,
        @Part("bio") bio: RequestBody?,
        @Part("location") location: RequestBody?,
        @Part("latitude") latitude: RequestBody?,
        @Part("longitude") longitude: RequestBody?,
        @Part video_thumbnail: MultipartBody.Part?,
    ): Call<StatusModel>

//    @Multipart
//    @POST("editMangePost.php")
//    fun editPostRequest(
//        @HeaderMap Token: HashMap<String,String>?,
//        @Part list: List<MultipartBody.Part>?,
//        @Part previous_list: List<MultipartBody.Part>?,
//        @Part("user_id") user_id: RequestBody?,
//        @Part("post_id") post_id: RequestBody?,
//        @Part("bio") bio: RequestBody?,
//        @Part("location") location: RequestBody?,
//        @Part("latitude") latitude: RequestBody?,
//        @Part("longitude") longitude: RequestBody?,
//        @Part("type") type: RequestBody?,
//        @Part video_thumbnail: MultipartBody.Part?
//    ): Call<StatusModel>

    @Multipart
    @POST("editMangePostv2.php")
    fun editPostRequest(
        @HeaderMap Token: HashMap<String,String>?,
        @Part list: List<MultipartBody.Part>?,
//        @Part previous_list: List<MultipartBody.Part>?,
        @Part("deleted_images") deleted_images: RequestBody?,
        @Part("user_id") user_id: RequestBody?,
        @Part("post_id") post_id: RequestBody?,
        @Part("bio") bio: RequestBody?,
        @Part("location") location: RequestBody?,
        @Part("latitude") latitude: RequestBody?,
        @Part("longitude") longitude: RequestBody?,
        @Part("type") type: RequestBody?,
        @Part video_thumbnail: MultipartBody.Part?
    ): Call<StatusModel>


    @Multipart
    @POST("editProfile.php")
    fun edituserProfileRequest(
        @HeaderMap Token: HashMap<String,String>?,
        @Part("previous_cover_image") previous_cover_image: RequestBody?,
        @Part("user_id") user_id: RequestBody?,
        @Part("name") name: RequestBody?,
        @Part("userName") userName: RequestBody?,
        @Part("status") status: RequestBody?,
        @Part("bio") bio: RequestBody?,
        @Part photo: MultipartBody.Part?,
        @Part cover_photo: MultipartBody.Part?
    ): Call<EditProfileModel>


    @POST("Homev2.php")
    fun homeRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>

    @POST("Homev3.php")
    fun homeRequestCaughtUp(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>

    @POST("Homev2.php")
    fun chatPostRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>

    @POST("AddComment.php")
    fun addCommentRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AddCommentModel>

    @POST("addCommentOnStory.php")
    fun addCommentonStoryRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CommentOnStatusModel>

    @POST("deleteUserAccount.php")
    fun deleteUserAccountRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("DeletePostByPostid.php")
    fun deletePostRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CodeStatusModel>

    @POST("deleteStatus.php")
    fun deleteStatusRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CodeStatusModel>

    @POST("DeleteChat.php")
    fun deleteChatRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("DeleteComment.php")
    fun deleteCommentRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("removeFollower.php")
    suspend fun removeUserRequest(@Header("Token") mAuthorization: String?, @Body body: RequestBodies.FollowBody): Response<CodeStatusModel>

    @POST("GetAllCommentBypostIdv2.php")
    fun getCommentRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetCommentsModel>

    @POST("GetFollowAnFollowingUsers.php")
    fun getFollowingRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<FollowingUsersModel>

    @POST("GetFollowUsers.php")
    fun getFollowRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<FollowingUsersModel>

//    @POST("getImageAndVideoListingv2.php")
//    fun getProfilePostsRequest(
//        @HeaderMap Token: Map<String, String>?,
//        @Body mParams: Map<String?, String?>?
//    ): Call<ProfilePostsModel>


    @POST("getImageAndVideoListingv2.php")
    fun getProfilePostsRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>

    @POST("AddComment.php")
    fun addReplyCommentRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<AddCommentModel>

    @POST("GetAllCommentBypostIdv2.php")
    fun getReplyCommentRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetCommentsModel>

    @POST("RequestChat.php")
    fun createRoomRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CreateRoomModel>

    @POST("GetChatUsers.php")
    fun getChatUsersRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<ChatUsersModel>

//    @POST("GetChatListing.php")
//    fun getOnetoOneChatRequest(
//        @Body mParams: Map<String?, String?>?
//    ): Call<GetOnetoOneChatModel>

    @POST("GetChatListingv2.php")
    fun getOnetoOneChatRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetOneToOneChatNewModel>

    @POST("getPostDetails.php")
    fun getPostDetailsRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GetPostDetailsModel>

    @POST("getPostDetailsByPostID.php")
    fun getPostDetailByPostId(
        @Body mParams: Map<String?, String?>?
    ): Call<GetPostDetailModel>

//    @POST("GetChatListingv2.php")
//    fun getOnetoOneChatRequest(
//        @Body mParams: Map<String?, String?>?
//    ): Call<GetOnetoOneChatModel>

//    @POST("AddMessage.php")
//    fun sendMessageRequest(
//        @Body mParams: Map<String?, String?>?
//    ): Call<SendMessageModel>


    @Multipart
    @POST("AddMessagev2.php")
    fun sendMessageRequest(
        @HeaderMap Token: HashMap<String,String>?,
        @Part("user_id") user_id: RequestBody?,
        @Part("message") message: RequestBody?,
        @Part("room_id") room_id: RequestBody?,
        @Part("document_type") document_type: RequestBody?,
//        @Part chat_images: ArrayList<MultipartBody.Part>
        @Part chat_images: Array<MultipartBody.Part?>,
        @Part chat_images_thumb: Array<MultipartBody.Part?>
    ): Call<SendMessageNewModel>


    @POST("blockUnblockSpecificUserByUserID.php")
    fun blockUnblockRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("reportUser.php")
    fun reportUserRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("getAllBlockedUsers.php")
    fun getAllblockUsersRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<BlockUsersListModel>

   @POST("UpdateMessageSeen.php")
    fun seenMessageRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel>

    @POST("AddMessagev3.php")
    fun sharePostRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SendMessageModel>

    @POST("notificationActivity.php")
    fun getNotificationRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<NotificationModel>

//    @POST("getSavepostByUserid.php")
//    fun savedRequest(
//        @HeaderMap Token: Map<String, String>?,
//        @Body mParams: Map<String?, String?>?
//    ): Call<ProfilePostsModel>
//
    @POST("getSavepostByUserid.php")
    fun savedRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeModel>

    @POST("getUsersLikesListing.php")
    fun getUsersLikeRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetLikeUsersModel>
//
//
//    @POST("Home.php")
//    fun savedRequest(
//        @HeaderMap Token: Map<String, String>?,
//        @Body mParams: Map<String?, String?>?
//    ): Call<HomeModel>

//    @POST("Home.php")
//    suspend fun saveddRequest(@Header("Token") mAuthorization: String?,@Body body: RequestBodies.HomeBody): Response<HomeModel>

    @POST("GetProfilefollowDetails.php")
    suspend fun profileRequest(@Header("Token") mAuthorization: String?, @Body body: RequestBodies.ProfileBody): Response<ProfileModel>

    @POST("addFollowAndUnfollow.php")
    suspend fun followRequest(@Header("Token") mAuthorization: String?, @Body body: RequestBodies.FollowBody): Response<CodeStatusModel>

    @POST("getAllSearchUser.php")
    suspend fun searchHomeRequest(@Header("Token") mAuthorization: String?,@Body body: RequestBodies.SearchHomeBody): Response<SearchModel>

    @GET
    fun searchPlacesRequest(
        @Url mUrl: String,
    ): Call<PlacesModel>?


    @GET
    fun searchPlacesLatLongRequest(
        @Url mUrl: String,
    ): Call<PlacesLatLongModel>?


    @POST("commentLikeAndUnlike.php")
    suspend fun likeUnlikeCommentRequest(@Body body: RequestBodies.LikeUnlikeCommentBody): Response<LikeUnlikeModel>


    @POST("GetStatusDetalsByuser_id.php")
    fun getStories(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<GetStoriesModel>

    @POST("updateStatusViewUser.php")
    fun updateStatusViewUser(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<CodeStatusModel>

    @POST("getStatusViewDetalsBystatus_id.php")
    fun getStatusViewUserDetail(
        @Body mParams: Map<String?, String?>?
    ): Call<GetStatusViewedUsersListModel>

    @Multipart
    @POST("AddStatus.php")
    fun addMultipleStatusImage(
        @HeaderMap Token: MutableMap<String, String>,
        @Part status_images: ArrayList<MultipartBody.Part>,
        @Part("user_id") user_id: RequestBody?,
        @Part("duration") duration: RequestBody?,
        @Part video_thumbnail: MultipartBody.Part?
    ): Call<StatusModel>

//live stream
    @POST("GoingliveV2.php")
    fun executeLiveStream(
        @Header("Token") mAuthorization: String?,
        @Body mParam: Map<String?, String?>?
    ): Call<GoingLiveModel?>?


    @POST("EndstreamV2.php")
    fun endLiveStreamV2(
        @Header("Token") authToken: String?,
        @Body mParam: Map<String?, String?>?
    ): Call<EndStream2Model?>?


    @POST("Addstreamcommentsv2.php")
    fun addStreamComment(
        @Header("Token") authToken: String?,
        @Body mParams12: Map<String?, String?>?
    ): Call<JsonObject?>?


    @POST("GetAllstreamcommentsv2.php")
    fun getStreamChatListing(
        @Header("Token") mAuthorization: String?,
        @Body mParams: Map<String?, String?>?
    ): Call<StreamMessageModel?>?

    @POST("Leavestreamv2.php")
    fun leaveLiveStream(
        @Header("Token") authToken: String?,
        @Body mParam: Map<String?, String?>?
    ): Call<StatusModel?>?


    @POST("GoingliveV3.php")
    fun executeCloudRecordStream(
        @Header("Token") mAuthorization: String?,
        @Body mParam: Map<String?, String?>?
    ): Call<CloudRecordingResponse?>?


    @POST("addStreamVideoLink.php")
    fun exceuteShareStream(
        @Header("Token") authToken: String?,
        @Body mParams1: Map<String?, String?>?
    ): Call<StatusModel?>?


    @POST("approveRejectStream.php")
    fun approveRejectStream(
        @Header("Token") authToken: String?,
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel?>?

    @POST("Hostlivedetails.php")
    fun getHostLiveDetails(
        @Header("Token") authToken: String?,
        @Body mParams: Map<String?, String?>?
    ): Call<StreamListModel?>?


    @POST("viewLiveStream.php")
    fun getViewCounts(
        @Header("Token") authToken: String?,
        @Body mParams: Map<String?, String?>?
    ): Call<ViewCounModel?>?

    @POST("getAllLiveUserByStreamID.php")
    fun getViewers(
        @Header("Token") authToken: String?,
        @Body mParams1: Map<String?, String?>?
    ): Call<LiveViewersModel?>?

    @POST("disableLikeComment.php")
    fun disableLikeComment(
        @Header("Token") authToken: String?,
        @Body mParamsN: Map<String?, String?>?
    ): Call<StatusModel?>?

    @POST("userstreamjoinrequestv2.php")
    fun requestRoom(
        @Header("Token") authToken: String?,
        @Body mParams: Map<String?, String?>?
    ): Call<StatusModel?>?

    @POST("getLiveUsers.php")
    fun showLiveUsers(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<LiveUsersModel?>?

//    @GET("getLiveUsers.php")
//    fun showLiveUsers(@Header("Token") authToken: String?): Call<LiveUsersModel?>?

    @POST("getAllStreamVideo.php")
    fun getAllStreams(
        @Header("Token") authToken: String?,
        @Body mParams1: Map<String, String>
    ): Call<StreamRecordedModel?>?

}