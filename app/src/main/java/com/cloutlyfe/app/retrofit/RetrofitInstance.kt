package com.cloutlyfe.app.retrofit


//import okhttp3.logging.HttpLoggingInterceptor


import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


class RetrofitInstance {

    companion object {
        //Staging

        //Live
//        private const val BASE_URL = "https://cloutlyfe.online/CloutLyfe/webservice/"

//        private const val BASE_URL = "https://cloutlyfe.online/CloutLyfe_staging/webservice/"

//        live
        private const val BASE_URL = "https://cloutlyfe.online/CloutLyfe//webservice/"

        private const val BASE_URL_LOCATION = "https://maps.googleapis.com/"


        val retrofitClientSearch: Retrofit.Builder by lazy {
            //An OkHttp interceptor which logs request and response information
//            val levelType: HttpLoggingInterceptor.Level = HttpLoggingInterceptor.Level.BODY
//            val logging = HttpLoggingInterceptor()
//            logging.setLevel(levelType)

            val okhttpClient = OkHttpClient.Builder()
//            okhttpClient.addInterceptor(logging)

            Retrofit.Builder()
                .baseUrl(BASE_URL_LOCATION)
                .client(okhttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
            //Call the created instance of that class (done via create()) to your Retrofit instance with the method addConverterFactory()
        }

        val apiInterface1: API by lazy {
            retrofitClientSearch
                .build()
                .create(API::class.java)
        }


        private val retrofitLogin by lazy {
            val gson = GsonBuilder()
                .setLenient()
                .create()

//            val logging = HttpLoggingInterceptor()
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
//                .addInterceptor(logging)
                .build()
            httpsTrustManager2.allowAllSSL()
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getUnsafeOkHttpClient11()!!.build())
//                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
              //  .client(client)

                .build()


        }


        private val retrofitnewLogin by lazy {

            val httpClient = OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build()

            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
        }


        val appApi by lazy {
            retrofitLogin.create(API::class.java)
        }


        //Auth issue of domain
        private fun getUnsafeOkHttpClient11(): OkHttpClient.Builder? {
            return try {

                val trustAllCerts = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                            return arrayOf()
                        }
                    }
                )

                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory
//                val interceptor = HttpLoggingInterceptor()
//                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                val builder: OkHttpClient.Builder =
                    OkHttpClient.Builder()
//                        .addInterceptor(interceptor)
                        .connectTimeout(5, TimeUnit.MINUTES)
                        .readTimeout(5, TimeUnit.MINUTES)
                        .writeTimeout(5, TimeUnit.MINUTES)
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier { hostname, session -> true }
                builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }


    }
}