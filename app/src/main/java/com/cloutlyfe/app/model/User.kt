package com.cloutlyfe.app.model

data class User(
    val name: String,
    val username: String,
    val photo: String,
    val user_id: String
)