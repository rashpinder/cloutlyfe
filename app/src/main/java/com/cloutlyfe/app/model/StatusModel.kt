package com.cloutlyfe.app.model

data class StatusModel(
    val message: String,
    val status: Int
)