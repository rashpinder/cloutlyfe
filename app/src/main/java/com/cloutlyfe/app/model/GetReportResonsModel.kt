package com.cloutlyfe.app.model

data class GetReportResonsModel(
    val `data`: List<DataX>,
    val message: String,
    val status: Int
)