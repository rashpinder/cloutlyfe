package com.cloutlyfe.app.model

data class SignUpModel(
    val code: Int,
    val `data`: Data,
    val message: String,
    val status: Int
)