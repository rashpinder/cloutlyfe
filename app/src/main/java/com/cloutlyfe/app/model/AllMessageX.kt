package com.cloutlyfe.app.model

data class AllMessageX(
    val creation_date: String,
    val id: String,
    val message: String,
    val photo: String,
    val read_status: String,
    val room_id: String,
    val user_id: String,
    val username: String
)