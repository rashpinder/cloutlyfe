package com.cloutlyfe.app.model

data class AllMessage(
    val creation_date: String,
    val created: String,
    val id: String,
    val message: String?="",
    val read_status: String,
    val room_id: String,
    val sender_profile_pic: String,
    val status_image: String,
    val status_details: StatusDetails?=null,
    val post_details: PostDetails?,
    val sender_username: String,
    val is_delete_status: String,
    val status_id: String,
    val user_id: String,
    var viewType: Int = -1,
    val photo: String,
    val username: String,
    val name: String,
    var is_delete:String="0",
    val chat_images: String?,
    val document_type: String,
    val other_id: String,
    val post_id: String,
    val mPos: String
)