package com.cloutlyfe.app.model

import java.io.Serializable

data class StatusImageModel(
    var status_id: String,
    var status_image: String,
): Serializable