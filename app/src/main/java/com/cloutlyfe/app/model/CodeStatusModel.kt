package com.cloutlyfe.app.model

data class CodeStatusModel(
    val code: Int,
    val message: String,
    val status: Int
)