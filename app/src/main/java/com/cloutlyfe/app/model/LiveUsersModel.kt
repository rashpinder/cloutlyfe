package com.cloutlyfe.app.model

data class LiveUsersModel(
        val message: String,
    val status: Int,
    val `data`: ArrayList<LiveDataModel>
)
