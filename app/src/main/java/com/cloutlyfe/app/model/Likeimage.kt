package com.cloutlyfe.app.model

data class Likeimage(
    val image: String,
    val username: String,
    val name: String,
    val user_id: String
)