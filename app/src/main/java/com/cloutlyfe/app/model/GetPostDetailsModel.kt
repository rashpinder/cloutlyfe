package com.cloutlyfe.app.model

data class GetPostDetailsModel(
    val `data`: DataOfPost,
    val message: String,
    val status: Int
)