package com.cloutlyfe.app.model

data class SendMessageData(
    val creation_date: String,
    val id: String,
    val message: String,
    val read_status: String,
    val room_id: String,
    val sender_profile_pic: String,
    val sender_username: String,
    val user_id: String,
    var viewType : Int = -1,
    val photo: String,
    val username: String

)