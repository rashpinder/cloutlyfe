package com.cloutlyfe.app.model

data class HomeModel(
    val chatCount: String,
    val notificationCount: Int,
    val count: String,
    val `data`: ArrayList<HomeData> = ArrayList(),
    val lastPage: String,
    val message: String,
    val status: Int
)