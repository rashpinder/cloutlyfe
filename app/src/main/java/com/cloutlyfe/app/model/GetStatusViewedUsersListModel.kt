package com.cloutlyfe.app.model

data class GetStatusViewedUsersListModel(
    val `data`: ArrayList<StatusViewedData>,
    val is_count: String,
    val message: String,
    val status: Int
)