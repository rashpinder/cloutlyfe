package com.cloutlyfe.app.model

data class ProfileModel(
    val Followers: String,
    val Followings: String,
    val postCount: String,
    val code: Int,
    val `data`: ProfileData,
    val message: String,
    val status: Int
)