package com.cloutlyfe.app.model

data class DeleteMsgDataClass(
    val id: String,
    val message: String?="",
    val room_id: String,
    val document_type: String,
    val is_delete: String,
    val user_id: String,
    val mPos: String
    )