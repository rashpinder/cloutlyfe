package com.cloutlyfe.app.model

data class NotificationModel(
    val count: Int,
    val `data`: ArrayList<NotificationData>,
    val message: String,
    val status: Int,
    val lastPage: String
)