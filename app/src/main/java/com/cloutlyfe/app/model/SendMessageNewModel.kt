package com.cloutlyfe.app.model

data class SendMessageNewModel(
    val `data`: AllMessage,
    val message: String,
    val status: Int
)