package com.cloutlyfe.app.model

data class AddCommentModel(
    val comment_id: Int,
    val message: String,
    val status: Int
)