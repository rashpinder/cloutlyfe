package com.cloutlyfe.app.model

data class GetLikeUsersModel(
    val `data`: ArrayList<LikeUsersData>,
    val message: String,
    val status: Int,
    val likeCount: String
)