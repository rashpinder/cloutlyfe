package com.cloutlyfe.app.model

data class GetOneToOneChatNewModel(
    val all_messages: ArrayList<AllMessage>,
    val message: String,
    val receiver_detail: ReceiverDetail,
    val receiver_id: String,
    val room_id: String,
    val status: Int
)