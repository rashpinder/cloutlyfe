package com.cloutlyfe.app.model

data class LikeimageX(
    val image: String,
    val username: String
)