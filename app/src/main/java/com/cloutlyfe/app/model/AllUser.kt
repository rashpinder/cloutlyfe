package com.cloutlyfe.app.model

data class AllUser(
    val creation_date: String,
    val message: String,
    val message_time: String,
    val photo: String,
    val receiver_id: String,
    val room_id: String,
    val room_no: String,
    val sender_id: String,
    val unread_count: String,
    val username: String,
    val name: String,
    val is_delete: Int,
    val is_blocked: String,
    val other_blocked: String
)