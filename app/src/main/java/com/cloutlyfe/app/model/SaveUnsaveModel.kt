package com.cloutlyfe.app.model

data class SaveUnsaveModel(
    var isSave: String,
    val message: String,
    val status: Int,
)