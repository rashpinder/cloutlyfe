package com.cloutlyfe.app.model

data class StatusViewedData(
    val created_at: String,
    val id: String,
    val name: String,
    val photo: String,
    val status_id: String,
    val user_id: String,
    val username: String
)