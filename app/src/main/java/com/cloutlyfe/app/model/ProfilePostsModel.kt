package com.cloutlyfe.app.model

data class ProfilePostsModel(
    val code: Int,
    val `data`: ArrayList<ProfilePostsData>,
    val lastPage: String,
    val message: String,
    val status: Int
)