package com.cloutlyfe.app.model

data class GetCommentsData(
    val comment: String,
    val comment_by: String,
    val comment_id: String,
    val created_at: String,
    val name: Any,
    val photo: String,
    val post_id: String,
    val username: Any
)