package com.cloutlyfe.app.model

data class SearchModel(
    val `data`: List<SearchData>,
    val message: String,
    val status: Int
)