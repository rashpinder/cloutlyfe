package com.cloutlyfe.app.model

data class PostData(
    val created_at: String,
    val image_id: String,
    val is_delete: String,
    val old_url: String,
    val post_id: String,
    val thumbnail_image: String,
    val type: String,
    val uploads: String,
    val user_id: String
)