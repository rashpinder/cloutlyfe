package com.cloutlyfe.app.model

data class StatusDetails(
    val createdate: String,
    val duration: String,
    val status_id: String,
    val status_image: String,
    val statusdate: String,
    val video_thumbnail: String,
    val user_id: String
)