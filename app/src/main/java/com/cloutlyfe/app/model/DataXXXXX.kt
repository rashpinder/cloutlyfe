package com.cloutlyfe.app.model

data class DataXXXXX(
    var commentCount: String,
    val created_at: String,
    val description: String,
    val image: ArrayList<String>,
    var isLike: Int,
    var isSave: Int,
    val is_report: String,
    val latitude: String,
    var likeCount: String,
    val likeimage: ArrayList<Likeimage>?,
    val location: String,
    val longitude: String,
    val name: String,
    val photo: String,
    val post_id: String,
    val thumbnail_image: ArrayList<String>,
    val user_id: String,
    val user_image: String,
    val username: String,
    val is_delete: Int
)