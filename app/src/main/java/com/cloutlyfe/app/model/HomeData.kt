package com.cloutlyfe.app.model

data class HomeData(
    var commentCount: String?,
    var is_report: String,
    val created_at: String,
    val description: String,
    val image: ArrayList<String>?,
    var isLike: Int,
    var isSave: Int,
    val latitude: String,
    var likeCount: String,
    val likeimage: ArrayList<Likeimage>?,
    val location: String,
    val longitude: String,
    val name: String,
    val photo: String,
    val post_id: String,

    val thumbnail_image: ArrayList<String>?,
    val user_id: String,
    val user_image: String,
    val username: String,

    var vol: Boolean=true,
    val type: Int,

    val IMAGE_TYPE: Int=0,
    val VIDEO_TYPE: Int=1,


    var notification_type: String,
    var room_id: String,
    var otherID: String,
    var postuser: String,
    var notification_id: String,
    var creation_date: String,
    var message: String,
    var followID: String,
    var title_message: String,
    var status_id: String,
    var notification_read_status: String,
    var viewed_status: String,
    var is_delete: Int,
    val is_reported: String,
    val disable_like: String,
    val stream_id: String,
    val disable_comment: String,
    val stream_room: String,
    var is_already_report: Boolean=false
)