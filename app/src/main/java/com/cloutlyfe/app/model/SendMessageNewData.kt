package com.cloutlyfe.app.model

data class SendMessageNewData(
    val chat_images: String,
    val created: String,
    val creation_date: String,
    val id: String,
    val message: String,
    val other_id: String,
    val photo: String,
    val post_id: String,
    val read_status: String,
    val room_id: String,
    val sender_username: String,
    val user_id: String,
    var viewType: Int = -1,
)