package com.cloutlyfe.app.model

data class DataXXXXXX(
    val allowPush: String,
    val apple_id: String,
    val auth_token: String,
    val bio: String,
    val block_user: String,
    val changeStatus: String,
    val country: String,
    val cover_image: String,
    val created_at: String,
    val device_token: String,
    val device_type: String,
    val disable_comment: String,
    val disable_like: String,
    val disabled: String,
    val email: String,
    val facebook_id: String,
    val google_id: String,
    val is_delete: String,
    val latitude: String,
    val location: String,
    val longitude: String,
    val mobile_number: String,
    val name: String,
    val password: String,
    val photo: String,
    val status: String,
    val stream_id: String,
    val stream_room: String,
    val twitter_id: String,
    val user_id: String,
    val user_image: String,
    val username: String,
    val verificate_code: String,
    val verified: String,
    val verified_badges: String
)