package com.cloutlyfe.app.model

data class GetOnetoOneChatModel(
    val all_messages: ArrayList<AllMessage>,
    val last_page: String,
    val message: String,
    val receiver_detail: ReceiverDetail,
    val receiver_id: String,
    val room_id: String,
    val status: Int
)