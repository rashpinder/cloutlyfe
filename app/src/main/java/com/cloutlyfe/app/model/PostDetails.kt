package com.cloutlyfe.app.model

data class PostDetails(
    var image: ArrayList<String>,
    val thumbnail_image: ArrayList<String>,
    val post_id: String,
    val user_id: String,
    val username: String,
    val description: String,
    val location: String,
    val latitude: String,
    val longitude: String,
    val created_at: String,
    val photo: String,
    val name: String,
    val is_delete: String ="0"
)