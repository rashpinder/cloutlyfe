package com.cloutlyfe.app.model

data class CommentOnStatusModel(
    val `data`: CommentOnStatusData,
    val message: String,
    val status: Int
)