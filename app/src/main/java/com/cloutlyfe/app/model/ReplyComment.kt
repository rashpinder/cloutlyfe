package com.cloutlyfe.app.model

data class ReplyComment(
    val comment_id: String,
    val created_at: String,
    val name: String,
    val photo: String,
    val post_id: String,
    val reply_id: String,
    val reply_message: String,
    val user_id: String,
    val username: String
)