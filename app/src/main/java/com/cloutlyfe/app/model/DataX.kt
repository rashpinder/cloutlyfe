package com.cloutlyfe.app.model

data class DataX(
    val reasonId: String,
    val reportReasons: String
)