package com.cloutlyfe.app.model

data class FollowingUsersModel(
    val `data`: List<DataXXX>,
    val message: String,
    val showProfile: Int,
    val status: Int,
    val user_data: UserData,
    val lastPage: String
)