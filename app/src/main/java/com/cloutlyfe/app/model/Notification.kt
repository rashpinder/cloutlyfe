package com.cloutlyfe.app.model

data class Notification(
    val creation_date: String,
    val creation_date_timestamp: String,
    val followID: String,
    val image: String,
    val message: String,
    val message_time: String,
    val name: String,
    val username: String,
    val notification_id: String,
    val notification_read_status: String,
    val notification_type: String,
    val otherID: String,
    val post_id: String,
    val room_id: String,
    val status_id: String,
    val title_message: String,
    val user_id: String,
    val viewed_status: String,
    val is_delete: Int
)