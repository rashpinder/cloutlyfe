package com.cloutlyfe.app.model

data class DataXX(
    val comment: String,
    val comment_by: String,
    val comment_id: String,
    val created_at: String,
    val name: String,
    val photo: String,
    var likeCount: String,
    var isLike: Int,
    val post_id: String,
    val reply_comment: ArrayList<ReplyComment>,
    val username: String
)