package com.cloutlyfe.app.model

data class BlockUsersListModel(
    val message: String,
    val status: Int,
    val code: Int,
    val `data`: ArrayList<UserData>
)
