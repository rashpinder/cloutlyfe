package com.cloutlyfe.app.model

data class AllUsersData (
    val room_id: String,
    val room_no: String,
    val sender_id: String,
    val receiver_id: String,
    val creation_date: String,
    val username: String,
    val message: String,
    val message_time: String,
    val unread_count: String,
    val photo: String,
)