package com.cloutlyfe.app.model

data class PostDetailsX(
    val image: List<Any>,
    val is_delete: String,
    val name: Any,
    val photo: String,
    val thumbnail_image: List<Any>
)