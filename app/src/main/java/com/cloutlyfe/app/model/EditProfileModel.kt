package com.cloutlyfe.app.model

data class EditProfileModel(
    val code: Int,
    val `data`: DataXXXX,
    val message: String,
    val status: Int
)