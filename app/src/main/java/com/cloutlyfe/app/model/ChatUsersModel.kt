package com.cloutlyfe.app.model

data class ChatUsersModel(
    val all_users: ArrayList<AllUser>,
    val last_page: String,
    val message: String,
    val status: Int,
    val unread_message_count: String,
    val user_data: UserData
)