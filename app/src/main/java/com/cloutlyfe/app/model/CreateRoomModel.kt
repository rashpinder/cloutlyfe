package com.cloutlyfe.app.model

data class CreateRoomModel(
    val loginProfile: String,
    val message: String,
    val owner_id: String,
    val room_id: String,
    val status: Int,
    val user_detail: UserDetail
)