package com.cloutlyfe.app.model

import java.io.Serializable

data class MyStory(
    val last_status_id: String,
    val last_update_date: String,
    val snaps: ArrayList<Snap>,
    val duration: ArrayList<String>,
    val snaps_count: String,
    val user: User
): Serializable