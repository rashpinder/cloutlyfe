package com.cloutlyfe.app.model

data class GetPostDetailModel(
    val `data`: DataXXXXX,
    val message: String,
    val status: Int,
    val delete_status: Int
)