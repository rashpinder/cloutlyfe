package com.cloutlyfe.app.model

data class NotificationData(
    val day: String,
    val notification: ArrayList<Notification>
)