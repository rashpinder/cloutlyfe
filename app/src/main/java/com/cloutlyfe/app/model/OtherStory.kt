package com.cloutlyfe.app.model

import java.io.Serializable

data class OtherStory(
    val last_status_id: String,
    val last_update_date: String,
    val snaps: ArrayList<Snap>,
    val snaps_count: String,
    val all_viewed_count: String,
    val user: User): Serializable
