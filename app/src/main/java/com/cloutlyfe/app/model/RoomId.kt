package com.cloutlyfe.app.model

data class RoomId(
    val room_id: String
)