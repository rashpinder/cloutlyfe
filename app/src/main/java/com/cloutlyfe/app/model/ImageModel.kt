package com.cloutlyfe.app.model
public class ImageModel{
	var image: String? = null
	var title: String? = null
	private var resImg = 0
	private var isSelected = false

	@JvmName("getImage1")
	fun getImage(): String? {
		return image
	}

	@JvmName("setImage1")
	fun setImage(image: String?) {
		this.image = image
	}


	@JvmName("getTitle1")
	fun getTitle(): String? {
		return title
	}

	@JvmName("setTitle1")
	fun setTitle(title: String?) {
		this.title = title as Nothing?
	}

	fun getResImg(): Int {
		return resImg
	}

	fun setResImg(resImg: Int) {
		this.resImg = resImg
	}

	fun isSelected(): Boolean {
		return isSelected
	}

	fun setSelected(isSelected: Boolean) {
		this.isSelected = isSelected
	}
}