package com.cloutlyfe.app.model

data class StoriesData(
    val myStories: ArrayList<MyStory>,
    val stories: ArrayList<OtherStory>
)