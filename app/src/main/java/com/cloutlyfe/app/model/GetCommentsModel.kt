package com.cloutlyfe.app.model

data class GetCommentsModel(
    val `data`: ArrayList<DataXX>,
    val lastPage: String,
    val message: String,
    val photo: String,
    val status: Int,
    val user_name: String
)