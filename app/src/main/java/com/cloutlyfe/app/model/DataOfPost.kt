package com.cloutlyfe.app.model

data class DataOfPost(
    val commentCount: String,
    val created_at: String,
    val description: String,
    val isLike: String,
    val isSave: String,
    val is_delete: String,
    val is_report: String,
    val latitude: String,
    val likeCount: String,
    val likeimage: ArrayList<PostData>,
    val location: String,
    val longitude: String,
    val name: String,
    val photo: String,
    val post_data: ArrayList<PostData>,
    val post_id: String,
    val user_id: String,
    val user_image: String,
    val username: String
)