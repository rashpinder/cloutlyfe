package com.cloutlyfe.app.model

data class LikeUnlikeModel(
    var isLike: String,
    val message: String,
    val status: Int,
    var totalLike: Int
)