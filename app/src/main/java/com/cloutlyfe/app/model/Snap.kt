package com.cloutlyfe.app.model

import java.io.Serializable

data class Snap(
    val createdate: String,
    val status_id: String,
    val status_image: String,
    val statusdate: String,
    val user_id: String,
    val duration: String,
    val is_viewed: String
): Serializable