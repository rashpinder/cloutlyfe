package com.cloutlyfe.app.model

data class GetStoriesModel(
    val `data`: StoriesData,
    val message: String,
    val status: Int
)