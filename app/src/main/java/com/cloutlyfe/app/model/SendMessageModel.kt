package com.cloutlyfe.app.model

data class SendMessageModel(
    val `data`: AllMessage,
    val message: String,
    val status: Int,
    val room_ids: ArrayList<RoomId>
)