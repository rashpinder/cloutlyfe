package com.cloutlyfe.app.model

data class CommentOnStatusData(
    val chat_images: String,
    val chat_images_thumb: String,
    val creation_date: String,
    val document_type: String,
    val id: String,
    val message: String,
    val old_url: String,
    val other_id: String,
    val post_id: String,
    val read_status: String,
    val room_id: String,
    val status_details: StatusDetails,
    val status_id: String,
    val user_id: String
)