package com.cloutlyfe.app.model

data class LikeUsersData(
    var changeStatus: String,
    val id: String,
    val name: String,
    val photo: String,
    val post_id: String,
    val user_id: String,
    val username: String
)