package com.cloutlyfe.app.adapters
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.ChatActivity
import com.cloutlyfe.app.interfaces.LoadMoreScrollListner
import com.cloutlyfe.app.model.DataXXX
import com.cloutlyfe.app.adapters.ShareBottomSheetAdapter.OnItemCheckListener
import java.util.ArrayList


class ShareBottomSheetAdapter(
    val mActivity: Context?,
    var mArrayList: java.util.ArrayList<DataXXX>,
    var mOnItemCheckListener: OnItemCheckListener,
    private var row_index: Int = 0
) :
    RecyclerView.Adapter<ShareBottomSheetAdapter.MyViewHolder>() {
//    private val onItemClick: OnItemCheckListener? = null

    interface OnItemCheckListener {
        fun onItemCheck(item: DataXXX?)
        fun onItemUncheck(item: DataXXX?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_share_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel: DataXXX = mArrayList.get(position)
        if(mModel.username=="N/A" ||  mModel.username==""|| mModel.username==null){
            holder.txtNameTV.text = mModel.name
        }
        else{
            holder.txtNameTV.text = mModel.username}
//        holder.txtNameTV.text = mModel.username

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.photo)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into(holder.profileIV)
        }

        holder.imgCheckIV.setOnClickListener({
            if (holder.imgCheckIV.isChecked()) {
                mOnItemCheckListener!!.onItemCheck(mModel)
                holder.imgCheckIV.background= mActivity!!.getDrawable(R.drawable.ic_check)
            } else {
                mOnItemCheckListener!!.onItemUncheck(mModel)
                holder.imgCheckIV.background= mActivity!!.getDrawable(R.drawable.ic_uncheck)
            }
        })

        holder.itemView.setOnClickListener({
//            val i = Intent(mActivity, ChatActivity::class.java)
//            i.putExtra("toilet_id", mModel.user_id)
//            i.putExtra("value", "home")
//            mActivity?.startActivity(i)
        })




    }

    fun updateList(list: List<DataXXX?>) {
        mArrayList = list as ArrayList<DataXXX>
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
       return mArrayList.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var txtNameTV: TextView
        lateinit var imgCheckIV: CheckBox
        lateinit var profileIV: ImageView

        init {
            txtNameTV = itemView.findViewById<View>(R.id.txtNameTV) as TextView
            imgCheckIV = itemView.findViewById<View>(R.id.imgCheckIV) as CheckBox
            profileIV = itemView.findViewById<View>(R.id.profileIV) as ImageView
        }
    }

}
