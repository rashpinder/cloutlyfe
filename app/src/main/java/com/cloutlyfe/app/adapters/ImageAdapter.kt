package com.cloutlyfe.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.OptionsInterface
import com.cloutlyfe.app.model.DataOfPost
import com.cloutlyfe.app.model.ImageModel

class ImageAdapter(
    context: Context,
    imageList: ArrayList<ImageModel>?,
    var selectedImagesArrayList: java.util.ArrayList<String>,
    var mOptionsClickListener: OptionsInterface
) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {
    private val context: Context? = context
    private val imageList: ArrayList<ImageModel>? = imageList
     private var onItemClickListener : StatusOnItemClickListener? = null
    private var selectedPosition = 0
    private  var list:ArrayList<DataOfPost>?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.image_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        Glide.with(context!!)
            .load(imageList!!.get(pos).getImage())
            .placeholder(R.color.grey)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(holder.image!!)
        if (selectedPosition == pos) {
//            holder.checkBox!!.setVisibility(VISIBLE)
//            holder.checkBox!!.setChecked(true)
        } else {
//            holder.checkBox!!.setVisibility(INVISIBLE)
//            holder.checkBox!!.setChecked(false)
        }
//        if (imageList!!.get(pos).isSelected()) {
//            holder.checkBox!!.setVisibility(VISIBLE)
//            holder.checkBox!!.setChecked(true)
//        } else {
//            holder.checkBox!!.setVisibility(INVISIBLE)
//            holder.checkBox!!.setChecked(false)
//        }
holder.imgCrossIV!!.setOnClickListener {
    mOptionsClickListener.onItemClickListner(pos, "")
}
        holder.itemView.setOnClickListener { v ->
            if (selectedPosition >= 0)
                notifyItemChanged(selectedPosition)
            selectedPosition = holder.getAdapterPosition()
            notifyItemChanged(selectedPosition)
            onItemClickListener!!.onItemClick(selectedPosition, v)
            }
    }

    override fun getItemCount(): Int {
        return imageList!!.size
    }

    fun setOnItemClickListener(statusOnItemClickListener: StatusOnItemClickListener) {
        this.onItemClickListener = statusOnItemClickListener
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView? = itemView.findViewById(R.id.image)
        var checkBox: CheckBox? = itemView.findViewById(R.id.circle)
        var imgCrossIV: TextView? = itemView.findViewById(R.id.imgCrossIV)

//            image = itemView.findViewById(R.id.image)
//            checkBox = itemView.findViewById(R.id.circle)
//            itemView.setOnClickListener { v ->
//                StatusOnItemClickListener.onItemClick(
//                    adapterPosition, v
//                )
//            }

    }

    interface StatusOnItemClickListener {
        fun onItemClick(position: Int, v: View?)
    }
}


