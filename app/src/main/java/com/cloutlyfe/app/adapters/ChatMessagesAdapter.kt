package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.chatviewholder.ItemLeftPostUnavailableVH
import com.cloutlyfe.app.chatviewholder.ItemLeftStoryUnavalableVH
import com.cloutlyfe.app.chatviewholder.ItemRightPostUnavailableVH
import com.cloutlyfe.app.chatviewholder.ItemRightStoryUnavailableVH
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.AllMessage
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_IMAGE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_SHARE_STORY_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_VIDEO_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.LEFT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.POST_UNAVAILABLE_LEFT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.POST_UNAVAILABLE_RIGHT_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_IMAGE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_SHARE_STORY_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_VIDEO_POST_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.RIGHT_VIEW_HOLDER
import com.merger.app.chatviewholder.*
import kotlin.collections.ArrayList

class ChatMessagesAdapter(val mActivity: Activity?, var mArrayList: ArrayList<AllMessage>,var mDeleteChatInterface: DeleteChatInterface) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_right, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_chat_left, parent, false)
            return ItemLeftViewHolder(mView)
        }else if (viewType == LEFT_POST_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_share_post_left, parent, false)
            return ItemLeftPostViewHolder(mView)
        }else if (viewType == RIGHT_POST_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_share_post_right, parent, false)
            return ItemRightPostViewHolder(mView)
        }else if (viewType == LEFT_VIDEO_POST_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_share_video_left, parent, false)
            return ItemLeftVideoPostVH(mView)
        }else if (viewType == RIGHT_VIDEO_POST_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_share_video_right, parent, false)
            return ItemRightVideoPostVH(mView)
        }else if (viewType == POST_UNAVAILABLE_RIGHT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_right_post_unavailable, parent, false)
            return ItemRightPostUnavailableVH(mView)
        }else if (viewType == POST_UNAVAILABLE_LEFT_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_left_post_unavailable, parent, false)
            return ItemLeftPostUnavailableVH(mView)
        }
        else if (viewType == LEFT_IMAGE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_left_img_chat, parent, false)
            return ItemLeftImageVH(mView)
        }
        else if (viewType == RIGHT_IMAGE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_right_img_chat, parent, false)
            return ItemRIghtImageVH(mView)
        }
        else if (viewType == LEFT_SHARE_STORY_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_left_share_story, parent, false)
            return ItemLeftShareImageVH(mView)
        }
        else if (viewType == RIGHT_SHARE_STORY_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_right_share_story, parent, false)
            return ItemRightShareImageVH(mView)
        }
        else if (viewType == LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.left_story_unavailable, parent, false)
            return ItemLeftStoryUnavalableVH(mView)
        }
        else if (viewType == RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.right_story_unavailable, parent, false)
            return ItemRightStoryUnavailableVH(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
            (holder as ItemRightViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
            (holder as ItemLeftViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        } else if (holder.itemViewType == LEFT_POST_VIEW_HOLDER) {
            (holder as ItemLeftPostViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }else if (holder.itemViewType == RIGHT_POST_VIEW_HOLDER) {
            (holder as ItemRightPostViewHolder).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }else if (holder.itemViewType == LEFT_VIDEO_POST_VIEW_HOLDER) {
            (holder as ItemLeftVideoPostVH).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }else if (holder.itemViewType == RIGHT_VIDEO_POST_VIEW_HOLDER) {
            (holder as ItemRightVideoPostVH).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }else if (holder.itemViewType == POST_UNAVAILABLE_RIGHT_VIEW_HOLDER) {
            (holder as ItemRightPostUnavailableVH).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }else if (holder.itemViewType == POST_UNAVAILABLE_LEFT_VIEW_HOLDER) {
            (holder as ItemLeftPostUnavailableVH).bindData(
                mActivity,
                mArrayList!![position] as AllMessage?,mDeleteChatInterface
            )
        }
        else if (holder.itemViewType == LEFT_IMAGE_VIEW_HOLDER) {
            (holder as ItemLeftImageVH).bindData(
                mActivity,
                mArrayList[position].chat_images.toString(), mArrayList[position].message.toString()
            )
        }
        else if (holder.itemViewType == RIGHT_IMAGE_VIEW_HOLDER) {
            (holder as ItemRIghtImageVH).bindData(
                mActivity,
                mArrayList[position].chat_images.toString(), mArrayList[position].message.toString()
            )
        }
        else if (holder.itemViewType == LEFT_SHARE_STORY_VIEW_HOLDER) {
            (holder as ItemLeftShareImageVH).bindData(
                mActivity,
                mArrayList[position].status_details, mArrayList[position].message.toString(),mArrayList[position].username,mArrayList[position].photo,mArrayList[position].name
            )
        }
        else if (holder.itemViewType == RIGHT_SHARE_STORY_VIEW_HOLDER) {
            (holder as ItemRightShareImageVH).bindData(
                mActivity,
                mArrayList[position].status_details, mArrayList[position].message.toString(),mArrayList[position].username,mArrayList[position].photo,mArrayList[position].name
            )
        } else if (holder.itemViewType == LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER) {
            (holder as ItemLeftStoryUnavalableVH).bindData(
                mActivity,
                mArrayList[position].status_details, mArrayList[position].message.toString(),mArrayList[position].photo,mArrayList[position].creation_date.toString()
            )
        }
        else if (holder.itemViewType == RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER) {
            (holder as ItemRightStoryUnavailableVH).bindData(
                mActivity,
                mArrayList[position].status_details, mArrayList[position].message.toString(),mArrayList[position].creation_date.toString()
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 2) return LEFT_POST_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 3) return RIGHT_POST_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 4) return LEFT_VIDEO_POST_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 5) return RIGHT_VIDEO_POST_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 6) return POST_UNAVAILABLE_LEFT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 7) return POST_UNAVAILABLE_RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 10) return LEFT_IMAGE_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 11) return RIGHT_IMAGE_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 15) return LEFT_SHARE_STORY_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 16) return RIGHT_SHARE_STORY_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 17) return LEFT_SHARE_STORY_UNAVAILABLE_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 18) return RIGHT_SHARE_STORY_UNAVAIALAVLE_VIEW_HOLDER
        return -1
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }
}