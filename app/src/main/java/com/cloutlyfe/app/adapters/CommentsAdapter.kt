package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.activities.ReplyActivity
import com.cloutlyfe.app.interfaces.CommentLoadMoreListener
import com.cloutlyfe.app.interfaces.LikeCommentInterface
import com.cloutlyfe.app.model.DataXX
import com.cloutlyfe.app.utils.Constants
import com.google.gson.Gson
import java.util.ArrayList

class CommentsAdapter(
    var activity: Activity?,
    var commentsList: ArrayList<DataXX>,
    var mLoadMoreScrollListner: CommentLoadMoreListener,
    var mLikeClickListener: LikeCommentInterface,
    var mPostId: String,
    var mOtheruserId: String?
): RecyclerView.Adapter<CommentsAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_comments, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = commentsList?.get(position)
        if(mModel!!.username=="N/A" || mModel.username==""|| mModel.username==null){
            holder.txtNameTV.text=mModel!!.name
        }
        else{
            holder.txtNameTV.text=mModel!!.username}

        holder.commentsTV.text=mModel.comment
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//

        Log.e("photo", mModel.photo)
        Glide.with(activity!!).load(mModel.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((holder.profileIV))
        if ((commentsList?.get(position)?.isLike)!! == 0) {
            holder.heartIV.setImageResource(R.drawable.ic_like)
        } else if ((commentsList?.get(position)?.isLike)!! == 1){
            holder.heartIV.setImageResource(R.drawable.ic_comment_like)
        }
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }
//        holder.txtReplyTV.setOnClickListener {
////            val context=holder.txtReplyTV.context
////            val intent = Intent( context, ChatActivity::class.java)
////            context.startActivity(intent)
//            mChatItemClickListner.onItemClickListner(mModel)
//        }
        if (commentsList!!.size % 10 == 0 && commentsList!!.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(commentsList)
        }


        if(mOtheruserId== AppPrefrences().readString(activity!!,Constants.ID,null)||AppPrefrences().readString(activity!!,Constants.ID,null)==commentsList[position].comment_by){
            holder.deleteIV.visibility=View.VISIBLE
        }
        else{
            holder.deleteIV.visibility=View.GONE
        }

//        if(AppPrefrences().readString(activity!!,Constants.ID,null)==commentsList[position].comment_by){
//            holder.deleteIV.visibility=View.VISIBLE
//        }
//        else{
//            holder.deleteIV.visibility=View.GONE
//        }

        holder.deleteRL.setOnClickListener(View.OnClickListener {
            mLikeClickListener.onItemClickListner(holder.adapterPosition,commentsList.get(position).comment_id,holder.deleteIV,commentsList[position].post_id,commentsList[position].comment_by)
        })

        holder.txtReplyTV.setOnClickListener(View.OnClickListener {
            val i = Intent(activity, ReplyActivity::class.java)
            val gson = Gson()
            val mModell = gson.toJson(mModel)
            i.putExtra("CommentsData",mModell)
            activity!!.startActivity(i)
        })

        holder.itemView.setOnClickListener {
            if(AppPrefrences().readString(activity!!,Constants.ID,null)==commentsList[position].comment_by){
                val i = Intent(activity, OtherUserProfileActivity::class.java)
                i.putExtra("other_user_id", mModel.comment_by)
                if(mModel!!.username=="N/A" || mModel.username==""|| mModel.username==null){
                    i.putExtra("name", mModel.username)
                }
                else{
                    i.putExtra("name", mModel.username)}


                i.putExtra("value", "logged_in_user")
                activity!!.startActivity(i)
            }
            else{
                val i = Intent(activity, OtherUserProfileActivity::class.java)
                i.putExtra("other_user_id", mModel.comment_by)
                if(mModel!!.username=="N/A" || mModel.username==""|| mModel.username==null){
                    i.putExtra("name", mModel.name)
                }
                else{
                    i.putExtra("name", mModel.username)}

                i.putExtra("value", "other_user")
                activity!!.startActivity(i)
            }

        }
    }

    override fun getItemCount(): Int {
        return commentsList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileIV: ImageView = itemView.findViewById(R.id.profileIV)
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var commentsTV: TextView =itemView.findViewById(R.id.commentsTV)
        var txtReplyTV : TextView=itemView.findViewById(R.id.txtReplyTV)
        var heartIV : ImageView = itemView.findViewById(R.id.heartIV)
        var deleteRL : RelativeLayout = itemView.findViewById(R.id.deleteRL)
        var deleteIV : ImageView = itemView.findViewById(R.id.deleteIV)
    }

}