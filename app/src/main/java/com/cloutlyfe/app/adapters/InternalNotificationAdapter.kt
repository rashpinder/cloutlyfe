package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.ChatActivity
import com.cloutlyfe.app.activities.CommentsActivity
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.LocationtemClickListner
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.Notification
import com.cloutlyfe.app.model.NotificationData
import com.cloutlyfe.app.model.PredictionsItem
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.TimeAgo2
import com.google.gson.Gson
import java.security.AccessController.getContext
import java.text.SimpleDateFormat
import java.util.*

class InternalNotificationAdapter(val mActivity: Activity,
                          var mArrayList: ArrayList<Notification>) :
    RecyclerView.Adapter<InternalNotificationAdapter.MyViewHolder>() {
    var mNotificationModel:HomeData?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.internal_item_notification, null)
        return MyViewHolder(view)
    }
    var nameStr:String?=""
    var usernamee:String?=""
    var namee:String?=""

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mNotificationItem : Notification = mArrayList!!.get(position)!!
        if(mNotificationItem.username=="N/A" || mNotificationItem.username==""|| mNotificationItem.username==null){
            nameStr= mNotificationItem.name
        }
        else{
            nameStr= mNotificationItem.username}

//        var messageStr= mNotificationItem.message
//        var part1:String=""
//        var part2:String=""
//        if(messageStr.contains(nameStr!!)){
//            val s = messageStr
//            val parts = s.split(nameStr!!.toRegex()).toTypedArray() // escape .
//             part1 = parts[0]
//             part2 = parts[1]
//        }

        var sourceString ="<b>"+ nameStr +"</b>"+" "+mNotificationItem.message
        holder.txtNameTV!!.text =Html.fromHtml(sourceString)
//        holder.txtNameTV.setText(mNotificationItem.name)
//        holder.txtMessageTV.setText(mNotificationItem.message)
        val Timestamp: Long = mNotificationItem!!.creation_date!!.toLong()
        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("MMM dd, yyyy hh:mm:ss a")
        val sdf = SimpleDateFormat("hh:mm a")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
//        holder.txtTimeTV.setText(Time)
//        val reference_time = mNotificationItem.message_time
        val reference_time = Time
//        val timeAgo2 = TimeAgo2()
//        val MyFinalValue = timeAgo2.covertTimeToText(reference_time)
        holder.txtTimeTV.text = Time
        Glide.with(mActivity).load(mNotificationItem.image)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(holder.profileIV!!)

        holder.itemView.setOnClickListener {
            if(mNotificationItem.is_delete==0){
                usernamee=mNotificationItem.username

                if(usernamee=="N/A" || usernamee==""|| usernamee==null){
                    namee=mNotificationItem.name
                }
                else{
                 namee=mNotificationItem.username
                }
            println("notification type : "+mNotificationItem?.notification_type)
            if (mNotificationItem?.notification_type == "0") {

                mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                    namee!!,"",
                    mNotificationItem.post_id,null,
                    mNotificationItem.otherID,"","",false,0,0,0,
                    mNotificationItem.notification_type!!,mNotificationItem.room_id,mNotificationItem.otherID,"",mNotificationItem.notification_id,mNotificationItem.creation_date,mNotificationItem.message,mNotificationItem.followID,mNotificationItem.title_message,mNotificationItem.status_id,mNotificationItem.notification_read_status,mNotificationItem.viewed_status,mNotificationItem.is_delete,"","","","","")
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mNotificationModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                mActivity.startActivity(i)
            }

            if (mNotificationItem?.notification_type == "1") {
                mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                    namee!!,"",
                    mNotificationItem.post_id,null,
                    mNotificationItem.otherID,"","",false,0,0,0,
                    mNotificationItem.notification_type!!,mNotificationItem.room_id,mNotificationItem.otherID,"",mNotificationItem.notification_id,mNotificationItem.creation_date,mNotificationItem.message,mNotificationItem.followID,mNotificationItem.title_message,mNotificationItem.status_id,mNotificationItem.notification_read_status,mNotificationItem.viewed_status,mNotificationItem.is_delete,"","","","","")
                val i = Intent(mActivity, OtherUserProfileActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mNotificationModel)
                i.putExtra("homeData", mModell)
                i.putExtra("value", "other_user")
                mActivity.startActivity(i)
            }

            if (mNotificationItem?.notification_type == "2") {
                // FollowRequestAppove
//                AppPrefrences().writeString(mActivity!!, OTHERUSERID,mModel?.otherID)
//                var mIntent : Intent = Intent(mActivity, OtherUserProfileActivity::class.java)
//                mActivity!!.startActivity(mIntent)
            }

            if (mNotificationItem?.notification_type == "3") {
//                AppPrefrences().writeString(mActivity!!, OTHERUSERID,mModel?.otherID)
//                var mIntent : Intent = Intent(mActivity, OtherUserProfileActivity::class.java)
//                mActivity!!.startActivity(mIntent)
            }

            if (mNotificationItem?.notification_type == "5") {
                var mIntent = Intent(mActivity, ChatActivity::class.java)
                mIntent!!.putExtra(Constants.ROOM_ID, mNotificationItem!!.room_id)
                mIntent!!.putExtra(Constants.USER_NAME, namee)
                mActivity.startActivity(mIntent)
            }

            if (mNotificationItem?.notification_type == "") {
//                var mAllUsersItem: AllUsersItem? = AllUsersItem(mModel?.roomId, "","", mModel?.otherID, mModel?.image,"","","", mModel?.userId,mModel?.username)
//                var mIntent = Intent(mActivity, ChatActivity::class.java)
//                mIntent!!.putExtra(MODEL, mAllUsersItem)
//                mIntent!!.putExtra("User_profile_pic", mModel?.image)
//                mActivity!!.startActivity(mIntent)
            }

            if (mNotificationItem?.notification_type == "6") {
                mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                    namee!!,"",
                    mNotificationItem.post_id,null,
                    mNotificationItem.user_id,"","",false,0,0,0,
                    mNotificationItem.notification_type!!,mNotificationItem.room_id,mNotificationItem.otherID,"",mNotificationItem.notification_id,mNotificationItem.creation_date,mNotificationItem.message,mNotificationItem.followID,mNotificationItem.title_message,mNotificationItem.status_id,mNotificationItem.notification_read_status,mNotificationItem.viewed_status,mNotificationItem.is_delete,"","","","","")
                val i = Intent(mActivity, CommentsActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mNotificationModel)
                i.putExtra("post_id", mNotificationItem.post_id)
                i.putExtra("user_id", mNotificationItem.user_id)
                i.putExtra("value", "other_user")
                i.putExtra("homeData", mModell)
                mActivity.startActivity(i)
            }

            if (mNotificationItem?.notification_type == "7") {
                mNotificationModel = HomeData("","","","",null,0,0,"","",null,"","",
                    namee!!,"",
                    mNotificationItem.post_id,null,
                    mNotificationItem.user_id,"","",false,0,0,0,
                    mNotificationItem.notification_type!!,mNotificationItem.room_id,mNotificationItem.otherID,"",mNotificationItem.notification_id,mNotificationItem.creation_date,mNotificationItem.message,mNotificationItem.followID,mNotificationItem.title_message,mNotificationItem.status_id,mNotificationItem.notification_read_status,mNotificationItem.viewed_status,mNotificationItem.is_delete,"","","","","")
                val i = Intent(mActivity, CommentsActivity::class.java)
                val gson = Gson()
                val mModell = gson.toJson(mNotificationModel)
                i.putExtra("homeData", mModell)
                i.putExtra("post_id", mNotificationItem.post_id)
                i.putExtra("user_id", mNotificationItem.user_id)
                i.putExtra("value", "other_user")
                mActivity.startActivity(i)
            }

        }
        else{
            Toast.makeText(mActivity, "This user is no longer available", Toast.LENGTH_LONG).show()
        }
    }}

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtNameTV: TextView
        var txtTimeTV: TextView
        var profileIV: ImageView
        var txtMessageTV: TextView

        init {
            txtNameTV = itemView.findViewById(R.id.txtNameTV)
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV)
            txtMessageTV = itemView.findViewById(R.id.txtMessageTV)
            profileIV = itemView.findViewById(R.id.profileIV)
        }
    }
}
