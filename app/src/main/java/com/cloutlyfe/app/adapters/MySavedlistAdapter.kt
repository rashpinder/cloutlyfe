package com.cloutlyfe.app.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.AllPostsActivity
import com.cloutlyfe.app.interfaces.ProfilePostsLoadMoreListener
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.ProfilePostsData
import java.util.ArrayList

//var ChatDetailList: ArrayList<ChatDataItem?>?,
//var mChatItemClickListner: ChatItemClickListner

class MySavedlistAdapter(
//    var list: MutableList<PostItem>
    private val mActivity: FragmentActivity,
    var dataList: ArrayList<HomeData>,
    var mLoadMoreScrollListner: ProfilePostsLoadMoreListener,
    var mType: String
): RecyclerView.Adapter<MySavedlistAdapter.ListViewHolder>()  {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)
        var imgVideoIV: ImageView = itemView.findViewById(R.id.imgVideoIV)
        var imgPicIV: ImageView = itemView.findViewById(R.id.imgPicIV)

        fun setPostImage(postItem: HomeData) {
         if(postItem.image!!.size>1){
                if (postItem.image[0].contains(".mp4")||postItem.image[0].contains("video")){
                    Glide.with(mActivity).load(postItem.thumbnail_image!![0])
                        .placeholder(R.drawable.ic_post_ph)
                        .error(R.drawable.ic_post_ph)
                        .into(imgImageIV)
                    imgVideoIV.visibility=View.GONE
                }
                else{
                        Glide.with(mActivity).load(postItem.image[0])
                            .placeholder(R.drawable.ic_post_ph)
                            .error(R.drawable.ic_post_ph)
                            .into(imgImageIV)

                    imgVideoIV.visibility=View.GONE
                    imgPicIV.visibility=View.VISIBLE
                }

            } else{
             if (postItem.image[0].contains(".mp4")||postItem.image[0].contains("video")){
                 Glide.with(mActivity).load(postItem.thumbnail_image!![0])
                     .placeholder(R.drawable.ic_post_ph)
                     .error(R.drawable.ic_post_ph)
                     .into(imgImageIV)
                 imgVideoIV.visibility=View.GONE

             }
             else{
                 Glide.with(mActivity).load(postItem.image[0])
                     .placeholder(R.drawable.ic_post_ph)
                     .error(R.drawable.ic_post_ph)
                     .into(imgImageIV)
                 imgVideoIV.visibility=View.GONE
             }
         }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_my_saved_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        if (dataList.size>0 ||dataList.isNotEmpty()){
            if(dataList[position]!=null){
        holder.setPostImage(dataList[position])}
        if(dataList[0].post_id==dataList[1].post_id){
            if (position==1){
            holder.itemView.visibility=View.GONE}
        }}
        else{

        }
//        var mModel  = ChatDetailList?.get(position)
//        holder.txtNameTV.text=ChatDetailList?.get(position)!!.name
//        holder.commentsTV.text=ChatDetailList?.get(position)!!.lastMessage
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
//        Glide.with(activity!!).load(ChatDetailList?.get(position)?.profileImage)
//            .placeholder(R.drawable.ic_placeholder)
//            .error(R.drawable.ic_placeholder)
//            .into((holder.profileIV))
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }

        holder.imgImageIV.setOnClickListener {
            var context=holder.imgImageIV.context
            var intent = Intent( context, AllPostsActivity::class.java)
            intent.putExtra("post_id",dataList[position].post_id)
            intent.putExtra("user_id",dataList[position].user_id)
            intent.putExtra("type",mType)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

//    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)
//    }

}