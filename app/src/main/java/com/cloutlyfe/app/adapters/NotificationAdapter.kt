package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.Notification
import com.cloutlyfe.app.model.NotificationData
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(val mActivity: Activity,
                           var mArrayList: ArrayList<Notification>,
                           var mList: ArrayList<NotificationData>,) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {
var mInternalAdapter:InternalNotificationAdapter?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_notification, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mNotificationItem : NotificationData = mList.get(position)
        if(mList.size>0){
        holder.txtNameTV.setText(mNotificationItem.day)}
        else{
            holder.txtNameTV.visibility=View.GONE
        }
       setAdapter(holder,position)
        holder.itemView.setOnClickListener{
//            mLocationtemClickListner.onItemClickListner(mNotificationItem)
        }
    }

    private fun setAdapter(holder: MyViewHolder, position: Int) {
        holder.notifcationRV.layoutManager = LinearLayoutManager(mActivity)
        mInternalAdapter = InternalNotificationAdapter(mActivity,mList[position].notification)
        holder.notifcationRV.adapter = mInternalAdapter
    }

    override fun getItemCount(): Int {
            return mList.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtNameTV: TextView
        var notifcationRV: RecyclerView

        init {
            txtNameTV = itemView.findViewById(R.id.txtNameTV)
            notifcationRV = itemView.findViewById(R.id.notifcationRV)
        }
    }
    }