package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.LiveDataModel
import com.makeramen.roundedimageview.RoundedImageView

class ItemCLoutVisibleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var imgImageIV: RoundedImageView = itemView.findViewById(R.id.imgImageIV)

    fun bindData(
        mActivity: Activity?,
        mData: ArrayList<LiveDataModel>,
        listener: CloutHomeAdapter.onItemClickClickListener?,
        position: Int
    ) {
            Glide.with(mActivity!!).load(mData[position].photo)
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into(imgImageIV)

        imgImageIV.setOnClickListener {
            listener!!.onItemClick(position,mData,imgImageIV)
        }

    }

}