package com.cloutlyfe.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cloutlyfe.app.R;
import com.cloutlyfe.app.model.LiveViewersModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LiveVieweradapter extends RecyclerView.Adapter<LiveVieweradapter.ViewHolder> {
    Activity mActivity;
    List<LiveViewersModel.DataL> liveUsers;
    private OnItemClickListener listener;


    public LiveVieweradapter(Activity mActivity, List<LiveViewersModel.DataL> liveUsers, OnItemClickListener listener) {
        this.mActivity=mActivity;
        this.liveUsers=liveUsers;
        this.listener = listener;

    }

    public interface OnItemClickListener {
        void onItemClick(int positon, LiveViewersModel.DataL item, View view);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.live_viewers_item, parent, false);
        return new ViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LiveViewersModel.DataL mModel=liveUsers.get(position);
        holder.bind(position,mModel,listener);
        holder.txUsernameViewer.setText(mModel.getName());
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(mActivity).load(mModel.getPhoto()).apply(options).into(holder.imUserPic);


    }


    @Override
    public int getItemCount() {
        return liveUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imUserPic;
        TextView txUsernameViewer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imUserPic=itemView.findViewById(R.id.imUserPicViewer);
            txUsernameViewer=itemView.findViewById(R.id.txUsernameViewer);

        }

        public void bind(int position, LiveViewersModel.DataL mModel, OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

            txUsernameViewer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

            imUserPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

        }
    }
}

