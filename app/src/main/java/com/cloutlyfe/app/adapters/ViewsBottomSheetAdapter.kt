package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.activities.SettingsActivity
import com.cloutlyfe.app.interfaces.ProfileClickInterface
import com.cloutlyfe.app.model.StatusViewedData
import com.cloutlyfe.app.utils.Constants
import de.hdodenhof.circleimageview.CircleImageView

class ViewsBottomSheetAdapter(
    var activity: Activity?,
    var data_list: ArrayList<StatusViewedData>?,
    var mItemClickListener: ProfileClickInterface?
): RecyclerView.Adapter<ViewsBottomSheetAdapter.MyViewHolder>()  {
var namee:String?=""
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_story_view, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = data_list!!.get(position)
        Glide.with(activity!!).load(mModel.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(holder.profileIV)
        if(mModel.username=="N/A" ||  mModel.username==""|| mModel.username==null){
            holder.txtNameTV.text = mModel.name
            namee=mModel.name
        }
        else{
            holder.txtNameTV.text= mModel.username
            namee=mModel.username
        }


        holder.itemView.setOnClickListener {
            mItemClickListener!!.onItemClickListner(position,"other_user",mModel.user_id, namee!!)
//            val i = Intent(activity, OtherUserProfileActivity::class.java)
//            i.putExtra("value", "other_user")
//            i.putExtra("other_user_id", mModel.user_id)
//            i.putExtra("name", mModel.name)
//            activity!!.startActivity(i)

        }
    }

    override fun getItemCount(): Int {
        return data_list!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var profileIV: CircleImageView = itemView.findViewById(R.id.profileIV)
    }

}