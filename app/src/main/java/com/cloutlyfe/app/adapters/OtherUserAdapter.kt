package com.cloutlyfe.app.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.AllPostsActivity
import com.cloutlyfe.app.activities.SavedActivity
import com.cloutlyfe.app.model.PostItem

//var ChatDetailList: ArrayList<ChatDataItem?>?,
//var mChatItemClickListner: ChatItemClickListner

class OtherUserAdapter(
//    var list: MutableList<PostItem>
    private val postItems: List<PostItem>
): RecyclerView.Adapter<OtherUserAdapter.ListViewHolder>()  {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)

        fun setPostImage(postItem: PostItem) {
//            imgImageIV.setImageResource(postItem.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_my_saved_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.setPostImage(postItems[position])
//        var mModel  = ChatDetailList?.get(position)
//        holder.txtNameTV.text=ChatDetailList?.get(position)!!.name
//        holder.commentsTV.text=ChatDetailList?.get(position)!!.lastMessage
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
//        Glide.with(activity!!).load(ChatDetailList?.get(position)?.profileImage)
//            .placeholder(R.drawable.ic_placeholder)
//            .error(R.drawable.ic_placeholder)
//            .into((holder.profileIV))
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }

        holder.imgImageIV.setOnClickListener {
            var context=holder.imgImageIV.context
            var intent = Intent( context, AllPostsActivity::class.java)
            intent.putExtra("value","other_user")
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return postItems.size
    }

//    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)
//    }

}