package com.cloutlyfe.app.adapters

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.UnfollowClickInterface
import com.cloutlyfe.app.model.LikeUsersData
import com.cloutlyfe.app.utils.Constants
import com.google.gson.Gson

class LikesListAdapter(
    var context: Context,
    var list: ArrayList<LikeUsersData>,
    var mFollowClickListener: UnfollowClickInterface
) : RecyclerView.Adapter<LikesListAdapter.ListViewHolder>() {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileIV: ImageView = itemView.findViewById(R.id.profileIV)
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var txtDescTV: TextView = itemView.findViewById(R.id.txtDescTV)
        var txtCloutTV: TextView = itemView.findViewById(R.id.txtCloutTV)

        fun setData(item: ArrayList<LikeUsersData>, position: Int) {
            if(item[position].username=="N/A" || item[position].username==""|| item[position].username==null){
                txtNameTV.text=item[position].name
            }
            else{
                txtNameTV.text=item[position].username}

//            txtDescTV.text=item[position].username
            Glide.with(context).load(item[position].photo)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into((profileIV))
            if(AppPrefrences().readString(context,Constants.ID,null)!=item[position].user_id){
            if (item[position].changeStatus == "0") {
                    Log.e(TAG, "setData: "+(AppPrefrences().readString(context,Constants.ID,null)))
                txtCloutTV.text = "Clout"
            } else {
                txtCloutTV.text = "Clouting"
            }}
            else{
                txtCloutTV.visibility=View.GONE
            }
            itemView.setOnClickListener {
                if (item[position].user_id == AppPrefrences().readString(
                        context,
                        Constants.ID,
                        null
                    )
                ) {
                    val i = Intent(context, OtherUserProfileActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(item[position])
                    i.putExtra("homeData", mModell)
                    i.putExtra("value", "logged_in_user")
                    i.putExtra("other_user_id",AppPrefrences().readString(
                        context,
                        Constants.ID,
                        null
                    ))
                    if(AppPrefrences().readString(context,Constants.USERNAME,null).isNullOrEmpty()){
                        i.putExtra("name",AppPrefrences().readString(
                            context,
                            Constants.NAME,
                            null
                        ))
                    }
                    else{
                        i.putExtra("name",AppPrefrences().readString(
                            context,
                            Constants.USERNAME,
                            null
                        ))
                    }


                    context.startActivity(i)
//                val i = Intent(context, HomeActivity::class.java)
//                val gson = Gson()
//                val mModell = gson.toJson(mModel)
//                i.putExtra("homeData",mModell)
//                i.putExtra("value","logged_in_user")
//                context.startActivity(i)
                } else {
                    val i = Intent(context, OtherUserProfileActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(item[position])
                    i.putExtra("homeData", mModell)
                    i.putExtra("value", "other_user")
                    i.putExtra("other_user_id", item[position].user_id)
                    if(item[position].username=="N/A" || item[position].username==""|| item[position].username==null){
                        i.putExtra("name",item[position].name)
                    }
                    else{
                        i.putExtra("name",item[position].username)}

                    context.startActivity(i)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_like, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.setData(list, position)

        holder.txtCloutTV.setOnClickListener {
            if(list[position].changeStatus=="0"){
                holder.txtCloutTV.text = "Clouting"
                list[position].changeStatus="1"
                notifyDataSetChanged()
                mFollowClickListener.onItemClickListner(position, list[position].user_id)
            }
            else{
                holder.txtCloutTV.text = "Clout"
                list[position].changeStatus="0"
                notifyDataSetChanged()
                mFollowClickListener.onItemClickListner(position, list[position].user_id)
            }
    }}

    override fun getItemCount(): Int {
        return list.size
    }


}