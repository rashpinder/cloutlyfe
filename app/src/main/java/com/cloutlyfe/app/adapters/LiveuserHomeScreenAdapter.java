package com.cloutlyfe.app.adapters;

import static com.cloutlyfe.app.utils.Constants.CLOUT_INVISIBLE_VIEW_HOLDER;
import static com.cloutlyfe.app.utils.Constants.CLOUT_VISIBLE_VIEW_HOLDER;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cloutlyfe.app.R;
import com.cloutlyfe.app.model.LiveDataModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class LiveuserHomeScreenAdapter extends RecyclerView.Adapter<LiveuserHomeScreenAdapter.ViewHolder> {
    ArrayList<LiveDataModel> mArrayList;
   Context context;
   private  onItemClickClickListener listener;

    public interface  onItemClickClickListener
    {
        void onItemClick(int position, ArrayList<LiveDataModel> mArrayList, View view);
    }


    public LiveuserHomeScreenAdapter(ArrayList<LiveDataModel> mArraylist, Context context, onItemClickClickListener listener) {
   this.mArrayList=mArraylist;
   this.context=context;
   this.listener=listener;
    }



    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_clout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        LiveDataModel mModel = mArrayList.get(position);
   Glide.with(context).load(mModel.getPhoto()).placeholder(R.drawable.ic_profile_ph).into(holder.imgImageIV);
    holder.bind(position,mArrayList,listener);
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgImageIV;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imgImageIV=itemView.findViewById(R.id.imgImageIV);
        }

        public void bind(int position, ArrayList<LiveDataModel> mArrayList, onItemClickClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });

            imgImageIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mArrayList,v);
                }
            });


        }
    }
}
