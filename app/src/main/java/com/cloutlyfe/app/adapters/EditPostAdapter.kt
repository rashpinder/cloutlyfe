package com.cloutlyfe.app.adapters

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.OptionsInterface
import com.cloutlyfe.app.model.DataOfPost
import kotlin.collections.ArrayList

class EditPostAdapter(
    var activity: Activity?,
    var list: ArrayList<Uri>,
    var mPreviousPostsList: ArrayList<DataOfPost>,
    var mOptionsClickListener: OptionsInterface
) : RecyclerView.Adapter<EditPostAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_create_post, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            Glide.with(activity!!).load(list[position])
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into((holder.imgPostIV))

        holder.imgCrossIV.setOnClickListener {
            mOptionsClickListener.onItemClickListner(position,mPreviousPostsList[0].post_data.get(position).image_id)
        }
    }

    override fun getItemCount(): Int {
            return list.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPostIV: ImageView = itemView.findViewById(R.id.imgPostIV)
        var imgCrossIV: TextView = itemView.findViewById(R.id.imgCrossIV)
    }

}