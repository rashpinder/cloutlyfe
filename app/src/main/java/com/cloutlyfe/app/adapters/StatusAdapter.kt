package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OthersStatusActivity
import com.cloutlyfe.app.activities.ScreenSlidePagerActivity
import com.cloutlyfe.app.model.StoriesData
import com.cloutlyfe.app.utils.Constants.Companion.MODEL
import de.hdodenhof.circleimageview.CircleImageView

class StatusAdapter(val mActivity: Activity?, val statusStoriesModel: StoriesData) :
    RecyclerView.Adapter<StatusAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_stories_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(mActivity!!).load(statusStoriesModel.stories!![position]?.user?.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(holder.imgImageIV)
        for(i in statusStoriesModel.stories.get(position).snaps.indices){
            if(statusStoriesModel.stories.get(position).snaps[i].is_viewed=="0"){
                holder.mainLayout.setBackground(ContextCompat.getDrawable(mActivity,R.drawable.ic_story_bg))
            }
            else{
                holder.mainLayout.setBackground(ContextCompat.getDrawable(mActivity,R.drawable.ic_grey_story_bg))
            }
        }
//        if(statusStoriesModel.stories!![position]?.all_viewed_count=="0"){
//            holder.mainLayout.setBackground(ContextCompat.getDrawable(mActivity,R.drawable.ic_story_bg))
//        }
//        else{
//            holder.mainLayout.setBackground(ContextCompat.getDrawable(mActivity,R.drawable.ic_grey_story_bg))
//        }

        holder.itemView.setOnClickListener {
//            val intent = Intent(mActivity, ScreenSlidePagerActivity::class.java)
            val intent = Intent(mActivity, OthersStatusActivity::class.java)
            intent.putExtra("model", ArrayList(statusStoriesModel.stories.get(position).snaps))
            intent.putExtra("snap_count", (statusStoriesModel.stories.get(position).snaps_count))
            if(statusStoriesModel.stories.get(position).user.username=="N/A" ||  statusStoriesModel.stories.get(position).user.username==""|| statusStoriesModel.stories.get(position).user.username==null){
                intent.putExtra("username", statusStoriesModel.stories[position].user.name)
            }
            else{
                intent.putExtra("username", statusStoriesModel.stories[position].user.username)
            }
            intent.putExtra("photo", statusStoriesModel.stories[position].user.photo)
            intent.putExtra("user_id", statusStoriesModel.stories[position].user.user_id)
            intent.putExtra("all_viewed_count", statusStoriesModel.stories[position].all_viewed_count)
            intent.putExtra("position",position.toString())
           mActivity.startActivity(intent)
        }
    }
    override fun getItemCount(): Int {
        return statusStoriesModel.stories!!.size
    }
    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val imgImageIV: CircleImageView = itemView.findViewById(R.id.imgImageIV)
        val mainLayout: RelativeLayout = itemView.findViewById(R.id.mainLayout)
    }
}

