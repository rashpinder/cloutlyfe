package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.AppPrefrences
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.CloutsLoadMoreListener
import com.cloutlyfe.app.interfaces.UnfollowClickInterface
import com.cloutlyfe.app.model.DataXXX
import com.cloutlyfe.app.utils.Constants
import java.util.ArrayList

//var ChatDetailList: ArrayList<ChatDataItem?>?,
//var mChatItemClickListner: ChatItemClickListner

class CloutersAdapter(
    var activity: Activity?,
    var cloutsList: ArrayList<DataXXX>,
    var mLoadMoreScrollListner: CloutsLoadMoreListener,
    var mUnfollowClickListener: UnfollowClickInterface,
    var strotherid: String?
): RecyclerView.Adapter<CloutersAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_clouters_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel=cloutsList[position]
        if(mModel.username=="N/A" || mModel.username==""|| mModel.username==null){
            holder.txtNameTV.text=cloutsList?.get(position)!!.name
        }
        else{
            holder.txtNameTV.text=cloutsList?.get(position)!!.username}


        Glide.with(activity!!).load(cloutsList?.get(position)?.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((holder.profileIV))
        if(!strotherid.isNullOrEmpty()){
            holder.txtUnfollowTV.visibility=View.GONE

        }
        else{
            holder.txtUnfollowTV.visibility=View.VISIBLE
        }
//        var mModel  = ChatDetailList?.get(position)
//        holder.txtNameTV.text=ChatDetailList?.get(position)!!.name
//        holder.commentsTV.text=ChatDetailList?.get(position)!!.lastMessage
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
//        Glide.with(activity!!).load(ChatDetailList?.get(position)?.profileImage)
//            .placeholder(R.drawable.ic_placeholder)
//            .error(R.drawable.ic_placeholder)
//            .into((holder.profileIV))
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }
        holder.txtUnfollowTV.setOnClickListener {
//            val context=holder.txtReplyTV.context
//            val intent = Intent( context, ChatActivity::class.java)
//            context.startActivity(intent)
            mUnfollowClickListener.onItemClickListner(position,
                cloutsList?.get(position)?.user_id!!
            )
        }
        holder.itemView.setOnClickListener {
            if(mModel.user_id== AppPrefrences().readString(activity!!,Constants.ID,null)){
                Log.e("TAG", "onBindViewHolder: "+mModel.user_id+""+ AppPrefrences().readString(activity!!,Constants.ID,null).toString())
                val i = Intent(activity, OtherUserProfileActivity::class.java)
                i.putExtra("other_user_id", mModel.user_id)
                i.putExtra("value","logged_in_user")
                if(mModel.username=="N/A" || mModel.username==""|| mModel.username==null){
                    i.putExtra("name", mModel.name)
                }
                else{
                    i.putExtra("name", mModel.username)}


                activity!!.startActivity(i)
            }
            else{
            val i = Intent(activity, OtherUserProfileActivity::class.java)
            i.putExtra("other_user_id", mModel.user_id)
            i.putExtra("value","other_id")
                if(mModel.username=="N/A" || mModel.username==""|| mModel.username==null){
                    i.putExtra("name", mModel.name)
                }
                else{
                    i.putExtra("name", mModel.username)}
            activity!!.startActivity(i)
        }}
    }

    override fun getItemCount(): Int {
        return cloutsList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileIV: ImageView = itemView.findViewById(R.id.profileIV)
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var txtUnfollowTV: TextView =itemView.findViewById(R.id.txtUnfollowTV)
    }

}