package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.ItemClicklistenerInterface
import com.cloutlyfe.app.model.SearchData
import com.google.gson.Gson

class HomeSearchAdapter(
    var activity: Activity?,
    var data_list: MutableList<SearchData>,
    var mItemClickListener: ItemClicklistenerInterface
): RecyclerView.Adapter<HomeSearchAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_home_search, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = data_list.get(position)
        if(mModel!!.username=="N/A" || mModel.username==""|| mModel.username==null){
            holder.txtNameTV.text= mModel.name
        }
        else{
            holder.txtNameTV.text= mModel.username}



        Glide.with(activity!!).load(mModel.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((holder.profileIV))

        holder.imgCancelIV.setOnClickListener {
            mItemClickListener.onItemClickListner(position, mModel.user_id)
        }

        holder.itemView.setOnClickListener {
            val i = Intent(activity, OtherUserProfileActivity::class.java)
            val gson = Gson()
            val mModell = gson.toJson(mModel)
            i.putExtra("homeData",mModell)
            i.putExtra("value","other_user")
            activity!!.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return data_list.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileIV: ImageView = itemView.findViewById(R.id.profileIV)
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var imgCancelIV: ImageView =itemView.findViewById(R.id.imgCancelIV)
    }

}