package com.cloutlyfe.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.StreamViewActivity
import com.cloutlyfe.app.model.DataItem
import java.util.*

class StreamRecHomeScreenAdapter(var mArrayList: ArrayList<DataItem>,val mActivity: Activity) :
    RecyclerView.Adapter<StreamRecHomeScreenAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_recorded_stream, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mDataItem : DataItem = mArrayList.get(position)

        Glide.with(mActivity).load(mDataItem.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .into(holder.imgImageIV)

        holder.itemView.setOnClickListener{
            val intent = Intent(mActivity, StreamViewActivity::class.java)
            intent.putExtra("videoUrl", mDataItem.getVideoLink())
            mActivity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgImageIV: ImageView

        init {
            imgImageIV = itemView.findViewById(R.id.imgImageIV)
        }
    }
}

