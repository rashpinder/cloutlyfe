package com.cloutlyfe.app.adapters

import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.interfaces.ChatItemClickListner
import com.cloutlyfe.app.interfaces.DeleteChatInterface
import com.cloutlyfe.app.model.AllUser
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ChatListAdapter(
    var activity: FragmentActivity?,
    var ChatDetailList: ArrayList<AllUser>,
    var mChatItemClickListner: ChatItemClickListner,
    var delInterface: DeleteChatInterface? = null
    ): RecyclerView.Adapter<ChatListAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.chat_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       var mModel  = ChatDetailList.get(position)
        if(mModel.is_delete==1){
            holder.chatNameTV.text= "This user is no longer available"
            holder.timeTV.visibility=View.GONE

            Glide.with(activity!!).load(R.drawable.ic_profile_ph)
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .into((holder.chatProfileIV))
            holder.badgeRL.visibility=View.GONE
            holder.chatMessageTV.visibility=View.GONE
        }
        else{
            if(ChatDetailList.get(position).username=="N/A"||ChatDetailList.get(position).username==""||ChatDetailList.get(position).username==null){
                holder.chatNameTV.text= ChatDetailList.get(position).name
            }
            else{
                holder.chatNameTV.text= ChatDetailList.get(position).username
            }

            val Timestamp: Long = ChatDetailList.get(position).message_time.toLong()
            val timeD = Date(Timestamp * 1000)
            val sdf = SimpleDateFormat("hh:mm a")
            sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
            val Time: String = sdf.format(timeD)
            holder.timeTV.text=Time


            if(ChatDetailList.get(position).other_blocked=="1"){
                if(ChatDetailList.get(position).username=="N/A"||ChatDetailList.get(position).username==""||ChatDetailList.get(position).username==null){
                    holder.chatMessageTV.text= ChatDetailList.get(position).name+" "+"has blocked you!"
                }
                else{
                    holder.chatMessageTV.text= ChatDetailList.get(position).username+" "+"has blocked you!"
                }


                holder.chatMessageTV.setTextColor(ContextCompat.getColor(activity!!,R.color.textColorGrey))
                holder.timeTV.visibility=View.GONE
                holder.badgeRL.visibility=View.GONE
                Glide.with(activity!!).load(R.drawable.ic_profile_ph)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((holder.chatProfileIV))
            }
            else if(ChatDetailList.get(position).is_blocked=="1"){
                if(ChatDetailList.get(position).username=="N/A"||ChatDetailList.get(position).username==""||ChatDetailList.get(position).username==null){
                    holder.chatMessageTV.text= "You Blocked"+" "+ChatDetailList.get(position).name+"!"
                }
                else{
                    holder.chatMessageTV.text= "You Blocked"+" "+ChatDetailList.get(position).username+"!"
                }

                holder.chatMessageTV.setTextColor(ContextCompat.getColor(activity!!,R.color.textColorGrey))
                holder.timeTV.visibility=View.GONE
                holder.badgeRL.visibility=View.GONE
                Glide.with(activity!!).load(R.drawable.ic_profile_ph)
                    .placeholder(R.drawable.ic_profile_ph)
                    .error(R.drawable.ic_profile_ph)
                    .into((holder.chatProfileIV))
            }
            else{
                if(ChatDetailList.get(position).unread_count=="0"||ChatDetailList.get(position).unread_count.equals(null)||ChatDetailList.get(position).unread_count.equals("")){
                if(ChatDetailList.get(position).message.isNullOrEmpty()){
                    holder.chatMessageTV.text= "shared an attachment"
                }
                    else{
                    holder.chatMessageTV.text= ChatDetailList.get(position).message
                }

                    holder.badgeRL.visibility=View.GONE
                    Glide.with(activity!!).load(ChatDetailList.get(position).photo)
                        .placeholder(R.drawable.ic_profile_ph)
                        .error(R.drawable.ic_profile_ph)
                        .into((holder.chatProfileIV))

                }
                else{
                    if(ChatDetailList.get(position).message.isNullOrEmpty()){
                        holder.chatMessageTV.text= "shared an attachment"
                    }
                    else{
                        val sourceString= "<b>"+ChatDetailList.get(position).message+"</b>"
                        holder.chatMessageTV.text= Html.fromHtml(sourceString)
                    }
                    holder.badgeRL.visibility=View.VISIBLE
                    holder.badgeTV.text= ChatDetailList.get(position).unread_count
                    Glide.with(activity!!).load(ChatDetailList.get(position).photo)
                        .placeholder(R.drawable.ic_profile_ph)
                        .error(R.drawable.ic_profile_ph)
                        .into((holder.chatProfileIV))

                }
            }

        }

        holder.chatListRL.setOnClickListener {
//            val context=holder.chatListRL.context
//            val intent = Intent( context, ChatActivity::class.java)
//            context.startActivity(intent)
            if(mModel.is_delete!=1){
            mChatItemClickListner.onItemClickListner(mModel)
        }}
        holder.chatProfileIV.setOnClickListener {
            if(mModel.is_delete!=1){
                val i = Intent(activity, OtherUserProfileActivity::class.java)
                i.putExtra("other_user_id", mModel.receiver_id)
                if(mModel.username=="N/A"||mModel.username==""||mModel.username==null){
                    i.putExtra("name", mModel.name)
                }
                else{
                    i.putExtra("name", mModel.username)
                }

                activity!!.startActivity(i)
            }


        }

        holder.txtDelete.setOnClickListener {
            delInterface!!.onItemClickListner(position, "", mModel.room_no)
//            showDeleteDialog(
//                mModel.room_no,position
//            )
        }
    }

    private fun showDeleteDialog(id: String, position: Int) {
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.edit_delete_dialog, null)

        var dialogDelete: BottomSheetDialog? =
            activity.let { BottomSheetDialog(it!!, R.style.BottomSheetDialog) }
        dialogDelete!!.setContentView(view)
        dialogDelete.setCanceledOnTouchOutside(true)
//disabling the drag down of sheet
        dialogDelete.setCancelable(true)
//cancel button click
        val deleteRL: RelativeLayout? = dialogDelete.findViewById(R.id.deleteRL)
        val editPostRL: RelativeLayout? = dialogDelete.findViewById(R.id.editPostRL)
        editPostRL!!.visibility = View.GONE

        deleteRL?.setOnClickListener {
            delInterface!!.onItemClickListner(position, "", id)
            dialogDelete.dismiss()
        }
        dialogDelete.show()
    }

    override fun getItemCount(): Int {
        return ChatDetailList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var chatProfileIV: ImageView = itemView.findViewById(R.id.chatProfileIV)
        var chatNameTV: TextView = itemView.findViewById(R.id.chatNameTV)
        var chatMessageTV: TextView =itemView.findViewById(R.id.chatMessageTV)
        var chatListRL : RelativeLayout =itemView.findViewById(R.id.chatListRL)
        var timeTV : TextView = itemView.findViewById(R.id.timeTV)
        var badgeRL : RelativeLayout =itemView.findViewById(R.id.badgeRL)
        var badgeTV :TextView =itemView.findViewById(R.id.badgeTV)
        var txtDelete :TextView =itemView.findViewById(R.id.txtDelete)
    }

}