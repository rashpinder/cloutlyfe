package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.CloutHomeRVModel
import java.util.*

class GoLiveRVAdapter(val mActivity: Activity,
                         var mArrayList: ArrayList<CloutHomeRVModel>) :
    RecyclerView.Adapter<GoLiveRVAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_live_users_list, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mDataItem : CloutHomeRVModel = mArrayList.get(position)
        Glide.with(mActivity).load(mDataItem.cloutImg)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into(holder.imgImageIV)
        holder.txtNameTV.setText("test")
        holder.txtDescTV.setText("test")
        holder.itemView.setOnClickListener{
//            mLocationtemClickListner.onItemClickListner(mNotificationItem)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgImageIV: ImageView
        var txtNameTV: TextView
        var txtDescTV: TextView

        init {
            imgImageIV = itemView.findViewById(R.id.imgImageIV)
            txtNameTV = itemView.findViewById(R.id.txtNameTV)
            txtDescTV = itemView.findViewById(R.id.txtDescTV)
        }
    }
}

