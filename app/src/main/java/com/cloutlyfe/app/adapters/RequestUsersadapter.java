package com.cloutlyfe.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cloutlyfe.app.R;
import com.cloutlyfe.app.model.StreamListModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class RequestUsersadapter  extends RecyclerView.Adapter<RequestUsersadapter.ViewHolder> {
   Activity mActivity;
   int roleSeeData;
    List<StreamListModel.Joinuserdetails> joinuserdetails;
    private OnItemClickListener listener;


    public RequestUsersadapter(Activity mActivity, List<StreamListModel.Joinuserdetails> joinuserdetails, int roleSeeData, OnItemClickListener listener) {
    this.mActivity=mActivity;
    this.joinuserdetails=joinuserdetails;
    this.roleSeeData=roleSeeData;
    this.listener = listener;

    }

    public interface OnItemClickListener {
        void onItemClick(int positon, StreamListModel.Joinuserdetails item, View view);
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_users, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        StreamListModel.Joinuserdetails mModel=joinuserdetails.get(position);
        holder.bind(position,mModel,listener);
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_profile_ph)
                .error(R.drawable.ic_profile_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        if (roleSeeData==1) {
            if (mModel.getStatus().equals("0")) {
                holder.llyAcceptreject.setVisibility(View.VISIBLE);
                holder.llykick.setVisibility(View.GONE);

            } else {
                holder.llyAcceptreject.setVisibility(View.GONE);
                holder.llykick.setVisibility(View.VISIBLE);

            }

            Glide.with(mActivity).load(mModel.getPhoto()).apply(options).into(holder.imUserPic);
            holder.txUsername.setText(mModel.getName());
        }
        else
        {
            holder.llyAcceptreject.setVisibility(View.GONE);
            holder.llykick.setVisibility(View.GONE);
            Glide.with(mActivity).load(mModel.getPhoto()).apply(options).into(holder.imUserPic);
            holder.txUsername.setText(mModel.getName());
        }
    }

    @Override
    public int getItemCount() {
        return joinuserdetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imUserPic;
        TextView txUsername;
        LinearLayout llyAcceptreject;
        LinearLayout llykick;
        ImageView imAccept;
        ImageView imReject;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imUserPic=itemView.findViewById(R.id.imUserPic);
            txUsername=itemView.findViewById(R.id.txUsername);
            llyAcceptreject=itemView.findViewById(R.id.llyAcceptReject);
            llykick=itemView.findViewById(R.id.llyKick);
            imAccept=itemView.findViewById(R.id.imAccept);
            imReject=itemView.findViewById(R.id.imReject);
        }

        public void bind(int position, StreamListModel.Joinuserdetails mModel, OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            llykick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            imAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            imReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });
            imUserPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position,mModel,v);
                }
            });

        }
    }
}
