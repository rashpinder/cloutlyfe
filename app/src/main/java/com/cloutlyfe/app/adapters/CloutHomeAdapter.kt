package com.cloutlyfe.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.model.LiveDataModel
import com.cloutlyfe.app.utils.Constants.Companion.CLOUT_INVISIBLE_VIEW_HOLDER
import com.cloutlyfe.app.utils.Constants.Companion.CLOUT_VISIBLE_VIEW_HOLDER
import com.merger.app.chatviewholder.ItemCloutInvisibleViewHolder

class CloutHomeAdapter(
    var mArrayList: ArrayList<LiveDataModel>,
    val mActivity: FragmentActivity?,
    var listener: onItemClickClickListener? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface onItemClickClickListener {
        fun onItemClick(
            position: Int,
            mArrayList: ArrayList<LiveDataModel>,
            view: View?
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == CLOUT_VISIBLE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity)
                    .inflate(R.layout.item_home_clout, parent, false)
            return ItemCLoutVisibleViewHolder(mView)
        } else if (viewType == CLOUT_INVISIBLE_VIEW_HOLDER) {
            var mView: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_disable_view_home_clout, parent, false)
            return ItemCloutInvisibleViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == CLOUT_VISIBLE_VIEW_HOLDER) {
            (holder as ItemCLoutVisibleViewHolder).bindData(
                mActivity,
                mArrayList,listener,position
            )
        } else if (holder.itemViewType == CLOUT_INVISIBLE_VIEW_HOLDER) {
            (holder as ItemCloutInvisibleViewHolder).bindData(
                mActivity,
                mArrayList ,listener,position
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
//        if ((position%2)==0){
            return CLOUT_VISIBLE_VIEW_HOLDER
//        }
//        else if((position%2)!=0){
//            return CLOUT_INVISIBLE_VIEW_HOLDER}
        return -1
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }
}