package com.cloutlyfe.app.ui.adapter

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.mode.Feed
import com.cloutlyfe.app.mode.Photo
import com.cloutlyfe.app.mode.Video
import com.cloutlyfe.app.ui.view.CameraAnimation
import com.cloutlyfe.app.ui.view.VideoViews
import com.hoanganhtuan95ptit.autoplayvideorecyclerview.VideoHolder
import me.relex.circleindicator.CircleIndicator

/**
 * Created by HoangAnhTuan on 1/21/2018.
 */
class SavedAdapter(var context: Context) :
    BaseAdapter<Feed?>(context) {
    var myCustomPagerAdapter: MyCustomPagerAdapter? = null
    override fun getItemViewType(position: Int): Int {
        val feed = list[position]!!
        return if (feed.info is Photo) {
            if (feed.model == Feed.Model.M1) PHOTO_M1 else PHOTO_M2
        } else {
            if (feed.model == Feed.Model.M1) VIDEO_M1 else VIDEO_M2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            PHOTO_M1 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false)
                Photo11Holder(view)
            }
            PHOTO_M2 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false)
                Photo169Holder(view)
            }
            VIDEO_M1 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
                Video11Holder(view)
            }
            else -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
                Video169Holder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val feed = list[position]!!
        if (holder is Video11Holder) {
            onBindVideo11Holder(holder, feed)
        } else if (holder is Video169Holder) {
            onBindVideo169Holder(holder, feed)
        } else if (holder is Photo11Holder) {
            onBindPhoto11Holder(holder, feed)
        } else if (holder is Photo169Holder) {
            onBindPhoto169Holder(holder, feed)
        }
    }

    private fun onBindPhoto11Holder(holder: Photo11Holder, feed: Feed) {
        holder.saveIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_save))

//        val images = arrayOf(R.drawable.ic_dummy, R.drawable.ic_dummy, R.drawable.ic_dummy)
//        myCustomPagerAdapter = MyCustomPagerAdapter(context, images)
        holder.viewPager.adapter = myCustomPagerAdapter
        holder.indicator.setViewPager(holder.viewPager)
//        holder.indicator.attachTo(holder.viewPager)
//        holder.tab_layout.setupWithViewPager(holder.viewPager, true)
        var height = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._340sdp)
        val padding = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._16sdp)
        if (holder.layoutPosition == 0) {
            holder.parentLayout.setPadding(padding, 100, padding, 0)
            holder.parentLayout.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT
            val params: ViewGroup.LayoutParams = holder.parentLayout.getLayoutParams()
            params.height = height
            holder.parentLayout.setLayoutParams(params)
        }
        //        Picasso.with(context)
//                .load(feed.getInfo().getUrlPhoto())
//                .resize(screenWight, screenWight)
//                .centerCrop()
//                .into(holder.ivInfo);
    }

    private fun onBindPhoto169Holder(holder: Photo169Holder, feed: Feed) {
        holder.saveIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_save))
//        val images = arrayOf(R.drawable.ic_dummy, R.drawable.ic_dummy, R.drawable.ic_dummy)
//        myCustomPagerAdapter = MyCustomPagerAdapter(context, images)
        holder.viewPager.adapter = myCustomPagerAdapter
        holder.indicator.setViewPager(holder.viewPager)
//        holder.indicator.attachTo(holder.viewPager)
//        holder.tab_layout.setupWithViewPager(holder.viewPager, true)
        //        Picasso.with(context)
//                .load(feed.getInfo().getUrlPhoto())
//                .resize(screenWight, screenWight)
//                .centerCrop()
//                .into(holder.ivInfo);
    }

    private fun onBindVideo11Holder(holder: DemoVideoHolder, feed: Feed) {
        holder.saveIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_save))
//        holder.vvInfo.setVideo(feed.info as Video)
        val padding = context.resources.getDimensionPixelSize(com.intuit.sdp.R.dimen._16sdp)
        Glide.with(context)
            .load((feed.info as Video).urlPhoto)
            .placeholder(R.drawable.ic_post_ph)
            .error(R.drawable.ic_post_ph)
            .into(holder.ivInfo)
        if (holder.layoutPosition == 0) {
            holder.parentLayout.setPadding(padding, 100, padding, 0)
        }
        //        Picasso.with(context)
//                .load(feed.getInfo().getUrlPhoto())
//                .resize(screenWight, screenWight)
//                .centerCrop()
//                .into(holder.ivInfo);
    }

    private fun onBindVideo169Holder(holder: DemoVideoHolder, feed: Feed) {
//        holder.vvInfo.setVideo(feed.info as Video)
        holder.saveIV.setImageDrawable(context.resources.getDrawable(R.drawable.ic_save))
        Glide.with(context)
            .load((feed.info as Video).urlPhoto)
            .placeholder(R.drawable.ic_post_ph)
            .error(R.drawable.ic_post_ph)
            .into(holder.ivInfo)

//        Picasso.with(context)
//                .load(feed.getInfo().getUrlPhoto())
//                .resize(screenWight, screenWight)
//                .centerCrop()
//                .into(holder.ivInfo);
    }

    private val screenWight: Int
        private get() {
            var displayMetrics = DisplayMetrics()
            displayMetrics = context.resources.displayMetrics
            return displayMetrics.widthPixels
        }

    open class DemoVideoHolder(itemView: View) : VideoHolder(itemView) {
        var vvInfo: VideoViews
        var ivInfo: ImageView
        var saveIV: ImageView
        var profileIV: ImageView
        var parentLayout: LinearLayout
        var ivCameraAnimation: CameraAnimation? = null

        override fun getVideoLayout(): View {
            return vvInfo
        }

        override fun playVideo() {
            ivInfo.visibility = View.VISIBLE
            //            ivCameraAnimation.start();

            vvInfo.play(object : VideoViews.OnPreparedListener {
                override fun onPrepared() {
                    ivInfo.visibility = View.GONE

//                    ivCameraAnimation.stop();

                }
            })
        }

        override fun stopVideo() {
            ivInfo.visibility = View.VISIBLE
            vvInfo.stop()
            vvInfo.setVideo(null)

//            if (vvInfo.isFocusable){
//
//            }
//            else{
//                vvInfo.release()
//            }
            vvInfo.release()
        }

        init {
            vvInfo = itemView.findViewById(R.id.vvInfo)
            ivInfo = itemView.findViewById(R.id.ivInfo)
            parentLayout = itemView.findViewById(R.id.parentLayout)
            saveIV = itemView.findViewById(R.id.saveIV)
            profileIV = itemView.findViewById(R.id.profileIV)
            //            ivCameraAnimation = itemView.findViewById(R.id.ivCameraAnimation);
        }
    }

    open inner class PhotoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //        ImageView ivInfo;
        var viewPager: ViewPager
        var indicator: CircleIndicator
//        var tab_layout: TabLayout
        var parentLayout: LinearLayout
        var saveIV: ImageView
        var profileIV: ImageView

        init {
            //            ivInfo = itemView.findViewById(R.id.ivInfo);
            viewPager = itemView.findViewById(R.id.viewPager)
            indicator = itemView.findViewById(R.id.indicator)
//            tab_layout = itemView.findViewById(R.id.tab_layout)
            parentLayout = itemView.findViewById(R.id.parentLayout)
            saveIV = itemView.findViewById(R.id.saveIV)
            profileIV = itemView.findViewById(R.id.profileIV)
        }
    }

    inner class Photo11Holder(itemView: View) : PhotoHolder(itemView)
    inner class Photo169Holder(itemView: View) : PhotoHolder(itemView)
    inner class Video11Holder(itemView: View) : DemoVideoHolder(itemView)
    inner class Video169Holder(itemView: View) : DemoVideoHolder(itemView)
    companion object {
        private const val PHOTO_M1 = 0
        private const val PHOTO_M2 = 1
        private const val VIDEO_M1 = 2
        private const val VIDEO_M2 = 3
        private var screenWight = 0
    }

    init {
        Companion.screenWight = screenWight
    }
}