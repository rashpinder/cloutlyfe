package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.CommentLoadMoreListener
import com.cloutlyfe.app.model.DataXX
import com.cloutlyfe.app.model.ReplyComment
import java.util.ArrayList

class ReplyCommentAdapter(
    var activity: Activity?,
    var commentsList: ArrayList<DataXX>,
    var mLoadMoreScrollListner: CommentLoadMoreListener,
    var replyList: ArrayList<ReplyComment>
): RecyclerView.Adapter<ReplyCommentAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_reply_comment, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = replyList?.get(position)
        if( replyList?.get(position)!!.username=="N/A" ||  replyList?.get(position)!!.username==""||  replyList?.get(position)!!.username==null){
            holder.txtNameTV.text=mModel!!.name
        }
        else{
            holder.txtNameTV.text=mModel!!.username}

        holder.commentsTV.text=mModel.reply_message
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
        Glide.with(activity!!).load(mModel.photo)
            .placeholder(R.drawable.ic_profile_ph)
            .error(R.drawable.ic_profile_ph)
            .into((holder.profileIV))
//        if ((commentsList?.get(position)?.isLike).equals(0)) {
//            holder.heartIV.setImageResource(R.drawable.ic_like)
//        } else if ((commentsList?.get(position)?.isLike).equals(1)){
//            holder.heartIV.setImageResource(R.drawable.ic_comment_like)
//        }
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }
//        holder.txtReplyTV.setOnClickListener {
////            val context=holder.txtReplyTV.context
////            val intent = Intent( context, ChatActivity::class.java)
////            context.startActivity(intent)
//            mChatItemClickListner.onItemClickListner(mModel)
//        }
//        if (replyList!!.size % 10 == 0 && replyList!!.size - 1 == position){
//            mLoadMoreScrollListner.onLoadMoreListner(replyList)
//        }

//        holder.likeRL.setOnClickListener(View.OnClickListener {
//            mLikeClickListener.onItemClickListner(holder.adapterPosition,commentsList.get(position).comment_id,holder.heartIV)
//        })
    }

    override fun getItemCount(): Int {
        return replyList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var profileIV: ImageView = itemView.findViewById(R.id.profileIV)
        var txtNameTV: TextView = itemView.findViewById(R.id.txtNameTV)
        var commentsTV: TextView =itemView.findViewById(R.id.commentsTV)
//        var txtReplyTV : TextView=itemView.findViewById(R.id.txtReplyTV)
//        var heartIV : ImageView = itemView.findViewById(R.id.heartIV)
//        var likeRL : RelativeLayout = itemView.findViewById(R.id.likeRL)
    }

}