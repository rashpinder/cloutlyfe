package com.cloutlyfe.app.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cloutlyfe.app.R
import com.cloutlyfe.app.activities.SavedActivity
import com.cloutlyfe.app.interfaces.LoadMoreScrollListner
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.model.ProfilePostsData

//var ChatDetailList: ArrayList<ChatDataItem?>?,
//var mChatItemClickListner: ChatItemClickListner

class AllSavedAdapter(
    var context: Context,
//    var list: MutableList<PostItem>
    private val postItems: ArrayList<HomeData>,
    var list: ArrayList<String>,
    var thumbnailList: ArrayList<String>,
    var mLoadMoreScrollListner: LoadMoreScrollListner
): RecyclerView.Adapter<AllSavedAdapter.ListViewHolder>()  {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)
        var imgPicIV: ImageView = itemView.findViewById(R.id.imgPicIV)
        var imgVideoIV: ImageView = itemView.findViewById(R.id.imgVideoIV)

        fun setPostImage(postItem: ArrayList<String>, position: Int) {
            if (postItem.get(position).contains(".mp4")|| postItem.get(position).contains(".MP4") || postItem.get(position).contains("video")){
                    Glide.with(context).load(thumbnailList[position])
                        .placeholder(R.drawable.ic_post_ph)
                        .error(R.drawable.ic_post_ph)
                        .into((imgImageIV))
                imgVideoIV.visibility=View.VISIBLE
                imgPicIV.visibility=View.GONE
                }
            else{
            Glide.with(context).load(postItem?.get(position))
                .placeholder(R.drawable.ic_post_ph)
                .error(R.drawable.ic_post_ph)
                .into((imgImageIV))
                imgPicIV.visibility=View.GONE
                imgVideoIV.visibility=View.GONE
        }
    }}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_all_saved_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.setPostImage(list,position)

        if (postItems.size % 10 == 0 && postItems.size - 1 == position){
            mLoadMoreScrollListner.onLoadMoreListner(postItems[position].image!!)
        }
//        var mModel  = ChatDetailList?.get(position)
//        holder.txtNameTV.text=ChatDetailList?.get(position)!!.name
//        holder.commentsTV.text=ChatDetailList?.get(position)!!.lastMessage
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
//        Glide.with(activity!!).load(ChatDetailList?.get(position)?.profileImage)
//            .placeholder(R.drawable.ic_placeholder)
//            .error(R.drawable.ic_placeholder)
//            .into((holder.profileIV))
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }

        holder.imgImageIV.setOnClickListener {
            var context=holder.imgImageIV.context
            var intent = Intent( context, SavedActivity::class.java)
           intent.putExtra("post_id",postItems[position].post_id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

//    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var imgImageIV: ImageView = itemView.findViewById(R.id.imgImageIV)
//    }

}