package com.cloutlyfe.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cloutlyfe.app.R
import com.cloutlyfe.app.interfaces.ReportItemClickInterface
import com.cloutlyfe.app.model.DataX

class ReportAdapter(
    var activity: Activity?,
    var data_list: ArrayList<DataX>,
    var mItemClickListener: ReportItemClickInterface
): RecyclerView.Adapter<ReportAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_report, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel  = data_list.get(position)
        holder.txtReasonTV.text= mModel.reportReasons

        holder.itemView.setOnClickListener {
            mItemClickListener.onItemClickListner(data_list.get(position).reasonId)
        }
    }

    override fun getItemCount(): Int {
        return data_list.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtReasonTV: TextView = itemView.findViewById(R.id.txtReasonTV)
    }

}