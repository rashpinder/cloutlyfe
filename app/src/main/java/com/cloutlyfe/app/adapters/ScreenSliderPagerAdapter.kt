package com.cloutlyfe.app.adapters


import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.cloutlyfe.app.fragments.ScreenSlidePageFragment
import com.cloutlyfe.app.model.Snap

class ScreenSliderPagerAdapter(
    var mActivity: Activity,
    fragmentManager: FragmentManager,
    val statusList: MutableList<Snap>,
    val mPager: ViewPager,
    val photo: String,
    val username: String,
    val user_id: String
) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return ScreenSlidePageFragment.newInstance(mActivity,position,statusList.size,mPager,statusList,photo,username, user_id)
    }

    override fun getCount(): Int {
        return statusList.size
    }
}


