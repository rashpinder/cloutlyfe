package com.cloutlyfe.app

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import com.cloutlyfe.app.Agora.ConstantsA
import com.cloutlyfe.app.Agora.utils.CloutNewPrefernces
import com.cloutlyfe.app.activities.ChatActivity
import com.cloutlyfe.app.activities.CommentsActivity
import com.cloutlyfe.app.activities.LiveStreaming2Activity
import com.cloutlyfe.app.activities.OtherUserProfileActivity
import com.cloutlyfe.app.model.HomeData
import com.cloutlyfe.app.utils.Constants
import com.cloutlyfe.app.utils.Constants.Companion.OTHERUSERID
import com.google.gson.Gson
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : BaseActivity() {
    var mNotificationModel: HomeData? = null
    var notificationType: String? = ""
    var roomId: String? = ""
    var otherID: String? = ""
    var postuser: String? = ""
    var photo: String? = ""
    var notificationId: String? = ""
    var is_delete: Int? = 0
    var creationDate: String? = ""
    var message: String? = ""
    var followID: String? = ""
    var titleMessage: String? = ""
    var statusId: String? = ""
    var notificationReadStatus: String? = ""
    var userId: String? = ""
    var post_id: String? = ""
    var mName: String = ""
    var viewedStatus: String? = ""
    var username: String? = ""
    var namee: String? = ""
    var disable_like: String? = ""
    var stream_id: String? = ""
    var disable_comment: String? = ""
    var stream_room: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        printKeyHash(mActivity)
        setUpSplash()

    }


    //    private fun setUpSplash() {
//        val mThread = object : Thread() {
//            override fun run() {
//                sleep(SPLASH_TIME_OUT.toLong())
//                if (isUserLogin()) {
//                    val i = Intent(mActivity, HomeActivity::class.java)
//                    startActivity(i)
//                    finish()
//
//                }
//                else {
//                    val i = Intent(mActivity, LoginActivity::class.java)
//                    startActivity(i)
//                    finish()
//                }
//            }
//        }
//        mThread.start()
//    }
    var i: Intent? = null
    private fun setUpSplash() {

        if (isUserLogin()) {
//                    Log.d(TAG, i.hasExtra["data"] as String)
//                    val extras = intent!!.extras
//                    if (extras != null) {
//                        if (intent.hasExtra("Data"))
//                        {

//                            mNotificationModel = intent.getSerializableExtra("Data") as CustomNotifModel?

            val extras = intent.extras
            if (extras != null) {
//                        Log.d(TAG, extras["notification_type"] as String)
                for (key in intent.extras!!.keySet()) {
                    val value = intent.extras!![key]
                    Log.d(TAG, "Key: $key Value: $value")
                    if (key.equals("notification_id", ignoreCase = true)) {
                        notificationId = value.toString()
                    }
                    if (key.equals("message", ignoreCase = true)) {
                        message = value.toString()
                    }
                    if (key.equals("notification_type", ignoreCase = true)) {
                        notificationType = value.toString()
                    }
                    if (key.equals("user_id", ignoreCase = true)) {
                        userId = value.toString()
                    }
                    if (key.equals("post_id", ignoreCase = true)) {
                        post_id = value.toString()
                    }
                    if (key.equals("otherID", ignoreCase = true)) {
                        otherID = value.toString()
                    }
                    if (key.equals("room_id", ignoreCase = true)) {
                        roomId = value.toString()
                    }
                    if (key.equals("followID", ignoreCase = true)) {
                        followID = value.toString()
                    }
                    if (key.equals("status_id", ignoreCase = true)) {
                        statusId = value.toString()
                    }
                    if (key.equals("postuser", ignoreCase = true)) {
                        postuser = value.toString()
                    }
                    if (key.equals("creation_date", ignoreCase = true)) {
                        creationDate = value.toString()
                    }
                    if (key.equals("viewed_status", ignoreCase = true)) {
                        viewedStatus = value.toString()
                    }
                    if (key.equals("notification_read_status", ignoreCase = true)) {
                        notificationReadStatus = value.toString()
                    }
                    if (key.equals("title_message", ignoreCase = true)) {
                        titleMessage = value.toString()
                    }
                    if (key.equals("username", ignoreCase = true)) {
                        username = value.toString()
                    }
                    if (key.equals("name", ignoreCase = true)) {
                        mName = value.toString()
                    }
                    if (key.equals("photo", ignoreCase = true)) {
                        photo = value.toString()
                    }
                    if (key.equals("is_delete", ignoreCase = true)) {
                        is_delete = value as Int?
                    }
                    if (key.equals("disable_like", ignoreCase = true)) {
                        disable_like = value as String?
                    }
                    if (key.equals("stream_id", ignoreCase = true)) {
                        stream_id = value as String?
                    }
                    if (key.equals("disable_comment", ignoreCase = true)) {
                        disable_comment = value as String?
                    }
                    if (key.equals("stream_room", ignoreCase = true)) {
                        stream_room = value as String?
                    }
                }
                if (username == "N/A" || username == null || username == "") {
                    namee = mName
                } else {
                    namee = username
                }
                if (notificationType.equals("0")) {
                    mNotificationModel = is_delete?.let {
                        HomeData(
                            "",
                            "",
                            "",
                            "",
                            null,
                            0,
                            0,
                            "",
                            "",
                            null,
                            "",
                            "",
                            mName.toString(),
                            "",
                            post_id.toString(),
                            null,
                            otherID.toString(),
                            "",
                            username.toString(),
                            false,
                            0,
                            0,
                            0,
                            notificationType.toString(),
                            roomId.toString(),
                            otherID.toString(),
                            postuser.toString(),
                            notificationId.toString(),
                            creationDate.toString(),
                            message.toString(),
                            followID.toString(),
                            titleMessage.toString(),
                            statusId.toString(),
                            notificationReadStatus.toString(),
                            viewedStatus.toString(), it, "", "", "", "", ""
                        )
                    }
                    i = Intent(mActivity, OtherUserProfileActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(mNotificationModel)
                    i!!.putExtra("homeData", mModell)
                    i!!.putExtra("value", "other_user")
                    i!!.putExtra("other_user_id", otherID)
                    i!!.putExtra("name", namee)
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("1")) {
                    mNotificationModel = is_delete?.let {
                        HomeData(
                            "",
                            "",
                            "",
                            "",
                            null,
                            0,
                            0,
                            "",
                            "",
                            null,
                            "",
                            "",
                            mName.toString(),
                            "",
                            post_id.toString(),
                            null,
                            otherID.toString(),
                            "",
                            username.toString(),
                            false,
                            0,
                            0,
                            0,
                            notificationType.toString(),
                            roomId.toString(),
                            otherID.toString(),
                            postuser.toString(),
                            notificationId.toString(),
                            creationDate.toString(),
                            message.toString(),
                            followID.toString(),
                            titleMessage.toString(),
                            statusId.toString(),
                            notificationReadStatus.toString(),
                            viewedStatus.toString(), it, "", "", "", "", ""
                        )
                    }
                    i = Intent(mActivity, OtherUserProfileActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(mNotificationModel)
                    i!!.putExtra("homeData", mModell)
                    i!!.putExtra("value", "other_user")
                    i!!.putExtra("other_user_id", otherID)
                    i!!.putExtra("name", namee)
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("2")) {
//                            AppPrefrences().writeString(mActivity!!, OTHERUSERID,mNotificationModel?.otherID)
//                            var mIntent : Intent = Intent(mActivity, OtherUserProfileActivity::class.java)
//                            startActivity(mIntent)
//                            finish()
                } else if (notificationType.equals("3")) {
//                            AppPrefrences().writeString(mActivity!!, OTHERUSERID,mNotificationModel?.otherID)
//                            var mIntent : Intent = Intent(mActivity, OtherUserProfileActivity::class.java)
//                            startActivity(mIntent)
//                            finish()
                } else if (notificationType.equals("5")) {
                    i = Intent(mActivity, ChatActivity::class.java)
                    i!!.putExtra(Constants.ROOM_ID, roomId.toString())
                    i!!.putExtra(OTHERUSERID, otherID.toString())
                    i!!.putExtra(Constants.USER_NAME, namee.toString())
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("8")) {
                    i = Intent(mActivity, ChatActivity::class.java)
                    i!!.putExtra(Constants.ROOM_ID, roomId.toString())
                    i!!.putExtra(OTHERUSERID, otherID.toString())
                    i!!.putExtra(Constants.USER_NAME, namee.toString())
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("")) {
//                            var mIntent = Intent(mActivity, ChatActivity::class.java)
//                            mIntent.putExtra(Constants.ROOM_ID, mNotificationModel!!.roomId)
//                            mIntent!!.putExtra(Constants.USER_NAME, mNotificationModel!!.name)
//                            startActivity(mIntent)
//                            finish()
                } else if (notificationType.equals("6")) {
                    mNotificationModel = is_delete?.let {
                        HomeData(
                            "",
                            "",
                            "",
                            "",
                            null,
                            0,
                            0,
                            "",
                            "",
                            null,
                            "",
                            "",
                            mName.toString(),
                            "",
                            post_id.toString(),
                            null,
                            otherID.toString(),
                            "",
                            username.toString(),
                            false,
                            0,
                            0,
                            0,
                            notificationType.toString(),
                            roomId.toString(),
                            otherID.toString(),
                            postuser.toString(),
                            notificationId.toString(),
                            creationDate.toString(),
                            message.toString(),
                            followID.toString(),
                            titleMessage.toString(),
                            statusId.toString(),
                            notificationReadStatus.toString(),
                            viewedStatus.toString(), it, "", "", "", "", ""
                        )
                    }
                    i = Intent(mActivity, CommentsActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(mNotificationModel)
                    i!!.putExtra("homeData", mModell)
                    i!!.putExtra("value", "push")
                    i!!.putExtra("other_user_id", mNotificationModel!!.otherID)
                    i!!.putExtra("name", namee)
                    i!!.putExtra("post_id", mNotificationModel!!.post_id)
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("7")) {
                    mNotificationModel = is_delete?.let {
                        HomeData(
                            "",
                            "",
                            "",
                            "",
                            null,
                            0,
                            0,
                            "",
                            "",
                            null,
                            "",
                            "",
                            mName.toString(),
                            "",
                            post_id.toString(),
                            null,
                            otherID.toString(),
                            "",
                            username.toString(),
                            false,
                            0,
                            0,
                            0,
                            notificationType.toString(),
                            roomId.toString(),
                            otherID.toString(),
                            postuser.toString(),
                            notificationId.toString(),
                            creationDate.toString(),
                            message.toString(),
                            followID.toString(),
                            titleMessage.toString(),
                            statusId.toString(),
                            notificationReadStatus.toString(),
                            viewedStatus.toString(), it, "", "", "", "", ""
                        )
                    }
                    i = Intent(mActivity, CommentsActivity::class.java)
                    val gson = Gson()
                    val mModell = gson.toJson(mNotificationModel)
                    i!!.putExtra("homeData", mModell)
                    i!!.putExtra("value", "push")
                    i!!.putExtra("other_user_id", mNotificationModel!!.otherID)
                    i!!.putExtra("name", namee)
                    i!!.putExtra("post_id", mNotificationModel!!.post_id)
                    i!!.putExtra("redirect_value", "push")

                } else if (notificationType.equals("10")) {
                    mNotificationModel =
                        is_delete?.let {
                            HomeData(
                                "",
                                "",
                                "",
                                "",
                                null,
                                0,
                                0,
                                "",
                                "",
                                null,
                                "",
                                "",
                                mName.toString(),
                                "",
                                post_id.toString(),
                                null,
                                otherID.toString(),
                                "",
                                username.toString(),
                                false,
                                0,
                                0,
                                0,
                                notificationType.toString(),
                                roomId.toString(),
                                otherID.toString(),
                                postuser.toString(),
                                notificationId.toString(),
                                creationDate.toString(),
                                message.toString(),
                                followID.toString(),
                                titleMessage.toString(),
                                statusId.toString(),
                                notificationReadStatus.toString(),
                                viewedStatus.toString(),
                                it,
                                "",
                                disable_like.toString(),
                                stream_id.toString(),
                                disable_comment.toString(),
                                stream_room.toString()
                            )
                        }
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.STREAM_USERID,
                        mNotificationModel?.user_id.toString()
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.STREAM_ROOMID,
                        mNotificationModel?.stream_room.toString()
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.STREAM_USERNAME,
                        namee
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.STREAM_ID,
                        mNotificationModel?.stream_id.toString()
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.STREAM_USERPHOTO,
                        mNotificationModel?.photo.toString()
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.COMMENT_DISABLE,
                        mNotificationModel?.disable_comment.toString()
                    )
                    CloutNewPrefernces.writeString(
                        this,
                        CloutNewPrefernces.LIKE_DISABLE,
                        mNotificationModel?.disable_like.toString()
                    )
                    gotoRoleActivity(
                        mNotificationModel?.stream_room.toString(),
                        AppPrefrences().readString(this, Constants.ID, null).toString()
                    )
                    i = Intent(this, LiveStreaming2Activity::class.java)
                }
            }
//                }
            else {
                i = Intent(mActivity, HomeActivity::class.java)
            }
        } else {
            i = Intent(mActivity, LoginActivity::class.java)

        }

        Handler().postDelayed({
            if (isUserLogin() && i == null) {
                i = Intent(mActivity, HomeActivity::class.java)
            }
//       i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
            finish()
        }, 2000)


//    mThread.start()
    }


    fun gotoRoleActivity(stream_room: String, userID: String) {
        val mil = (System.currentTimeMillis() / 1000L).toInt()
        ConstantsA.channelNameA = stream_room
        ConstantsA.useridA = userID
        onJoinAsAudience()
    }

    fun onJoinAsAudience() {
        gotoLiveActivity(io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE)
    }

    private fun gotoLiveActivity(role: Int) {
        i = Intent(this, LiveStreaming2Activity::class.java)
        i!!.putExtra(ConstantsA.KEY_CLIENT_ROLE, role)
    }


    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package name, as defined in manifest
            val packageName = context.applicationContext.packageName

            //Retriving package info
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
                Log.e(TAG, "Key Hash=++++++=$key")
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: java.lang.Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }
}