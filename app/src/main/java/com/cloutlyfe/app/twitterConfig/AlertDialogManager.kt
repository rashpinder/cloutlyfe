package com.cloutlyfe.app.twitterConfig

import android.content.DialogInterface

import android.R
import android.app.AlertDialog
import android.content.Context


class AlertDialogManager {
    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * - pass null if you don't want icon
     */
//    fun showAlertDialog(
//        context: Context?, title: String?, message: String?,
//        status: Boolean?
//    ) {
//        val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
//
//        // Setting Dialog Title
//        alertDialog.setTitle(title)
//
//        // Setting Dialog Message
//        alertDialog.setMessage(message)
//        if (status != null) // Setting alert dialog icon
//            alertDialog.setIcon(if (status) R.drawable.ic_twitter else R.drawable.ic_twitter)
//
//        // Setting OK Button
//        alertDialog.setButton("OK",
//            DialogInterface.OnClickListener { dialog, which -> })
//
//        // Showing Alert Message
//        alertDialog.show()
//    }
}