package com.cloutlyfe.app.mode

class Video : Info {
    var urlVideo: String
    var urlVideoLocal: String? = null
    var seekTo: Int

    constructor(urlVideo: String, seekTo: Int) {
        this.urlVideo = urlVideo
        this.seekTo = seekTo
    }

    constructor(urlPhoto: String?, urlVideo: String, seekTo: Int) : super(urlPhoto) {
        this.urlVideo = urlVideo
        this.seekTo = seekTo
    }
}
