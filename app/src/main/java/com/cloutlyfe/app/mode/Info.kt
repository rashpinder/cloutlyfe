package com.cloutlyfe.app.mode

import java.io.Serializable

abstract class Info : Serializable {
    var urlPhoto: String? = null

    constructor() {}
    constructor(urlPhoto: String?) {
        this.urlPhoto = urlPhoto
    }
}