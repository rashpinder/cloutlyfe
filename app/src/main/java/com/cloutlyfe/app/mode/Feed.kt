package com.cloutlyfe.app.mode

import java.io.Serializable

/**
 * Created by HoangAnhTuan on 1/21/2018.
 */
class Feed : Serializable {
    var info: Info? = null
    var model: Model? = null

    constructor() {}
    constructor(info: Info?, model: Model?) {
        this.info = info
        this.model = model
    }

    enum class Model {
        M1, M2
    }
}