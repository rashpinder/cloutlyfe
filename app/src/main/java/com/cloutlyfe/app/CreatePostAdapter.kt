package com.cloutlyfe.app

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

//var ChatDetailList: ArrayList<ChatDataItem?>?,
//var mChatItemClickListner: ChatItemClickListner

class CreatePostAdapter(
    var activity: Activity?,
    var mBitmap: ArrayList<Uri>,
    var bitmap: Bitmap?,
    val value: String
): RecyclerView.Adapter<CreatePostAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_create_post, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        var mModel  = ChatDetailList?.get(position)
//        holder.txtNameTV.text=ChatDetailList?.get(position)!!.name
//        holder.commentsTV.text=ChatDetailList?.get(position)!!.lastMessage
//
//        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
//        val timeD = Date(Timestamp * 1000)
//        val sdf = SimpleDateFormat("hh:mm a")
//        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
//        val Time: String = sdf.format(timeD)
//         holder.heartIV.text=Time
//
if(value.isNotEmpty()){
        Glide.with(activity!!)
            .load(bitmap)
            .placeholder(R.drawable.ic_post_ph)
            .error(R.drawable.ic_post_ph)
            .into(holder.imgPostIV)}
        else{
    Glide.with(activity!!)
        .load(mBitmap.get(position))
        .placeholder(R.drawable.ic_post_ph)
        .error(R.drawable.ic_post_ph)
        .into(holder.imgPostIV)
        }

//        Glide.with(activity!!).load(mBitmap.get(position))
//            .into((holder.imgPostIV))
//        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
//            holder.Chat_badgeRL.visibility=View.GONE
//        }
//        else{
//            holder.Chat_badgeRL.visibility=View.VISIBLE
//            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
//        }
        holder.imgCrossIV.setOnClickListener {
//            mOptionsClickListener.onItemClickListner(holder.adapterPosition)
        }
    }

    override fun getItemCount(): Int {
        return mBitmap.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPostIV: ImageView = itemView.findViewById(R.id.imgPostIV)
        var imgCrossIV: TextView = itemView.findViewById(R.id.imgCrossIV)
    }

}